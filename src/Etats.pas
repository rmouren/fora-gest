unit Etats;

interface

procedure ImprimerHebdoSaisies;
procedure ImprimerHebdoUnique(NumS: integer; bis: boolean);

procedure ImprimerHebdoEcart(SemDeb, SemFin: integer);
procedure ImprimerHebdoMois(Mois: integer; Fiches, Reguls: boolean);

procedure ImprimerCumulHebdo(bis: boolean; NumS, ChoixF, filtre: integer;
  ChoixChantiers: boolean; idcha: integer);

procedure ImprimerCumulMois(NumMois, ChoixF, filtre: integer;
  ChoixChantiers: boolean; idcha: integer);
procedure ImprimerCumulAnnee(NumMois, ChoixF, filtre: integer;
  ChoixChantiers: boolean; idcha: integer);
procedure ImprimerRecap(RecapAnnuel: boolean;
  NumMois, ChoixFiltre, filtre: integer);
procedure ImprimerRecapSimple(RecapAnnuel: boolean;
  NMois, ChoixFiltre, filtre: integer);
procedure ImprimerRecapClients(RecapAnnuel: boolean; NMois, idCom: integer);

procedure ImprimerRecapChantier(RecapAnnuel: boolean;
  NMois, ChoixFiltre, filtre: integer);

procedure VerrouHebdo(NumS: integer; bis: boolean; verrou: boolean);
procedure VerrouMois(Mois: integer; verrou: boolean);

procedure ExporteRecapClients(RecapAnnuel: boolean; NMois: integer);

procedure fermehebdo;

implementation

uses windows, UITypes, Dialogs, printers, SysUtils, forms, Messages, Classes,
  Controls,
  winbase, FicheEtats, accesBDD, Hebdo, utils, Impression,
  MasqueChantiers, MasqueSemaine, MasqueEmployes, MasqueStructures,
  MasqueVehicules, MasqueDiamant, MasqueAchats,
  MasqueExterne, MasqueMateriels, ExportCSV;

var
  NomFiltre: string;
  NumFiltre: integer;
  NumPageEtat5: integer;

procedure fermehebdo;
begin
  with FormBase.TableHebdo do
  begin
    close;
    active := true;
    filtered := false;
  end;
end;

procedure fermefiltrechantier;
begin
  with FormBase.Tablechantiers do
  begin
    close;
    filtered := false;
    active := true;
  end;
end;

function LirePointeurChantier: integer;
begin
  result := FormBase.Tablechantiers.FieldByName('id_chantier').asinteger;
end;

procedure ChantierPremier;
begin
  FormBase.Tablechantiers.First;
end;

procedure ChantierSuivant;
begin
  FormBase.Tablechantiers.Next;
end;

procedure ClientPremier;
begin
  FormBase.TableClients.First;
end;

procedure ClientSuivant;
begin
  FormBase.TableClients.Next;
end;

function LireNombreClients: integer;
begin
  result := FormBase.TableClients.RecordCount;
end;

function LirePointeurClient: integer;
begin
  result := FormBase.TableClients.FieldByName('id_client').asinteger;
end;

function LireNomClient: string;
begin
  result := FormBase.TableClients.FieldByName('cli_libellenom').asstring;
end;

function ouvrirchantiersclient(filtre: integer): integer;
{ ouvre la table chantiers en la filtrant sur un client }
var
  t: string;
begin
  str(filtre, t);
  with FormBase.Tablechantiers do
  begin
    filter := 'id_client = ' + t;
    filtered := true;
    open;
    setkey;
    result := RecordCount;
    edit;
  end;

  if result = 0 then
  begin
    // fermer les filtres chantiers et hebdo
    fermehebdo;
    fermefiltrechantier;
  end;
end;

function ouvrirfiltrechantier(ChoixFiltre, filtre: integer): integer;
{ ouvre la table chantiers en la filtrant ou non selon le filtre voulu }
var
  t: string;
begin
  NumFiltre := ChoixFiltre;

  with FormBase.Tablechantiers do
  begin
    if ChoixFiltre <> SansFiltre then
    begin
      // preparer le filtre
      str(filtre, t);
      case ChoixFiltre of
        FiltreAgence:
          t := 'id_agence = ' + t;
        FiltreCommercial:
          t := 'id_commercial = ' + t;
        FiltreClient:
          t := 'id_client = ' + t;
      end;
      filter := t;
      filtered := true;
    end
    else
    begin
      filtered := false;
    end;

    open;
    setkey;
    result := RecordCount;
    edit;
  end;

  if result = 0 then
  begin
    // fermer les filtres chantiers et hebdo
    fermehebdo;
    fermefiltrechantier;
  end;

  NomFiltre := '';
  if ChoixFiltre = SansFiltre then
    exit;

  // recuperer nom filtre
  case ChoixFiltre of
    FiltreAgence:
      with FormBase.TableSecteurs do
      begin
        open;
        editkey;
        FieldByName('id_agence').asinteger := filtre;
        GotoKey;
        NomFiltre := FieldByName('age_libellenom').asstring;
        close;
        active;
      end;
    FiltreCommercial:
      with FormBase.TableCommerciaux do
      begin
        open;
        editkey;
        FieldByName('id_commercial').asinteger := filtre;
        GotoKey;
        NomFiltre := FieldByName('com_libellenom').asstring;
        close;
        active;
      end;
    FiltreClient:
      with FormBase.TableClients do
      begin
        open;
        editkey;
        FieldByName('id_client').asinteger := filtre;
        GotoKey;
        NomFiltre := FieldByName('cli_libellenom').asstring;
        close;
        active;
      end;
  end;
end;

function ouvrirtoutchantier: integer;
{ ouvre la table chantiers en ne la filtrant pas }
begin
  with FormEtats.Tablechantiers do
  begin
    filter := '';
    filtered := false;
    open;
    setkey;
    result := RecordCount;
    edit;
  end;

  if result = 0 then
  begin
    // fermer les filtres chantiers et hebdo
    fermehebdo;
    with FormEtats.Tablechantiers do
    begin
      close;
      filtered := false;
      active := true;
    end;
  end;
end;

procedure VerrouHebdo(NumS: integer; bis: boolean; verrou: boolean);
{ routine de verrouillage ou deverrouillage des fiches d'une semaine donnee
  verrou    true    verrouillage des fiches de la semaine NumS
  false   deverrouillage ------------''-------------
}
var
  nbc: integer;
  idc: integer;
  i: integer;
begin
  // lire nombre de chantier a traiter
  nbc := ouvrirtoutchantier;
  if nbc = 0 then
  begin
    // message
    showmessage('Aucun chantier � traiter');
    exit;
  end;

  // pointer sur premier chantier
  FormEtats.Tablechantiers.First;

  for i := 1 to nbc do
  begin
    // recupere l'identifiant de chantier
    idc := FormEtats.Tablechantiers.FieldByName('id_chantier').asinteger;

    if ChercheHebdo(false, bis, idc, NumS) then
    begin
      // verrouiller ou deverrouiller
      with FormBase.TableHebdo do
        FieldByName('heb_verrou').asBoolean := verrou;
    end;

    // pointer sur chantier suivant
    FormEtats.Tablechantiers.Next;
  end;

  // fermer les filtres chantiers et hebdo
  // fermehebdo;
  // fermefiltrechantier;
end;

procedure VerrouMois(Mois: integer; verrou: boolean);
{ routine de verrouillage ou deverrouillage des fiches reguls du mois donne
  verrou    true    verrouillage des fiches de la semaine Mois
  false   deverrouillage ------------''-------------
}
var
  nbc: integer;
  i: integer;
  idc: integer;
begin
  // lire nombre de chantier a traiter
  nbc := ouvrirtoutchantier;
  if nbc = 0 then
  begin
    // message
    showmessage('Aucun chantier dans le choix de filtre');
    exit;
  end;

  // pointer sur premier chantier
  FormEtats.Tablechantiers.First;

  for i := 1 to nbc do
  begin
    // recupere l'identifiant de chantier
    idc := FormEtats.Tablechantiers.FieldByName('id_chantier').asinteger;

    if ChercheHebdo(true, false, idc, Mois) then
    begin
      // verrouiller ou deverrouiller
      with FormBase.TableHebdo do
        FieldByName('heb_verrou').asBoolean := verrou;
    end;

    // pointer sur chantier suivant
    FormEtats.Tablechantiers.Next;
  end;

  // fermer les filtres chantiers et hebdo
  // fermehebdo;
  // fermefiltrechantier;
end;

procedure ImprimerHebdoSaisies;
{ edition de toutes les fiches non encore imprimees }
var
  t: string;
begin
  // demande de confirmation d'impression des dernieres fiches saisies
  t := 'Veuillez confimer impression des dernieres fiches saisies';
  if MessageDlg(t, mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    // execution impression des dernieres fiches saisies
    ImpressionEnCours;
  end;
end;

procedure ImprimerHebdoUnique(NumS: integer; bis: boolean);
var
  nbc: integer;
  idc: integer;
  ok: boolean;
  i: integer;
begin
  // lire nombre de chantier a traiter
  nbc := ouvrirtoutchantier;
  if nbc = 0 then
  begin
    // message
    showmessage('Aucun chantier � traiter');
    exit;
  end;

  // pointer sur premier chantier
  FormEtats.Tablechantiers.First;

  ok := false;
  for i := 1 to nbc do
  begin
    // recupere l'identifiant de chantier
    idc := FormEtats.Tablechantiers.FieldByName('id_chantier').asinteger;

    if ChercheHebdo(false, bis, idc, NumS) then
    begin
      HEBDOImprimer;
      ok := true;
    end;

    // pointer sur chantier suivant
    FormEtats.Tablechantiers.Next;
  end;

  // aucune impression effectuee
  if not ok then
    showmessage('Aucune fiche n''a �t� imprim�e.');

  // fermer les filtres chantiers et hebdo
  fermehebdo;
  fermefiltrechantier;
end;

procedure ImprimerHebdoEcart(SemDeb, SemFin: integer);
{ edition de toutes les fiches hebdomadaires de la semaine (SemDeb) a (SemFin) }
var
  nbc: integer;
  NumS: integer;
  i: integer;
  ok: boolean;
  idc: integer;
begin
  // lire nombre de chantier a traiter
  nbc := ouvrirtoutchantier;
  if nbc = 0 then
  begin
    // message
    showmessage('Aucun chantier � traiter');
    exit;
  end;

  // pointer sur premier chantier
  FormEtats.Tablechantiers.First;

  ok := false;
  for i := 1 to nbc do
  begin
    // recupere l'identifiant de chantier
    idc := FormEtats.Tablechantiers.FieldByName('id_chantier').asinteger;

    // pour traiter toutes ces semaines demand�es
    for NumS := SemDeb to SemFin do
    begin
      // si premiere semaine tester semaine bis
      if SemaineBis(NumS, AnneeCourante) then
        if ChercheHebdo(false, true, idc, NumS) then
        begin
          HEBDOImprimer;
          ok := true;
        end;

      if ChercheHebdo(false, false, idc, NumS) then
      begin
        HEBDOImprimer;
        ok := true;
      end;
    end;

    // pointer sur chantier suivant
    FormEtats.Tablechantiers.Next;
  end;

  // aucune impression effectuee
  if not ok then
    showmessage('Aucune fiche n''a �t� imprim�e.');

  // fermer les filtres chantiers et hebdo
  fermehebdo;
  fermefiltrechantier;
end;

procedure ImprimerHebdoMois(Mois: integer; Fiches, Reguls: boolean);
{ edition selon le choix de toutes les fiches hebdo et reguls du mois }
var
  nbc: integer;
  NumS: integer;
  n: integer;
  i: integer;
  ok: boolean;
  SemDeb: integer;
  SemFin: integer;
  idc: integer;
begin
  // lire nombre de chantier a traiter
  nbc := ouvrirtoutchantier;
  if nbc = 0 then
  begin
    // message
    showmessage('Aucun chantier dans le choix de filtre');
    exit;
  end;

  // pointer sur premier chantier
  FormEtats.Tablechantiers.First;

  ok := false;
  for i := 1 to nbc do
  begin
    // recupere l'identifiant de chantier
    idc := FormEtats.Tablechantiers.FieldByName('id_chantier').asinteger;

    // pour traiter toutes ces semaines demand�es
    if Fiches then
    begin
      // definir l'ecart de semaines du mois a traiter
      EcartSemaine(Mois, AnneeCourante, SemDeb, SemFin);

      for NumS := SemDeb to SemFin do
      begin
        // si premiere semaine tester semaine bis
        n := NumS - SemDeb;
        if SemaineBis(NumS, AnneeCourante) and (n = 0) then
        begin
          if ChercheHebdo(false, true, idc, NumS) then
          begin
            HEBDOImprimer;
            ok := true;
          end;
        end
        else if ChercheHebdo(false, false, idc, NumS) then
        begin
          HEBDOImprimer;
          ok := true;
        end;
      end;
    end;

    if Reguls then
    begin
      if ChercheHebdo(true, false, idc, Mois) then
      begin
        HEBDOImprimer;
        ok := true;
      end;
    end;

    // pointer sur chantier suivant
    FormEtats.Tablechantiers.Next;
  end;

  // aucune impression effectuee
  if not ok then
    showmessage('Aucune fiche n''a �t� imprim�e.');

  // fermer les filtres chantiers et hebdo
  fermehebdo;
  fermefiltrechantier;
end;

function HebdoIdentifiant: integer;
begin
  result := FormBase.TableHebdo.FieldByName('id_hebdo').asinteger;
end;

procedure ResetCumuls;
var
  i: integer;
begin
  SetLength(TableauValHebdo, 14);
  for i := 0 to 13 do
    TableauValHebdo[i] := 0;

  ResetCumulsAchat;
  ResetCumulsExterne;
  ResetCumulsVehicule;
  ResetCumulsStructure;
  ResetCumulsMateriel;
  PointageCumulsReset;
  ResetCumulsDiamant;
end;

procedure SortieDebutCumuls;
begin
  // demarre impression
  Printer.Orientation := poPortrait;
  Printer.BeginDoc;

  // imprimer le cadre de l'etat et les grilles des masques
  imprimefondhebdo(nbligemp, nbligint);
end;

procedure SortieCalculsCumuls;
var
  nbr: integer;
begin
  PointageCumulsLire(nbr);
  ImprimePointage(nbr);

  CumulsAchatsLire(nbr);
  ImprimeAchat(nbr);

  CumulsDiamantLire(nbr);
  ImprimeDiamant(nbr);

  CumulsMaterielLire(nbr);
  ImprimeMateriel(nbr);

  CumulsVehiculeLire(nbr);
  ImprimeVehicule(nbr);

  CumulsStructureLire(nbr);
  ImprimeStructure(nbr);

  CumulsExterneLire(nbr);
  ImprimeExterne(nbr);

  ImprimeTotaux;

  // fin de l'impression
  Printer.EndDoc
end;

procedure ExecutionCumulsFiche;
var
  idh: integer;
  c: currency;
begin
  // charge totaux hebdo
  with FormBase.TableHebdo do
  begin
    c := FieldByName('heb_totalinduits').asCurrency;
    TableauValHebdo[0] := TableauValHebdo[0] + c;

    c := FieldByName('heb_chiffreaffairedefini').asCurrency;
    TableauValHebdo[1] := TableauValHebdo[1] + c;

    c := FieldByName('heb_totalsalarie').asCurrency;
    TableauValHebdo[2] := TableauValHebdo[2] + c;

    c := FieldByName('heb_totalinterim').asCurrency;
    TableauValHebdo[3] := TableauValHebdo[3] + c;

    c := FieldByName('heb_totalvehicule').asCurrency;
    TableauValHebdo[4] := TableauValHebdo[4] + c;

    c := FieldByName('heb_totaldiamant').asCurrency;
    TableauValHebdo[5] := TableauValHebdo[5] + c;

    c := FieldByName('heb_totalmateriel').asCurrency;
    TableauValHebdo[6] := TableauValHebdo[6] + c;

    c := FieldByName('heb_totaloutils').asCurrency;
    TableauValHebdo[7] := TableauValHebdo[7] + c;

    c := FieldByName('heb_totalexterne').asCurrency;
    TableauValHebdo[8] := TableauValHebdo[8] + c;

    c := FieldByName('heb_totalinterne').asCurrency;
    TableauValHebdo[9] := TableauValHebdo[9] + c;

    c := FieldByName('heb_totaldeboursesec').asCurrency;
    TableauValHebdo[10] := TableauValHebdo[10] + c;

    c := FieldByName('heb_totalstructures').asCurrency;
    TableauValHebdo[11] := TableauValHebdo[11] + c;

    c := FieldByName('heb_totalprixrevient').asCurrency;
    TableauValHebdo[12] := TableauValHebdo[12] + c;

    c := FieldByName('heb_totalresultat').asCurrency;
    TableauValHebdo[13] := TableauValHebdo[13] + c;
  end;

  // traiter les cumuls des saisies
  idh := HebdoIdentifiant;
  CumulsPointage(idh);
  CumulsAchat(idh);
  CumulsDiamant(idh);
  CumulsMateriel(idh);
  CumulsVehicule(idh);
  CumulsStructure(idh);
  CumulsExterne(idh);
end;

function TraiteCumulSemaine(r, b: boolean; c, n: integer): boolean;
begin
  result := false;
  if ChercheHebdo(r, b, c, n) then
  begin
    // cumuler contenu de la fiche
    ExecutionCumulsFiche;
    result := true;
  end;
end;

function TraiterCumulMois(NumMois, idc: integer): boolean;
var
  NumS: integer;
  sd: integer;
  sf: integer;
  bis: boolean;
begin
  result := false;

  // calcul des numero de semaine du mois
  EcartSemaine(NumMois, AnneeCourante, sd, sf);

  // pour traiter toutes ces semaines
  for NumS := sd to sf do
  begin
    // si premiere semaine tester semaine bis
    if NumS = sd then
      bis := SemaineBis(NumS, AnneeCourante)
    else
      bis := false;

    if TraiteCumulSemaine(false, bis, idc, NumS) then
      result := true;
  end;

  // recupere les cumuls de regul
  if TraiteCumulSemaine(true, false, idc, NumMois) then
    result := true;
end;

function TraiterCumulsAnnee(idc: integer): boolean;
var
  numM: integer;
begin
  result := false;

  // pour traiter toutes les mois
  for numM := 1 to 12 do
    if TraiterCumulMois(numM, idc) then
      result := true;
end;

function CumulerFichesSemaine(bis: boolean; NumS: integer): boolean;
var
  temp: string;
  nb: integer;
  i: integer;
  s: boolean;
  vl: integer;
begin
  result := false;

  with FormBase.TableHebdo do
  begin
    // preparer le filtre de recherche
    str(NumS, temp);
    filter := 'heb_numerosemaine = ' + temp;
    filtered := true;
    indexname := '';

    open;
    setkey;

    First;
    nb := RecordCount;

    if nb <> 0 then
    begin
      // traitement recherche fiche bis si necessaire
      for i := 1 to nb do
      begin
        s := FieldByName('heb_semainebis').asBoolean;
        vl := FieldByName('heb_numerosemaine').asinteger;
        if (s = bis) and TestAnneeEnCours and (vl = NumS) then
        begin
          // cumuler contenu de la fiche
          result := true;
          ExecutionCumulsFiche;
        end;

        // cherche fiche suivante
        Next;
      end;
    end;

    close;
  end;

  // enreg dans table cumuls des semaines
end;

procedure ImprimerCumulHebdo(bis: boolean; NumS, ChoixF, filtre: integer;
  ChoixChantiers: boolean; idcha: integer);
var
  nbc: integer;
  { impression du cumul des fiches hebdo de la semaine selon un choix defini }
begin
  // lire nombre de chantier a traiter
  nbc := ouvrirfiltrechantier(ChoixF, filtre);
  if nbc = 0 then
  begin
    // message
    showmessage('Aucun chantier dans le choix de filtre');
    exit;
  end;

  // reset buffers cumuls
  ResetCumuls;

  // aucune impression effectuee
  if not CumulerFichesSemaine(bis, NumS) then
    showmessage('Aucun cumul n''est � imprimer.')
  else
  begin
    // demarre impression
    SortieDebutCumuls;

    imprimeenteteETAT2(bis, NumS, ChoixF, filtre, NomFiltre);

    SortieCalculsCumuls;
  end;

  // fermer les filtres chantiers et hebdo
  fermehebdo;
  fermefiltrechantier;
end;

function CumulerFichesMois(NumMois: integer): boolean;
var
  temp: string;
  nb: integer;
  i: integer;
  s: boolean;
  vl: integer;
begin
  result := false;

  with FormBase.TableHebdo do
  begin
    // preparer le filtre de recherche
    str(NumMois, temp);
    filter := 'heb_numeromois = ' + temp;
    filtered := true;
    indexname := '';

    open;
    setkey;

    First;
    nb := RecordCount;

    if nb <> 0 then
    begin
      // verifie si fiche de regularisation existe
      for i := 1 to nb do
      begin
        s := FieldByName('heb_regul').asBoolean;
        vl := FieldByName('heb_numeromois').asinteger;
        if s and TestAnneeEnCours and (vl = NumMois) then
        begin
          // cumuler contenu de la fiche
          result := true;
          ExecutionCumulsFiche;
        end;

        // cherche fiche suivante
        Next;
      end;
    end;

    close;
  end;

  // enreg dans table cumuls des mois
end;

function CalculCumulMois(NumMois: integer): boolean;
var
  sd: integer;
  sf: integer;
  bis: boolean;
  ok: boolean;
  NumS: integer;
begin
  ok := false;

  // calcul des numero de semaine du mois
  EcartSemaine(NumMois, AnneeCourante, sd, sf);

  // pour traiter toutes ces semaines
  for NumS := sd to sf do
  begin
    // si premiere semaine tester semaine bis
    if NumS = sd then
      bis := SemaineBis(NumS, AnneeCourante)
    else
      bis := false;

    // verif si enreg cumul semaine dans table cumuls
    // si oui cumuler avec et sauter suite
    // sinon executer suite
    if CumulerFichesSemaine(bis, NumS) then
      ok := true;
  end;

  // traiter les cumuls des reguls
  if CumulerFichesMois(NumMois) then
    ok := true;

  result := ok;
end;

procedure ImprimerCumulMois(NumMois, ChoixF, filtre: integer;
  ChoixChantiers: boolean; idcha: integer);
{ impression du cumul des fiches hebdo et regul du mois selon un choix defini }
var
  ok: boolean;
  nbc: integer;
begin
  // lire nombre de chantier a traiter
  nbc := ouvrirfiltrechantier(ChoixF, filtre);
  if nbc = 0 then
  begin
    // message
    showmessage('Aucun chantier dans le choix de filtre');
    exit;
  end;

  // reset buffers cumuls
  ResetCumuls;

  ok := CalculCumulMois(NumMois);
  if not ok then
    showmessage('Aucun cumul n''est � imprimer.')
  else
  begin
    // demarre impression
    SortieDebutCumuls;

    imprimeenteteETAT3(NumMois, ChoixF, filtre, NomFiltre);

    SortieCalculsCumuls;
  end;

  // fermer les filtres chantiers et hebdo
  fermehebdo;
  fermefiltrechantier;
end;

function CumulerFichesAnnee: boolean;
var
  nb: integer;
  i: integer;
begin
  result := false;

  with FormBase.TableHebdo do
  begin
    // preparer le filtre de recherche
    filtered := false;
    indexname := '';

    open;
    setkey;

    First;
    nb := RecordCount;

    if nb <> 0 then
    begin
      // verifie si fiche de regularisation existe
      for i := 1 to nb do
      begin
        if TestAnneeEnCours then
        begin
          // cumuler contenu de la fiche
          result := true;
          ExecutionCumulsFiche;
        end;

        // cherche fiche suivante
        Next;
      end;
    end;
    //
    close;
  end;
end;

procedure ImprimerCumulAnnee(NumMois, ChoixF, filtre: integer;
  ChoixChantiers: boolean; idcha: integer);
{ impression du cumul des fiches hebdo et regul de l'annee selon un choix
  defini }
var
  nbc: integer;
  ok: boolean;
  i: integer;
begin
  // lire nombre de chantier a traiter
  nbc := ouvrirfiltrechantier(ChoixF, filtre);
  if nbc = 0 then
  begin
    // message
    showmessage('Aucun chantier dans le choix de filtre');
    exit;
  end;

  // reset buffers cumuls
  ResetCumuls;

  ok := false;
  if ChoixChantiers then
  begin
    for i := 1 to NumMois do
      ok := CalculCumulMois(i);
  end
  else
  begin
    if TraiterCumulsAnnee(idcha) then
      ok := true;
  end;

  if not ok then
    showmessage('Aucun cumul n''est � imprimer.')
  else
  begin
    // impression fiche cumul
    SortieDebutCumuls;

    imprimeenteteETAT9(LireChaineChantier);

    SortieCalculsCumuls;
  end;

  // fermer les filtres chantiers et hebdo
  fermehebdo;
  fermefiltrechantier;

end;

{ ----------------------------------------------------------------------------- }
procedure RAZtableau(var tableau: array of currency);
var
  i: integer;
begin
  for i := 0 to 11 do
    tableau[i] := 0;
end;

procedure SauveTableau(var des: array of currency; sou: array of currency);
var
  i: integer;
begin
  for i := 0 to 11 do
    des[i] := sou[i];
end;

procedure AjouteTableau(var tot: array of currency; var t: array of currency);
var
  i: integer;
begin
  for i := 0 to 11 do
    tot[i] := tot[i] + t[i];
end;

procedure PCsTableau(var tot: array of currency; var t: array of currency);
var
  i: integer;
begin
  if tot[0] <> 0 then
  begin
    t[2] := (tot[2] / tot[0]) * 100;

    for i := 4 to 11 do
      t[i] := (tot[i] / tot[0]) * 100;
  end;
end;

procedure FinTableauLOC(nj: integer; var v, m, s, pr, tot: currency;
  totaux: array of currency);
begin
  // calculer valeurs
  v := nj * coutfixevehicule;
  m := nj * coutfixemateriel;
  s := nj * coutfixestructure;

  pr := totaux[10] - totaux[9] - totaux[6] - totaux[4] + v + m + s;
  tot := totaux[0] - pr;
end;

procedure FinTableau(IdSemaine: integer; var nj: integer;
  var v, m, s, pr, tot: currency; totaux: array of currency);
begin
  // calculer valeurs
  nj := LireNBJSaisie(IdSemaine);
  FinTableauLOC(nj, v, m, s, pr, tot, totaux);
end;

procedure FinTableauMois(NMois: integer; var nj: integer;
  var v, m, s, pr, tot: currency; totaux: array of currency);
var
  i: integer;
begin
  // calculer valeurs
  nj := 0;
  if NMois = 0 then
    for i := 1 to 12 do
      nj := nj + LireNBJmois(i)
  else
    nj := LireNBJmois(NMois);

  FinTableauLOC(nj, v, m, s, pr, tot, totaux);
end;

procedure FinTableauMoisClients(NMois: integer; var nj: integer;
  var v, m, s, pr, tot: currency; totaux: array of currency);
var
  i: integer;
begin
  // calculer valeurs
  nj := 0;
  for i := 1 to NMois do
    nj := nj + LireNBJmois(i);

  FinTableauLOC(nj, v, m, s, pr, tot, totaux);
end;

procedure LireLigneClient(var tab: array of currency);
{ lecture du contenu de la fiche en cours et renvoie la ligne de recap (tab) }
var
  index: integer;
  nbrec: integer;
  i: integer;
  c: currency;
begin
  // lire id hebdo
  index := LireIdentifiantHebdo;

  // lire pointages et calculer total JTR
  PointageChargeChaines(index, nbrec);
  tab[1] := 0;
  if nbrec <> 0 then
    for i := 0 to (nbrec - 1) do
      tab[1] := tab[1] + Tableauvaltemp[3, i];

  // lire vehicules et calculer total JV
  VehiculeChargeChaines(index, nbrec);
  tab[3] := 0;
  if nbrec <> 0 then
    for i := 0 to (nbrec - 1) do
      tab[3] := tab[3] + Tableauvaltemp[3, i];

  // lire structures et calculer total FS
  // StructureChargeChaines(index, nbrec);
  // tab[9] := 0;
  // if nbrec <> 0 then
  // for i := 0 to (nbrec - 1) do
  // tab[9] := tab[9] + Tableauvaltemp[3, i];

  // lire donnees dans fiche hebdo et calculer ligne
  with FormBase.TableHebdo do
  begin
    tab[0] := FieldByName('heb_chiffreaffairedefini').asCurrency;

    c := FieldByName('heb_totalsalarie').asCurrency;
    c := c + FieldByName('heb_totalinterim').asCurrency;
    tab[2] := c;

    tab[4] := FieldByName('heb_totalvehicule').asCurrency;

    tab[5] := FieldByName('heb_totaldiamant').asCurrency;

    c := FieldByName('heb_totaloutils').asCurrency;
    c := c + FieldByName('heb_totalmateriel').asCurrency;
    tab[6] := c;

    c := FieldByName('heb_totalexterne').asCurrency;
    c := c + FieldByName('heb_totalinterne').asCurrency;
    tab[7] := c;

    tab[8] := FieldByName('heb_totaldeboursesec').asCurrency;

    // correction bug (nbr au lieu de somme)
    tab[9] := FieldByName('heb_totalstructures').asCurrency;

    tab[10] := FieldByName('heb_totalprixrevient').asCurrency;

    tab[11] := FieldByName('heb_totalresultat').asCurrency;
  end;
end;

procedure ImprimeRecapDebutPage(numM, NumS: integer);
var
  titre: string;
  semaine: string;
  Mois: string;
begin
  // debut impression page
  Printer.Orientation := poLandscape;
  Printer.BeginDoc;

  // imprimer fond de l'etat5
  tracefondETAT5;

  // imprimer entete de l'etat5
  if NumS <> 0 then
    traceenteteETAT5
  else
    traceenteteETAT5regul;

  // determiner nom recap
  titre := '';
  case NumFiltre of
    FiltreAgence:
      titre := 'Agence = ';
    FiltreCommercial:
      titre := 'Commercial = ';
    FiltreClient:
      titre := 'Client = ';
  end;
  titre := titre + NomFiltre;

  // trace le titre de la page
  semaine := inttostr(NumS);
  if NumS < 10 then
    semaine := '0' + semaine;

  Mois := NumMois(numM);
  if semaine = '00' then
    semaine := '';
  traceenteteETAT5remplir(semaine, titre, Mois);
end;

procedure ImprimeRecapLigneClient(lig: integer; var totaux: array of currency);
var
  numc: string;
  nom: string;
  tab: array [0 .. 11] of currency;
  col: integer;
begin
  // lire numero du chantier
  numc := ChercheNumeroChantier;

  // lire nom client du chantier en cours
  nom := ChercheNomClient;
  IF Length(nom) > 13 then
    nom := Copy(nom, 1, 13);

  // lire couleur du commercial
  col := ChercheCouleurCommercial;

  // lire ligne hebdo du chantier en question
  LireLigneClient(tab);

  // totaux = totaux + ligne
  AjouteTableau(totaux, tab);

  // imprime ligne
  TraceLigneETAT5(col, lig + 1, numc, nom, tab);
end;

procedure ImprimeRecapLigneReport(var totaux: array of currency);
begin
  // imprime ligne de reports
  TraceReportETAT5(totaux);
end;

procedure ImprimeRecapFinPage(IdSemaine: integer; totaux: array of currency);
var
  nj: integer;
  temp: array [0 .. 11] of currency;
  v, m, s: currency;
  pr, tot: currency;
begin
  // imprime totaux
  TraceTotauxETAT5(totaux);

  // calculer les pourcents et imprime pourcents
  RAZtableau(temp);
  PCsTableau(totaux, temp);

  TracePourcentETAT5(temp);

  // calculer et imprime valeurs
  FinTableau(IdSemaine, nj, v, m, s, pr, tot, totaux);
  TraceValeursETAT5(NumPageEtat5, nj, v, m, s, pr, tot);

  // fin impression page
  Printer.EndDoc;

  inc(NumPageEtat5);
end;

{ ----------------------------------------------------------------------------- }
procedure traiteSemaineRecap(regul, bis: boolean;
  numM, NumS, IdSem, nbr: integer);
{ traite le recapitulatif de la semaine

  regul     vrai  fiches de regularisation traitees
  faux  fiches hebdomadaire traitees

  bis       lorsqu'il s'agit des fiches hebdomadaire

  vrai  fiches bis traitees appartenants au mois suivant la semaine
  traitee

  faux  fiches traitees appartenants au mois de la semaine traitee

  NumM      numero de mois pour les fiches traitees

  NumS      numero de semaine pour les fiches traitees

  idsem     numero de la semaine dans le mois

  nbr       nombre de chantiers traites
}
var
  i: integer;
  okc: boolean;
  idc: integer;
  nbl: integer;
  report: boolean;
  totaux: array [0 .. 11] of currency;
begin
  nbl := 0;
  report := false;

  // raz tableau
  RAZtableau(totaux);

  // pointer sur premier chantier
  ChantierPremier;

  // pour traiter tous les chantiers
  for i := 1 to nbr do
  begin
    // recupere l'identifiant de chantier
    idc := LirePointeurChantier;

    // trouver l'hebdo ou la regul de ce chantier/semaine
    if regul then
      okc := ChercheHebdo(true, false, idc, numM)
    else
      okc := ChercheHebdo(false, bis, idc, NumS);

    if okc then
    begin
      if nbl = 0 then
      begin
        // debut impression de la page
        ImprimeRecapDebutPage(numM, NumS);

        if report then
        begin
          // imprime ligne de report avec totaux
          ImprimeRecapLigneReport(totaux);
          inc(nbl);
        end;
      end;

      // imprime ligne client
      ImprimeRecapLigneClient(nbl, totaux);
      inc(nbl);

      if nbl = maxligneet5 then
      begin
        // imprime fin de page
        ImprimeRecapFinPage(IdSem, totaux);

        nbl := 0;
        report := true;
      end;
    end;

    // pointer sur chantier suivant
    ChantierSuivant;
  end;

  // tester si page en cours
  if nbl <> 0 then
  begin
    // imprime fin de page
    ImprimeRecapFinPage(IdSem, totaux);
  end;
end;

procedure traiteMoisRecap(NumMois, nbc: integer);
{ traite le recapitulatif du mois

  NumMois   numero de mois pour les fiches traitees

  nbc       nombre de chantiers traites
}
var
  sd: integer;
  sf: integer;
  NumS: integer;
  IdSem: integer;
  bis: boolean;
begin
  // calcul des numero de semaine du mois
  EcartSemaine(NumMois, AnneeCourante, sd, sf);

  // pour traiter toutes ces semaines
  for NumS := sd to sf do
  begin
    IdSem := NumS - sd + 1;

    bis := false;
    if IdSem = 1 then
      bis := SemaineBis(NumS, AnneeCourante);

    if ValideImprimeRecap(IdSem) then
      traiteSemaineRecap(false, bis, NumMois, NumS, IdSem, nbc);
  end;

  // pour traiter les reguls
  if ValideImprimeRecap(0) then
    traiteSemaineRecap(true, false, NumMois, 0, 0, nbc);
end;

procedure ImprimerRecap(RecapAnnuel: boolean;
  NumMois, ChoixFiltre, filtre: integer);
{ traite le recapitulatif

  RecapAnnuel   vrai  impression du recapitulatif annuel
  faux  impression du recapitulatif mensuel

  NumMois       numero de mois pour les fiches traitees

  ChoixFiltre   type de filtrage effectue dans le recapitulatif (voir routine
  'ouvrirfiltrechantier')

  filtre        index de filtrage effectue dans le recapitulatif (voir routine
  'ouvrirfiltrechantier')
}
var
  nbc: integer;
  i: integer;
  savidx: string;
begin
  // lire nombre de chantier a traiter
  nbc := ouvrirfiltrechantier(ChoixFiltre, filtre);
  if nbc = 0 then
  begin
    // message
    showmessage('Aucun chantier dans le choix de filtre');
    exit;
  end;

  NumPageEtat5 := 1;

  // sauver l'index defini et prendre celui par id
  savidx := FormBase.TableClients.indexname;
  FormBase.TableClients.indexname := '';

  if RecapAnnuel then
    for i := 1 to 12 do
      // traiteMoisRecap(i, ChoixFiltre, filtre, nbc)
      traiteMoisRecap(i, nbc)
  else
    // traiteMoisRecap(NumMois, ChoixFiltre, filtre, nbc);
    traiteMoisRecap(NumMois, nbc);

  // reprendre l'index initial
  FormBase.TableClients.indexname := savidx;

  // fermer les filtres chantiers et hebdo
  fermehebdo;
  fermefiltrechantier;
end;

{ ----------------------------------------------------------------------------- }
procedure ImprimeRecapMensuelDebutPage(numM: integer);
{ imprime l'entete du recapitulatif mensuel simple

  NumM       numero de mois du mois traite
}
var
  titre: string;
  Mois: string;
begin
  // debut impression page
  Printer.Orientation := poLandscape;
  Printer.BeginDoc;

  // imprimer fond de l'etat5
  tracefondETAT6;

  // imprimer entete de l'etat5
  traceenteteETAT6;

  // determiner nom recap
  titre := '';
  case NumFiltre of
    FiltreAgence:
      titre := 'Agence = ';
    FiltreCommercial:
      titre := 'Commercial = ';
    FiltreClient:
      titre := 'Client = ';
  end;
  titre := titre + NomFiltre;

  Mois := NumMois(numM);
  traceenteteETAT6remplir(titre, Mois);
end;

procedure ImprimeRecapMensuelFinPage(NMois: integer; totaux: array of currency);
{ imprime la fin du recapitulatif mensuel simple

  NumMois       numero de mois du mois traite

  totaux        totaux generaux du recapitulatif mensuel simple
}
var
  nj: integer;
  temp: array [0 .. 11] of currency;
  v, m, s: currency;
  pr, tot: currency;
begin
  // imprime totaux
  TraceTotauxETAT6(totaux);

  // calculer les pourcents et imprime pourcents
  RAZtableau(temp);
  PCsTableau(totaux, temp);

  TracePourcentETAT6(temp);

  // calculer valeurs
  FinTableauMois(NMois, nj, v, m, s, pr, tot, totaux);

  // calculer et imprime valeurs
  TraceValeursETAT6(nj, v, m, s, pr, tot);

  // fin impression page
  Printer.EndDoc;
end;

procedure traiteSemaineRecapMensuel(regul, bis: boolean;
  numM, NumS, IdSem, nbr: integer; var totaux: array of currency);
{ traite le recapitulatif

  regul         vrai  fiches de regularisation traitees
  faux  fiches hebdomadaire traitees

  bis           lorsqu'il s'agit des fiches hebdomadaire

  vrai  fiches bis traitees appartenants au mois suivant
  la semaine traitee

  faux  fiches traitees appartenants au mois de la semaine
  traitee

  NumM          numero de mois pour les fiches traitees

  NumS          numero de semaine pour les fiches traitees

  idsem         numero de la semaine dans le mois

  nbr           nombre de chantiers traites

  totaux        totaux generaux de la semaine
}
var
  i: integer;
  okc: boolean;
  ligneclient: array [0 .. 11] of currency;
  lignetotal: array [0 .. 11] of currency;
  nom: string;
  idc: integer;
begin
  // raz tableau
  RAZtableau(lignetotal);

  // pointer sur premier chantier
  ChantierPremier;

  // pour traiter tous les chantiers
  for i := 1 to nbr do
  begin
    // recupere l'identifiant de chantier
    idc := LirePointeurChantier;

    // trouver l'hebdo ou la regul de ce chantier/semaine
    if regul then
      okc := ChercheHebdo(true, false, idc, numM)
    else
      okc := ChercheHebdo(false, bis, idc, NumS);

    if okc then
    begin
      // lire ligne hebdo du chantier en question
      LireLigneClient(ligneclient);

      // totaux = totaux + ligne
      AjouteTableau(lignetotal, ligneclient);
    end;

    // pointer sur chantier suivant
    ChantierSuivant;
  end;

  // trace la ligne des cumuls de la semaine
  if NumS <> 0 then
    nom := 'Semaine ' + inttostr(NumS)
  else
    nom := 'Regul';

  TraceLigneETAT6(IdSem, '', nom, lignetotal);

  AjouteTableau(totaux, lignetotal);
end;

procedure traiteMoisRecapSimple(NMois, ChoixFiltre, filtre, nbc: integer);
{ traite les fiches du mois du recapitulatif simple

  NMois         numero de mois pour les fiches traitees

  ChoixFiltre   type de filtrage effectue dans le recapitulatif simple
  (voir routine 'ouvrirfiltrechantier')

  filtre        index de filtrage effectue dans le recapitulatif simple
  (voir routine 'ouvrirfiltrechantier')

  nbc           nombre de chantiers traites
}

var
  IdSem: integer;
  sd: integer;
  sf: integer;
  bis: boolean;
  NumS: integer;
  totaux: array [0 .. 11] of currency;
begin
  // calcul des numero de semaine du mois
  EcartSemaine(NMois, AnneeCourante, sd, sf);

  ImprimeRecapMensuelDebutPage(NMois);

  RAZtableau(totaux);

  // pour traiter toutes ces semaines
  IdSem := 1;
  for NumS := sd to sf do
  begin
    IdSem := NumS - sd + 1;

    bis := false;
    if IdSem = 1 then
      bis := SemaineBis(NumS, AnneeCourante);

    traiteSemaineRecapMensuel(false, bis, NMois, NumS, IdSem, nbc, totaux);
  end;

  // pour traiter les reguls
  traiteSemaineRecapMensuel(true, false, NMois, 0, IdSem + 1, nbc, totaux);

  // trace totaux et fin de page
  ImprimeRecapMensuelFinPage(NMois, totaux);
end;

procedure ImprimerRecapSimple(RecapAnnuel: boolean;
  NMois, ChoixFiltre, filtre: integer);
{ traite le recapitulatif simple

  RecapAnnuel   vrai  impression du recapitulatif simple annuel
  faux  impression du recapitulatif simple mensuel

  NMois         numero de mois pour les fiches traitees

  ChoixFiltre   type de filtrage effectue dans le recapitulatif simple
  (voir routine 'ouvrirfiltrechantier')

  filtre        index de filtrage effectue dans le recapitulatif simple
  (voir routine 'ouvrirfiltrechantier')
}
var
  i: integer;
  nbc: integer;
begin
  // lire nombre de chantier a traiter
  nbc := ouvrirfiltrechantier(ChoixFiltre, filtre);
  if nbc = 0 then
  begin
    // message
    showmessage('Aucun chantier dans le choix de filtre');
    exit;
  end;

  if RecapAnnuel then
    for i := 1 to 12 do
      traiteMoisRecapSimple(i, ChoixFiltre, filtre, nbc)
  else
    traiteMoisRecapSimple(NMois, ChoixFiltre, filtre, nbc);

  // fermer les filtres chantiers et hebdo
  fermehebdo;
  fermefiltrechantier;
end;

{ ----------------------------------------------------------------------------- }
procedure CumulsChantier(regul, bis: boolean; numM, NumS: integer;
  var totaux: array of currency; var trouve: boolean);
var
  idc: integer;
  okc: boolean;
  temp: array [0 .. 11] of currency;
begin
  // recupere l'identifiant de chantier
  idc := LirePointeurChantier;
  trouve := false;

  // trouver l'hebdo ou la regul de ce chantier/semaine
  if regul then
    okc := ChercheHebdo(true, false, idc, numM)
  else
    okc := ChercheHebdo(false, bis, idc, NumS);

  if okc then
  begin
    // lire ligne hebdo du chantier en question
    LireLigneClient(temp);

    // totaux = totaux + ligne
    AjouteTableau(totaux, temp);

    trouve := true;
  end;
end;

procedure CumulsMoisChantier(NMois: integer; var totalligne: array of currency;
  var trouve: boolean);
var
  sd: integer;
  sf: integer;
  NumS: integer;
  IdSem: integer;
  bis: boolean;
  sortie: boolean;
begin
  trouve := false;

  // calcul des numero de semaine du mois
  EcartSemaine(NMois, AnneeCourante, sd, sf);

  // pour traiter toutes les semaines du chantier
  for NumS := sd to sf do
  begin
    IdSem := NumS - sd + 1;

    bis := false;
    if IdSem = 1 then
      bis := SemaineBis(NumS, AnneeCourante);

    CumulsChantier(false, bis, NMois, NumS, totalligne, sortie);
    if sortie then
      trouve := true;
  end;

  // pour traiter fiche regul
  CumulsChantier(true, false, NMois, 0, totalligne, sortie);

  if sortie then
    trouve := true;
end;

procedure ImprimerRecapClients(RecapAnnuel: boolean; NMois, idCom: integer);
{ traite le recapitulatif clients

  RecapAnnuel   vrai  impression du recapitulatif clients annuel
  faux  impression du recapitulatif clients mensuel

  NMois         numero de mois pour les fiches traitees

  idcom         identifiant du commercial traite
}

  procedure ImprimeDebutPage(numM, idCom: integer);
  var
    titre: string;
    nom: string;
  begin
    // debut impression page
    Printer.Orientation := poLandscape;
    Printer.BeginDoc;

    // imprimer fond de l'etat5
    tracefondETATRECAPCLI;

    // imprimer entete de l'etat recap clients
    traceenteteETATRECAPCLI;

    // determiner nom recap
    titre := '';
    if idCom <> 0 then
    begin
      with FormBase.TableCommerciaux do
        nom := FieldByName('com_libellenom').asstring;

      titre := 'Commercial = ' + nom;
    end;

    // mois := NumMois(NumM);
    traceenteteETAT5remplir('', titre, '');
  end;

  procedure ImprimeTotalClient(nom: string; lig: integer;
    var totaux: array of currency);
  begin
    // imprime ligne
    if Length(nom) > 13 then
      nom := Copy(nom, 1, 13);

    TraceLigneETAT5(0, lig + 1, '', nom, totaux);
    TraceLigneEncadreETAT5(lig + 1);
  end;

  procedure ImprimeLigneClient(nom: string; lig: integer);
  begin
    // imprime ligne
    if Length(nom) > 13 then
      nom := Copy(nom, 1, 13);

    TraceLignenomETAT5(lig + 1, nom);
    TraceLigneEncadreETAT5(lig + 1);
  end;

  procedure ImprimeLigneChantier(lig, idCom: integer;
    var totaux: array of currency);
  var
    numc: string;
    nom: string;
    col: integer;
  begin
    // lire numero du chantier
    numc := ChercheNumeroChantier;

    // lire couleur du commercial du chantier
    col := 0;
    if idCom = 0 then
      col := ChercheCouleurCommercial;

    // lire nom chantier
    nom := ChercheNomChantier;
    if Length(nom) > 17 then
      nom := Copy(nom, 1, 13);

    // imprime ligne
    TraceLigneETAT5(col, lig + 1, numc, nom, totaux);
  end;

  procedure ImprimeLigneReport(var totaux: array of currency);
  begin
    // imprime ligne de reports
    TraceLigneETAT5(0, 1, '', 'Report', totaux);

    // sousligner la ligne de report
    TraceLignereportETAT5;

    // raz tableau
    RAZtableau(totaux);
  end;

  procedure ImprimeFinPage(fin: boolean; NMois: integer;
    totaux: array of currency);
  var
    nj: integer;
    temp: array [0 .. 11] of currency;
    v, m, s: currency;
    pr, tot: currency;
  begin
    // imprime totaux si derniere page
    if fin then
    begin
      TraceFinETAT5;

      TraceTotauxETAT5(totaux);

      // calculer les pourcents et imprime pourcents
      RAZtableau(temp);
      PCsTableau(totaux, temp);
      TracePourcentETAT5(temp);

      // calculer valeurs
      FinTableauMoisClients(NMois, nj, v, m, s, pr, tot, totaux);

      // calculer et imprime valeurs
      TraceValeursETATRECAPCLI(nj, v, m, s, pr, tot);
    end;

    // fin impression page
    TracePageETATRECAPCLI(NumPageEtat5);
    Printer.EndDoc;

    inc(NumPageEtat5);
  end;

  procedure ImprimeAnnule;
  begin
    Printer.Abort;
  end;

  procedure CumulChantier(NMois: integer; var totaux: array of currency;
    var trouve: boolean);
  var
    // idc:      integer;
    temp: array [0 .. 11] of currency;
    // t:        string;
    // n:        integer;
    i: integer;
    ok: boolean;

  begin
    trouve := false;

    for i := 1 to NMois do
    begin
      RAZtableau(temp);
      CumulsMoisChantier(i, temp, ok);

      if ok then
      begin
        AjouteTableau(totaux, temp);
        trouve := true;
      end;
    end;

    exit;
    {
      // recupere l'identifiant de chantier
      idc := LirePointeurChantier;
      str(idc, t);
      t := 'id_chantier = ' + t;
      trouve := false;

      // trouver les fiches de ce chantier
      with FormBase.TableHebdo do
      begin
      // preparer le filtre de recherche
      filter := t;
      filtered := true;
      indexname := 'indfc';
      open;
      setkey;

      // sortie si aucunes fiches a traiter
      n := recordcount;
      if n = 0 then
      exit;

      // premiere fiche
      first;

      // traiter toutes les fiches
      for i := 0 to n do
      begin
      if TestAnneeEnCours and TestMoisEnCours(NMois) then
      begin
      // lire ligne hebdo du chantier
      LireLigneClient(temp);

      // totaux = totaux + ligne
      AjouteTableau(totaux, temp);

      // lire nombre de fiches
      trouve := true;

      // fiche suivante
      next;
      end;
      end;

      close;
      end;
    }
  end;

var
  nbc: integer;
  nbcli: integer;
  idcli: integer;
  nom: string;
  n: integer;
  i: integer;
  client: array [0 .. 11] of currency;
  ligne: array [0 .. 11] of currency;
  totaux: array [0 .. 11] of currency;
  nbl: integer;
  deb: boolean;
  fin: boolean;
  ok: boolean;
  impCliOK: boolean;
  trouve: boolean;
  nbcha: integer;
  imp: boolean;
begin
  NumPageEtat5 := 1;
  nbl := 0;
  deb := false;
  imp := false;
  RAZtableau(totaux);

  // lire le nombre de clients
  nbcli := LireNombreClients;

  // pointer sur le premier client
  ClientPremier;

  for n := 1 to nbcli do
  begin
    // lire pointeur et nom client
    idcli := LirePointeurClient;
    nom := LireNomClient;

    // lire nombre de chantier du client a traiter
    nbc := ouvrirchantiersclient(idcli);

    // s'il y a des chantiers a traiter
    if nbc <> 0 then
    begin
      impCliOK := false;

      // pointer sur premier chantier du client
      ChantierPremier; // FormBase.Tablechantiers.First;

      // raz cumul client
      RAZtableau(client);
      nbcha := 0;

      // pour traiter tous les chantiers du client
      for i := 1 to nbc do
      begin
        // verifier si impression
        ok := false;
        if idCom = 0 then
          ok := true
        else
          // chantier du commercial
          if LirePointeurCommercial = idCom then
            ok := true;

        if ok then
        begin
          impCliOK := true;

          // raz cumul chantier
          RAZtableau(ligne);

          // calcul cumuls du chantier
          CumulChantier(NMois, ligne, trouve);

          // calcul total du client
          AjouteTableau(client, ligne);

          // calcul report
          AjouteTableau(totaux, ligne);

          // debut impression de la page
          if (nbl = 0) and (deb = false) then
          begin
            ImprimeDebutPage(NMois, idCom);
            deb := true;
          end;

          if nbl = (maxligneet5 - 1) then
          begin
            // imprime fin de page
            ImprimeFinPage(false, NMois, totaux);
            nbl := 0;

            // debut impression de la page
            ImprimeDebutPage(NMois, idCom);
            deb := true;
          end;

          // imprime ligne chantier si fiche ok
          if trouve then
          begin
            ImprimeLigneChantier(nbl, idCom, ligne);
            inc(nbl);
            inc(nbcha);

            imp := true;
          end;

          if nbl = (maxligneet5 - 1) then
          begin
            // imprime ou non le total client
            if nbcha <> 0 then
            begin
              if i = nbc then
                ImprimeTotalClient(nom, nbl, client)
              else
                ImprimeLigneClient(nom, nbl);
            end;

            imp := true;
            inc(nbl);
          end;

          if nbl = maxligneet5 then
          begin
            // imprime fin de page
            fin := false;
            if (i = nbc) and (n = nbcli) then
              fin := true;

            ImprimeFinPage(fin, NMois, totaux);
            nbl := 0;
            imp := true;
            deb := false;
          end;
        end;

        // pointer sur chantier suivant
        ChantierSuivant;
      end;

      if (nbl <> 0) and impCliOK and (nbcha <> 0) then
      begin
        ImprimeTotalClient(nom, nbl, client);
        inc(nbl);
        imp := true;
      end;
    end;

    // pointer sur client suivant
    ClientSuivant;
  end;

  if not imp then
  begin
    // imprime fin de page
    ImprimeAnnule;
    exit;
  end;

  // tester si page en cours
  if (nbl <> 0) or deb then
  begin
    // imprime fin de page
    ImprimeFinPage(true, NMois, totaux);
  end;
end;

procedure ImprimerRecapChantier(RecapAnnuel: boolean;
  NMois, ChoixFiltre, filtre: integer);

  procedure ImprimeDebutPage(numM, ChoixFiltre, filtre: integer);
  var
    titre: string;
    Mois: string;
  begin
    // debut impression page
    Printer.Orientation := poLandscape;
    Printer.BeginDoc;

    // imprimer fond de l'etat5
    tracefondETAT5;

    // imprimer entete de l'etat5
    traceenteteETATRECAPCHA;

    // determiner nom recap
    titre := '';
    case NumFiltre of
      FiltreAgence:
        titre := 'Agence = ';
      FiltreCommercial:
        titre := 'Commercial = ';
      FiltreClient:
        titre := 'Client = ';
    end;
    titre := titre + NomFiltre;

    Mois := NumMois(numM);
    traceenteteETAT8remplir(titre, Mois);
  end;

  procedure ImprimeLigneClient(lig: integer; var totaux: array of currency);
  var
    numc: string;
    nom: string;
    col: integer;
  begin
    // lire numero du chantier
    numc := ChercheNumeroChantier;

    // lire nom client du chantier en cours
    nom := ChercheNomClient;
    if Length(nom) > 13 then
      nom := Copy(nom, 1, 13);

    // lire couleur du commercial du chantier
    col := ChercheCouleurCommercial;

    // imprime ligne
    TraceLigneETAT5(col, lig + 1, numc, nom, totaux);
  end;

  procedure ImprimeLigneReport(var totaux: array of currency);
  begin
    // imprime ligne de reports
    TraceLigneETAT5(0, 1, '', 'Report', totaux);
  end;

  procedure ImprimeFinPage(NMois: integer; totaux: array of currency);
  var
    nj: integer;
    temp: array [0 .. 11] of currency;
    v, m, s: currency;
    pr, tot: currency;
  begin
    // imprime totaux
    TraceTotauxETAT5(totaux);

    // calculer les pourcents et imprime pourcents
    RAZtableau(temp);
    PCsTableau(totaux, temp);
    TracePourcentETAT5(temp);

    // calculer valeurs
    FinTableauMois(NMois, nj, v, m, s, pr, tot, totaux);

    // imprime valeurs
    TraceValeursETAT5(NumPageEtat5, nj, v, m, s, pr, tot);

    // fin impression page
    Printer.EndDoc;

    inc(NumPageEtat5);
  end;

  procedure ImprimeAnnule;
  begin
    Printer.Abort;
  end;

var
  nbc: integer;
  i: integer;
  j: integer;
  sd: integer;
  sf: integer;
  ligne: array [0 .. 11] of currency;
  total: array [0 .. 11] of currency;
  nbl: integer;
  savidx: string;
  trouve: boolean;
  imp: boolean;
  deb: boolean;
begin
  // lire nombre de chantier a traiter
  nbc := ouvrirfiltrechantier(ChoixFiltre, filtre);
  if nbc = 0 then
  begin
    // message
    showmessage('Aucun chantier dans le choix de filtre');
    exit;
  end;

  // calcul des numero de semaine du mois
  EcartSemaine(NMois, AnneeCourante, sd, sf);

  NumPageEtat5 := 1;
  nbl := 0;
  deb := false;
  imp := false;

  // sauver l'index defini et prendre celui par id
  savidx := FormBase.TableClients.indexname;
  FormBase.TableClients.indexname := '';

  // raz tableau report
  RAZtableau(total);

  // pointer sur premier chantier
  ChantierPremier;

  // pour traiter tous les chantiers
  for i := 1 to nbc do
  begin
    // raz cumul chantier
    RAZtableau(ligne);

    if RecapAnnuel then
      for j := 1 to 12 do
        CumulsMoisChantier(j, ligne, trouve)
    else
      CumulsMoisChantier(NMois, ligne, trouve);

    if nbl = 0 then
    begin
      // debut impression de la page
      if not deb then
      begin
        ImprimeDebutPage(NMois, ChoixFiltre, filtre);
        deb := true;
      end;

      if NumPageEtat5 <> 1 then
      begin
        // imprime ligne de report avec totaux
        ImprimeLigneReport(total);
        inc(nbl);
        imp := true;
      end;
    end;

    if trouve then
    begin
      // imprime ligne client
      ImprimeLigneClient(nbl, ligne);
      inc(nbl);
      imp := true;
    end;

    // calcul report
    AjouteTableau(total, ligne);

    if nbl = maxligneet5 then
    begin
      // imprime fin de page
      ImprimeFinPage(NMois, total);
      nbl := 0;
      deb := false;
      imp := true;
    end;

    // pointer sur chantier suivant
    ChantierSuivant;
  end;

  // aucune impression
  if not imp then
  begin
    ImprimeAnnule;
    exit;
  end;

  // tester si page en cours
  if (nbl <> 0) or deb then
  begin
    // imprime fin de page
    ImprimeFinPage(NMois, total);
  end;

  // reprendre l'index initial
  FormBase.TableClients.indexname := savidx;

  // fermer les filtres chantiers et hebdo
  fermehebdo;
  fermefiltrechantier;
end;

procedure ExporteRecapClients(RecapAnnuel: boolean; NMois: integer);
{ exporte le recapitulatif clients dans un fichier texte

  nomfichier    nom du fichier destine a l'exportation

  RecapAnnuel   vrai  impression du recapitulatif clients annuel
  faux  impression du recapitulatif clients mensuel

  NMois         numero de mois pour les fiches traitees
}

var
  numc: string;
  nomc: string;
  nbc: integer;
  nbcli: integer;
  idcli: integer;
  nom: string;
  n: integer;
  i: integer;
  j: integer;
  totauxclient: array [0 .. 11] of currency;
  lignechantier: array [0 .. 11] of currency;
  fichier: textfile;
  temp: string;
  nomfichier: string;
  c: currency;
  trouve: boolean;
  ok: boolean;
  nbcha: integer;
begin
  // debut export
  if not FormEtats.ExporteDialog.Execute then
    exit;

  nomfichier := FormEtats.ExporteDialog.FileName;
  assignfile(fichier, nomfichier);

  // lire le nombre de clients
  nbcli := LireNombreClients;

  // recouvrir si fichier existe
  rewrite(fichier);

  // pointer sur le premier client
  ClientPremier;

  for n := 1 to nbcli do
  begin
    // lire pointeur et nom client
    idcli := LirePointeurClient;
    nom := LireNomClient;

    // lire nombre de chantier du client a traiter
    nbc := ouvrirchantiersclient(idcli);

    // s'il ya des chantiers a traiter
    if nbc <> 0 then
    begin
      // pointer sur premier chantier du client
      ChantierPremier;

      // raz cumul client
      RAZtableau(totauxclient);

      // lire numero du chantier et nom chantier
      numc := ChercheNumeroChantier;
      nomc := ChercheNomChantier;

      // pour traiter tous les chantiers du client
      nbcha := 0;
      for i := 1 to nbc do
      begin
        // raz cumul chantier
        RAZtableau(lignechantier);
        ok := false;

        // cumuler le chantier sur l'annee
        for j := 1 to NMois do
        begin
          CumulsMoisChantier(j, lignechantier, trouve);
          if trouve then
            ok := true;
        end;

        // calcul total du client
        AjouteTableau(totauxclient, lignechantier);

        if ok then
        begin
          // un chantier de plus au client
          inc(nbcha);

          // Exporte Ligne Chantier
          write(fichier, numc, ';', nomc, ';');
          for j := 1 to 12 do
          begin
            c := lignechantier[j];
            temp := FormatCurr('0.00', c);
            write(fichier, temp, ';');
          end;

          // pour terminer la ligne
          writeln(fichier, '');
        end;

        // pointer sur chantier suivant
        ChantierSuivant;
      end;

      // le client a t'il plus d'un chantier
      if nbcha <> 0 then
      begin
        // Exporte Ligne Client('', nom, totauxclient)
        write(fichier, ';', nom, ';');
        for j := 1 to 12 do
        begin
          c := totauxclient[j];
          temp := FormatCurr('0.00', c);
          write(fichier, temp, ';');
        end;

        // pour terminer la ligne
        writeln(fichier, '');
      end;
    end;

    // pointer sur client suivant
    ClientSuivant;
  end;

  // fin export
  closefile(fichier);
end;

end.
