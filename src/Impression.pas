unit Impression;

interface

procedure InitImpression;

procedure imprimefondhebdo(nbEmpoye: integer; nbInterim: integer);
procedure imprimefondhebdoSansEmployes;
procedure imprimefondhebdoPage2(nbEmpoye: integer; nbInterim: integer);
procedure imprimeentete;
procedure ImprimePointage(nblig: integer);
procedure ImprimeVehicule(nblig: integer);
procedure ImprimeStructure(nblig: integer);
procedure ImprimeMateriel(nblig: integer);
procedure ImprimeAchat(nblig: integer);
procedure ImprimeDiamant(nblig: integer);
procedure ImprimeExterne(nblig: integer);
procedure ImprimeTotaux;
procedure ecritMessageEmployes;
procedure ecritMessageInterims;

procedure imprimeenteteETAT2(Bis: boolean; NumS, ChoixF, filtre: integer;
  nom: string);
procedure imprimeenteteETAT3(NMois, ChoixF, filtre: integer; nom: string);
procedure imprimeenteteETAT4(ChoixF, filtre: integer; nom: string);

procedure tracefondETAT5;
procedure tracefondETAT6;
procedure traceenteteETAT5;
procedure traceenteteETAT5regul;
procedure traceenteteETAT6;
procedure traceenteteETAT5remplir(sem, titre, mois: string);
procedure traceenteteETAT6remplir(titre, mois: string);
procedure TraceTotauxETAT5(t: array of currency); // TextWidth
procedure TraceTotauxETAT6(t: array of currency); // TextWidth
procedure TracePourcentETAT5(t: array of currency);
procedure TracePourcentETAT6(t: array of currency);
procedure TraceValeursETAT5(np: integer; nj, p1, p2, p3, pr, pt: currency);
procedure TraceValeursETAT6(nj: integer; p1, p2, p3, pr, pt: currency);
procedure TraceLignereportETAT5;
procedure TraceLigneEncadreETAT5(lig: integer);
procedure TraceReportETAT5(totaux: array of currency);
procedure TraceLignenomETAT5(lig: integer; nom: string);
procedure TraceLigneETAT5(col, lig: integer; num, nom: string;
  t: array of currency);
procedure TraceLigneETAT6(lig: integer; num, nom: string; t: array of currency);

procedure TraceFinETAT5;
procedure tracefondETATRECAPCLI;
procedure traceenteteETATRECAPCLI;
procedure TraceValeursETATRECAPCLI(nj, p1, p2, p3, pr, pt: currency);
procedure TracePageETATRECAPCLI(np: integer);

procedure traceenteteETATRECAPCHA;
procedure traceenteteETAT8remplir(titre, mois: string);

procedure ImprimeDiamantCoeffON;
procedure ImprimeDiamantCoeffOFF;

procedure imprimeenteteETAT9(chantier: string);
procedure traceenteteCUMULCHANTIERANNEE(chantier: string);

const

  maxnet5 = 32; // 32
  maxnet6 = 16;
  maxligneet5 = (maxnet5 div 2) - 2;
  maxligneet6 = (maxnet6 div 2) - 2;

  coltitre0 = 0;
  coltitre1 = 69;
  coltitre2 = 120;
  coltitre3 = 160;

  coltxt0 = 25;
  coltxt1 = 95;
  coltxt2 = 146;
  coltxt3 = 180;

var

  nbligmat: integer = 21; // nb lignes des grilles
  nbligemp: integer = 13;
  nbligint: integer = 3;
  nbligext: integer = 11 + 4;
  nbligveh: integer = 2;
  nbligstr: integer = 3;
  nbligach: integer = 4;
  nbligdia: integer = 4;

  posXemp: integer = 5;
  posYemp: integer = 18;

  posXint: integer = 5;
  posYint: integer = 89;

implementation

uses

  windows, printers, Graphics, Classes, SysUtils,
  utils, accesBDD,
  winbase;

type

  Tligvertemp = array [0 .. 8] of integer;
  Ttitreemp = array [0 .. 9] of string;
  Tdectitemp = array [0 .. 9] of integer;

  Tligvertveh = array [0 .. 2] of integer;
  Ttitreveh = array [0 .. 3] of string;
  Tdectitveh = array [0 .. 3] of integer;

  Tligvertstr = array [0 .. 2] of integer;
  Ttitrestr = array [0 .. 3] of string;
  Tdectitstr = array [0 .. 3] of integer;

  Tligvertdia = array [0 .. 4] of integer;
  Ttitredia = array [0 .. 4] of string;
  Tdectitdia = array [0 .. 4] of integer;

  Tligvertach = array [0 .. 0] of integer;
  Ttitreach = array [0 .. 1] of string;
  Tdectitach = array [0 .. 1] of integer;

  Tligvertext = array [0 .. 0] of integer;
  Ttitreext = array [0 .. 1] of string;
  Tdectitext = array [0 .. 1] of integer;

  Tligvertmat = array [0 .. 2] of integer;
  Ttitremat = array [0 .. 3] of string;
  Tdectitmat = array [0 .. 3] of integer;

  Tligvertcal = array [0 .. 1] of integer;

  Tligvertet5 = array [0 .. 12] of integer;
  Ttitreet5 = array [0 .. 13] of string;
  Tdectitet5 = array [0 .. 13] of integer;

const

  posXent = 5;
  posYent = 1;

  ligvertemp: Tligvertemp = (60, 75, 90, 105, 120, 135, 150, 160, 170);
  titreemp: Ttitreemp = ('NOM', 'T/H', 'J TR', 'H RTE', 'H JR', 'H SAM',
    'H N+D', 'PP', 'GP', 'TOTAL');
  dectitemp: Tdectitemp = (26, 4, 4, 2, 3, 2, 1, 3, 3, 6);

  titreint: Ttitreemp = ('INTERIM', 'T/H', 'J TR', 'H RTE', 'H JR', 'H SAM',
    'H N+D', 'PP', 'GP', 'TOTAL');
  dectitint: Tdectitemp = (23, 4, 4, 2, 3, 2, 1, 3, 3, 6);

  ligvertveh: Tligvertveh = (60, 75, 90);
  titreveh: Ttitreveh = ('VEHICULE', 'Cout', 'Nbr', 'TOTAL');
  dectitveh: Tdectitveh = (22, 4, 5, 6);

  posXveh = 5;
  posYveh = 109;

  ligvertach: Tligvertach = (54);
  titreach: Ttitreach = ('ACHAT AGENCE', 'VALEUR');
  dectitach: Tdectitach = (16, 4);

  posXach = 121;
  posYach = 109;

  ligvertstr: Tligvertstr = (60, 75, 90);
  titrestr: Ttitrestr = ('FRAIS STRUCTURE', 'Cout', 'Nbr', 'TOTAL');
  dectitstr: Tdectitstr = (18, 4, 5, 6);

  posXstr = 5;
  posYstr = 125;

  ligvertext: Tligvertext = (54);
  titreext: Ttitreext = ('FRAIS EXTERNES', 'VALEUR');
  dectitext: Tdectitext = (16, 6);

  posXext = 121;
  posYext = 135;

  ligvertdia: Tligvertdia = (54, 67, 80, 93, 106);
  titredia: Ttitredia = ('DIAMANT', 'Cout', 'Nbr', 'Coeff', 'TOTAL');
  dectitdia: Tdectitdia = (24, 3, 3, 2, 5);

  posXdia = 5;
  posYdia = 146;

  ligvertmat: Tligvertmat = (60, 75, 90);
  titremat: Ttitremat = ('MATERIEL', 'Cout', 'Nbr', 'TOTAL');
  dectitmat: Tdectitmat = (22, 4, 5, 4);

  posXmat = 5;
  posYmat = 172;

  ligvertcal: Tligvertcal = (36, 61);

  posXcal = 121;
  posYcal = 197 + 20; // 198

  ligvertet5: Tligvertet5 = (13, 43, 63, 75, 93, 106, 125, 143, 164, 187, 208,
    228, 249);

  {
    titreet5: Ttitreet5 = ('CH', 'NOM CLIENT', 'CA SEM', 'JT', 'MO', 'JV',
    'VEHICULE', 'DIAMANT', 'MATERIEL', 'FRAIS EXT',
    'DEBOURSE', 'FRAIS STR', 'PRIX REV','RESULTAT');
  }

  titreet5: Ttitreet5 = ('CH', 'NOM CLIENT', 'CA SEM', 'JT', 'MO', 'JV',
    'VEHICULE', 'DIAMANT', 'MAT + OC', 'FRAIS EXT', 'DEBOURSE', 'FRAIS STR',
    'PRIX REV', 'RESULTAT');

  dectitet5: Tdectitet5 = (4, 5, 4, 4, 6, 4, 1, 2, 2, 3, 1, 1, 3, 4);

  posXet5 = 7;
  posYet5 = 19;
  larXet5 = 274;

  posXet6 = 7;
  posYet6 = 52;

  frmtmon = '0.00';
  frmtdec = '000.00';

var

  coef: real = 23.8;
  ImprimeDiamantCoeff: boolean;

  xpixelsparpouce: integer;
  ypixelsparpouce: integer;
  horizon: integer;
  vertical: integer;

  savcouleur: tcolor;

procedure InitImpression;
{ reglage destine a rendre possible l'usage de toute imprimante
  avec la variable coef
}
begin
  xpixelsparpouce := getdevicecaps(printer.handle, LOGPIXELSX);
  ypixelsparpouce := getdevicecaps(printer.handle, LOGPIXELSY);

  vertical := getdevicecaps(printer.handle, PHYSICALOFFSETY);
  horizon := getdevicecaps(printer.handle, PHYSICALOFFSETX);

  coef := trunc(1 / (25.4 / xpixelsparpouce)) * 1.016;
end;

function mm2pX(mm: integer): integer;
begin
  result := trunc(mm / (25.4 / xpixelsparpouce)) - horizon;
end;

function mm2pY(mm: integer): integer;
begin
  result := trunc(mm / (25.4 / ypixelsparpouce)) - vertical;
end;

procedure ExempleImprimeTexte(txt: string; posx, posy: integer);
var
  x: integer;
  y: integer;
begin
  x := mm2pX(posx);
  y := mm2pY(posy);
  printer.Canvas.TextOut(x, y, txt);
end;

procedure setfondtexte;
begin
  printer.Canvas.Brush.Color := clWhite;
end;

procedure setfondtitre;
begin
  printer.Canvas.Brush.Color := clLtGray;
end;

function FormatTotal(valeur: currency): string;
var
  temp: string;
  n: integer;
begin
  temp := FormatCurr(' ##,###,##0.00', valeur);
  n := length(temp);
  if n < 13 then
    n := 13 - n
  else
    n := 0;

  result := FillSpaces(n) + temp;
end;

function Largeurtexte(txt: string): real;
begin
  result := printer.Canvas.TextWidth(txt) / coef;
end;

procedure ImprimeTexte(txt: string; posx, posy: integer);
begin
  printer.Canvas.TextOut(trunc(posx * coef), trunc(posy * coef), txt);
end;

procedure traceligne(xd, yd, xf, yf: integer);
begin
  printer.Canvas.MoveTo(trunc(xd * coef), trunc(yd * coef));
  printer.Canvas.LineTo(trunc(xf * coef), trunc(yf * coef));
end;

procedure fondtitre(x, y, w: integer);
var
  xr, yr, xt, yt: integer;
begin
  setfondtitre;
  xr := trunc(x * coef);
  yr := trunc(y * coef);
  xt := trunc((x + w) * coef);
  yt := trunc((y + 5) * coef);
  printer.Canvas.FillRect(Rect(xr, yr, xt, yt));
end;

procedure fondtitre5(x, y, w: integer);
var
  xr, yr, xt, yt: integer;
begin
  setfondtitre;
  xr := trunc(x * coef);
  yr := trunc(y * coef);
  xt := trunc((x + w) * coef);
  yt := trunc((y + 10) * coef);
  printer.Canvas.FillRect(Rect(xr, yr, xt, yt))
end;

procedure tracetexte(c, l: integer; txt: string);
begin
  ImprimeTexte(txt, posXent + 2 + c, posYent + 2 + (l * 5))
end;

procedure settracefin;
begin
  printer.Canvas.Font.Style := []
end;

procedure settracefinitalique;
begin
  printer.Canvas.Font.Style := [fsItalic]
end;

procedure settracegras;
begin
  printer.Canvas.Font.Style := [fsBold]
end;

procedure setcouleurecrituretexte;
begin
  printer.Canvas.Font.Color := clBlack
end;

procedure setcouleurtexte(couleur: integer);
begin
  printer.Canvas.Font.Color := clBlack;
  case couleur of
    1:
      printer.Canvas.Font.Color := clMaroon;
    2:
      printer.Canvas.Font.Color := clGreen;
    3:
      printer.Canvas.Font.Color := clOlive;
    4:
      printer.Canvas.Font.Color := clNavy;
    5:
      printer.Canvas.Font.Color := clPurple;
    6:
      printer.Canvas.Font.Color := clTeal;
    7:
      printer.Canvas.Font.Color := clGray;
    8:
      printer.Canvas.Font.Color := clDkGray;
    9:
      printer.Canvas.Font.Color := clRed;
    10:
      printer.Canvas.Font.Color := clLime;
    11:
      printer.Canvas.Font.Color := clYellow;
    12:
      printer.Canvas.Font.Color := clBlue;
    13:
      printer.Canvas.Font.Color := clFuchsia;
    14:
      printer.Canvas.Font.Color := clAqua;
    15:
      printer.Canvas.Font.Color := clWhite;
  end;
end;

procedure setcouleurtexterouge;
begin
  setcouleurtexte(9);
end;

procedure setcouleurtextenoir;
begin
  setcouleurtexte(0);
end;

procedure restituecouleurtexte;
begin
  printer.Canvas.Font.Color := savcouleur;
end;

function testcouleurnegatif(valeur: currency): boolean;
begin
  result := false;
  if valeur < 0 then
  begin
    savcouleur := printer.Canvas.Font.Color;
    setcouleurtexterouge;
    result := true
  end;
end;

procedure settracetitrecalcul;
begin
  settracegras;
  printer.Canvas.Brush.Color := clWhite
end;

procedure settracegrandetaille;
begin
  printer.Canvas.Font.Size := 12;
end;

procedure settracemoyennetaille;
begin
  printer.Canvas.Font.Size := 10;
end;

procedure settracepetitetaille;
begin
  printer.Canvas.Font.Size := 9;
end;

procedure settracegrille;
begin
  settracegras;
  setcouleurecrituretexte;
  settracepetitetaille;
end;

{ ----------------------------------------------------------------------------- }
procedure cadre(x, y, w, h: integer);
begin
  traceligne(x, y, x + w, y);
  traceligne(x + w, y, x + w, y + h);
  traceligne(x + w, y + h, x, y + h);
  traceligne(x, y + h, x, y);
end;

procedure grille(x, y, w, h: integer; nlh, nbc: integer;
  ligver: array of integer);
var
  i: integer;
begin
  // trace du cadre de la grille employ�s
  cadre(x, y, w, (nlh * 5));

  // trace des lignes verticales
  setcouleurecrituretexte;
  for i := 0 to nbc do
  begin
    traceligne(x + ligver[i], y, x + ligver[i], y + (nlh * 5));
  end;

  // trace les lignes horizontales
  for i := 1 to nlh do
    traceligne(x, y + (i * 5), x + w, y + (i * 5));
end;

procedure titre(posx, posy, nbt: integer; titres: array of string;
  dep: array of integer; ligv: array of integer);
var
  i: integer;
  x: integer;
begin
  setfondtitre;
  ImprimeTexte(titres[0], posx + dep[0], posy + 1);
  for i := 1 to nbt do
  begin
    x := posx + ligv[i - 1] + dep[i];
    ImprimeTexte(titres[i], x, posy + 1);
  end;
end;

{ ----------------------------------------------------------------------------- }
procedure tracegrilleemploye(nbEmpoye: integer; nbInterim: integer);

begin
  settracegrille;

  fondtitre(posXemp, posYemp, 194);

  // trace de la grille employ�s
  grille(posXemp, posYemp, 194, 60, nbEmpoye + 1, 8, ligvertemp);

  // trace le titre de la grille
  titre(posXemp, posYemp, 9, titreemp, dectitemp, ligvertemp);
end;

function FormatCellule(txt: string; tableau: array of integer;
  col, colmax, larg: integer): integer;
var
  lt: real;
  lc: integer;
begin
  lt := Largeurtexte(txt);
  if col = 0 then
    lc := tableau[0]
  else if col = colmax then
    lc := larg - tableau[col - 2]
  else
    lc := tableau[col - 1] - tableau[col - 2];

  result := tableau[col - 2] + lc - trunc(lt) - 2;
end;

procedure celluleemp(c, l: integer; txt: string);
var
  p: integer;
begin
  p := FormatCellule(txt, ligvertemp, c, 10, 194);

  settracefin;
  if c = 1 then
    ImprimeTexte(' ' + txt, posXemp + 2, posYemp + 1 + (l * 5))
  else
    ImprimeTexte(txt, posXemp + p, posYemp + 1 + (l * 5));
end;

{ ----------------------------------------------------------------------------- }
procedure tracegrilleinterim(nbEmpoye: integer; nbInterim: integer);
begin
  settracegrille;

  fondtitre(posXint, posYint, 194);

  // trace de la grille employ�s
  grille(posXint, posYint, 194, 18, nbInterim + 1, 8, ligvertemp);

  // trace le titre de la grille
  titre(posXint, posYint, 9, titreint, dectitint, ligvertemp);
end;

procedure celluleint(c, l: integer; txt: string);
var
  p: integer;
begin
  p := FormatCellule(txt, ligvertemp, c, 10, 194);

  settracefin;
  if c = 1 then
    ImprimeTexte(txt, posXint + 2, posYint + 1 + (l * 5))
  else
    ImprimeTexte(txt, posXint + p, posYint + 1 + (l * 5));
end;

{ ----------------------------------------------------------------------------- }
procedure tracegrillevehicule;
begin
  settracegrille;

  fondtitre(posXveh, posYveh, 114);

  // trace de la grille employ�s
  grille(posXveh, posYveh, 114, 18, nbligveh + 1, 2, ligvertveh);

  // trace le titre de la grille
  titre(posXveh, posYveh, 3, titreveh, dectitveh, ligvertveh);
end;

procedure celluleveh(c, l: integer; txt: string);
var
  p: integer;
begin
  p := FormatCellule(txt, ligvertveh, c, 4, 114);

  settracefin;
  if c = 1 then
    ImprimeTexte(txt, posXveh + 2, posYveh + 1 + (l * 5))
  else
    ImprimeTexte(txt, posXveh + p, posYveh + 1 + (l * 5));
end;

{ ----------------------------------------------------------------------------- }
procedure tracegrillestructure;
begin
  settracegrille;

  fondtitre(posXstr, posYstr, 114);

  // trace de la grille employ�s
  grille(posXstr, posYstr, 114, 18, nbligstr + 1, 2, ligvertstr); // 107

  // trace le titre de la grille
  titre(posXstr, posYstr, 3, titrestr, dectitstr, ligvertstr);
end;

procedure cellulestr(c, l: integer; txt: string);
var
  p: integer;
begin
  p := FormatCellule(txt, ligvertstr, c, 4, 114);

  settracefin;
  if c = 1 then
    ImprimeTexte(txt, posXstr + 2, posYstr + 1 + (l * 5))
  else
    ImprimeTexte(txt, posXstr + p, posYstr + 1 + (l * 5));
end;

{ ----------------------------------------------------------------------------- }
procedure tracegrilleachat;
begin
  settracegrille;

  fondtitre(posXach, posYach, 78);

  // trace de la grille achat
  grille(posXach, posYach, 78, 18, nbligach + 1, 0, ligvertach);

  // trace le titre de la grille
  titre(posXach, posYach, 1, titreach, dectitach, ligvertach);
end;

procedure celluleach(c, l: integer; txt: string);
var
  p: integer;
begin
  p := FormatCellule(txt, ligvertach, c, 2, 78);

  settracefin;
  if c = 1 then
    ImprimeTexte(txt, posXach + 2, posYach + 1 + (l * 5))
  else
    ImprimeTexte(txt, posXach + p, posYach + 1 + (l * 5));
end;

{ ----------------------------------------------------------------------------- }
procedure tracegrilleexterne;
begin
  settracegrille;

  fondtitre(posXext, posYext, 78);

  // trace de la grille employ�s
  grille(posXext, posYext, 78, 18, nbligext + 1, 0, ligvertext);

  // trace le titre de la grille
  titre(posXext, posYext, 1, titreext, dectitext, ligvertext);
end;

procedure celluleext(c, l: integer; txt: string);
var
  p: integer;
begin
  p := FormatCellule(txt, ligvertext, c, 2, 78);

  settracefin;
  if c = 1 then
    ImprimeTexte(txt, posXext + 2, posYext + 1 + (l * 5))
  else
    ImprimeTexte(txt, posXext + p, posYext + 1 + (l * 5));
end;

{ ----------------------------------------------------------------------------- }
procedure tracegrillediamant;
begin
  settracegrille;

  fondtitre(posXdia, posYdia, 114);

  // trace de la grille employ�s
  grille(posXdia, posYdia, 114, 18, nbligdia + 1, 3, ligvertdia);

  // trace le titre de la grille
  titre(posXdia, posYdia, 4, titredia, dectitdia, ligvertdia);
end;

procedure celluledia(c, l: integer; txt: string);
var
  p: integer;
begin
  p := FormatCellule(txt, ligvertdia, c, 5, 114);

  settracefin;
  if c = 1 then
    ImprimeTexte(txt, posXdia + 2, posYdia + 1 + (l * 5))
  else
    ImprimeTexte(txt, posXdia + p, posYdia + 1 + (l * 5));
end;

{ ----------------------------------------------------------------------------- }
procedure tracegrillemateriel;
begin
  settracegrille;

  fondtitre(posXmat, posYmat, 114); // 108

  // trace de la grille employ�s
  grille(posXmat, posYmat, 114, 18, nbligmat + 1, 2, ligvertmat);

  // trace le titre de la grille
  titre(posXmat, posYmat, 3, titremat, dectitmat, ligvertmat);
end;

procedure cellulemat(c, l: integer; txt: string);
var
  p: integer;
begin
  p := FormatCellule(txt, ligvertmat, c, 4, 114);

  settracefin;
  if c = 1 then
    ImprimeTexte(txt, posXmat + 2, posYmat + 1 + (l * 5))
  else
    ImprimeTexte(txt, posXmat + p, posYmat + 1 + (l * 5));
end;

{ ----------------------------------------------------------------------------- }
procedure cellulecal(c, l: integer; txt: string);
var
  p: integer;
begin
  p := FormatCellule(txt, ligvertcal, c, 3, 78);

  settracefin;

  if l = 12 then
    setfondtitre;

  if c = 1 then
    ImprimeTexte(txt, posXcal + 2, posYcal + 1 + (l * 5))
  else
    ImprimeTexte(txt, posXcal + p, posYcal + 1 + (l * 5));

  if l = 12 then
    setfondtexte;
end;

procedure cellulecaltitre(l: integer; txt: string);
var
  x: integer;
  y: integer;
begin
  x := posXcal + 1;
  y := posYcal + 1 + (l * 5);

  if l = 12 then
  begin
    // trace fond resultat
    setfondtitre;
    ImprimeTexte(txt, x + 1, y);
    setfondtexte;
  end
  else
    ImprimeTexte(txt, x + 1, y)
end;

procedure tracegrillecalculs;
begin
  settracegrille;

  setfondtitre;
  fondtitre(posXcal, posYcal + (12 * 5), 78);
  setfondtexte;

  settracetitrecalcul;
  cellulecaltitre(0, 'C.A. SEMAINE');
  cellulecaltitre(1, 'SALARIE');
  cellulecaltitre(2, 'INTERIM');
  cellulecaltitre(3, 'VEHICULE');
  cellulecaltitre(4, 'DIAMANT');
  cellulecaltitre(5, 'MATERIEL');
  cellulecaltitre(6, 'OUTILLAGE+CONSO');
  cellulecaltitre(7, 'FRAIS EXTERNES');
  cellulecaltitre(8, 'ACHATS TRX INTER');
  cellulecaltitre(9, 'DEBOURSE SEC');
  cellulecaltitre(10, 'FRAIS STRUC');
  cellulecaltitre(11, 'PRIX DE REVIENT');
  cellulecaltitre(12, 'RESULTAT');

  // trace de la grille employ�s
  grille(posXcal, posYcal, 78, 18, 13, 1, ligvertcal);

end;

procedure imprimefondhebdo(nbEmpoye: integer; nbInterim: integer);
begin
  // impression cadre grille et titre
  setcouleurecrituretexte;
  tracegrilleemploye(nbEmpoye, nbInterim);
  tracegrilleinterim(nbEmpoye, nbInterim);
  tracegrillevehicule;
  tracegrilleachat;
  tracegrilleexterne;
  tracegrillestructure;
  tracegrillediamant;
  tracegrillemateriel;
  tracegrillecalculs;
end;

procedure imprimefondhebdoSansEmployes;
begin
  // impression cadre grille et titre
  setcouleurecrituretexte;

  tracegrilleemploye(0, 0);
  tracegrilleinterim(0, 0);

  ecritMessageEmployes;
  ecritMessageInterims;

  tracegrillevehicule;
  tracegrilleachat;
  tracegrilleexterne;
  tracegrillestructure;
  tracegrillediamant;
  tracegrillemateriel;
  tracegrillecalculs;
end;

procedure imprimefondhebdoPage2(nbEmpoye: integer; nbInterim: integer);

begin
  // impression cadre grille et titre

  if (nbEmpoye > nbligemp) then
    posYint := posYint + ((nbEmpoye - nbligemp) * 5);

  tracegrilleemploye(nbEmpoye, nbInterim);
  tracegrilleinterim(nbEmpoye, nbInterim);

  setcouleurecrituretexte;
  setfondtexte;

end;

procedure traceenteteHEBDO;
var
  t: string;
begin
  settracegras;
  tracetexte(coltitre0, 0, 'SECTEUR : ');
  tracetexte(coltitre0, 1, 'COMMERCIAL : ');
  tracetexte(coltitre0, 2, 'CLIENT : ');
  tracetexte(coltitre1, 1, 'CHANTIER : ');
  tracetexte(coltitre1, 0, 'N� CHANTIER : ');
  tracetexte(coltitre1, 2, 'CA SEMAINE  : ');
  tracetexte(coltitre2, 0, 'SEMAINE : ');
  tracetexte(coltitre3, 0, 'ANNEE : ');

  settracefin;
  t := inttostr(AnneeCourante);
  tracetexte(coltxt3, 0, t);
  tracetexte(coltitre3, 1, DateDuJour);
end;

procedure traceenteteREGUL;
var
  t: string;
begin
  settracegras;
  tracetexte(coltitre0, 0, 'SECTEUR : ');
  tracetexte(coltitre0, 1, 'COMMERCIAL : ');
  tracetexte(coltitre0, 2, 'CLIENT : ');
  tracetexte(coltitre1, 1, 'CHANTIER : ');
  tracetexte(coltitre1, 0, 'N� CHANTIER : ');
  tracetexte(coltitre1, 2, 'CA MOIS : ');
  tracetexte(coltitre2, 0, 'REGUL MOIS : ');
  tracetexte(coltitre3, 0, 'ANNEE : ');

  settracefin;
  t := inttostr(AnneeCourante);
  tracetexte(coltxt3, 0, t);
  tracetexte(coltitre3, 1, DateDuJour);
end;

procedure traceenteteHSECTEUR(txt: string);
begin
  settracefin;
  tracetexte(coltxt0, 0, txt);
end;

procedure traceenteteHCOMMERCIAL(txt: string);
begin
  settracefin;
  tracetexte(coltxt0, 1, txt);
end;

procedure traceenteteHCLIENT(txt: string);
begin
  settracefin;
  tracetexte(coltxt0, 2, txt);
end;

procedure traceenteteHCHANTIER(txt: string);
begin
  settracefin;
  settracegras;
  tracetexte(coltxt1, 0, txt);
end;

procedure traceenteteHCHANTIERNOM(txt: string);
begin
  settracefin;
  tracetexte(coltxt1, 1, txt); // coltxt0
end;

procedure traceenteteHCHANTIERVILLE(txt: string);
begin
  settracefin;
end;

procedure traceenteteHCA(txt: string);
begin
  settracefin;
  tracetexte(coltxt1, 2, txt);
end;

procedure traceenteteHSEMAINE(txt: string);
begin
  settracefin;
  tracetexte(coltxt2, 0, txt);
end;

procedure traceenteteRMOIS(txt: string);
begin
  settracefin;
  tracetexte(coltxt2, 0, txt);
end;

procedure imprimeentete;
begin

  settracegrille;

  if TableauTemp[0, 0] = 'Regularisation' then
  begin
    traceenteteREGUL;

    traceenteteRMOIS(TableauTemp[2, 0]);
  end
  else
  begin
    traceenteteHEBDO;

    // numerosemaine   +  semainebis
    traceenteteHSEMAINE(TableauTemp[0, 0] + ' ' + TableauTemp[1, 0]);
  end;

  // nom agence
  traceenteteHSECTEUR(TableauTemp[9, 0]);

  // libelle chantier
  traceenteteHCHANTIER(TableauTemp[4, 0]);

  // nom client
  traceenteteHCLIENT(TableauTemp[7, 0]);

  // chiffre d'affaire
  traceenteteHCA(FormatTotal(TableauValHebdo[1]));

  // nom commercial
  traceenteteHCOMMERCIAL(TableauTemp[8, 0]);

  // nom chantier
  traceenteteHCHANTIERNOM(TableauTemp[5, 0]);
end;

procedure traceenteteCUMULANNEE;
var
  t: string;
begin
  settracegras;
  tracetexte(coltitre0, 0, 'CUMUL ANNEE');
  tracetexte(coltitre0, 2, 'CHIFFRE AFFAIRE');
  tracetexte(coltitre3, 0, 'ANNEE');

  settracefin;
  tracetexte(coltitre3, 2, DateDuJour);
  t := inttostr(AnneeCourante);
  tracetexte(coltxt3, 0, t);
end;

procedure traceenteteCUMULMOIS;
var
  t: string;
begin
  settracegras;
  tracetexte(coltitre0, 0, 'CUMUL MOIS');
  tracetexte(coltitre0, 2, 'CHIFFRE AFFAIRE');
  tracetexte(coltitre3, 0, 'ANNEE');

  settracefin;
  tracetexte(coltitre3, 2, DateDuJour);
  t := inttostr(AnneeCourante);
  tracetexte(coltxt3, 0, t);
end;

procedure traceenteteCUMULSEMAINE;
var
  t: string;
begin
  settracegras;
  tracetexte(coltitre0, 0, 'CUMUL SEMAINE');
  tracetexte(coltitre0, 2, 'CHIFFRE AFFAIRE');
  tracetexte(coltitre3, 0, 'ANNEE');

  settracefin;
  tracetexte(coltitre3, 2, DateDuJour);
  t := inttostr(AnneeCourante);
  tracetexte(coltxt3, 0, t);
end;

procedure traceenteteCUMULNUM(txt: string);
begin
  settracefin;
  tracetexte(40, 0, txt);
end;

procedure traceenteteCUMULTYPE(txt, nom: string);
begin
  settracefin;
  settracemoyennetaille;
  tracetexte(coltitre1, 0, txt);
  tracetexte(coltxt1, 0, nom);
  settracepetitetaille;
end;

procedure traceenteteCUMULCHANTIERANNEE(chantier: string);
begin
  settracegras;
  settracemoyennetaille;
  tracetexte(coltitre1, 0, 'CHANTIER : ');
  tracetexte(coltxt1, 0, chantier);
  settracepetitetaille;
  settracefin;
end;

procedure traceenteteCUMULCA;
var
  c: integer;
  l: integer;
  txt: string;
begin
  txt := FormatTotal(TableauValHebdo[1]);

  settracegras;
  settracegrandetaille;

  l := posYent + 2 + (2 * 5) - 3;

  c := coltitre0;
  ImprimeTexte('CHIFFRE D''AFFAIRES', posXent + 2 + c, l);

  c := 45;
  ImprimeTexte(txt, posXent + 2 + c, l)
end;

procedure traceenteteANNEE;
var
  t: string;
  c: integer;
  l: integer;
begin
  settracefin;
  settracemoyennetaille;
  tracetexte(coltitre3, 0, 'ANNEE');

  c := coltitre3;
  l := posYent + 2 + (2 * 5) - 3;
  ImprimeTexte(DateDuJour, posXent + 2 + c + 1, l);

  t := inttostr(AnneeCourante);
  tracetexte(coltxt3, 0, t);
end;

procedure ecritMessageEmployes;
begin
  setfondtexte;
  setcouleurecrituretexte;
  settracegras;
  ImprimeTexte('Voir d�tail en page 2', posXemp + 5, posYemp + 10);
end;

procedure ecritMessageInterims;
begin
  setfondtexte;
  setcouleurecrituretexte;
  settracegras;
  ImprimeTexte('Voir d�tail en page 2', posXint + 5, posYint + 10);
end;

procedure imprimeenteteETAT2(Bis: boolean; NumS, ChoixF, filtre: integer;
  nom: string);
var
  t: string;
begin
  settracefin;
  settracemoyennetaille;
  tracetexte(coltitre0, 0, 'CUMUL SEMAINE');

  // imprime le chiffre d'affaire
  traceenteteCUMULCA;

  traceenteteANNEE;

  // remplit le numero de semaine
  str(NumS, t);
  if NumS < 10 then
    t := '0' + t;

  // remplit le type de semaine
  if Bis then
    t := t + ' bis';

  // imprime la semaine et le type
  settracegras;
  settracegrandetaille;
  tracetexte(40, 0, t);

  t := '';
  case ChoixF of
    1:
      t := 'Agence :';
    2:
      t := 'Commercial :';
    3:
      t := 'Client :';
  end;
  traceenteteCUMULTYPE(t, nom);
end;

procedure imprimeenteteETAT3(NMois, ChoixF, filtre: integer; nom: string);
var
  t: string;
begin
  // imprime l'entete des cumuls
  settracefin;
  settracemoyennetaille;
  tracetexte(coltitre0, 0, 'CUMUL MOIS');

  // imprime le chiffre d'affaire
  traceenteteCUMULCA;

  traceenteteANNEE;

  // remplit le numero de mois
  t := NumMois(NMois);

  // imprime le mois
  settracegras;
  settracegrandetaille;
  tracetexte(40, 0, t);

  t := '';
  case ChoixF of
    1:
      t := 'Agence :';
    2:
      t := 'Commercial :';
    3:
      t := 'Client :';
  end;
  traceenteteCUMULTYPE(t, nom);
end;

procedure imprimeenteteETAT4(ChoixF, filtre: integer; nom: string);
var
  t: string;
begin
  // imprime l'entete des cumuls
  settracefin;
  settracemoyennetaille;
  tracetexte(coltitre0, 0, 'CUMUL ANNEE');

  // imprime le chiffre d'affaire
  traceenteteCUMULCA;

  traceenteteANNEE;

  t := '';
  case ChoixF of
    1:
      t := 'Agence :';
    2:
      t := 'Commercial :';
    3:
      t := 'Client :';
  end;
  traceenteteCUMULTYPE(t, nom);
end;

procedure imprimeenteteETAT9(chantier: string);
begin
  // imprime l'entete des cumuls
  settracefin;
  settracemoyennetaille;
  tracetexte(coltitre0, 0, 'CUMUL ANNEE');

  // imprime le chiffre d'affaire
  traceenteteCUMULCA;

  traceenteteANNEE;

  traceenteteCUMULCHANTIERANNEE(chantier);
  // traceenteteCUMULTYPE(t, chantier);
end;

procedure ImprimePointage(nblig: integer);
var
  l: integer;
  le: integer;
  li: integer;
  c: integer;
  t: string;
begin
  le := 0;
  li := 0;
  for l := 0 to (nblig - 1) do
  begin
    t := TableauTemp[0, l];
    if t = '' then
    begin
      celluleemp(1, le + 1, TableauTemp[1, l]);

      for c := 3 to 10 do
        celluleemp(c, le + 1, TableauTemp[c, l]);

      inc(le);
    end
    else
    begin
      celluleint(1, li + 1, TableauTemp[1, l]);

      for c := 3 to 10 do
        celluleint(c, li + 1, TableauTemp[c, l]);

      inc(li);
    end;
  end;
end;

procedure ImprimeVehicule(nblig: integer);
var
  l: integer;
  c: integer;
begin
  for l := 0 to (nblig - 1) do
  begin
    for c := 1 to 4 do
      celluleveh(c, l + 1, TableauTemp[c, l])
  end;
end;

procedure ImprimeStructure(nblig: integer);
var
  l: integer;
  c: integer;
begin
  for l := 0 to (nblig - 1) do
  begin
    for c := 1 to 4 do
      cellulestr(c, l + 1, TableauTemp[c, l])
  end;
end;

procedure ImprimeMateriel(nblig: integer);
var
  l: integer;
  c: integer;
begin
  for l := 0 to (nblig - 1) do
  begin
    for c := 1 to 4 do
      cellulemat(c, l + 1, TableauTemp[c, l])
  end;
end;

procedure ImprimeAchat(nblig: integer);
var
  l: integer;
  c: integer;
begin
  for l := 0 to (nblig - 1) do
  begin
    for c := 1 to 2 do
      celluleach(c, l + 1, TableauTemp[c, l])
  end;
end;

procedure ImprimeDiamantCoeffON;
begin
  ImprimeDiamantCoeff := true;
end;

procedure ImprimeDiamantCoeffOFF;
begin
  ImprimeDiamantCoeff := false;
end;

procedure ImprimeDiamant(nblig: integer);
var
  l: integer;
begin
  for l := 0 to (nblig - 1) do
  begin
    celluledia(1, l + 1, TableauTemp[1, l]);
    celluledia(2, l + 1, TableauTemp[2, l]);
    celluledia(3, l + 1, TableauTemp[3, l]);
    if ImprimeDiamantCoeff then
      celluledia(4, l + 1, TableauTemp[4, l]);
    celluledia(5, l + 1, TableauTemp[5, l]);
  end;
end;

procedure ImprimeExterne(nblig: integer);
var
  l: integer;
  c: integer;
begin
  for l := 0 to (nblig - 1) do
  begin
    for c := 1 to 2 do
      celluleext(c, l + 2, TableauTemp[c, l])
  end;
end;

procedure ImprimeTotaux;

  function FormatInduits(valeur: currency): string;
  var
    temp: string;
    n: integer;
  begin
    temp := FormatCurr('0.00', valeur);
    n := length(temp);
    if n < 8 then
      n := 8 - n
    else
      n := 0;

    result := FillSpaces(n) + temp;
  end;

  procedure AfficheTotal(l: integer; valeur: currency);
  var
    temp: string;
    c: currency;
  begin
    // trace le total
    temp := FormatTotal(valeur);
    cellulecal(2, l - 1, temp);

    temp := ' XXX %';
    if TableauValHebdo[1] <> 0 then
    begin
      c := (valeur / TableauValHebdo[1]) * 100;
      temp := FormatCurr('0.00', c) + ' %';
    end;

    // remplir le pourcentage
    cellulecal(2 + 1, l - 1, temp)
  end;

  procedure cellulecalfinal(c, l: integer; txt: string);
  var
    p: integer;
  begin
    p := FormatCellule(txt, ligvertcal, c, 3, 78);

    if l = 12 then
      setfondtitre;

    if c = 1 then
      ImprimeTexte(txt, posXcal + 2, posYcal + 1 + (l * 5))
    else
      ImprimeTexte(txt, posXcal + p, posYcal + 1 + (l * 5));

    if l = 12 then
      setfondtexte;
  end;

  procedure AfficheTotalfinal(l: integer; valeur: currency);
  var
    temp: string;
    c: currency;
  begin
    // trace le total
    temp := FormatTotal(valeur);
    cellulecalfinal(2, l - 1, temp);

    temp := ' XXX %';
    if TableauValHebdo[1] <> 0 then
    begin
      c := (valeur / TableauValHebdo[1]) * 100;
      temp := FormatCurr('0.00', c) + ' %';
    end;

    // remplir le pourcentage
    cellulecalfinal(2 + 1, l - 1, temp)
  end;

begin
  celluleext(1, 1, 'Frais induits');
  celluleext(2, 1, FormatInduits(TableauValHebdo[0]));

  AfficheTotal(1, TableauValHebdo[1]);
  AfficheTotal(2, TableauValHebdo[2]);
  AfficheTotal(3, TableauValHebdo[3]);
  AfficheTotal(4, TableauValHebdo[4]);
  AfficheTotal(5, TableauValHebdo[5]);
  AfficheTotal(6, TableauValHebdo[6]);
  AfficheTotal(7, TableauValHebdo[7]);
  AfficheTotal(8, TableauValHebdo[8]);
  AfficheTotal(9, TableauValHebdo[9]);
  AfficheTotal(10, TableauValHebdo[10]);
  AfficheTotal(11, TableauValHebdo[11]);
  AfficheTotal(12, TableauValHebdo[12]);

  settracegras;
  AfficheTotalfinal(13, TableauValHebdo[13]);
  settracefin;

  setcouleurecrituretexte;
  setfondtexte;
end;

procedure grille5(x, y, w, h, nlh: integer);
var
  i: integer;
  xt: integer;
  yt: integer;
begin
  // trace du cadre de la grille employ�s
  cadre(x, y, w, (nlh * 5));

  // trace des lignes verticales
  setcouleurecrituretexte;
  yt := y + (nlh * 5) + 5;
  for i := 0 to 12 do
  begin
    xt := x + ligvertet5[i];
    traceligne(xt, y, xt, yt);
  end;

  // trace du cadre des %
  i := 0;
  cadre(x + ligvertet5[i], y + (nlh * 5), w - ligvertet5[i], (5));

  // trace du cadre des totaux
  i := 1;
  cadre(x + ligvertet5[i], y + ((nlh - 2) * 5), w - ligvertet5[i], 10);

  // fin cadre de titre
  i := 2;
  traceligne(x, y + (i * 5), x + w, y + (i * 5));

  // derniere ligne cadre
  i := nlh;
  traceligne(x, y + (i * 5), x + w, y + (i * 5));

  i := 1;
  xt := ligvertet5[i + 1] - ligvertet5[i];
  cadre(x + ligvertet5[i], y + ((nlh + 1) * 5), xt, 5);

  i := 5;
  xt := ligvertet5[i + 1] - ligvertet5[i];
  cadre(x + ligvertet5[i], y + ((nlh + 1) * 5), xt, 5);

  i := 7;
  xt := ligvertet5[i + 1] - ligvertet5[i];
  cadre(x + ligvertet5[i], y + ((nlh + 1) * 5), xt, 5);

  i := 10;
  xt := ligvertet5[i + 1] - ligvertet5[i];
  cadre(x + ligvertet5[i], y + ((nlh + 1) * 5), xt, 5);

  i := 11;
  xt := ligvertet5[i + 1] - ligvertet5[i];
  cadre(x + ligvertet5[i], y + ((nlh + 1) * 5), xt, 5);

  i := 12;
  xt := w - ligvertet5[i];
  cadre(x + ligvertet5[i], y + ((nlh + 1) * 5), xt, 5);

  settracetitrecalcul;
  setcouleurecrituretexte;
end;

procedure grilledeb5(x, y, w, h, nlh: integer);
var
  i: integer;
  xt: integer;
  yt: integer;
begin
  // trace du cadre de la grille employ�s
  cadre(x, y, w, (nlh * 5));

  // trace des lignes verticales
  setcouleurecrituretexte;
  yt := y + (nlh * 5) + 5 - 5;
  for i := 0 to 12 do
  begin
    xt := x + ligvertet5[i];
    traceligne(xt, y, xt, yt);
  end;

  // fin cadre de titre
  traceligne(x, y + (2 * 5), x + w, y + (2 * 5));

  settracetitrecalcul;
  setcouleurecrituretexte;
end;

procedure grillefin5(x, y, w, h, nlh: integer);
var
  i: integer;
  xt: integer;
  yt: integer;
begin
  // trace des lignes verticales
  setcouleurecrituretexte;
  yt := y + (nlh * 5);
  for i := 0 to 12 do
  begin
    xt := x + ligvertet5[i];
    traceligne(xt, yt, xt, yt + 5);
  end;

  // trace du cadre des %
  i := 0;
  cadre(x + ligvertet5[i], y + (nlh * 5), w - ligvertet5[i], (5));

  // trace du cadre des totaux
  i := 1;
  cadre(x + ligvertet5[i], y + ((nlh - 2) * 5), w - ligvertet5[i], 10);

  // fin cadre de titre
  traceligne(x, y + (2 * 5), x + w, y + (2 * 5));

  // derniere ligne cadre
  i := nlh;
  traceligne(x, y + (i * 5), x + w, y + (i * 5));

  i := 1;
  xt := ligvertet5[i + 1] - ligvertet5[i];
  cadre(x + ligvertet5[i], y + ((nlh + 1) * 5), xt, 5);

  i := 5;
  xt := ligvertet5[i + 1] - ligvertet5[i];
  cadre(x + ligvertet5[i], y + ((nlh + 1) * 5), xt, 5);

  i := 7;
  xt := ligvertet5[i + 1] - ligvertet5[i];
  cadre(x + ligvertet5[i], y + ((nlh + 1) * 5), xt, 5);

  i := 10;
  xt := ligvertet5[i + 1] - ligvertet5[i];
  cadre(x + ligvertet5[i], y + ((nlh + 1) * 5), xt, 5);

  i := 11;
  xt := ligvertet5[i + 1] - ligvertet5[i];
  cadre(x + ligvertet5[i], y + ((nlh + 1) * 5), xt, 5);

  i := 12;
  xt := w - ligvertet5[i];
  cadre(x + ligvertet5[i], y + ((nlh + 1) * 5), xt, 5);

  settracetitrecalcul;
  setcouleurecrituretexte;
end;

procedure tracefondETAT5;
begin
  // mise en couleur noir
  setcouleurecrituretexte;

  // gras taille entete et noir
  settracegrille;

  // dessine fond du titre
  fondtitre5(posXet5, posYet5, larXet5);

  // trace de la grille de fond
  grille5(posXet5, posYet5, larXet5, (maxnet5 * 5) - 15, maxnet5);

  // trace le titre de la grille
  titre(posXet5, posYet5 + 2, 13, titreet5, dectitet5, ligvertet5);
end;

procedure tracefondETATRECAPCLI;
begin
  // mise en couleur noir
  setcouleurecrituretexte;

  // gras taille entete et noir
  settracegrille;

  // dessine fond du titre
  fondtitre5(posXet5, posYet5, larXet5);

  // trace de la grille de fond
  grilledeb5(posXet5, posYet5, larXet5, (maxnet5 * 5) - 15, maxnet5);

  // trace le titre de la grille
  titre(posXet5, posYet5 + 2, 13, titreet5, dectitet5, ligvertet5);
end;

procedure tracefondETAT6;
begin
  // mise en couleur noir
  setcouleurecrituretexte;

  // gras taille entete et noir
  settracegrille;

  // dessine fond du titre
  fondtitre5(posXet6, posYet6, larXet5);

  // trace de la grille
  grille5(posXet6, posYet6, larXet5, (maxnet6 * 5) - 15, maxnet6);

  // trace le titre de la grille
  titre(posXet6, posYet6 + 2, 13, titreet5, dectitet5, ligvertet5);
end;

procedure traceenteteETAT5;
var
  t: string;
begin
  // trace titre
  settracetitrecalcul;

  settracegrandetaille;
  settracegras;

  tracetexte(10, 0, 'SEMAINE : ');
  tracetexte(80, 0, 'RECAP SEMAINE : ');
  tracetexte(210, 0, 'MOIS : ');

  tracetexte(230, 2, DateDuJour);

  t := inttostr(AnneeCourante);
  tracetexte(257, 0, t);

  settracefin;
  settracepetitetaille;
end;

procedure traceenteteETAT5regul;
var
  t: string;
begin
  // trace titre
  settracetitrecalcul;

  settracegrandetaille;
  settracegras;

  tracetexte(10, 0, 'REGULARISATION');
  tracetexte(100, 0, 'RECAP : ');
  tracetexte(210, 0, 'MOIS : ');

  tracetexte(230, 2, DateDuJour);

  t := inttostr(AnneeCourante);
  tracetexte(257, 0, t);

  settracefin;
  settracepetitetaille;
end;

procedure traceenteteETAT5remplir(sem, titre, mois: string);
begin
  settracegrandetaille;
  settracegras;

  tracetexte(34, 0, sem);
  tracetexte(120, 0, titre);

  tracetexte(226, 0, mois);
  settracefin;
  settracepetitetaille;
end;

procedure traceenteteETAT6;
var
  t: string;
begin
  // trace titre
  printer.Canvas.Brush.Color := clWhite;

  settracegrandetaille;
  settracegras;

  tracetexte(80, 6, 'RECAP MENSUEL : ');
  tracetexte(210, 6, 'MOIS : ');

  t := inttostr(AnneeCourante);
  tracetexte(257, 6, t);

  tracetexte(230, 8, DateDuJour);

  settracefin;
  settracepetitetaille;
end;

procedure traceenteteETAT6remplir(titre, mois: string);
begin
  settracegrandetaille;
  settracegras;

  tracetexte(120, 6, titre);

  tracetexte(226, 6, mois);

  settracefin;
  settracepetitetaille;
end;

procedure traceenteteETATRECAPCHA;
var
  t: string;
begin
  // trace titre
  settracetitrecalcul;

  settracegrandetaille;
  settracegras;

  tracetexte(80, 0, 'RECAP CHANTIERS');
  tracetexte(210, 0, 'MOIS : ');

  tracetexte(230, 2, DateDuJour);

  t := inttostr(AnneeCourante);
  tracetexte(257, 0, t);

  settracefin;
  settracepetitetaille;
end;

procedure traceenteteETAT8remplir(titre, mois: string);
begin
  settracegrandetaille;
  settracegras;

  tracetexte(120, 0, titre);

  settracegrandetaille;
  tracetexte(226, 0, mois);

  settracefin;
  settracepetitetaille;
end;

procedure traceenteteETATRECAPCLI;
var
  t: string;
begin
  // trace titre
  settracetitrecalcul;

  settracegrandetaille;
  settracegras;

  tracetexte(80, 0, 'RECAP CLIENTS');

  tracetexte(230, 2, DateDuJour);

  t := inttostr(AnneeCourante);
  tracetexte(240, 0, t);

  settracefin;
  settracepetitetaille;
end;

function aligncolet5(valeur: currency; c: integer; var txt: string): integer;
var
  lt: real;
  lc: integer;
begin
  txt := FormatCurr('##,###,##0.00', valeur);
  lt := Largeurtexte(txt);

  if c = 1 then
    lc := ligvertet5[0]
  else if c = 14 then
    lc := larXet5 - ligvertet5[12]
  else
    lc := ligvertet5[c - 1] - ligvertet5[c - 2];

  result := lc - trunc(lt) - 2;
end;

procedure TraceFinETAT5;
begin
  // trace de la grille de fond
  grillefin5(posXet5, posYet5, larXet5, (maxnet5 * 5) - 15, maxnet5);
end;

procedure TraceTotauxETAT5(t: array of currency); // TextWidth
var
  x, y: integer;
  c: integer;
  n: integer;
  txt: string;
  ok: boolean;
begin
  settracegras;
  for c := 1 to 12 do
  begin

    n := aligncolet5(t[c - 1], c + 2, txt);

    x := posXet5 + ligvertet5[c] + n;
    y := posYent + (maxnet5 * 5) + 12;

    ok := testcouleurnegatif(t[c - 1]);
    ImprimeTexte(txt, x, y);
    if ok then
      restituecouleurtexte;
  end;
end;

procedure TraceTotauxETAT6(t: array of currency); // TextWidth
var
  x, y: integer;
  c: integer;
  n: integer;
  txt: string;
  ok: boolean;
begin
  settracegras;
  for c := 1 to 12 do
  begin

    n := aligncolet5(t[c - 1], c + 2, txt);

    x := posXet6 + ligvertet5[c] + n;
    y := posYet6 + (maxnet6 * 5) - 8;

    ok := testcouleurnegatif(t[c - 1]);
    ImprimeTexte(txt, x, y);
    if ok then
      restituecouleurtexte;
  end;
end;

procedure TracePourcentETAT5(t: array of currency);
var
  x, y: integer;
  c: integer;
  txt: string;
  n: integer;
  ok: boolean;
begin
  settracefin;
  for c := 1 to 12 do
  begin

    n := aligncolet5(t[c - 1], c + 2, txt);

    x := posXet5 + ligvertet5[c] + n - 3;
    y := posYent + (maxnet5 * 5) + 20;

    txt := txt + ' %';

    if (c <> 1) and (c <> 2) and (c <> 4) then
    begin
      ok := testcouleurnegatif(t[c - 1]);
      ImprimeTexte(txt, x, y);
      if ok then
        restituecouleurtexte;
    end;
  end;

  x := posXet6 + ligvertet5[1] - 17;
  ImprimeTexte('%', x, y);
end;

procedure TracePourcentETAT6(t: array of currency);
var
  x, y: integer;
  c: integer;
  txt: string;
  n: integer;
  ok: boolean;
begin
  settracefin;
  for c := 1 to 12 do
  begin

    n := aligncolet5(t[c - 1], c + 2, txt);

    x := posXet6 + ligvertet5[c] + n - 3;
    y := posYet6 + (maxnet6 * 5) + 1;

    txt := txt + ' %';

    if (c <> 1) and (c <> 2) and (c <> 4) then
    begin
      ok := testcouleurnegatif(t[c - 1]);
      ImprimeTexte(txt, x, y);
      if ok then
        restituecouleurtexte;
    end;
  end;

  x := posXet6 + ligvertet5[1] - 17;
  ImprimeTexte('%', x, y);

end;

procedure TraceValeursETAT5(np: integer; nj, p1, p2, p3, pr, pt: currency);
var
  x, y: integer;
  c: integer;
  txt: string;
  n: integer;
  ok: boolean;
begin
  settracefin;

  y := posYent + (maxnet5 * 5) + 25;

  x := posXet6 + ligvertet5[1] - 17;
  ImprimeTexte('NBj:', x, y);

  x := posXet5;
  c := 0;
  txt := 'Page ' + inttostr(np);
  ImprimeTexte(txt, x + ligvertet5[c] - 11, y);

  c := 1;
  n := aligncolet5(nj, c + 2, txt);
  ok := testcouleurnegatif(nj);
  ImprimeTexte(txt, x + ligvertet5[c] + n, y);
  if ok then
    restituecouleurtexte;

  c := 5;
  n := aligncolet5(p1, c + 2, txt);
  ok := testcouleurnegatif(p1);
  ImprimeTexte(txt, x + ligvertet5[c] + n, y);
  if ok then
    restituecouleurtexte;

  c := 7;
  n := aligncolet5(p2, c + 2, txt);
  ok := testcouleurnegatif(p2);
  ImprimeTexte(txt, x + ligvertet5[c] + n, y);
  if ok then
    restituecouleurtexte;

  c := 10;
  n := aligncolet5(p3, c + 2, txt);
  ok := testcouleurnegatif(p3);
  ImprimeTexte(txt, x + ligvertet5[c] + n, y);
  if ok then
    restituecouleurtexte;

  c := 11;
  n := aligncolet5(pr, c + 2, txt);
  ok := testcouleurnegatif(pr);
  ImprimeTexte(txt, x + ligvertet5[c] + n, y);
  if ok then
    restituecouleurtexte;

  c := 12;
  n := aligncolet5(pt, c + 2, txt);
  ok := testcouleurnegatif(pt);
  ImprimeTexte(txt, x + ligvertet5[c] + n, y);
  if ok then
    restituecouleurtexte;
end;

procedure TracePageETATRECAPCLI(np: integer);
var
  x, y: integer;
  c: integer;
  txt: string;
begin
  settracefin;

  x := posXet5;
  y := posYent + (maxnet5 * 5) + 25;
  c := 0;
  txt := 'Page ' + inttostr(np);
  ImprimeTexte(txt, x + ligvertet5[c] - 11, y);
end;

procedure TraceValeursETATRECAPCLI(nj, p1, p2, p3, pr, pt: currency);
var
  x, y: integer;
  c: integer;
  txt: string;
  n: integer;
  ok: boolean;
begin
  settracefin;

  y := posYent + (maxnet5 * 5) + 25;

  x := posXet6 + ligvertet5[1] - 17;
  ImprimeTexte('NBj:', x, y);

  x := posXet5;
  c := 1;
  n := aligncolet5(nj, c + 2, txt);
  ok := testcouleurnegatif(nj);
  ImprimeTexte(txt, x + ligvertet5[c] + n, y);
  if ok then
    restituecouleurtexte;

  c := 5;
  n := aligncolet5(p1, c + 2, txt);
  ok := testcouleurnegatif(p1);
  ImprimeTexte(txt, x + ligvertet5[c] + n, y);
  if ok then
    restituecouleurtexte;

  c := 7;
  n := aligncolet5(p2, c + 2, txt);
  ok := testcouleurnegatif(p2);
  ImprimeTexte(txt, x + ligvertet5[c] + n, y);
  if ok then
    restituecouleurtexte;

  c := 10;
  n := aligncolet5(p3, c + 2, txt);
  ok := testcouleurnegatif(p3);
  ImprimeTexte(txt, x + ligvertet5[c] + n, y);
  if ok then
    restituecouleurtexte;

  c := 11;
  n := aligncolet5(pr, c + 2, txt);
  ok := testcouleurnegatif(pr);
  ImprimeTexte(txt, x + ligvertet5[c] + n, y);
  if ok then
    restituecouleurtexte;

  c := 12;
  n := aligncolet5(pt, c + 2, txt);
  ok := testcouleurnegatif(pt);
  ImprimeTexte(txt, x + ligvertet5[c] + n, y);
  if ok then
    restituecouleurtexte;
end;

procedure TraceValeursETAT6(nj: integer; p1, p2, p3, pr, pt: currency);
var
  x, y: integer;
  c: integer;
  txt: string;
  n: integer;
  ok: boolean;
begin
  settracefin;

  y := posYet6 + (maxnet6 * 5) + 6;

  x := posXet6 + ligvertet5[1] - 17;
  ImprimeTexte('NBj:', x, y);

  c := 1;
  x := posXet6;
  n := aligncolet5(nj, c + 2, txt);
  ok := testcouleurnegatif(nj);
  ImprimeTexte(txt, x + ligvertet5[c] + n, y);
  if ok then
    restituecouleurtexte;

  c := 5;
  n := aligncolet5(p1, c + 2, txt);
  ok := testcouleurnegatif(p1);
  ImprimeTexte(txt, x + ligvertet5[c] + n, y);
  if ok then
    restituecouleurtexte;

  c := 7;
  n := aligncolet5(p2, c + 2, txt);
  ok := testcouleurnegatif(p2);
  ImprimeTexte(txt, x + ligvertet5[c] + n, y);
  if ok then
    restituecouleurtexte;

  c := 10;
  n := aligncolet5(p3, c + 2, txt);
  ok := testcouleurnegatif(p3);
  ImprimeTexte(txt, x + ligvertet5[c] + n, y);
  if ok then
    restituecouleurtexte;

  c := 11;
  n := aligncolet5(pr, c + 2, txt);
  ok := testcouleurnegatif(pr);
  ImprimeTexte(txt, x + ligvertet5[c] + n, y);
  if ok then
    restituecouleurtexte;

  c := 12;
  n := aligncolet5(pt, c + 2, txt);
  ok := testcouleurnegatif(pt);
  ImprimeTexte(txt, x + ligvertet5[c] + n, y);
  if ok then
    restituecouleurtexte;
end;

procedure TraceLigneETAT5(col, lig: integer; num, nom: string;
  t: array of currency);
var
  c: integer;
  x, y: integer;
  n: integer;
  txt: string;
  ok: boolean;
begin
  settracefin;

  setcouleurtexte(col);

  // trace nom ligne
  ImprimeTexte(nom, posXet5 + 1 + ligvertet5[0], posYet5 + 1 + (lig * 10));

  // trace numero de chantier
  if num <> '' then
    ImprimeTexte(num, posXet5 + 1, posYet5 + 1 + (lig * 10));

  for c := 1 to 12 do
  begin
    n := aligncolet5(t[c - 1], c + 2, txt);
    x := posXet5 + ligvertet5[c] + n;
    y := posYet5 + 1 + (lig * 10);
    ok := testcouleurnegatif(t[c - 1]);
    ImprimeTexte(txt, x, y);
    if ok then
      restituecouleurtexte;
  end;

  setcouleurecrituretexte;
end;

procedure TraceLignenomETAT5(lig: integer; nom: string);
begin
  settracefin;

  setcouleurtexte(0);

  // trace nom ligne
  ImprimeTexte(nom, posXet5 + 1 + ligvertet5[0], posYet5 + 1 + (lig * 10));

  setcouleurecrituretexte;
end;

procedure TraceLignegraphiqueETAT5(lig: integer);
var
  x: integer;
  y: integer;
  w: integer;
begin
  x := posXet5 + ligvertet5[0];
  y := posYet5 + 1 + (lig * 10) + 5;
  w := x + larXet5 - ligvertet5[0];
  traceligne(x, y, w, y);
end;

procedure TraceLignereportETAT5;
begin
  TraceLignegraphiqueETAT5(1);
end;

procedure TraceLigneEncadreETAT5(lig: integer);
var
  x: integer;
  y: integer;
  w: integer;
begin
  x := posXet5 + ligvertet5[0];
  w := x + larXet5 - ligvertet5[0];

  y := posYet5 + 1 + (lig * 10) - 2;
  traceligne(x, y, w, y);

  y := posYet5 + 1 + (lig * 10) + 5;
  traceligne(x, y, w, y);
end;

procedure TraceReportETAT5(totaux: array of currency);
var
  c: integer;
  x, y: integer;
  n: integer;
  txt: string;
begin
  settracefinitalique;

  setcouleurtexte(0);

  // trace nom ligne
  ImprimeTexte('Report', posXet5 + 1 + ligvertet5[0], posYet5 + 1 + 10);

  for c := 1 to 12 do
  begin
    n := aligncolet5(totaux[c - 1], c + 2, txt);
    x := posXet5 + ligvertet5[c] + n;
    y := posYet5 + 1 + 10;
    ImprimeTexte(txt, x, y);
  end;

  setcouleurecrituretexte;

  TraceLignereportETAT5;
end;

procedure TraceLigneETAT6(lig: integer; num, nom: string; t: array of currency);
var
  c: integer;
  x, y: integer;
  n: integer;
  txt: string;
  ok: boolean;
begin
  settracefin;

  // trace nom ligne
  ImprimeTexte(nom, posXet6 + 1 + ligvertet5[0], posYet6 + 1 + (lig * 10));

  for c := 1 to 12 do
  begin
    n := aligncolet5(t[c - 1], c + 2, txt);
    x := posXet6 + ligvertet5[c] + n;
    y := posYet6 + 1 + (lig * 10);
    ok := testcouleurnegatif(t[c - 1]);
    ImprimeTexte(txt, x, y);
    if ok then
      restituecouleurtexte;
  end;
end;

end.
