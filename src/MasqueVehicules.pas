unit MasqueVehicules;

interface

procedure ResetCumulsVehicule;
procedure CumulsVehiculeLire(var nbr: integer);
procedure CumulsVehicule(idh: integer);
procedure VehiculeViderListe;
procedure VehiculeInitMasque;
procedure VehiculeExecuterCalcul;
function VehiculeLireCalcul: currency;
function VehiculeLireSomme: currency;
procedure VehiculeEnreg(numhebdo: integer);
procedure VehiculeCharge(index: integer);
procedure VehiculeChargeChaines(index: integer; var nbrec: integer);
procedure VehiculeSupprime(index: integer);
procedure VehiculeModifsNon;
procedure VehiculeModifsOui;
function VehiculeLireModifsEtat: boolean;

function traiteVehiculeLireFormatCellules(ACol: integer): string;
procedure traiteVehiculesouris;
procedure traiteVehiculeInitialiserProcEffaceLigne(X, Y: integer);
function TraiteVehiculeSelection(ACol, ARow: integer): boolean;
procedure TraiteVehiculeValide(var Key: Char);
procedure traiteVehiculetouches(Key: word);
procedure traiteVehiculetouchesFIN();
procedure TraiteVehiculeChargeLigne;
procedure TraiteVehiculeEffacerLigne;

function CalculNombreVehicule: currency;

implementation

uses

  SysUtils, grids, Dialogs, windows,
  MasqueMateriels,
  winbase, accesBDD, Hebdo, impression;

const

  colindex = 0;
  colNom = 0;
  colcout = 1;
  colnombre = 2;
  colsommeligne = 3;

  coldeselection = 10;
  colautoselection = colnombre;

type

  TlargeurLigne = array [colNom .. colsommeligne] of integer;

const

  Largeur: TlargeurLigne = (185, 40, 40, 70);

var

  TblNomVEH: array of array of string;
  TblValVEH: array of array of currency;
  nbVEH: integer;

  positioncolonne: integer = 1;
  positionligne: integer;
  LigneEnCours: boolean = true;
  TableauVehicules: array of array of currency;
  CelluleEnCours: boolean;
  EditionEnCours: boolean;
  CelluleEnCoursCol: integer;
  CelluleEnCoursRow: integer;
  Suppressionencours: boolean;
  ModeLigneAppend: boolean;

  ModifsEtat: boolean;
  AutoriseModifs: boolean;

  SommeVEH: currency;
  SommeNombre: currency;

  /// ///////////////////////////////////////////////////////////////////////////
procedure unselectVEH;
begin
  Suppressionencours := true;
  FormBase.ListeUtilisationVEH.Col := coldeselection;
  FormBase.ListeUtilisationVEH.Row := 0;
  Suppressionencours := false;
end;

procedure VehiculeInitMasque;
var
  i: integer;
begin
  for i := colindex to colsommeligne do
    FormBase.ListeUtilisationVEH.ColWidths[i] := Largeur[i];

  for i := colsommeligne + 1 to coldeselection do
    FormBase.ListeUtilisationVEH.ColWidths[i] := 0;

  unselectVEH;
end;

procedure VehiculeModifsOui;
begin
  AutoriseModifs := true;
  FormBase.ListeVehicules.Enabled := true;
end;

procedure VehiculeModifsNon;
begin
  unselectVEH;
  AutoriseModifs := false;
  FormBase.ListeVehicules.Enabled := false;
  FormBase.ListeVehicules.KeyValue := -1;
end;

function VehiculeLireModifsEtat: boolean;
begin
  result := ModifsEtat;
end;

procedure SetModifsEtat;
begin
  ModifsEtat := true;

  // prevenir qu'il y a eu une modif d'un masque
  EvenementModifMasqueAnnexe;
end;

procedure ResetModifsEtat;
begin
  ModifsEtat := false;
end;

procedure VehiculeViderListe;

  procedure viderligne(fin: integer);
  var
    C: integer;
  begin
    for C := colindex to colsommeligne do
      FormBase.ListeUtilisationVEH.Cells[C, fin - 1] := '';
  end;

begin
  // pour ne pas repasser par le traitement evenement masque
  LigneEnCours := true;

  while FormBase.ListeUtilisationVEH.RowCount > 1 do
  begin
    positioncolonne := 0;
    positionligne := FormBase.ListeUtilisationVEH.RowCount - 2;
    TraiteVehiculeEffacerLigne;
  end;

  positioncolonne := 1;
  LigneEnCours := false;
end;

function VerifMaxiOK(ACol: integer; valeur: currency): boolean;
begin
  result := false;
  case ACol of
    colnombre:
      if (valeur < -999.9) or (valeur > 999.9) then
        exit;
  end;
  result := true;
end;

function traiteVehiculeLireFormatCellules(ACol: integer): string;
begin
  case ACol of
    colNom:
      result := '';
    colcout:
      result := '999,99';
    colnombre:
      result := '';
    colsommeligne:
      result := '#####,##';
  end;
end;

function FormatSortie(Col: integer): string;
begin
  case Col of
    colcout:
      result := '000.00';
    colnombre:
      result := '000.00';
    colsommeligne:
      result := '00000.00';
  else
    result := '';
  end;
end;

Procedure RechargerCellule(Col, lig: integer);
var
  valeur: currency;
  temp: string;
begin
  valeur := TableauVehicules[Col, lig];
  temp := FormatCurr(FormatSortie(Col), valeur);
  FormBase.ListeUtilisationVEH.Cells[Col, lig] := temp;
end;

function VehiculeLireCalcul: currency;
begin
  result := SommeVEH
end;

function VehiculeLireSomme: currency;
begin
  result := SommeNombre
end;

procedure CalculVehicule(lig: integer);
var
  C, N, Somme: currency;
  temp: string;
begin
  C := TableauVehicules[colcout, lig];
  N := TableauVehicules[colnombre, lig];

  Somme := (C * N);

  // somme du Vehicule
  TableauVehicules[colsommeligne, lig] := Somme;
  temp := FormatCurr(FormatSortie(colsommeligne), Somme);
  FormBase.ListeUtilisationVEH.Cells[colsommeligne, lig] := temp;
end;

function CalculNombreVehicule: currency;
var
  nbl: integer;
  N: currency;
  i: integer;
begin
  N := 0;
  nbl := FormBase.ListeUtilisationVEH.RowCount - 1;
  if nbl >= 0 then
  begin
    for i := 0 to (nbl - 1) do
    begin
      N := N + TableauVehicules[colnombre, i];
    end;
  end;

  // result := StrToInt(CurrToStr(n));
  result := N;
end;

procedure VehiculeExecuterCalcul;
var
  nbl: integer;
  t, N: currency;
  i: integer;
begin
  t := 0;
  N := 0;
  nbl := FormBase.ListeUtilisationVEH.RowCount - 1;
  if nbl >= 0 then
  begin
    for i := 0 to (nbl - 1) do
    begin
      t := t + TableauVehicules[colsommeligne, i];
      N := N + TableauVehicules[colnombre, i];
    end;
  end;

  SommeVEH := t;
  SommeNombre := N;

  // lance le calcul generalde la fiche Hebdo;
  CalculGeneralHebdo;
end;

function CurrToInt(valeur: currency): integer;
begin
  result := StrToInt(CurrToStr(valeur));
end;

function IntToCurr(valeur: integer): currency;
begin
  result := StrToCurr(IntToStr(valeur));
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure VehiculeEnreg(numhebdo: integer);
var
  nbe: integer;
  C: currency;
  l: integer;
  s: string;

  procedure ChercheEnreg(ligne: integer);
  var
    C: currency;
    id: integer;
  begin
    C := TableauVehicules[colindex, ligne];

    id := CurrToInt(C);
    FormBase.TabFichVehic.editkey;
    FormBase.TabFichVehic.FieldByName('id_Vehicule').asinteger := id;
    FormBase.TabFichVehic.gotokey;
    FormBase.TabFichVehic.edit;
  end;

begin
  // supprimer les enregs precedents
  VehiculeSupprime(numhebdo);
  ModeLigneAppend := false;

  nbe := FormBase.ListeUtilisationVEH.RowCount - 1;
  if (nbe = 0) then
    exit;

  // pas de mofifs en cours
  ResetModifsEtat;

  with FormBase.TabFichVehic do
  begin
    Open;

    if nbe <> 0 then
    begin
      // creer chaque ligne et la remplir
      for l := 0 to (nbe - 1) do
      begin
        if ModeLigneAppend = false then
          // creer nouveau enregistrement de ligne
          insert
        else
          // chercher enreg existant
          ChercheEnreg(l);

        s := FormBase.ListeUtilisationVEH.Cells[colNom, l];
        FieldByName('veh_enr_libellenom').AsString := s;

        C := TableauVehicules[colcout, l];
        FieldByName('veh_enr_cout').Ascurrency := C;

        C := TableauVehicules[colnombre, l];
        FieldByName('veh_nombre').Ascurrency := C;

        C := TableauVehicules[colsommeligne, l];
        FieldByName('veh_total').Ascurrency := C;

        FieldByName('id_hebdo').asinteger := numhebdo;

        // valider nouvelle ligne avec ces valeurs
        post;
      end;
    end;

    // fermeture de la table apres ajouts
    close;
  end;

  // sortie du mode de modification
  ModeLigneAppend := false;
end;

procedure ChargeLigneExiste;
var
  nc: integer;
  nblig: integer;

  idemp: integer;
  monaie: currency;
  valeur: currency;
  nomemp: string;

  procedure Ecrire(Col, lig: integer; valeur: currency);
  var
    t: string;
  begin
    t := FormatCurr(FormatSortie(Col), valeur);
    TableauVehicules[Col, lig] := valeur;
    FormBase.ListeUtilisationVEH.Cells[Col, lig] := t;
  end;

begin
  with FormBase.TabFichVehic do
  begin
    // definition de la ligne de Vehicule
    nblig := FormBase.ListeUtilisationVEH.RowCount - 1;

    SetLength(TableauVehicules, 10, nblig + 1);

    idemp := FieldByName('id_transport').asinteger;
    TableauVehicules[colindex, nblig] := IntToCurr(idemp);

    nomemp := FieldByName('veh_enr_libellenom').AsString;
    FormBase.ListeUtilisationVEH.Cells[colNom, nblig] := nomemp;

    // debut chargement liste ecran
    LigneEnCours := true;

    monaie := FieldByName('veh_enr_cout').Ascurrency;
    Ecrire(colcout, nblig, monaie);

    valeur := FieldByName('veh_nombre').Ascurrency;
    Ecrire(colnombre, nblig, valeur);

    // calculer la ligne
    CalculVehicule(nblig);

    // fin chargement liste ecran
    LigneEnCours := false;

    // defintion de la prochaine ligne
    nc := FormBase.ListeUtilisationVEH.RowCount;
    inc(nc);
    FormBase.ListeUtilisationVEH.RowCount := nc;

    // selection de la 1ere colonne a editer
    FormBase.ListeUtilisationVEH.SetFocus;
    FormBase.ListeUtilisationVEH.Col := colnombre;
    FormBase.ListeUtilisationVEH.Row := nblig;
  end;
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure VehiculeCharge(index: integer);
var
  nbrec: integer;
  t: string;
  nblig: integer;
begin
  ChargeConfiguration;

  // pas de mofifs en cours
  ResetModifsEtat;

  // definition des lignes de Vehicule
  FormBase.ListeUtilisationVEH.RowCount := 1;

  with FormBase.TabFichVehic do
  begin
    // recherche des enreg contenant 'index'

    // preparer le filtre
    str(index, t);
    filter := 'id_hebdo = ' + t;
    filtered := true;

    Open;
    setkey;

    nbrec := recordcount;
    if nbrec <> 0 then
    begin
      // entree dans le mode de modification
      ModeLigneAppend := true;
      edit;

      for nblig := 0 to (nbrec - 1) do
      begin
        // se positionner sur 1er enreg ou sur suivant
        if nblig = 0 then
          first
        else
          next;

        // charger liste de Vehicule
        ChargeLigneExiste;
      end;
    end;

    // termine la lecture de la table
    close;
    filtered := false;

    // recalcul tout
    VehiculeExecuterCalcul;
  end;
end;

procedure GetEnregVehiculeCourant(var i: integer; var N: string;
  var C: currency);
begin
  with FormBase.TableVehicules do
  begin
    edit;
    i := FieldValues['id_vehicule'];
    N := FieldByName('veh_libellenom').AsString;
    C := FieldByName('veh_cout').Ascurrency;
    post;
  end;
end;

procedure TraiteVehiculeChargeLigne;
var
  nc: integer;
  nblig: integer;

  idemp: integer;
  monaie: currency;
  nomemp: string;

  function ChercheExiste(nblig: integer; nomemp: string): boolean;
  var
    i: integer;
    t: string;
  begin
    result := false;
    for i := 0 to nblig - 1 do
    begin
      t := FormBase.ListeUtilisationVEH.Cells[colNom, i];
      if nomemp = trim(t) then
        result := true;
    end;
  end;

  procedure Ecrire(Col, lig: integer; valeur: currency);
  var
    t: string;
  begin
    t := FormatCurr(FormatSortie(Col), valeur);
    TableauVehicules[Col, lig] := valeur;
    FormBase.ListeUtilisationVEH.Cells[Col, lig] := t;
  end;

begin
  // inclure un employ� dans la liste de Vehicule

  if FormBase.TableVehicules.recordcount = 0 then
    exit;

  // modifs en cours
  SetModifsEtat;

  // lire l'enreg correspondant de l'employe modifier
  GetEnregVehiculeCourant(idemp, nomemp, monaie);

  ChargeConfiguration;

  // definition de la ligne de Vehicule
  nblig := FormBase.ListeUtilisationVEH.RowCount - 1;

  // verifier si nb maxi de lignes atteint
  if nblig = nbligveh then
    exit;

  if nblig <> 0 then
  begin
    // verifier si le nom existe dans liste alors ne pas charger
    if ChercheExiste(nblig, nomemp) then
      exit;
  end;

  SetLength(TableauVehicules, 10, nblig + 1);

  TableauVehicules[colindex, nblig] := IntToCurr(idemp);
  FormBase.ListeUtilisationVEH.Cells[colNom, nblig] := nomemp;

  LigneEnCours := true; // debut chargement liste ecran

  Ecrire(colcout, nblig, monaie);
  Ecrire(colnombre, nblig, 0);

  // calculer la ligne
  CalculVehicule(nblig);

  LigneEnCours := false; // fin chargement liste ecran

  // defintion de la prochaine ligne
  nc := FormBase.ListeUtilisationVEH.RowCount;
  inc(nc);
  FormBase.ListeUtilisationVEH.RowCount := nc;

  // selection de la 1ere colonne a editer
  FormBase.ListeUtilisationVEH.SetFocus;
  FormBase.ListeUtilisationVEH.Col := colnombre;
  FormBase.ListeUtilisationVEH.Row := nblig;

  // recalcul tout
  VehiculeExecuterCalcul;
end;

function TraiteVehiculeSelection(ACol, ARow: integer): boolean;
var
  OK: boolean;
  i: integer;
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    result := false;
    exit;
  end;

  result := true;
  OK := true;

  // utile pour la suppression de la derniere ligne
  if Suppressionencours and (ACol = 10) then
    exit;

  // verifier si depassement nbr de lignes
  i := FormBase.ListeUtilisationVEH.RowCount - 1;
  if ARow >= i then
    OK := false;

  if (ACol = colnombre) and OK then
  begin
    // autoriser l'edition de la cellule
    with FormBase.ListeUtilisationVEH do
      options := options + [goEditing];

    RechargerCellule(ACol, ARow);
  end
  else
  begin
    // interdire l'edition et la selection de la cellule
    with FormBase.ListeUtilisationVEH do
      options := options - [goEditing];
    result := false;
    exit;
  end;
end;

procedure TraiteVehiculeValide(var Key: Char);
var
  ACol: integer;
  ARow: integer;
  temp: string;
  valeur: currency;
  numlig: integer;
  nbv: currency;
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    Key := chr(0);
    exit;
  end;

  if (Key = ' ') then
  begin
    // ne traite pas ces touches
    Key := chr(0);
    exit;
  end;

  if (Key = '.') then
  begin;
    // modifs en cours
    SetModifsEtat;

    // pour faciliter la saisie des nombres decimaux
    Key := ',';

    exit;
  end;

  // valide la saisie
  if Key = chr(13) then
  begin
    // modifs en cours
    SetModifsEtat;

    // Stocker la saisie entree au clavier
    ACol := FormBase.ListeUtilisationVEH.Col;
    ARow := FormBase.ListeUtilisationVEH.Row;

    try
      begin
        // Stocker la saisie entree au clavier
        temp := FormBase.ListeUtilisationVEH.Cells[ACol, ARow];
        valeur := StrToCurr(trim(temp));

        // test maxi
        if VerifMaxiOK(ACol, valeur) then
        begin
          TableauVehicules[ACol, ARow] := valeur;
          temp := FormatCurr(FormatSortie(ACol), valeur);
          FormBase.ListeUtilisationVEH.Cells[ACol, ARow] := temp;
        end
        else
          // valeur hors de la bone maximum donc recharge
          RechargerCellule(ACol, ARow);

        // calculer la ligne
        CalculVehicule(ARow);
      end;

      // recalcul tout
      VehiculeExecuterCalcul;

      // traiter materiel divers
      numlig := Veriflignematerieldiversexiste;
      if numlig <> -1 then
      begin
        // recuperer le Nombre de Vehicule
        nbv := CalculNombreVehicule;

        // l'affecter a la ligne materiels divers
        AffecterNombreMaterieldivers(numlig, nbv);
      end;

    except
      // si erreur de saisie dans la cellule
      on EConvertError do
        RechargerCellule(ACol, ARow);
    end;
  end;

  // si touche numerique
  CelluleEnCours := false;
  EditionEnCours := false;
  if ((ord(Key) > 47) and (ord(Key) < 58)) or (Key = '-') or (Key = '+') then
  begin
    // modifs en cours
    SetModifsEtat;

    CelluleEnCours := true;
    EditionEnCours := true;

    // Stocker la saisie entree au clavier
    CelluleEnCoursCol := FormBase.ListeUtilisationVEH.Col;
    CelluleEnCoursRow := FormBase.ListeUtilisationVEH.Row;
  end;
end;

procedure traiteVehiculeInitialiserProcEffaceLigne(X, Y: integer);
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  with FormBase.ListeUtilisationVEH do
    MouseToCell(X, Y, positioncolonne, positionligne)
end;

procedure TraiteVehiculeEffacerLigne;
var
  nbl: integer;
  i: integer;

  procedure transfert(deb, fin: integer);
  var
    l: integer;
    C: integer;
    t: currency;
  begin
    for l := deb to fin - 2 do
    begin
      for C := colindex to colsommeligne do
      begin
        with FormBase.ListeUtilisationVEH do
          Cells[C, l] := Cells[C, l + 1];

        t := TableauVehicules[C, l + 1];
        TableauVehicules[C, l] := t;
      end;
    end;
  end;

  procedure viderligne(fin: integer);
  var
    C: integer;
  begin
    for C := colindex to colsommeligne do
      FormBase.ListeUtilisationVEH.Cells[C, fin - 1] := '';
  end;

begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  // suppression ligne de Vehicule selectionn�e
  if (positioncolonne = colNom) then
  begin
    // modifs en cours
    SetModifsEtat;

    // diminuer d'une ligne verifier si depassement nbr de lignes
    nbl := FormBase.ListeUtilisationVEH.RowCount - 1;
    if positionligne >= nbl then
      exit;

    // remonter d'une unite toutes les lignes
    if positionligne < nbl then
      transfert(positionligne, nbl);

    if nbl = 1 then
      unselectVEH
    else
    begin
      FormBase.ListeUtilisationVEH.Col := colautoselection;
      FormBase.ListeUtilisationVEH.Row := 0;
    end;

    // effacer derniere ligne
    viderligne(nbl);

    i := FormBase.ListeUtilisationVEH.RowCount;
    dec(i);
    FormBase.ListeUtilisationVEH.RowCount := i;

    // pour eviter des suppression intenpestives
    positioncolonne := 1;
  end;

  // recalcul tout
  VehiculeExecuterCalcul;
end;

procedure traiteVehiculetouchesFIN();
begin
  CelluleEnCours := false;
end;

procedure traiteVehiculetouches(Key: word);
var
  ACol: integer;
  ARow: integer;

  // recupere position cellule en cours
  procedure Cellule(var ACol: integer; var ARow: integer);
  begin
    ACol := FormBase.ListeUtilisationVEH.Col;
    ARow := FormBase.ListeUtilisationVEH.Row;
  end;

  function lignes: boolean;
  begin
    result := false;
    if FormBase.ListeUtilisationVEH.RowCount > 2 then
      result := true;
  end;

begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  // sortie si 0 lignes
  if not lignes then
    exit;

  if CelluleEnCours = true then
    exit;

  if TestTouchesFleches(Key) or (VK_TAB = Key) then
  begin
    if LigneEnCours = false then
    begin
      // retour en arriere d'une cellule suivant touche
      Cellule(ACol, ARow);
      case Key of
        VK_TAB:
          dec(ACol);
        VK_LEFT:
          inc(ACol);
        VK_RIGHT:
          dec(ACol);
        VK_DOWN:
          if lignes then
            dec(ARow);
        VK_UP:
          if lignes then
            inc(ARow);
      end;

      // reaffichage suite a entree non validee par CRLF
      if CelluleEnCours = false then
        if colsommeligne <> ACol then
        begin
          // modifs en cours
          SetModifsEtat;

          RechargerCellule(ACol, ARow);
        end;
    end;
  end;
end;

procedure traiteVehiculesouris;
var
  ACol: integer;
  ARow: integer;
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  if EditionEnCours = false then
    exit;

  ACol := FormBase.ListeUtilisationVEH.Col;
  ARow := FormBase.ListeUtilisationVEH.Row;

  if not ForcerRestitution then
    if (CelluleEnCoursCol = ACol) and (CelluleEnCoursRow = ARow) then
      exit;

  EditionEnCours := false;

  // modifs en cours
  SetModifsEtat;

  RechargerCellule(CelluleEnCoursCol, CelluleEnCoursRow);
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure VehiculeChargeChaines(index: integer; var nbrec: integer);

  function LireChaine(param: string): string;
  begin
    result := FormBase.TabFichVehic.FieldByName(param).AsString;
  end;

  function LireValeur(param: string): currency;
  begin
    result := FormBase.TabFichVehic.FieldByName(param).Ascurrency;
  end;

  procedure Charge(nblig: integer);
  var
    temp: string;
    valeur: currency;
  begin
    // recuperer le type de ligne
    TableauTemp[0, nblig] := '';

    TableauTemp[1, nblig] := LireChaine('veh_enr_libellenom');

    temp := FormatSortie(colcout);
    valeur := LireValeur('veh_enr_cout');
    temp := FormatCurr('0.00', valeur);
    TableauTemp[2, nblig] := temp;
    TableauvalTemp[2, nblig] := valeur;

    temp := FormatSortie(colnombre);
    valeur := LireValeur('veh_nombre');
    temp := FormatCurr('0.00', valeur);
    TableauTemp[3, nblig] := temp;
    TableauvalTemp[3, nblig] := valeur;

    temp := FormatSortie(colsommeligne);
    valeur := LireValeur('veh_total');
    temp := FormatCurr('0.00', valeur);
    TableauTemp[4, nblig] := temp;
    TableauvalTemp[4, nblig] := valeur;
  end;

var
  t: string;
  nblig: integer;

begin
  with FormBase.TabFichVehic do
  begin
    // preparer le filtre recherche des enreg contenant 'index'
    str(index, t);
    filter := 'id_hebdo = ' + t;
    filtered := true;

    Open;
    setkey;

    nbrec := recordcount;
    if nbrec <> 0 then
    begin
      // entree dans le mode de modification
      SetLength(TableauTemp, 11, nbrec + 1);
      SetLength(TableauvalTemp, 11, nbrec + 1);
      edit;

      for nblig := 0 to (nbrec - 1) do
      begin
        // se positionner sur 1er enreg ou sur suivant
        if nblig = 0 then
          first
        else
          next;

        // charger liste de Vehicule
        Charge(nblig);
      end;
    end;

    // termine la lecture de la table
    close;
    filtered := false;
  end;
end;

procedure VehiculeSupprime(index: integer);
var
  t: string;
begin
  with FormBase.TabFichVehic do
  begin
    // preparer le filtre recherche des enreg contenant 'index'
    str(index, t);
    filter := 'id_hebdo = ' + t;
    filtered := true;

    Open;
    setkey;

    // supprimer tous les enregs
    while recordcount <> 0 do
      delete;

    // termine la suppression dans la table
    close;
    filtered := false;
  end;
end;

procedure ResetCumulsVehicule;
begin
  nbVEH := 0;

  SetLength(TblNomVEH, 2, nbVEH);
  SetLength(TblValVEH, 3, nbVEH);
end;

procedure CumulsVehicule(idh: integer);

  function ChercheAjoute(nom: string; var ind: integer): boolean;
  var
    i: integer;
    t: string;
  begin
    result := true;

    if nbVEH = 0 then
      exit;
    for i := 0 to (nbVEH - 1) do
    begin
      t := TblNomVEH[1, i];
      if t = nom then
      begin
        ind := i;
        result := false;
        exit;
      end;
    end;
  end;

var
  ind: integer;
  nbrec: integer;
  i: integer;
  N: integer;
  C: currency;
begin
  // charger les pointages
  VehiculeChargeChaines(idh, nbrec);

  // si aucuns pointage sortir
  if nbrec = 0 then
    exit;

  // traiter tous les pointages
  for i := 0 to (nbrec - 1) do
  begin
    // verifier si le pointage existe deja
    if ChercheAjoute(TableauTemp[1, i], ind) then
    begin
      // ajoute une ligne au tableau
      inc(nbVEH);
      SetLength(TblNomVEH, 2, nbVEH);
      SetLength(TblValVEH, 3, nbVEH);

      // stocker nom et type ligne de pointage
      ind := nbVEH - 1;
      TblNomVEH[0, ind] := '';
      TblNomVEH[1, ind] := TableauTemp[1, i];

      // raz valeurs nouvelle ligne
      for N := 1 to 2 do
        TblValVEH[N, ind] := 0;

      // stocker le cout
      TblValVEH[0, ind] := TableauvalTemp[2, i];
    end;

    // cumuler a la ligne en cours la nouvelle ligne
    for N := 1 to 2 do
    begin
      C := TableauvalTemp[N + 2, i];
      C := TblValVEH[N, ind] + C;
      TblValVEH[N, ind] := C;
    end;
  end;
end;

procedure CumulsVehiculeLire(var nbr: integer);
var
  i: integer;
  t: string;
begin
  nbr := nbVEH;
  if nbVEH = 0 then
    exit;

  // pour stocker tous les pointages
  SetLength(TableauTemp, 11, nbVEH);

  for i := 0 to (nbVEH - 1) do
  begin
    TableauTemp[0, i] := TblNomVEH[0, i];
    TableauTemp[1, i] := TblNomVEH[1, i];

    t := FormatSortie(colcout);
    t := FormatCurr('0.00', TblValVEH[0, i]);
    TableauTemp[2, i] := t;

    t := FormatSortie(colnombre);
    t := FormatCurr('0.00', TblValVEH[1, i]);
    TableauTemp[3, i] := t;

    t := FormatSortie(colsommeligne);
    t := FormatCurr('0.00', TblValVEH[2, i]);
    TableauTemp[4, i] := t;
  end;
end;
/// ///////////////////////////////////////////////////////////////////////////

end.
