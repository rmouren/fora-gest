unit MasqueExterne;

interface

procedure ResetCumulsExterne;
procedure CumulsExterne(idh: integer);
procedure CumulsExterneLire(var nbr: integer);
procedure ExterneViderListe;
procedure ExterneInitMasque;
procedure ExterneExecuterCalcul;
function ExterneLireCalcul: currency;
procedure ExterneEnreg(numhebdo: integer);
procedure ExterneCharge(index: integer);
procedure ExterneChargeChaines(index: integer; var nbrec: integer);
procedure ExterneSupprime(index: integer);
procedure ExterneModifsNon;
procedure ExterneModifsOui;
function ExterneLireModifsEtat: boolean;

function traiteExterneLireFormatCellules(ACol: integer): string;
procedure traiteExternesouris;
procedure traiteExterneInitialiserProcEffaceLigne(X, Y: integer);
function TraiteExterneSelection(ACol, ARow: integer): boolean;
procedure TraiteExterneValide(var Key: Char);
procedure traiteExternetouches(Key: word);
procedure traiteExternetouchesFIN();
procedure TraiteExterneChargeLigne;
procedure TraiteExterneEffacerLigne;

implementation

uses

  SysUtils, grids, Dialogs, windows,
  winbase, accesBDD, Hebdo, impression;

const

  colindex = 0;
  colNom = 0;
  colcout = 1;

  coldeselection = 10;
  colautoselection = colcout;

type

  TlargeurLigne = array [colNom .. colcout] of integer;

const

  Largeur: TlargeurLigne = (240, 80);

var

  TblNomEXT: array of array of string;
  TblvalEXT: array of array of currency;
  nbEXT: integer;

  positioncolonne: integer = 1;
  positionligne: integer;
  LigneEnCours: boolean = true;
  TableauExternes: array of array of currency;
  CelluleEnCours: boolean;
  EditionEnCours: boolean;
  CelluleEnCoursCol: integer;
  CelluleEnCoursRow: integer;
  Suppressionencours: boolean;
  ModeLigneAppend: boolean;

  ModifsEtat: boolean;
  AutoriseModifs: boolean;

  SommeEXT: currency;

  /// ///////////////////////////////////////////////////////////////////////////
procedure unselectEXT;
begin
  Suppressionencours := true;
  FormBase.ListeUtilisationEXT.Col := coldeselection;
  FormBase.ListeUtilisationEXT.Row := 0;
  Suppressionencours := false;
end;

procedure ExterneInitMasque;
var
  i: integer;
begin
  for i := colindex to colcout do
    FormBase.ListeUtilisationEXT.ColWidths[i] := Largeur[i];

  for i := colcout + 1 to coldeselection do
    FormBase.ListeUtilisationEXT.ColWidths[i] := 0;

  unselectEXT;
end;

procedure ExterneModifsOui;
begin
  AutoriseModifs := true;
  FormBase.ListeExterne.Enabled := true;
end;

procedure ExterneModifsNon;
begin
  unselectEXT;
  AutoriseModifs := false;
  FormBase.ListeExterne.Enabled := false;
  FormBase.ListeExterne.KeyValue := -1;
end;

function ExterneLireModifsEtat: boolean;
begin
  result := ModifsEtat;
end;

procedure SetModifsEtat;
begin
  ModifsEtat := true;

  // prevenir qu'il y a eu une modif d'un masque
  EvenementModifMasqueAnnexe;
end;

procedure ResetModifsEtat;
begin
  ModifsEtat := false;
end;

procedure ExterneViderListe;

  procedure viderligne(fin: integer);
  var
    C: integer;
  begin
    for C := colindex to colcout do
      FormBase.ListeUtilisationEXT.Cells[C, fin - 1] := '';
  end;

begin
  // pour ne pas repasser par le traitement evenement masque
  LigneEnCours := true;

  while FormBase.ListeUtilisationEXT.RowCount > 1 do
  begin
    positioncolonne := 0;
    positionligne := FormBase.ListeUtilisationEXT.RowCount - 2;
    TraiteExterneEffacerLigne;
  end;

  positioncolonne := 1;
  LigneEnCours := false;
end;

function VerifMaxiOK(ACol: integer; valeur: currency): boolean;
begin
  result := false;
  case ACol of
    colcout:
      if (valeur < -99999.99) or (valeur > 99999.99) then
        exit;
  end;
  result := true;
end;

function traiteExterneLireFormatCellules(ACol: integer): string;
begin
  case ACol of
    colNom:
      result := '';
    colcout:
      result := '';
  end;
end;

function FormatSortie(Col: integer): string;
begin
  case Col of
    colcout:
      result := '00000.00';
  else
    result := '';
  end;
end;

Procedure RechargerCellule(Col, lig: integer);
var
  valeur: currency;
  temp: string;
begin
  valeur := TableauExternes[Col, lig];
  temp := FormatCurr(FormatSortie(Col), valeur);
  FormBase.ListeUtilisationEXT.Cells[Col, lig] := temp;
end;

function ExterneLireCalcul: currency;
begin
  result := SommeEXT
end;

procedure CalculExterne(lig: integer);
var
  D, Somme: currency;
  temp: string;
begin
  D := TableauExternes[colcout, lig];

  Somme := (D);

  // somme du Externe
  temp := FormatCurr(FormatSortie(colcout), Somme);
  FormBase.ListeUtilisationEXT.Cells[colcout, lig] := temp;
end;

procedure ExterneExecuterCalcul;
var
  nbl: integer;
  t: currency;
  i: integer;
begin
  t := 0;
  nbl := FormBase.ListeUtilisationEXT.RowCount - 1;
  if nbl >= 0 then
  begin
    for i := 0 to (nbl - 1) do
      t := t + TableauExternes[colcout, i];
  end;

  SommeEXT := t;

  // lance le calcul generalde la fiche Hebdo;
  CalculGeneralHebdo;
end;

function CurrToInt(valeur: currency): integer;
begin
  result := StrToInt(CurrToStr(valeur));
end;

function IntToCurr(valeur: integer): currency;
begin
  result := StrToCurr(IntToStr(valeur));
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure ExterneEnreg(numhebdo: integer);
var
  nbe: integer;
  C: currency;
  l: integer;
  s: string;

  procedure ChercheEnreg(ligne: integer);
  var
    C: currency;
    id: integer;
  begin
    C := TableauExternes[colindex, ligne];

    id := CurrToInt(C);
    FormBase.TabFichExter.editkey;
    FormBase.TabFichExter.FieldByName('id_Externe').asinteger := id;
    FormBase.TabFichExter.gotokey;
    FormBase.TabFichExter.edit;
  end;

begin
  // supprimer les enregs precedents
  ExterneSupprime(numhebdo);
  ModeLigneAppend := false;

  nbe := FormBase.ListeUtilisationEXT.RowCount - 1;
  if (nbe = 0) then
    exit;

  // pas de mofifs en cours
  ResetModifsEtat;

  with FormBase.TabFichExter do
  begin
    Open;

    if nbe <> 0 then
    begin
      // creer chaque ligne et la remplir
      for l := 0 to (nbe - 1) do
      begin
        if ModeLigneAppend = false then
          // creer nouveau enregistrement de ligne
          insert
        else
          // chercher enreg existant
          ChercheEnreg(l);

        s := FormBase.ListeUtilisationEXT.Cells[colNom, l];
        FieldByName('ext_enr_libellenom').AsString := s;

        C := TableauExternes[colcout, l];
        FieldByName('ext_cout').Ascurrency := C;

        FieldByName('id_hebdo').asinteger := numhebdo;

        // valider nouvelle ligne avec ces valeurs
        post;
      end;
    end;

    // fermeture de la table apres ajouts
    close;
  end;

  // sortie du mode de modification
  ModeLigneAppend := false;
end;

procedure ChargeLigneExiste;
var
  nc: integer;
  nblig: integer;

  idemp: integer;
  monaie: currency;
  nomemp: string;

  procedure Ecrire(Col, lig: integer; valeur: currency);
  var
    t: string;
  begin
    t := FormatCurr(FormatSortie(Col), valeur);
    TableauExternes[Col, lig] := valeur;
    FormBase.ListeUtilisationEXT.Cells[Col, lig] := t;
  end;

begin
  with FormBase.TabFichExter do
  begin
    // definition de la ligne de Externe
    nblig := FormBase.ListeUtilisationEXT.RowCount - 1;

    SetLength(TableauExternes, 10, nblig + 1);

    idemp := FieldByName('id_Externe').asinteger;
    TableauExternes[colindex, nblig] := IntToCurr(idemp);

    nomemp := FieldByName('ext_enr_libellenom').AsString;
    FormBase.ListeUtilisationEXT.Cells[colNom, nblig] := nomemp;

    // debut chargement liste ecran
    LigneEnCours := true;

    monaie := FieldByName('ext_cout').Ascurrency;
    Ecrire(colcout, nblig, monaie);

    // calculer la ligne
    CalculExterne(nblig);

    // fin chargement liste ecran
    LigneEnCours := false;

    // defintion de la prochaine ligne
    nc := FormBase.ListeUtilisationEXT.RowCount;
    inc(nc);
    FormBase.ListeUtilisationEXT.RowCount := nc;

    // selection de la 1ere colonne a editer
    FormBase.ListeUtilisationEXT.SetFocus;
    FormBase.ListeUtilisationEXT.Col := colcout;
    FormBase.ListeUtilisationEXT.Row := nblig;
  end;
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure ExterneCharge(index: integer);
var
  nbrec: integer;
  t: string;
  nblig: integer;
begin
  ChargeConfiguration;

  // pas de mofifs en cours
  ResetModifsEtat;

  // definition des lignes de Externe
  FormBase.ListeUtilisationEXT.RowCount := 1;

  with FormBase.TabFichExter do
  begin
    // recherche des enreg contenant 'index'

    // preparer le filtre
    str(index, t);
    filter := 'id_hebdo = ' + t;
    filtered := true;

    Open;
    setkey;

    nbrec := recordcount;
    if nbrec <> 0 then
    begin
      // entree dans le mode de modification
      ModeLigneAppend := true;
      edit;

      for nblig := 0 to (nbrec - 1) do
      begin
        // se positionner sur 1er enreg ou sur suivant
        if nblig = 0 then
          first
        else
          next;

        // charger liste de Externe
        ChargeLigneExiste;
      end;
    end;

    // termine la lecture de la table
    close;
    filtered := false;

    // recalcul tout
    ExterneExecuterCalcul;
  end;
end;

procedure GetEnregExterneCourant(var i: integer; var n: string);
begin
  with FormBase.TableExterne do
  begin
    edit;
    i := FieldValues['id_fraisexterne'];
    n := FieldByName('ext_libellenom').AsString;
    post;
  end;
end;

procedure TraiteExterneChargeLigne;
var
  nc: integer;
  nblig: integer;

  idemp: integer;
  nomemp: string;

  function ChercheExiste(nblig: integer; nomemp: string): boolean;
  var
    i: integer;
    t: string;
  begin
    result := false;
    for i := 0 to nblig - 1 do
    begin
      t := FormBase.ListeUtilisationEXT.Cells[colNom, i];
      if nomemp = trim(t) then
        result := true;
    end;
  end;

  procedure Ecrire(Col, lig: integer; valeur: currency);
  var
    t: string;
  begin
    t := FormatCurr(FormatSortie(Col), valeur);
    TableauExternes[Col, lig] := valeur;
    FormBase.ListeUtilisationEXT.Cells[Col, lig] := t;
  end;

begin
  // inclure un employ� dans la liste de Externe

  if FormBase.TableExterne.recordcount = 0 then
    exit;

  // modifs en cours
  SetModifsEtat;

  // lire l'enreg correspondant de l'employe modifier
  GetEnregExterneCourant(idemp, nomemp);

  ChargeConfiguration;

  // definition de la ligne de Externe
  nblig := FormBase.ListeUtilisationEXT.RowCount - 1;

  // verifier si nb maxi de lignes atteint
  if nblig = (nbligext - 1) then
    exit;

  if nblig <> 0 then
  begin
    // verifier si le nom existe dans liste alors ne pas charger
    if ChercheExiste(nblig, nomemp) then
      exit;
  end;

  SetLength(TableauExternes, 10, nblig + 1);

  TableauExternes[colindex, nblig] := IntToCurr(idemp);
  FormBase.ListeUtilisationEXT.Cells[colNom, nblig] := nomemp;

  LigneEnCours := true; // debut chargement liste ecran

  Ecrire(colcout, nblig, 0);

  // calculer la ligne
  CalculExterne(nblig);

  LigneEnCours := false; // fin chargement liste ecran

  // defintion de la prochaine ligne
  nc := FormBase.ListeUtilisationEXT.RowCount;
  inc(nc);
  FormBase.ListeUtilisationEXT.RowCount := nc;

  // selection de la 1ere colonne a editer
  FormBase.ListeUtilisationEXT.SetFocus;
  FormBase.ListeUtilisationEXT.Col := colcout;
  FormBase.ListeUtilisationEXT.Row := nblig;

  // recalcul tout
  ExterneExecuterCalcul;
end;

function TraiteExterneSelection(ACol, ARow: integer): boolean;
var
  OK: boolean;
  i: integer;
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    result := false;
    exit;
  end;

  result := true;
  OK := true;

  // utile pour la suppression de la derniere ligne
  if Suppressionencours and (ACol = 10) then
    exit;

  // verifier si depassement nbr de lignes
  i := FormBase.ListeUtilisationEXT.RowCount - 1;
  if ARow >= i then
    OK := false;

  if (ACol = colcout) and OK then
  begin
    // autoriser l'edition de la cellule
    with FormBase.ListeUtilisationEXT do
      options := options + [goEditing];

    RechargerCellule(ACol, ARow);
  end
  else
  begin
    // interdire l'edition et la selection de la cellule
    with FormBase.ListeUtilisationEXT do
      options := options - [goEditing];
    result := false;
    exit;
  end;
end;

procedure TraiteExterneValide(var Key: Char);
var
  ACol: integer;
  ARow: integer;
  temp: string;
  valeur: currency;
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    Key := chr(0);
    exit;
  end;

  if (Key = ' ') then
  begin
    // ne traite pas ces touches
    Key := chr(0);
    exit;
  end;

  if (Key = '.') then
  begin;
    // modifs en cours
    SetModifsEtat;

    // pour faciliter la saisie des nombres decimaux
    Key := ',';

    exit;
  end;

  // valide la saisie
  if Key = chr(13) then
  begin
    // modifs en cours
    SetModifsEtat;

    // Stocker la saisie entree au clavier
    ACol := FormBase.ListeUtilisationEXT.Col;
    ARow := FormBase.ListeUtilisationEXT.Row;

    try
      begin
        // Stocker la saisie entree au clavier
        temp := FormBase.ListeUtilisationEXT.Cells[ACol, ARow];
        valeur := StrToCurr(trim(temp));

        // test maxi
        if VerifMaxiOK(ACol, valeur) then
        begin
          TableauExternes[ACol, ARow] := valeur;
          temp := FormatCurr(FormatSortie(ACol), valeur);
          FormBase.ListeUtilisationEXT.Cells[ACol, ARow] := temp;
        end
        else
          // valeur hors de la bone maximum donc recharge
          RechargerCellule(ACol, ARow);

        // calculer la ligne
        CalculExterne(ARow);
      end;

      // recalcul tout
      ExterneExecuterCalcul;

    except
      // si erreur de saisie dans la cellule
      on EConvertError do
        RechargerCellule(ACol, ARow);
    end;
  end;

  // si touche numerique
  CelluleEnCours := false;
  EditionEnCours := false;
  if ((ord(Key) > 47) and (ord(Key) < 58)) or (Key = '-') or (Key = '+') then
  begin
    // modifs en cours
    SetModifsEtat;

    CelluleEnCours := true;
    EditionEnCours := true;

    // Stocker la saisie entree au clavier
    CelluleEnCoursCol := FormBase.ListeUtilisationEXT.Col;
    CelluleEnCoursRow := FormBase.ListeUtilisationEXT.Row;
  end;
end;

procedure traiteExterneInitialiserProcEffaceLigne(X, Y: integer);
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  with FormBase.ListeUtilisationEXT do
    MouseToCell(X, Y, positioncolonne, positionligne)
end;

procedure TraiteExterneEffacerLigne;
var
  nbl: integer;
  i: integer;

  procedure transfert(deb, fin: integer);
  var
    l: integer;
    C: integer;
    t: currency;
  begin
    for l := deb to fin - 2 do
    begin
      for C := colindex to colcout do
      begin
        with FormBase.ListeUtilisationEXT do
          Cells[C, l] := Cells[C, l + 1];

        t := TableauExternes[C, l + 1];
        TableauExternes[C, l] := t;
      end;
    end;
  end;

  procedure viderligne(fin: integer);
  var
    C: integer;
  begin
    for C := colindex to colcout do
      FormBase.ListeUtilisationEXT.Cells[C, fin - 1] := '';
  end;

begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  // suppression ligne de Externe selectionn�e
  if (positioncolonne = colNom) then
  begin
    // modifs en cours
    SetModifsEtat;

    // diminuer d'une ligne verifier si depassement nbr de lignes
    nbl := FormBase.ListeUtilisationEXT.RowCount - 1;
    if positionligne >= nbl then
      exit;

    // remonter d'une unite toutes les lignes
    if positionligne < nbl then
      transfert(positionligne, nbl);

    if nbl = 1 then
      unselectEXT
    else
    begin
      FormBase.ListeUtilisationEXT.Col := colautoselection;
      FormBase.ListeUtilisationEXT.Row := 0;
    end;

    // effacer derniere ligne
    viderligne(nbl);

    i := FormBase.ListeUtilisationEXT.RowCount;
    dec(i);
    FormBase.ListeUtilisationEXT.RowCount := i;

    // pour eviter des suppression intenpestives
    positioncolonne := 1;
  end;

  // recalcul tout
  ExterneExecuterCalcul;
end;

procedure traiteExternetouchesFIN();
begin
  CelluleEnCours := false;
end;

procedure traiteExternetouches(Key: word);
var
  ACol: integer;
  ARow: integer;

  // recupere position cellule en cours
  procedure Cellule(var ACol: integer; var ARow: integer);
  begin
    ACol := FormBase.ListeUtilisationEXT.Col;
    ARow := FormBase.ListeUtilisationEXT.Row;
  end;

  function lignes: boolean;
  begin
    result := false;
    if FormBase.ListeUtilisationEXT.RowCount > 2 then
      result := true;
  end;

begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  // sortie si 0 lignes
  if not lignes then
    exit;

  if CelluleEnCours = true then
    exit;

  if TestTouchesFleches(Key) or (VK_TAB = Key) then
  begin
    if LigneEnCours = false then
    begin
      // retour en arriere d'une cellule suivant touche
      Cellule(ACol, ARow);
      case Key of
        VK_TAB:
          dec(ACol);
        VK_LEFT:
          inc(ACol);
        VK_RIGHT:
          dec(ACol);
        VK_DOWN:
          if lignes then
            dec(ARow);
        VK_UP:
          if lignes then
            inc(ARow);
      end;

      // reaffichage suite a entree non validee par CRLF
      if CelluleEnCours = false then
        if colcout <> ACol then
        begin
          // modifs en cours
          SetModifsEtat;

          RechargerCellule(ACol, ARow);
        end;
    end;
  end;
end;

procedure traiteExternesouris;
var
  ACol: integer;
  ARow: integer;
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  if EditionEnCours = false then
    exit;

  ACol := FormBase.ListeUtilisationEXT.Col;
  ARow := FormBase.ListeUtilisationEXT.Row;

  if not ForcerRestitution then
    if (CelluleEnCoursCol = ACol) and (CelluleEnCoursRow = ARow) then
      exit;

  EditionEnCours := false;

  // modifs en cours
  SetModifsEtat;

  RechargerCellule(CelluleEnCoursCol, CelluleEnCoursRow);
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure ExterneChargeChaines(index: integer; var nbrec: integer);

  function LireChaine(param: string): string;
  begin
    result := FormBase.TabFichExter.FieldByName(param).AsString;
  end;

  function LireValeur(param: string): currency;
  begin
    result := FormBase.TabFichExter.FieldByName(param).Ascurrency;
  end;

  procedure Charge(nblig: integer);
  var
    temp: string;
    valeur: currency;
  begin
    TableauTemp[0, nblig] := '';

    TableauTemp[1, nblig] := LireChaine('ext_enr_libellenom');

    temp := FormatSortie(colcout);
    valeur := LireValeur('ext_cout');
    temp := FormatCurr('0.00', valeur);
    TableauTemp[2, nblig] := temp;
    TableauvalTemp[2, nblig] := valeur;
  end;

var
  t: string;
  nblig: integer;

begin
  with FormBase.TabFichExter do
  begin
    // preparer le filtre recherche des enreg contenant 'index'
    str(index, t);
    filter := 'id_hebdo = ' + t;
    filtered := true;

    Open;
    setkey;

    nbrec := recordcount;
    if nbrec <> 0 then
    begin
      // entree dans le mode de modification
      SetLength(TableauTemp, 11, nbrec + 1);
      SetLength(TableauvalTemp, 11, nbrec + 1);
      edit;

      for nblig := 0 to (nbrec - 1) do
      begin
        // se positionner sur 1er enreg ou sur suivant
        if nblig = 0 then
          first
        else
          next;

        // charger liste de Externe
        Charge(nblig);
      end;
    end;

    // termine la lecture de la table
    close;
    filtered := false;
  end;
end;

procedure ExterneSupprime(index: integer);
var
  t: string;
begin
  with FormBase.TabFichExter do
  begin
    // preparer le filtre recherche des enreg contenant 'index'
    str(index, t);
    filter := 'id_hebdo = ' + t;
    filtered := true;

    Open;
    setkey;

    // supprimer tous les enregs
    while recordcount <> 0 do
      delete;

    // termine la suppression dans la table
    close;
    filtered := false;
  end;
end;

procedure ResetCumulsExterne;
begin
  nbEXT := 0;

  SetLength(TblNomEXT, 2, nbEXT);
  SetLength(TblvalEXT, 1, nbEXT);
end;

procedure CumulsExterne(idh: integer);

  function ChercheAjoute(nom: string; var ind: integer): boolean;
  var
    i: integer;
    t: string;
  begin
    result := true;

    if nbEXT = 0 then
      exit;
    for i := 0 to (nbEXT - 1) do
    begin
      t := TblNomEXT[1, i];
      if t = nom then
      begin
        ind := i;
        result := false;
        exit;
      end;
    end;
  end;

var
  ind: integer;
  nbrec: integer;
  i: integer;
  C: currency;
begin
  // charger les pointages
  ExterneChargeChaines(idh, nbrec);

  // si aucuns pointage sortir
  if nbrec = 0 then
    exit;

  // traiter tous les pointages
  for i := 0 to (nbrec - 1) do
  begin
    // verifier si le pointage existe deja
    if ChercheAjoute(TableauTemp[1, i], ind) then
    begin
      // ajoute une ligne au tableau
      inc(nbEXT);
      SetLength(TblNomEXT, 2, nbEXT);
      SetLength(TblvalEXT, 1, nbEXT);

      // stocker nom et type ligne de pointage
      ind := nbEXT - 1;
      TblNomEXT[0, ind] := '';
      TblNomEXT[1, ind] := TableauTemp[1, i];

      // raz valeur nouvelle ligne
      TblvalEXT[0, ind] := 0;
    end;

    // cumuler a la ligne en cours la nouvelle ligne
    C := TableauvalTemp[2, i];
    C := TblvalEXT[0, ind] + C;
    TblvalEXT[0, ind] := C;
  end;
end;

procedure CumulsExterneLire(var nbr: integer);
var
  i: integer;
  t: string;
begin
  nbr := nbEXT;
  if nbEXT = 0 then
    exit;

  // pour stocker tous les pointages
  SetLength(TableauTemp, 11, nbEXT);

  for i := 0 to (nbEXT - 1) do
  begin
    TableauTemp[0, i] := TblNomEXT[0, i];
    TableauTemp[1, i] := TblNomEXT[1, i];

    t := FormatSortie(colcout);
    t := FormatCurr('0.00', TblvalEXT[0, i]);
    TableauTemp[2, i] := t;
  end;
end;
/// ///////////////////////////////////////////////////////////////////////////

end.
