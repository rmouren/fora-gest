unit MasqueChantiers;

interface

procedure TraiteListeChantiers;
procedure TraiteNumChantier;
procedure SetModeCreationChantier;
procedure VerifieListesChantier;
procedure TraiteFicheChantier;
function ChoixChantierOK: boolean;
function LireIdentifiantChantier: integer;

procedure setSaisieNumCha(txt: string);

implementation

uses
  SysUtils,
  utils,
  accesBDD,
  winbase;

const

  ModeConsultationChantier = 0;
  ModeCreationChantier = 1;

Var

  ModeEnCoursChantier: integer;
  ChoixChantier: boolean;
  identifiantchantier: integer;

  /// ///////////////////////////////////////////////////////////////////////////
function GetSaisieNumCha: string;
begin
  result := FormBase.SaisieNumChantier.Text;
end;

procedure setSaisieNumCha(txt: string);
begin
  FormBase.SaisieNumChantier.Text := txt;
end;

procedure TraiteListeChantiers;
{ installe dans le champ numero de chantier le numero choisi dans la liste
}
var
  t: string;
begin
  t := FormBase.ListeChantiers.Text;
  setSaisieNumCha(FillZero(6 - Length(t)) + t);

  if FicheRegul then
    FormBase.ListeMois.SetFocus
  else
    FormBase.saisieNumSemaine.SetFocus;
end;

function LireIdentifiantChantier: integer;
begin
  result := identifiantchantier;
end;

procedure SetChoixChantier;
begin
  ChoixChantier := true;

  with FormBase.Tablechantiers do
    identifiantchantier := fieldbyname('id_chantier').asinteger;
end;

procedure ResetChoixChantier;
begin
  ChoixChantier := false;
end;

function ChoixChantierOK: boolean;
begin
  result := false;
  if ChoixChantier then
    result := true;
end;

procedure SetModeCreationChantier;
begin
  ModeEnCoursChantier := ModeCreationChantier;
end;

procedure SetModeConsultationChantier;
begin
  ModeEnCoursChantier := ModeConsultationChantier;
end;

procedure BoutonsInValide;
begin
  FormBase.BoutonCreation.Enabled := false;
  FormBase.BoutonConsultation.Enabled := false;
end;

procedure BoutonsCreationInValide;
begin
  FormBase.BoutonCreation.Enabled := false;
end;

procedure BoutonsConsultationInValide;
begin
  FormBase.BoutonConsultation.Enabled := false;
end;

procedure BoutonsCreationValide;
begin
  FormBase.BoutonCreation.Enabled := true;
end;

procedure BoutonsConsultationValide;
begin
  FormBase.BoutonConsultation.Enabled := true;
end;

function GetModeChantier: integer;
begin
  result := ModeEnCoursChantier;
end;

function IsModeCreationChantier: boolean;
begin
  result := false;
  if GetModeChantier = ModeCreationChantier then
    result := true;
end;

function IsModeConsultationChantier: boolean;
begin
  result := false;
  if GetModeChantier = ModeConsultationChantier then
    result := true;
end;

procedure VerifieListesChantier;
{ verifie la validite des choix dans les listes secteur, commercial et client }
begin
  with FormBase do
  begin
    // verifie si lignes select�es dans les listes
    if ListeClient.KeyValue = -1 then
    begin
      // un choix de chantier pas encore valide
      ResetChoixChantier;
      exit;
    end;

    if ListeSecteurs.KeyValue = -1 then
    begin
      // un choix de chantier pas encore valide
      ResetChoixChantier;
      exit;
    end;

    if ListeCommerciaux.KeyValue = -1 then
    begin
      // un choix de chantier pas encore valide
      ResetChoixChantier;
      exit;
    end;
  end;

  if IsModeConsultationChantier then
    BoutonsConsultationValide
  else
    BoutonsCreationValide;

  // un choix de chantier valide
  SetChoixChantier;
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure TraiteNumChantier;
{ analyse la saisie du numero de chantier
}
  function ChercheChantier(chantier: string): boolean;
  begin
    with FormBase.Tablechantiers do
    begin
      close;
      indexname := 'indcha';
      open;
      setkey;
      fieldbyname('cha_numchantier').asstring := chantier;
      result := gotokey;
    end;
  end;

  procedure FermerCherche;
  begin
    with FormBase.Tablechantiers do
    begin
      close;
      indexname := '';
      open;
    end;
  end;

  procedure ResetListes;
  begin
    with FormBase do
    begin
      // aucunes lignes select�es dans les listes
      ListeClient.KeyValue := -1;
      ListeSecteurs.KeyValue := -1;
      ListeCommerciaux.KeyValue := -1;
    end;
  end;

  procedure SetListes;
  var
    pnt: integer;
  begin
    with FormBase.Tablechantiers do
    begin
      // lire pointeur clients et pointer la liste
      pnt := fieldbyname('id_client').asinteger;
      FormBase.ListeClient.KeyValue := pnt;

      // lire pointeur agence et pointer la liste
      pnt := fieldbyname('id_agence').asinteger;
      FormBase.ListeSecteurs.KeyValue := pnt;

      // lire pointeur commercial et pointer la liste
      pnt := fieldbyname('id_commercial').asinteger;
      FormBase.ListeCommerciaux.KeyValue := pnt;
    end;
  end;

  procedure ChargeCommentaires;
  var
    temp: string;
  begin
    with FormBase.Tablechantiers do
    begin
      temp := fieldbyname('cha_nom').asstring;
      FormBase.NomChantier.Text := temp;

      temp := fieldbyname('cha_libelle').asstring;
      FormBase.LibelleChantier.Text := temp;
    end;
  end;

  procedure ResetCommentaires;
  begin
    FormBase.NomChantier.Text := '';
    FormBase.LibelleChantier.Text := '';
  end;

var
  temp: string;

begin
  // verifier que le numero de chantier existe
  temp := GetSaisieNumCha;

  // rendre les boutons d'acces invalide
  BoutonsInValide;

  // preparer recherche
  if ChercheChantier(temp) then
  begin
    // selection des parametres du chantier
    SetListes;

    // charge les commentaires de chantier
    ChargeCommentaires;

    // mise en mode consultation
    SetModeConsultationChantier;

    // un choix de chantier valide
    SetChoixChantier;
  end
  else
  begin
    // pas de ligne select�e
    ResetListes;

    // efface les commentaires de chantier
    ResetCommentaires;

    // mise en mode creation
    SetModeCreationChantier;

    // un choix de chantier pas encore valide
    ResetChoixChantier;
  end;

  // verifie etat des listes chantier
  VerifieListesChantier;
end;

procedure TraiteFicheChantier;
{ selon le mode de traitement en cours des chantiers modifie un enregistrement
  de chantier ou en creer un nouveau }
var
  temp: string;
  i: integer;
begin
  // traitement de la table chantier
  with FormBase.Tablechantiers do
  begin
    if IsModeConsultationChantier then
    begin
      // entree en mode de modification
      edit;
    end
    else
    begin
      // entree en mode de creation
      insert;

      // enreg le numero saisi de chantier
      temp := GetSaisieNumCha;
      i := StrToInt(temp);
      fieldbyname('cha_numchantier').asinteger := i;
    end;

    // enregistre les pointeurs sur les listes
    i := FormBase.ListeClient.KeyValue;
    fieldbyname('id_client').asinteger := i;

    i := FormBase.ListeSecteurs.KeyValue;
    fieldbyname('id_agence').asinteger := i;

    i := FormBase.ListeCommerciaux.KeyValue;
    fieldbyname('id_commercial').asinteger := i;

    // enregistre les commentaires sur le chantier
    temp := FormBase.NomChantier.Text;
    fieldbyname('cha_nom').asstring := temp;

    temp := FormBase.LibelleChantier.Text;
    fieldbyname('cha_libelle').asstring := temp;

    // valider les enregistrements dans la table
    post;
  end;

  // rendre invalide le bouton qui a lance l'action
  if IsModeConsultationChantier then
    BoutonsConsultationInValide
  else
    BoutonsCreationInValide;

  // pour ne pas recreer le meme chantier basculer en consultation
  SetModeConsultationChantier;

  // un choix de chantier valide
  SetChoixChantier;
end;

/// ///////////////////////////////////////////////////////////////////////////

end.
