unit GCforaPATCH;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, inifiles;

type
  TForm1 = class(TForm)
    TableTemp: TTable;
    TableEmployes: TTable;
    Database: TDatabase;
    procedure FormCreate(Sender: TObject);
  private
    { D�clarations priv�es}
  public
    { D�clarations publiques}
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

function NomBaseDonnees:string;
var
   chemin:     string;
   ok:         boolean;
   DelphiIni:  TIniFile;
begin
     chemin := ExtractFilePath(application.exename);

     DelphiIni := TIniFile.Create(chemin + '\config.ini');
     ok := DelphiIni.ValueExists ('BASE', 'nom');

     if ok then
        result := DelphiIni.ReadString('BASE', 'nom', 'gcFORA')
     else
        result := 'gcFORA';
end;

procedure PatchEmployesP3;

   procedure EnregEMP(nom: string; taux: currency; inter, actuel: boolean);
   begin
     with Form1.TableEmployes do
          begin
               open;
               insert;
               FieldByName('emp_nomemploye').Asstring    := nom;
               FieldByName('emp_tauxhoraire').Ascurrency := taux;
               FieldByName('emp_interimaire').Asboolean  := inter;
               FieldByName('emp_actuel').Asboolean       := actuel;
               post;
               close;
          end;
   end;

var
   nb:       integer;
   i:        integer;
   nom:      string;
   taux:     currency;
   inter:    boolean;
   actuel:   boolean;
begin
     with Form1.TableEmployes do
          begin
               open;
               nb := RecordCount;
               close;

               if nb = 0 then
                  exit;
          end;

     // renommer ancienne table
     with Form1.TableTemp do
          begin
               Active := false;
               TableName := 'employes';
               RenameTable('tempo');
          end;

     // creation nouvelle table
     with Form1.TableEmployes do
          begin
                Active := false;
                TableName := 'employes';
                FieldDefs.Clear;
                FieldDefs.Add('id_employe',              ftAutoInc,   0, True);
                FieldDefs.Add('emp_nomemploye',          ftString,   30, False);
                FieldDefs.Add('emp_tauxhoraire',         ftCurrency,  0, false);
                FieldDefs.Add('emp_interimaire',         ftBoolean,   0, False);
                FieldDefs.Add('emp_actuel',              ftBoolean,   0, False);
                IndexDefs.Clear;
                IndexDefs.Add('', 'id_employe', [ixPrimary, ixUnique]);
                CreateTable;
          end;

     // transferer tempo vers employes
     with Form1.TableTemp do
          begin
               filter := '';
               filtered := false;
               indexname := '';
               open;
               setkey;
               first;
               nb := RecordCount;
               for i := 1 to nb do
                   begin
                        nom    := FieldByName('emp_nomemploye').Asstring;
                        taux   := FieldByName('emp_tauxhoraire').Ascurrency;
                        inter  := FieldByName('emp_interimaire').Asboolean;
                        actuel := FieldByName('emp_actuel').Asboolean;
                        EnregEMP(nom, taux, inter, actuel);
                        next;
                   end;
               close;

               // supprimer tempo
               deletetable;
          end;
end;

procedure PatcheIndexSupp;
begin
     with Form1.TableTemp do
          begin
               Active := false;

               TableName := 'vehicules';
               addindex('indveh', 'veh_libellenom', [ixCaseInsensitive]);

               TableName := 'employes';
               addindex('indemp', 'emp_nomemploye', [ixCaseInsensitive]);

               TableName := 'diamant';
               addindex('inddia', 'tra_libellenom', [ixCaseInsensitive]);

               TableName := 'externe';
               addindex('indext', 'ext_libellenom', [ixCaseInsensitive]);

               TableName := 'structure';
               addindex('indstr', 'str_libellenom', [ixCaseInsensitive]);

               TableName := 'materiels';
               addindex('indmat', 'mat_libellenom', [ixCaseInsensitive]);
          end;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
   nomalias:    string;
begin

     // change l'emplacement de PDOXUSRS.NET pour Windows 7 et 8 .
     Session.NetFileDir :=  GetCurrentDir;

     // verifie si l'alias n'existe pas et dans ce cas sortie
     nomalias := NomBaseDonnees;
     if not Session.IsAlias(nomalias) then
     begin
          showmessage('Il y a un probleme avec l''alias de la base de donnees');
          exit;
     end;

     { Le composant Table ne doit pas �tre actif }
     Form1.TableTemp.Active := false;

     // attribution de l'alias a la BDD
     Form1.Database.Connected := false;
     Form1.Database.AliasName := nomalias;
     Form1.Database.Connected := true;

     PatchEmployesP3;

     PatcheIndexSupp;

     application.Terminate;
end;

end.
