unit MasqueAchats;

interface

procedure ResetCumulsAchat;
procedure CumulsAchat(idh: integer);
procedure CumulsAchatsLire(var nbr: integer);
procedure AchatViderListe;
procedure AchatInitMasque;
procedure AchatExecuterCalcul;
function AchatLireCalcul: currency;
procedure AchatEnreg(numhebdo: integer);
procedure AchatCharge(index: integer);
procedure AchatChargeChaines(index: integer; var nbrec: integer);
procedure AchatSupprime(index: integer);
procedure AchatModifsNon;
procedure AchatModifsOui;
function AchatLireModifsEtat: boolean;

function traiteAchatLireFormatCellules(ACol: integer): string;
procedure traiteAchatsouris;
procedure traiteAchatInitialiserProcEffaceLigne(X, Y: integer);
function TraiteAchatSelection(ACol, ARow: integer): boolean;
procedure TraiteAchatValide(var Key: Char);
procedure traiteAchattouches(Key: word);
procedure traiteAchattouchesFIN();
procedure TraiteAchatChargeLigne;
procedure TraiteAchatEffacerLigne;

implementation

uses

  SysUtils, grids, Dialogs, windows,
  winbase, accesBDD, Hebdo, impression;

const

  colindex = 0;
  colNom = 0;
  colcout = 1;

  coldeselection = 10;
  colautoselection = colcout;

type

  TlargeurLigne = array [colNom .. colcout] of integer;

const

  Largeur: TlargeurLigne = (175, 60);

var

  TblNomACH: array of array of string;
  TblValACH: array of array of currency;
  nbACH: integer;

  positioncolonne: integer = 1;
  positionligne: integer;
  LigneEnCours: boolean = true;
  TableauAchats: array of array of currency;
  CelluleEnCours: boolean;
  EditionEnCours: boolean;
  CelluleEnCoursCol: integer;
  CelluleEnCoursRow: integer;
  Suppressionencours: boolean;
  ModeLigneAppend: boolean;

  ModifsEtat: boolean;
  AutoriseModifs: boolean;

  SommeACH: currency;

  /// ///////////////////////////////////////////////////////////////////////////
procedure unselectACH;
begin
  Suppressionencours := true;
  FormBase.ListeUtilisationACH.Col := coldeselection;
  FormBase.ListeUtilisationACH.Row := 0;
  Suppressionencours := false;
end;

procedure AchatInitMasque;
var
  i: integer;
begin
  for i := colindex to colcout do
    FormBase.ListeUtilisationACH.ColWidths[i] := Largeur[i];

  for i := colcout + 1 to coldeselection do
    FormBase.ListeUtilisationACH.ColWidths[i] := 0;

  unselectACH;
end;

procedure AchatModifsOui;
begin
  AutoriseModifs := true;
  FormBase.ListeAchats.Enabled := true;
end;

procedure AchatModifsNon;
begin
  unselectACH;
  AutoriseModifs := false;
  FormBase.ListeAchats.Enabled := false;
  FormBase.ListeAchats.KeyValue := -1;
end;

function AchatLireModifsEtat: boolean;
begin
  result := ModifsEtat;
end;

procedure SetModifsEtat;
begin
  ModifsEtat := true;

  // prevenir qu'il y a eu une modif d'un masque
  EvenementModifMasqueAnnexe;
end;

procedure ResetModifsEtat;
begin
  ModifsEtat := false;
end;

procedure AchatViderListe;

  procedure viderligne(fin: integer);
  var
    C: integer;
  begin
    for C := colindex to colcout do
      FormBase.ListeUtilisationACH.Cells[C, fin - 1] := '';
  end;

begin
  // pour ne pas repasser par le traitement evenement masque
  LigneEnCours := true;

  while FormBase.ListeUtilisationACH.RowCount > 1 do
  begin
    positioncolonne := 0;
    positionligne := FormBase.ListeUtilisationACH.RowCount - 2;
    TraiteAchatEffacerLigne;
  end;

  positioncolonne := 1;
  LigneEnCours := false;
end;

function VerifMaxiOK(ACol: integer; valeur: currency): boolean;
begin
  result := false;
  case ACol of
    colcout:
      if (valeur < -99999.99) or (valeur > 99999.99) then
        exit;
  end;
  result := true;
end;

function traiteAchatLireFormatCellules(ACol: integer): string;
begin
  case ACol of
    colNom:
      result := '';
    colcout:
      result := '';
  end;
end;

function FormatSortie(Col: integer): string;
begin
  case Col of
    colcout:
      result := '00000.00';
  else
    result := '';
  end;
end;

Procedure RechargerCellule(Col, lig: integer);
var
  valeur: currency;
  temp: string;
begin
  valeur := TableauAchats[Col, lig];
  temp := FormatCurr(FormatSortie(Col), valeur);
  FormBase.ListeUtilisationACH.Cells[Col, lig] := temp;
end;

function AchatLireCalcul: currency;
begin
  result := SommeACH
end;

procedure CalculAchat(lig: integer);
var
  C, Somme: currency;
  temp: string;
begin
  C := TableauAchats[colcout, lig];

  Somme := (C);

  // somme du Achat
  temp := FormatCurr(FormatSortie(colcout), Somme);
  FormBase.ListeUtilisationACH.Cells[colcout, lig] := temp;
end;

procedure AchatExecuterCalcul;
var
  nbl: integer;
  t: currency;
  i: integer;
begin
  t := 0;
  nbl := FormBase.ListeUtilisationACH.RowCount - 1;
  if nbl >= 0 then
  begin
    for i := 0 to (nbl - 1) do
      t := t + TableauAchats[colcout, i];
  end;

  SommeACH := t;

  // lance le calcul generalde la fiche Hebdo;
  CalculGeneralHebdo;
end;

function CurrToInt(valeur: currency): integer;
begin
  result := StrToInt(CurrToStr(valeur));
end;

function IntToCurr(valeur: integer): currency;
begin
  result := StrToCurr(IntToStr(valeur));
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure AchatEnreg(numhebdo: integer);
var
  nbe: integer;
  C: currency;
  l: integer;
  s: string;

  procedure ChercheEnreg(ligne: integer);
  var
    C: currency;
    id: integer;
  begin
    C := TableauAchats[colindex, ligne];

    id := CurrToInt(C);
    FormBase.TabFichAcha.editkey;
    FormBase.TabFichAcha.FieldByName('id_Achat').asinteger := id;
    FormBase.TabFichAcha.gotokey;
    FormBase.TabFichAcha.edit;
  end;

begin
  // supprimer les enregs precedents
  AchatSupprime(numhebdo);
  ModeLigneAppend := false;

  nbe := FormBase.ListeUtilisationACH.RowCount - 1;
  if (nbe = 0) then
    exit;

  // pas de mofifs en cours
  ResetModifsEtat;

  with FormBase.TabFichAcha do
  begin
    Open;

    if nbe <> 0 then
    begin
      // creer chaque ligne et la remplir
      for l := 0 to (nbe - 1) do
      begin
        if ModeLigneAppend = false then
          // creer nouveau enregistrement de ligne
          insert
        else
          // chercher enreg existant
          ChercheEnreg(l);

        s := FormBase.ListeUtilisationACH.Cells[colNom, l];
        FieldByName('int_enr_libellenom').AsString := s;

        C := TableauAchats[colcout, l];
        FieldByName('int_cout').Ascurrency := C;

        FieldByName('id_hebdo').asinteger := numhebdo;

        // valider nouvelle ligne avec ces valeurs
        post;
      end;
    end;

    // fermeture de la table apres ajouts
    close;
  end;

  // sortie du mode de modification
  ModeLigneAppend := false;
end;

procedure ChargeLigneExiste;
var
  nc: integer;
  nblig: integer;

  idemp: integer;
  monaie: currency;
  nomemp: string;

  procedure Ecrire(Col, lig: integer; valeur: currency);
  var
    t: string;
  begin
    t := FormatCurr(FormatSortie(Col), valeur);
    TableauAchats[Col, lig] := valeur;
    FormBase.ListeUtilisationACH.Cells[Col, lig] := t;
  end;

begin
  with FormBase.TabFichAcha do
  begin

    // definition de la ligne de Achat
    nblig := FormBase.ListeUtilisationACH.RowCount - 1;

    SetLength(TableauAchats, 10, nblig + 1);

    idemp := FieldByName('id_Achat').asinteger;
    TableauAchats[colindex, nblig] := IntToCurr(idemp);

    nomemp := FieldByName('int_enr_libellenom').AsString;
    FormBase.ListeUtilisationACH.Cells[colNom, nblig] := nomemp;

    // debut chargement liste ecran
    LigneEnCours := true;

    monaie := FieldByName('int_cout').Ascurrency;
    Ecrire(colcout, nblig, monaie);

    // calculer la ligne
    CalculAchat(nblig);

    // fin chargement liste ecran
    LigneEnCours := false;

    // defintion de la prochaine ligne
    nc := FormBase.ListeUtilisationACH.RowCount;
    inc(nc);
    FormBase.ListeUtilisationACH.RowCount := nc;

    // selection de la 1ere colonne a editer
    FormBase.ListeUtilisationACH.SetFocus;
    FormBase.ListeUtilisationACH.Col := colcout;
    FormBase.ListeUtilisationACH.Row := nblig;
  end;
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure AchatCharge(index: integer);
var
  nbrec: integer;
  t: string;
  nblig: integer;
begin
  ChargeConfiguration;

  // pas de mofifs en cours
  ResetModifsEtat;

  // definition des lignes de Achat
  FormBase.ListeUtilisationACH.RowCount := 1;

  with FormBase.TabFichAcha do
  begin
    // recherche des enreg contenant 'index'

    // preparer le filtre
    str(index, t);
    filter := 'id_hebdo = ' + t;
    filtered := true;

    Open;
    setkey;

    nbrec := recordcount;
    if nbrec <> 0 then
    begin
      // entree dans le mode de modification
      ModeLigneAppend := true;
      edit;

      for nblig := 0 to (nbrec - 1) do
      begin
        // se positionner sur 1er enreg ou sur suivant
        if nblig = 0 then
          first
        else
          next;

        // charger liste de Achat
        ChargeLigneExiste;
      end;
    end;

    // termine la lecture de la table
    close;
    filtered := false;

    // recalcul tout
    AchatExecuterCalcul;
  end;
end;

procedure GetEnregAchatCourant(var i: integer; var n: string);
begin
  with FormBase.TableSecteurs do
  begin
    edit;
    i := FieldValues['id_agence'];
    n := FieldByName('age_libellenom').AsString;
    post;
  end;
end;

procedure TraiteAchatChargeLigne;
var
  nc: integer;
  nblig: integer;

  idemp: integer;
  nomemp: string;

  function ChercheExiste(nblig: integer; nomemp: string): boolean;
  var
    i: integer;
    t: string;
  begin
    result := false;
    for i := 0 to nblig - 1 do
    begin
      t := FormBase.ListeUtilisationACH.Cells[colNom, i];
      if nomemp = trim(t) then
        result := true;
    end;
  end;

  procedure Ecrire(Col, lig: integer; valeur: currency);
  var
    t: string;
  begin
    t := FormatCurr(FormatSortie(Col), valeur);
    TableauAchats[Col, lig] := valeur;
    FormBase.ListeUtilisationACH.Cells[Col, lig] := t;
  end;

begin
  // inclure une ligne dans la liste de Achat

  if FormBase.TableSecteurs.recordcount = 0 then
    exit;

  // modifs en cours
  SetModifsEtat;

  // lire l'enreg correspondant
  GetEnregAchatCourant(idemp, nomemp);

  ChargeConfiguration;

  // definition de la ligne d'Achat
  nblig := FormBase.ListeUtilisationACH.RowCount - 1;

  // verifier si nb maxi de lignes atteint
  if nblig = nbligach then
    exit;

  if nblig <> 0 then
  begin
    // verifier si le nom existe dans liste alors ne pas charger
    if ChercheExiste(nblig, nomemp) then
      exit;
  end;

  SetLength(TableauAchats, 10, nblig + 1);

  TableauAchats[colindex, nblig] := IntToCurr(idemp);
  FormBase.ListeUtilisationACH.Cells[colNom, nblig] := nomemp;

  LigneEnCours := true; // debut chargement liste ecran

  Ecrire(colcout, nblig, 0);

  // calculer la ligne
  CalculAchat(nblig);

  LigneEnCours := false; // fin chargement liste ecran

  // defintion de la prochaine ligne
  nc := FormBase.ListeUtilisationACH.RowCount;
  inc(nc);
  FormBase.ListeUtilisationACH.RowCount := nc;

  // selection de la 1ere colonne a editer
  FormBase.ListeUtilisationACH.SetFocus;
  FormBase.ListeUtilisationACH.Col := colcout;
  FormBase.ListeUtilisationACH.Row := nblig;

  // recalcul tout
  AchatExecuterCalcul;
end;

function TraiteAchatSelection(ACol, ARow: integer): boolean;
var
  OK: boolean;
  i: integer;
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    result := false;
    exit;
  end;

  result := true;
  OK := true;

  // utile pour la suppression de la derniere ligne
  if Suppressionencours and (ACol = 10) then
    exit;

  // verifier si depassement nbr de lignes
  i := FormBase.ListeUtilisationACH.RowCount - 1;
  if ARow >= i then
    OK := false;

  if (ACol = colcout) and OK then
  begin
    // autoriser l'edition de la cellule
    with FormBase.ListeUtilisationACH do
      options := options + [goEditing];

    RechargerCellule(ACol, ARow);
  end
  else
  begin
    // interdire l'edition et la selection de la cellule
    with FormBase.ListeUtilisationACH do
      options := options - [goEditing];
    result := false;
    exit;
  end;
end;

procedure TraiteAchatValide(var Key: Char);
var
  ACol: integer;
  ARow: integer;
  temp: string;
  valeur: currency;
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    Key := chr(0);
    exit;
  end;

  if (Key = ' ') then
  begin
    // ne traite pas ces touches
    Key := chr(0);
    exit;
  end;

  if (Key = '.') then
  begin;
    // modifs en cours
    SetModifsEtat;

    // pour faciliter la saisie des nombres decimaux
    Key := ',';

    exit;
  end;

  // valide la saisie
  if Key = chr(13) then
  begin
    // modifs en cours
    SetModifsEtat;

    // Stocker la saisie entree au clavier
    ACol := FormBase.ListeUtilisationACH.Col;
    ARow := FormBase.ListeUtilisationACH.Row;

    try
      begin
        // Stocker la saisie entree au clavier
        temp := FormBase.ListeUtilisationACH.Cells[ACol, ARow];
        valeur := StrToCurr(trim(temp));

        // test maxi
        if VerifMaxiOK(ACol, valeur) then
        begin
          TableauAchats[ACol, ARow] := valeur;
          temp := FormatCurr(FormatSortie(ACol), valeur);
          FormBase.ListeUtilisationACH.Cells[ACol, ARow] := temp;
        end
        else
          // valeur hors de la bone maximum donc recharge
          RechargerCellule(ACol, ARow);

        // calculer la ligne
        CalculAchat(ARow);
      end;

      // recalcul tout
      AchatExecuterCalcul;

    except
      // si erreur de saisie dans la cellule
      on EConvertError do
        RechargerCellule(ACol, ARow);
    end;
  end;

  // si touche numerique
  CelluleEnCours := false;
  EditionEnCours := false;
  if (ord(Key) > 47) and (ord(Key) < 58) or (Key = '-') or (Key = '+') then
  begin
    // modifs en cours
    SetModifsEtat;

    CelluleEnCours := true;
    EditionEnCours := true;

    // Stocker la saisie entree au clavier
    CelluleEnCoursCol := FormBase.ListeUtilisationACH.Col;
    CelluleEnCoursRow := FormBase.ListeUtilisationACH.Row;
  end;
end;

procedure traiteAchatInitialiserProcEffaceLigne(X, Y: integer);
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  with FormBase.ListeUtilisationACH do
    MouseToCell(X, Y, positioncolonne, positionligne)
end;

procedure TraiteAchatEffacerLigne;
var
  nbl: integer;
  i: integer;

  procedure transfert(deb, fin: integer);
  var
    l: integer;
    C: integer;
    t: currency;
  begin
    for l := deb to fin - 2 do
    begin
      for C := colindex to colcout do
      begin
        with FormBase.ListeUtilisationACH do
          Cells[C, l] := Cells[C, l + 1];

        t := TableauAchats[C, l + 1];
        TableauAchats[C, l] := t;
      end;
    end;
  end;

  procedure viderligne(fin: integer);
  var
    C: integer;
  begin
    for C := colindex to colcout do
      FormBase.ListeUtilisationACH.Cells[C, fin - 1] := '';
  end;

begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  // suppression ligne de Achat selectionnée
  if (positioncolonne = colNom) then
  begin
    // modifs en cours
    SetModifsEtat;

    // diminuer d'une ligne verifier si depassement nbr de lignes
    nbl := FormBase.ListeUtilisationACH.RowCount - 1;
    if positionligne >= nbl then
      exit;

    // remonter d'une unite toutes les lignes
    if positionligne < nbl then
      transfert(positionligne, nbl);

    if nbl = 1 then
      unselectACH
    else
    begin
      FormBase.ListeUtilisationACH.Col := colautoselection;
      FormBase.ListeUtilisationACH.Row := 0;
    end;

    // effacer derniere ligne
    viderligne(nbl);

    i := FormBase.ListeUtilisationACH.RowCount;
    dec(i);
    FormBase.ListeUtilisationACH.RowCount := i;

    // pour eviter des suppression intenpestives
    positioncolonne := 1;
  end;

  // recalcul tout
  AchatExecuterCalcul;
end;

procedure traiteAchattouchesFIN();
begin
  CelluleEnCours := false;
end;

procedure traiteAchattouches(Key: word);
var
  ACol: integer;
  ARow: integer;

  // recupere position cellule en cours
  procedure Cellule(var ACol: integer; var ARow: integer);
  begin
    ACol := FormBase.ListeUtilisationACH.Col;
    ARow := FormBase.ListeUtilisationACH.Row;
  end;

  function lignes: boolean;
  begin
    result := false;
    if FormBase.ListeUtilisationACH.RowCount > 2 then
      result := true;
  end;

begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  // sortie si 0 lignes
  if not lignes then
    exit;

  if CelluleEnCours = true then
    exit;

  if TestTouchesFleches(Key) or (VK_TAB = Key) then
  begin
    if LigneEnCours = false then
    begin
      // retour en arriere d'une cellule suivant touche
      Cellule(ACol, ARow);
      case Key of
        VK_TAB:
          dec(ACol);
        VK_LEFT:
          inc(ACol);
        VK_RIGHT:
          dec(ACol);
        VK_DOWN:
          if lignes then
            dec(ARow);
        VK_UP:
          if lignes then
            inc(ARow);
      end;

      // reaffichage suite a entree non validee par CRLF
      if CelluleEnCours = false then
        if colcout <> ACol then
        begin
          // modifs en cours
          SetModifsEtat;

          RechargerCellule(ACol, ARow);
        end;
    end;
  end;
end;

procedure traiteAchatsouris;
var
  ACol: integer;
  ARow: integer;
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  if EditionEnCours = false then
    exit;

  ACol := FormBase.ListeUtilisationACH.Col;
  ARow := FormBase.ListeUtilisationACH.Row;

  if not ForcerRestitution then
    if (CelluleEnCoursCol = ACol) and (CelluleEnCoursRow = ARow) then
      exit;

  EditionEnCours := false;

  // modifs en cours
  SetModifsEtat;

  RechargerCellule(CelluleEnCoursCol, CelluleEnCoursRow);
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure AchatChargeChaines(index: integer; var nbrec: integer);

  function LireChaine(param: string): string;
  begin
    result := FormBase.TabFichAcha.FieldByName(param).AsString;
  end;

  function LireValeur(param: string): currency;
  begin
    result := FormBase.TabFichAcha.FieldByName(param).Ascurrency;
  end;

  procedure Charge(nblig: integer);
  var
    temp: string;
    valeur: currency;
  begin
    TableauTemp[0, nblig] := '';

    TableauTemp[1, nblig] := LireChaine('int_enr_libellenom');

    temp := FormatSortie(colcout);
    valeur := LireValeur('int_cout');
    temp := FormatCurr('0.00', valeur);
    TableauTemp[2, nblig] := temp;
    TableauvalTemp[2, nblig] := valeur;
  end;

var
  t: string;
  nblig: integer;

begin
  with FormBase.TabFichAcha do
  begin
    // preparer le filtre recherche des enreg contenant 'index'
    str(index, t);
    filter := 'id_hebdo = ' + t;
    filtered := true;

    Open;
    setkey;

    nbrec := recordcount;
    if nbrec <> 0 then
    begin
      // entree dans le mode de modification
      SetLength(TableauTemp, 11, nbrec + 1);
      SetLength(TableauvalTemp, 11, nbrec + 1);
      edit;

      for nblig := 0 to (nbrec - 1) do
      begin
        // se positionner sur 1er enreg ou sur suivant
        if nblig = 0 then
          first
        else
          next;

        // charger liste de Achat
        Charge(nblig);
      end;
    end;

    // termine la lecture de la table
    close;
    filtered := false;
  end;
end;

procedure AchatSupprime(index: integer);
var
  t: string;
begin
  with FormBase.TabFichAcha do
  begin
    // preparer le filtre recherche des enreg contenant 'index'
    str(index, t);
    filter := 'id_hebdo = ' + t;
    filtered := true;

    Open;
    setkey;

    // supprimer tous les enregs
    while recordcount <> 0 do
      delete;

    // termine la suppression dans la table
    close;
    filtered := false;
  end;
end;

procedure ResetCumulsAchat;
begin
  nbACH := 0;

  SetLength(TblNomACH, 2, nbACH);
  SetLength(TblValACH, 1, nbACH);
end;

procedure CumulsAchat(idh: integer);

  function ChercheAjoute(nom: string; var ind: integer): boolean;
  var
    i: integer;
    t: string;
  begin
    result := true;

    if nbACH = 0 then
      exit;
    for i := 0 to (nbACH - 1) do
    begin
      t := TblNomACH[1, i];
      if t = nom then
      begin
        ind := i;
        result := false;
        exit;
      end;
    end;
  end;

var
  ind: integer;
  nbrec: integer;
  i: integer;
  C: currency;
begin
  // charger les pointages
  AchatChargeChaines(idh, nbrec);

  // si aucuns pointage sortir
  if nbrec = 0 then
    exit;

  // traiter tous les pointages
  for i := 0 to (nbrec - 1) do
  begin
    // verifier si le pointage existe deja
    if ChercheAjoute(TableauTemp[1, i], ind) then
    begin
      // ajoute une ligne au tableau
      inc(nbACH);
      SetLength(TblNomACH, 2, nbACH);
      SetLength(TblValACH, 1, nbACH);

      // stocker nom et type ligne de pointage
      ind := nbACH - 1;
      TblNomACH[0, ind] := '';
      TblNomACH[1, ind] := TableauTemp[1, i];

      // raz valeur nouvelle ligne
      TblValACH[0, ind] := 0;
    end;

    // cumuler a la ligne en cours la nouvelle ligne
    C := TableauvalTemp[2, i];
    C := TblValACH[0, ind] + C;
    TblValACH[0, ind] := C;
  end;
end;

procedure CumulsAchatsLire(var nbr: integer);
var
  i: integer;
  t: string;
begin
  nbr := nbACH;
  if nbACH = 0 then
    exit;

  // pour stocker tous les pointages
  SetLength(TableauTemp, 11, nbACH);

  for i := 0 to (nbACH - 1) do
  begin
    TableauTemp[0, i] := TblNomACH[0, i];
    TableauTemp[1, i] := TblNomACH[1, i];

    t := FormatSortie(colcout);
    t := FormatCurr('0.00', TblValACH[0, i]);
    TableauTemp[2, i] := t;
  end;
end;
/// ///////////////////////////////////////////////////////////////////////////

end.
