unit accesBDD;

interface

uses

  Windows, UITypes, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs,
  StdCtrls, Mask, DBCtrls, ExtCtrls, Db, DBTables, inifiles, UtilsBDE,
  Hebdo, MasqueChantiers, utils, MasqueSemaine,
  winbase, Ficheconfig, MasqueEmployes, FicheEtats;

function LireTypeSemaineBis(numsemaine: integer): boolean;
function TestAnneeEnCours: boolean;
function TestMoisEnCours(NMois: integer): boolean;

function PointerHebdo(idh: integer): boolean;

function ChercheHebdo(c, b: boolean; idcha: integer; num: integer): boolean;
function LireNombreFiches(mode, num: integer): integer;
function ChercheNomChantier: string;
function ChercheNumeroChantier: string;
function ChercheNomClient: string;
function ChercheCouleurCommercial: integer;
function LirePointeurCommercial: integer;

function LireNombreCommerciaux: integer;
procedure CommercialPremier;
procedure CommercialSuivant;
function LireIdentifiantCommercial: integer;

function LireNBJmois(mois: integer): integer;

procedure HebdoChargeChaines;
procedure SetTableConfig(parametre: string; valeur: variant);
procedure GetTableConfig(parametre: string; var valeur: variant);

function VerificationBDD: boolean;
function CreationBDD: boolean;
function ModificationBDD: boolean;

procedure tablemodifoui;
procedure tablemodifnon;
procedure tablemodifverif(table: TTable);

procedure ChargeConfiguration;

procedure ChargeHebdo;
procedure SauveHebdo;
procedure SupprimeHebdo;

procedure VerrouON;
procedure VerrouOFF;

function ModificationBDDajoutTablecumuls: boolean;
procedure RAZTableCumuls;

procedure PatchEmployesP3;
procedure PatcheIndexSupp;
function TesterNecessiteP3: boolean;
function TesterNecessiteP4: boolean;

var

  AnneeCourante: integer;
  FicheRegul: boolean;
  moisfichecourante: integer;
  TauxHoraireSup: currency;
  TauxHoraireSupInt: currency;
  TauxChargeBrut: currency;
  CoutPetitPagnier: currency;
  CoutGrandPagnier: currency;
  FraisInduits: currency;
  FraisOutilsConso: currency;
  ChargeEncours: boolean;

  coutfixevehicule: currency;
  coutfixemateriel: currency;
  coutfixestructure: currency;
  nbjoursmois: array [1 .. 12] of integer;

  TypeSemaineBis: array [1 .. 52] of boolean;

  VerifierVerrou: boolean;

implementation

type
  TtotauxFiche = array [0 .. 13] of currency;

var
  configtablemodifs: boolean;

{$H-}

function ChercheNumeroChantier: string;
{ renvoie le numero de chantier du chantier en cours selectionne dans la BDD }
var
  t: string;
  n: integer;
begin
  // lire numero du chantier
  n := FormBase.Tablechantiers.FieldByName('cha_numchantier').asinteger;
  t := inttostr(n);
  result := FillZero(6 - Length(t)) + t;
end;

function ChercheNomChantier: string;
{ renvoie le nom de chantier du chantier en cours selectionne dans la BDD }
begin
  result := FormBase.Tablechantiers.FieldByName('cha_nom').asstring;
end;

function LirePointeurCommercial: integer;
begin
  result := FormBase.Tablechantiers.FieldByName('id_commercial').asinteger;
end;

function LireIdentifiantCommercial: integer;
begin
  result := FormBase.TableCommerciaux.FieldByName('id_commercial').asinteger;
end;

procedure CommercialPremier;
begin
  FormBase.TableCommerciaux.first;
end;

procedure CommercialSuivant;
begin
  FormBase.TableCommerciaux.next;
end;

function LireNombreCommerciaux: integer;
begin
  result := FormBase.TableCommerciaux.RecordCount;
end;

function ChercheCouleurCommercial: integer;
{ renvoie la couleur attribuee au commercial du chantier en cours selectionne
  dans la BDD }
var
  id: integer;
begin
  // id := FormBase.Tablechantiers.FieldByName('id_commercial').asinteger;
  id := LirePointeurCommercial;

  with FormBase.TableCommerciaux do
  begin
    open;
    editkey;
    FieldByName('id_commercial').asinteger := id;
    GotoKey;
    result := FieldByName('com_couleur').asinteger;
    close;
    active := true;
  end;
end;

function ChercheNomClient: string;
{ renvoie le nom du client du chantier en cours selectionne dans la BDD }
var
  id: integer;
begin
  id := FormBase.Tablechantiers.FieldByName('id_client').asinteger;

  with FormBase.TableClients do
  begin
    open;
    editkey;
    FieldByName('id_client').asinteger := id;
    GotoKey;
    result := FieldByName('cli_libellenom').asstring;
    close;
    active := true;
  end;
end;

function TestAnneeEnCours: boolean;
var
  annee: integer;
begin
  result := false;
  annee := FormBase.TableHebdo.FieldByName('heb_annee').asinteger;
  if annee = AnneeCourante then
    result := true;
end;

function TestMoisEnCours(NMois: integer): boolean;
var
  regul: boolean;
  m: integer;
  s: integer;
  st, sd, sf: integer;
begin
  result := false;

  regul := FormBase.TableHebdo.FieldByName('heb_regul').asboolean;
  if regul then
  begin
    m := FormBase.TableHebdo.FieldByName('heb_numeromois').asinteger;

    if m <= NMois then
      result := true;
  end
  else
  begin
    s := FormBase.TableHebdo.FieldByName('heb_numerosemaine').asinteger;

    EcartSemaine(1, AnneeCourante, sd, st);
    EcartSemaine(NMois, AnneeCourante, st, sf);

    if (s >= sd) and (s <= sf) then
      result := true;
  end;
end;

function LireNombreFiches(mode, num: integer): integer;
{ cette routine renvoie le nombre de fiches pour une configuration donnee

  mode        0     renvoie le nombre total de fiches enregistrees

  1     renvoie le nombre total de fiches de regularisation

  2     renvoie le nombre total de fiches hebdomadaires
}
begin
  result := 0;

  with FormBase.TableHebdo do
  begin
    case mode of
      0:
        begin
          filtered := false;
          indexname := '';
          open;
          setkey;
          first;
          result := RecordCount;
          close;
          exit;
        end;
      1:
        begin
          //
        end;
      2:
        begin
          //
        end;
    end;

    { for i := 1 to 12 do
      begin
      Str(i, t);
      fieldbyname(cleindex).asstring := t;
      first;
      nb := recordcount;
      nbe := 0;

      for n := 1 to nb do
      begin
      l := FieldByName(cleindex).asinteger;
      if l <> i then
      begin
      err := true;
      nbe := nbe + 1;
      end;

      next;
      end;

      close;

      end;
    }
  end;
end;

function PointerHebdo(idh: integer): boolean;
begin
  with FormBase.TableHebdo do
  begin
    close;
    filter := '';
    filtered := false;
    indexname := '';
    open;
    setkey;
    FieldByName('id_hebdo').asstring := inttostr(idh);

    result := GotoKey;
  end;
end;

{ routine de recherche d'une fiche dans la base de donnees
  (a noter gestion de l'annee en cours)

  c         vrai  fiche de regularisation recherchee
  faux  fiche hebdomadaire recherchee

  b         lorsqu'il s'agit d'une fiche hebdomadaire

  vrai  fiche hebdomadaire bis recherchee appartenant au mois
  suivant la semaine traitee

  faux  fiche hebdomadaire recherchee appartenant au mois de la
  semaine traitee

  idcha     identifiant pointeur de chantier

  num       numero de semaine pour la fiche hebdo
  numero de mois pour la fiche regul

  renvoie

  vrai    fiche trouvee
  faux    fiche introuvable
}

function ChercheHebdo(c, b: boolean; idcha: integer; num: integer): boolean;
var
  temp: string;
  nb: integer;
  i: integer;
  ok: boolean;
  vl: integer;
begin
  // filtrer sur l'identifiant de chantier 'id_chantier'
  with FormBase.TableHebdo do
  begin
    // preparer le filtre de recherche
    close;
    str(idcha, temp);
    filter := 'id_chantier = ' + temp;
    filtered := true;

    if c then
      indexname := 'indmo'
    else
      indexname := 'indse';

    open;
    setkey;

    // recherche de la semaine ou du moisnumero
    str(num, temp);
    if c then
      FieldByName('heb_numeromois').asstring := temp
    else
      FieldByName('heb_numerosemaine').asstring := temp;

    // chercher et sortie si rien trouv�
    result := false;
    ok := GotoKey;
    if not ok then
      exit;

    nb := RecordCount;
    if c then
    begin
      // verifie si fiche de regularisation existe
      for i := 1 to nb do
      begin
        ok := FieldByName('heb_regul').asboolean;
        vl := FieldByName('heb_numeromois').asinteger;
        if ok and TestAnneeEnCours and (vl = num) then
        begin
          // regul trouvee alors sortie
          result := true;
          exit;
        end;

        // cherche dans suivant
        next;
      end;
    end
    else
    begin
      // traitement recherche fiche bis si necessaire
      for i := 1 to nb do
      begin
        ok := FieldByName('heb_semainebis').asboolean;
        vl := FieldByName('heb_numerosemaine').asinteger;
        if (ok = b) and TestAnneeEnCours and (vl = num) then
        begin
          // hebdo trouvee alors sortie
          result := true;
          exit;
        end;

        // cherche dans suivant
        next;
      end;
    end;
  end;
end;

procedure SetTableConfig(parametre: string; valeur: variant);
{ enregistre un parametre de la configuration }
begin
  with FormBase.TableTemp do
  begin
    TableName := 'config';
    open;
    edit;
    FormBase.TableTemp.FieldValues[parametre] := valeur;
    post;
    close;
  end;
end;

procedure GetTableConfig(parametre: string; var valeur: variant);
{ lit un parametre de la configuration }
begin
  with FormBase.TableTemp do
  begin
    TableName := 'config';
    open;
    edit;
    valeur := FormBase.TableTemp.FieldValues[parametre];
    post;
    close;
  end;
end;

function LireNBJmois(mois: integer): integer;
{ renvoie le nombre de jours defini dans la base de donnes pour un mois donne }
var
  valeur: variant;
  temp: string;
begin
  case mois of
    1:
      temp := 'cfg_nbj_jan';
    2:
      temp := 'cfg_nbj_fev';
    3:
      temp := 'cfg_nbj_mar';
    4:
      temp := 'cfg_nbj_avr';
    5:
      temp := 'cfg_nbj_mai';
    6:
      temp := 'cfg_nbj_jui';
    7:
      temp := 'cfg_nbj_jul';
    8:
      temp := 'cfg_nbj_aou';
    9:
      temp := 'cfg_nbj_sep';
    10:
      temp := 'cfg_nbj_oct';
    11:
      temp := 'cfg_nbj_nov';
    12:
      temp := 'cfg_nbj_dec';
  end;

  GetTableConfig(temp, valeur);
  result := valeur;
end;

function veriftable(nomtable: string): boolean;
{ renvoie vrai si une table existe dans la base de donnees sinon faux }
begin
  result := false;

  // verifie l'existence d'une table de la base de donnees
  FormBase.TableTemp.TableName := nomtable;
  if not FormBase.TableTemp.Exists then
  begin
    showmessage('Il y a un probleme avec la table : ' + nomtable);
    exit;
  end;

  result := true;
end;

function NomBaseDonnees: string;
var
  chemin: string;
  ok: boolean;
  DelphiIni: TIniFile;
begin
  chemin := ExtractFilePath(application.exename);

  DelphiIni := TIniFile.Create(chemin + '\config.ini');
  ok := DelphiIni.ValueExists('BASE', 'nom');

  if ok then
    result := DelphiIni.ReadString('BASE', 'nom', 'gcFORA')
  else
    result := 'gcFORA';
end;

function VerificationBDD: boolean;
{ cette routine verifie l'existence de la base de donnees renvoie faux dans le
  cas d'abscence sinon renvoie vrai }
var
  nomalias: string;
begin
  result := false;

  // verifie si l'alias n'existe pas et dans ce cas sortie
  nomalias := NomBaseDonnees;
  if not Session.IsAlias(nomalias) then
  begin
    showmessage('Il y a un probleme avec l''alias de la base de donnees');
    exit;
  end;

  { Le composant Table ne doit pas �tre actif }
  FormBase.TableTemp.active := false;

  // attribution de l'alias a la BDD
  FormBase.Database.Connected := false;
  FormBase.Database.AliasName := nomalias;
  FormBase.Database.Connected := true;

  // verifie l'existence des tables de la base de donnees
  if not veriftable('config') then
    exit;
  if not veriftable('employes') then
    exit;
  if not veriftable('fiche_pointage') then
    exit;
  if not veriftable('clients') then
    exit;
  if not veriftable('commercial') then
    exit;
  if not veriftable('secteurs') then
    exit;
  if not veriftable('fiche_achat') then
    exit;
  if not veriftable('chantiers') then
    exit;
  if not veriftable('fiche_hebdo') then
    exit;
  if not veriftable('materiels') then
    exit;
  if not veriftable('fiche_materiel') then
    exit;
  if not veriftable('fiche_vehicule') then
    exit;
  if not veriftable('fiche_externe') then
    exit;
  if not veriftable('fiche_structure') then
    exit;
  if not veriftable('fiche_diamant') then
    exit;
  if not veriftable('vehicules') then
    exit;
  if not veriftable('externe') then
    exit;
  if not veriftable('structure') then
    exit;
  if not veriftable('diamant') then
    exit;
  // if not veriftable('cumuls') then exit;

  // toutes les tables sont pretes a l'emploi
  result := true;
end;

procedure CreationTableCumuls;

  procedure creationgroupe(groupe: string; nbvaleurs: integer);
  var
    v: integer;
    t: string;
  begin
    with FormBase.TableTemp do
    begin
      TableName := 'cumuls' + groupe;

      FieldDefs.Clear;

      t := groupe + '_';
      FieldDefs.Add(t + 'idnom', ftInteger, 0, false);
      FieldDefs.Add(t + 'nom', ftString, 0, false);

      for v := 0 to (nbvaleurs - 1) do
        FieldDefs.Add(t + inttostr(v), ftString, 0, false);

      IndexDefs.Clear;
      IndexDefs.Add('', t + 'idnom', [ixPrimary]);
      // , ixUnique   ixCaseInsensitive

      CreateTable;
    end;
  end;

var
  i: integer;

begin
  // lancement cr�ation
  with FormBase.TableTemp do
  begin
    { Le composant Table ne doit pas �tre actif }
    active := false;

    { En premier, description de la table qui doit �tre nomm�e }
    databasename := 'BASEGCFORA';
    TableType := ttParadox;

    // -------------------------------------------------------------//
    TableName := 'cumuls';

    FieldDefs.Clear;
    FieldDefs.Add('numsemaine', ftInteger, 0, false);
    FieldDefs.Add('enrcumul', ftBoolean, 0, false);

    FieldDefs.Add('empnb', ftInteger, 0, false);
    FieldDefs.Add('intnb', ftInteger, 0, false);
    FieldDefs.Add('vehnb', ftInteger, 0, false);
    FieldDefs.Add('strnb', ftInteger, 0, false);
    FieldDefs.Add('dianb', ftInteger, 0, false);
    FieldDefs.Add('matnb', ftInteger, 0, false);
    FieldDefs.Add('achnb', ftInteger, 0, false);
    FieldDefs.Add('extnb', ftInteger, 0, false);

    for i := 0 to 13 do
      FieldDefs.Add('totaux_' + inttostr(i), ftCurrency, 0, false);

    IndexDefs.Clear;
    IndexDefs.Add('', 'numsemaine', [ixPrimary, ixUnique]);

    CreateTable;
  end;

  creationgroupe('emp', 9);
  creationgroupe('int', 9);
  creationgroupe('veh', 3);
  creationgroupe('str', 3);
  creationgroupe('dia', 4);
  creationgroupe('mat', 3);
  creationgroupe('ach', 1);
  creationgroupe('ext', 1);
end;

procedure RAZTableCumuls;
var
  i: integer;
begin
  with FormBase.TableTemp do
  begin
    TableName := 'cumuls';
    open;

    for i := 1 to 52 do
    begin
      first;
      MoveBy(i - 1);
      edit;
      FieldValues['enrcumul'] := false;
      post;
    end;

    close;
  end;
end;

procedure InitTableCumuls;
var
  i: integer;

  procedure addenreg(nom: string; nbr: integer);
  var
    i: integer;
  begin
    with FormBase.TableTemp do
    begin
      TableName := 'cumuls' + nom;
      open;

      for i := 0 to (nbr - 1) do
      begin
        insert;
        FieldByName(nom + '_idnom').asinteger := 12;
        post;
      end;

      close;
    end;
  end;

begin
  with FormBase.TableTemp do
  begin
    TableName := 'cumuls';
    open;

    for i := 1 to 52 do
    begin
      insert;
      FieldByName('numsemaine').asinteger := i;
      post;
    end;

    close;
  end;
  {
    addenreg('emp', 14);
    addenreg('int', 2);
    addenreg('veh', 2);
    addenreg('str', 3);
    addenreg('dia', 3);
    addenreg('mat', 22);
    addenreg('ach', 4);
    addenreg('ext', 11);
  }
end;

procedure CreationBaseJeuEssai;
{ creation d'un minimum de fiches de base de l'application et definition de la
  configuration }

  procedure ouvretable(table: TTable);
  begin
    table.open;
  end;

  procedure fermetable(table: TTable);
  begin
    table.close;
  end;

  procedure Employes;

    procedure ajoute(t: string; c: currency; a, i: boolean);
    begin
      with FormBase.TableEmployes do
      begin
        insert;
        FieldByName('emp_nomemploye').asstring := t;
        FieldByName('emp_tauxhoraire').asCurrency := c;
        FieldByName('emp_actuel').asboolean := a;
        FieldByName('emp_interimaire').asboolean := i;
        post;
      end;
    end;

  begin
    ouvretable(FormBase.TableEmployes);

    ajoute('ANGEL', 9.00, true, false);
    ajoute('BENAMARA', 11.40, true, false);
    ajoute('BERMOND', 11.40, true, false);
    ajoute('ESPOSITO', 12.20, true, false);
    ajoute('FRANCOIS', 12.50, true, false);
    ajoute('GARCIA', 9.50, true, false);
    ajoute('LEFE', 18.00, true, true);
    ajoute('LEGENDRE', 12.50, true, false);
    ajoute('LEMAIN', 12.50, true, false);
    ajoute('LEQUESNE', 10.36, true, false);
    ajoute('MALHAOUI', 18.00, true, true);
    ajoute('MANNINI', 9.30, true, false);
    ajoute('SALHI', 9.50, true, false);
    ajoute('SPATH', 10.56, true, false);
    ajoute('TERMINI', 18.00, true, true);
    ajoute('TORRE', 18.00, true, true);

    fermetable(FormBase.TableEmployes);
  end;

  procedure Materiels;

    procedure ajoute(t: string; c: currency);
    begin
      with FormBase.TableMateriels do
      begin
        insert;
        FieldByName('mat_libellenom').asstring := t;
        FieldByName('mat_cout').asCurrency := c;
        post;
      end;
    end;

  begin
    ouvretable(FormBase.TableMateriels);

    ajoute('Carotteuse hydraulique', 60.00);
    ajoute('Carotteuse 220V', 30.00);
    ajoute('Carotteuse 380V', 35.00);
    ajoute('Carotteuse 220V portable', 20.00);
    ajoute('Scie murale hydraulique HYD', 110.00);
    ajoute('Scie murale PENTRUDER', 120.00);
    ajoute('Scie electrique HYD', 110.00);
    ajoute('Scie a sol DB', 122.00);
    ajoute('Scie a sol electrique TARGET', 152.00);
    ajoute('Scie a cable EURODIMA', 150.00);
    ajoute('Scie a cable electrique', 150.00);
    ajoute('Echafaudage alu', 20.00);
    ajoute('Rainureuse + aspirateur', 50.00);
    ajoute('Verin Enerpac', 15.00);
    ajoute('Palan electrique', 38.00);
    ajoute('Ponceuse', 30.00);
    ajoute('Poste a souder', 20.00);
    ajoute('Transpalette', 8.00);
    ajoute('Groupe 40 KVA y/c GO', 70.00);
    ajoute('Remorque', 15.00);
    ajoute('Dynatest + Durometre', 50.00);
    ajoute('Perceuse magnetique', 8.00);
    ajoute('Materiel divers', 6.00);

    fermetable(FormBase.TableMateriels);
  end;

  procedure Diamants;

    procedure ajoute(t: string; c: currency);
    begin
      with FormBase.TableDiamant do
      begin
        insert;
        FieldByName('tra_libellenom').asstring := t;
        FieldByName('tra_cout').asCurrency := c;
        post;
      end;
    end;

  begin
    ouvretable(FormBase.TableDiamant);

    ajoute('Sciage', 41.00);
    ajoute('Carottage', 32.00);
    ajoute('Cablage', 100.00);

    fermetable(FormBase.TableDiamant);
  end;

  procedure Vehicules;

    procedure ajoute(t: string; c: currency);
    begin
      with FormBase.TableVehicules do
      begin
        insert;
        FieldByName('veh_libellenom').asstring := t;
        FieldByName('veh_cout').asCurrency := c;
        post;
      end;
    end;

  begin
    ouvretable(FormBase.TableVehicules);

    ajoute('FORA', 45.00);
    ajoute('LOCATION', 53.00);

    fermetable(FormBase.TableVehicules);
  end;

  procedure Externes;

    procedure ajoute(t: string);
    begin
      with FormBase.TableExterne do
      begin
        insert;
        FieldByName('ext_libellenom').asstring := t;
        post;
      end;
    end;

  begin
    ouvretable(FormBase.TableExterne);

    ajoute('Assurance Decennale');
    ajoute('AUXILIAIRE');
    ajoute('BRONZO');
    ajoute('JMS');
    ajoute('LOXAM / SEIDITA');
    ajoute('Peage');
    ajoute('STL');
    ajoute('TIERCELIN');

    fermetable(FormBase.TableExterne);
  end;

  procedure Structures;

    procedure ajoute(t: string; c: currency);
    begin
      with FormBase.TableStructures do
      begin
        insert;
        FieldByName('str_libellenom').asstring := t;
        FieldByName('str_cout').asCurrency := c;
        post;
      end;
    end;

  begin
    ouvretable(FormBase.TableStructures);

    ajoute('STR 1', 180.00);
    ajoute('STR 2', 100.00);
    ajoute('STR 3', 140.00);

    fermetable(FormBase.TableStructures);
  end;

  procedure Agences;

    procedure ajoute(t: string);
    begin
      with FormBase.TableSecteurs do
      begin
        insert;
        FieldByName('age_libellenom').asstring := t;
        post;
      end;
    end;

  begin
    ouvretable(FormBase.TableSecteurs);

    ajoute('CARROS');
    ajoute('LANGUEDOC');
    ajoute('LES PENNES MIRABEAU');

    fermetable(FormBase.TableSecteurs);
  end;

  procedure Commerciaux;

    procedure ajoute(t: string; c: integer);
    begin
      with FormBase.TableCommerciaux do
      begin
        insert;
        FieldByName('com_libellenom').asstring := t;
        FieldByName('com_couleur').asinteger := c;
        post;
      end;
    end;

  begin
    ouvretable(FormBase.TableCommerciaux);

    ajoute('Fernand PEREZ', 12);
    ajoute('Frederique POISSON', 1);
    ajoute('Michel SAPPEI', 10);

    fermetable(FormBase.TableCommerciaux);
  end;

  procedure Clients;

    procedure ajoute(t: string);
    begin
      with FormBase.TableClients do
      begin
        insert;
        FieldByName('cli_libellenom').asstring := t;
        post;
      end;
    end;

  begin
    ouvretable(FormBase.TableClients);

    ajoute('SPI BATIGNOLES');
    ajoute('CAMPENON BLANCARDE');
    ajoute('TORRENTINO');
    ajoute('CEGELEC');
    ajoute('DUMEZ BTP');
    ajoute('CAMPENON');
    ajoute('AREP');
    ajoute('SOLETANCHE');
    ajoute('SOREN');
    ajoute('ATELIER');
    ajoute('MOP');

    fermetable(FormBase.TableClients);
  end;

begin
  Employes;
  Materiels;
  Diamants;
  Vehicules;
  Externes;
  Structures;
  Agences;
  Commerciaux;
  Clients;
end;

function TesterNecessiteP3: boolean;
var
  i: integer;
  nb: integer;
  t: string;
begin
  result := true;

  with FormBase.TableEmployes do
  begin
    active := false;

    IndexDefs.update;
    nb := IndexDefs.Count;
    for i := 0 to nb - 1 do
    begin
      t := IndexDefs.Items[i].name;
      if t = 'indexi' then
        exit;
    end;

    result := false;
  end;
end;

function TesterNecessiteP4: boolean;
var
  i: integer;
  ok: boolean;
begin
  with FormBase.TableEmployes do
  begin
    ok := false;
    active := false;
    IndexDefs.update;
    for i := 0 to IndexDefs.Count - 1 do
    begin
      if IndexDefs.Items[i].name = 'indemp' then
      begin
        ok := true;
        break;
      end;
    end;
  end;

  result := false;
  if not ok then
  begin
    result := true;
    exit;
  end;

  with FormBase.TableEmployes do
  begin
    active := false;
    open;
    indexname := 'indemp';
    close;
  end;

  with FormBase.TableVehicules do
  begin
    active := false;
    open;
    indexname := 'indveh';
    close;
  end;

  with FormBase.TableDiamant do
  begin
    active := false;
    open;
    indexname := 'inddia';
    close;
  end;

  with FormBase.TableExterne do
  begin
    active := false;
    open;
    indexname := 'indext';
    close;
  end;

  with FormBase.TableStructures do
  begin
    active := false;
    open;
    indexname := 'indstr';
    close;
  end;

  with FormBase.TableMateriels do
  begin
    active := false;
    open;
    indexname := 'indmat';
    close;
  end;
end;

procedure PatchEmployesP3;

  procedure EnregEMP(nom: string; taux: currency; inter, actuel: boolean);
  begin
    with FormBase.TableEmployes do
    begin
      open;
      insert;
      FieldByName('emp_nomemploye').asstring := nom;
      FieldByName('emp_tauxhoraire').asCurrency := taux;
      FieldByName('emp_interimaire').asboolean := inter;
      FieldByName('emp_actuel').asboolean := actuel;
      post;
      close;
    end;
  end;

var
  nb: integer;
  i: integer;
  nom: string;
  taux: currency;
  inter: boolean;
  actuel: boolean;
begin
  with FormBase.TableEmployes do
  begin
    open;
    nb := RecordCount;
    close;

    if nb = 0 then
      exit;
  end;

  // renommer ancienne table
  with FormBase.TableTemp do
  begin
    active := false;
    TableName := 'employes';
    RenameTable('tempo');
  end;

  // creation nouvelle table
  with FormBase.TableEmployes do
  begin
    active := false;
    TableName := 'employes';
    FieldDefs.Clear;
    FieldDefs.Add('id_employe', ftAutoInc, 0, true);
    FieldDefs.Add('emp_nomemploye', ftString, 30, false);
    FieldDefs.Add('emp_tauxhoraire', ftCurrency, 0, false);
    FieldDefs.Add('emp_interimaire', ftBoolean, 0, false);
    FieldDefs.Add('emp_actuel', ftBoolean, 0, false);
    IndexDefs.Clear;
    IndexDefs.Add('', 'id_employe', [ixPrimary, ixUnique]);
    CreateTable;
  end;

  // transferer tempo vers employes
  with FormBase.TableTemp do
  begin
    filter := '';
    filtered := false;
    indexname := '';
    open;
    setkey;
    first;
    nb := RecordCount;
    for i := 1 to nb do
    begin
      nom := FieldByName('emp_nomemploye').asstring;
      taux := FieldByName('emp_tauxhoraire').asCurrency;
      inter := FieldByName('emp_interimaire').asboolean;
      actuel := FieldByName('emp_actuel').asboolean;
      EnregEMP(nom, taux, inter, actuel);
      next;
    end;
    close;

    // supprimer tempo
    deletetable;
  end;
end;

procedure PatcheIndexSupp;
begin
  with FormBase.TableTemp do
  begin
    active := false;

    TableName := 'vehicules';
    addindex('indveh', 'veh_libellenom', [ixCaseInsensitive]);

    TableName := 'employes';
    addindex('indemp', 'emp_nomemploye', [ixCaseInsensitive]);

    TableName := 'diamant';
    addindex('inddia', 'tra_libellenom', [ixCaseInsensitive]);

    TableName := 'externe';
    addindex('indext', 'ext_libellenom', [ixCaseInsensitive]);

    TableName := 'structure';
    addindex('indstr', 'str_libellenom', [ixCaseInsensitive]);

    TableName := 'materiels';
    addindex('indmat', 'mat_libellenom', [ixCaseInsensitive]);
  end;
end;

procedure CreationTables;
var
  temp: string;
  t: string;
  i: integer;
begin
  // lancement cr�ation
  with FormBase.TableTemp do
  begin
    { Le composant Table ne doit pas �tre actif }
    active := false;

    { En premier, description de la table qui doit �tre nomm�e }
    databasename := 'BASEGCFORA';
    TableType := ttParadox;

    /// /////////////////////////////////////////////////////////
    TableName := 'vehicules';

    FieldDefs.Clear;
    FieldDefs.Add('id_vehicule', ftAutoInc, 0, true);
    FieldDefs.Add('veh_libellenom', ftString, 30, false);
    FieldDefs.Add('veh_cout', ftCurrency, 0, false);

    IndexDefs.Clear;
    IndexDefs.Add('', 'id_vehicule', [ixPrimary, ixUnique]);
    IndexDefs.Add('indveh', 'veh_libellenom', [ixCaseInsensitive]);

    CreateTable;
    /// /////////////////////////////////////////////////////////
    TableName := 'fiche_vehicule';

    FieldDefs.Clear;
    FieldDefs.Add('id_transport', ftAutoInc, 0, true);
    FieldDefs.Add('id_hebdo', ftInteger, 0, true);
    FieldDefs.Add('veh_enr_libellenom', ftString, 30, false);
    FieldDefs.Add('veh_enr_cout', ftCurrency, 0, false);
    FieldDefs.Add('veh_dureepret', ftCurrency, 0, false);
    FieldDefs.Add('veh_nombre', ftCurrency, 0, false);
    FieldDefs.Add('veh_total', ftCurrency, 0, false);

    IndexDefs.Clear;
    IndexDefs.Add('', 'id_transport', [ixPrimary, ixUnique]);
    IndexDefs.Add('indhebveh', 'id_hebdo', [ixCaseInsensitive]);

    CreateTable;
    /// /////////////////////////////////////////////////////////

    /// /////////////////////////////////////////////////////////
    TableName := 'structure';

    FieldDefs.Clear;
    FieldDefs.Add('id_fraisstructure', ftAutoInc, 0, true);
    FieldDefs.Add('str_libellenom', ftString, 30, false);
    FieldDefs.Add('str_cout', ftCurrency, 0, false);

    IndexDefs.Clear;
    IndexDefs.Add('', 'id_fraisstructure', [ixPrimary, ixUnique]);
    IndexDefs.Add('indstr', 'str_libellenom', [ixCaseInsensitive]);

    CreateTable;
    /// /////////////////////////////////////////////////////////
    TableName := 'fiche_structure';

    FieldDefs.Clear;
    FieldDefs.Add('id_structure', ftAutoInc, 0, true);
    FieldDefs.Add('id_hebdo', ftInteger, 0, true);
    FieldDefs.Add('str_enr_libellenom', ftString, 30, false);
    FieldDefs.Add('str_enr_cout', ftCurrency, 0, false);
    FieldDefs.Add('veh_dureepret', ftCurrency, 0, false);
    FieldDefs.Add('str_nombre', ftCurrency, 0, false);
    FieldDefs.Add('str_total', ftCurrency, 0, false);

    IndexDefs.Clear;
    IndexDefs.Add('', 'id_structure', [ixPrimary, ixUnique]);
    IndexDefs.Add('indhebstr', 'id_hebdo', [ixCaseInsensitive]);

    CreateTable;
    /// /////////////////////////////////////////////////////////

    /// /////////////////////////////////////////////////////////
    TableName := 'externe';

    FieldDefs.Clear;
    FieldDefs.Add('id_fraisexterne', ftAutoInc, 0, true);
    FieldDefs.Add('ext_libellenom', ftString, 30, false);

    IndexDefs.Clear;
    IndexDefs.Add('', 'id_fraisexterne', [ixPrimary, ixUnique]);
    IndexDefs.Add('indext', 'ext_libellenom', [ixCaseInsensitive]);

    CreateTable;
    /// /////////////////////////////////////////////////////////
    TableName := 'fiche_externe';

    FieldDefs.Clear;
    FieldDefs.Add('id_externe', ftAutoInc, 0, true);
    FieldDefs.Add('id_hebdo', ftInteger, 0, true);
    FieldDefs.Add('ext_enr_libellenom', ftString, 30, false);
    FieldDefs.Add('ext_dureepret', ftCurrency, 0, false);
    FieldDefs.Add('ext_cout', ftCurrency, 0, false);

    IndexDefs.Clear;
    IndexDefs.Add('', 'id_externe', [ixPrimary, ixUnique]);
    IndexDefs.Add('indhebext', 'id_hebdo', [ixCaseInsensitive]);

    CreateTable;
    /// /////////////////////////////////////////////////////////

    /// /////////////////////////////////////////////////////////
    TableName := 'diamant';

    FieldDefs.Clear;
    FieldDefs.Add('id_travail', ftAutoInc, 0, true);
    FieldDefs.Add('tra_libellenom', ftString, 30, false);
    FieldDefs.Add('tra_cout', ftCurrency, 0, false);

    IndexDefs.Clear;
    IndexDefs.Add('', 'id_travail', [ixPrimary, ixUnique]);
    IndexDefs.Add('inddia', 'tra_libellenom', [ixCaseInsensitive]);

    CreateTable;
    /// /////////////////////////////////////////////////////////
    TableName := 'fiche_diamant';

    FieldDefs.Clear;
    FieldDefs.Add('id_diamant', ftAutoInc, 0, true);
    FieldDefs.Add('id_hebdo', ftInteger, 0, true);
    FieldDefs.Add('tra_enr_libellenom', ftString, 30, false);
    FieldDefs.Add('tra_enr_cout', ftCurrency, 0, false);
    FieldDefs.Add('tra_dureepret', ftCurrency, 0, false);
    FieldDefs.Add('tra_nombre', ftCurrency, 0, false);
    FieldDefs.Add('tra_coeffficient', ftCurrency, 0, false);
    FieldDefs.Add('tra_total', ftCurrency, 0, false);

    IndexDefs.Clear;
    IndexDefs.Add('', 'id_diamant', [ixPrimary, ixUnique]);
    IndexDefs.Add('indhebtra', 'id_hebdo', [ixCaseInsensitive]);

    CreateTable;
    /// /////////////////////////////////////////////////////////

    /// /////////////////////////////////////////////////////////
    TableName := 'secteurs';

    FieldDefs.Clear;
    FieldDefs.Add('id_agence', ftAutoInc, 0, true);
    FieldDefs.Add('age_libellenom', ftString, 30, false);

    IndexDefs.Clear;
    IndexDefs.Add('', 'id_agence', [ixPrimary, ixUnique]);

    CreateTable;
    /// /////////////////////////////////////////////////////////
    TableName := 'commercial';

    FieldDefs.Clear;
    FieldDefs.Add('id_commercial', ftAutoInc, 0, true);
    FieldDefs.Add('com_libellenom', ftString, 30, false);
    FieldDefs.Add('com_couleur', ftInteger, 0, false);

    IndexDefs.Clear;
    IndexDefs.Add('', 'id_commercial', [ixPrimary, ixUnique]);

    CreateTable;
    /// /////////////////////////////////////////////////////////
    TableName := 'clients';

    FieldDefs.Clear;
    FieldDefs.Add('id_client', ftAutoInc, 0, true);
    FieldDefs.Add('cli_libellenom', ftString, 30, false);

    IndexDefs.Clear;
    IndexDefs.Add('', 'id_client', [ixPrimary, ixUnique]);
    IndexDefs.Add('indnom', 'cli_libellenom', [ixCaseInsensitive]);

    CreateTable;
    /// /////////////////////////////////////////////////////////
    TableName := 'chantiers';

    FieldDefs.Clear;
    FieldDefs.Add('id_chantier', ftAutoInc, 0, true);
    FieldDefs.Add('cha_numchantier', ftInteger, 0, false);
    FieldDefs.Add('cha_nom', ftString, 30, false);
    FieldDefs.Add('cha_libelle', ftString, 30, false);
    FieldDefs.Add('id_agence', ftInteger, 0, false);
    FieldDefs.Add('id_client', ftInteger, 0, false);
    FieldDefs.Add('id_commercial', ftInteger, 0, false);

    IndexDefs.Clear;
    IndexDefs.Add('', 'id_chantier', [ixPrimary, ixUnique]);
    IndexDefs.Add('indcha', 'cha_numchantier', [ixCaseInsensitive]);

    IndexDefs.Add('indage', 'id_agence', [ixCaseInsensitive]);
    IndexDefs.Add('indcli', 'id_client', [ixCaseInsensitive]);
    IndexDefs.Add('indcom', 'id_commercial', [ixCaseInsensitive]);

    CreateTable;
    /// /////////////////////////////////////////////////////////

    /// /////////////////////////////////////////////////////////
    TableName := 'fiche_achat';

    FieldDefs.Clear;
    FieldDefs.Add('id_achat', ftAutoInc, 0, true);
    FieldDefs.Add('id_hebdo', ftInteger, 0, true);
    FieldDefs.Add('int_enr_libellenom', ftString, 30, false);
    FieldDefs.Add('int_cout', ftCurrency, 0, false);

    IndexDefs.Clear;
    IndexDefs.Add('', 'id_achat', [ixPrimary, ixUnique]);
    IndexDefs.Add('indhebint', 'id_hebdo', [ixCaseInsensitive]);

    CreateTable;
    /// /////////////////////////////////////////////////////////

    /// /////////////////////////////////////////////////////////

    TableName := 'config';

    FieldDefs.Clear;
    FieldDefs.Add('id_config', ftAutoInc, 0, true);
    FieldDefs.Add('cfg_courantannee', ftInteger, 0, false);

    FieldDefs.Add('cfg_coutfixevehicule', ftCurrency, 0, false);
    FieldDefs.Add('cfg_coutfixemateriel', ftCurrency, 0, false);
    FieldDefs.Add('cfg_coutfixestructure', ftCurrency, 0, false);

    FieldDefs.Add('cfg_petitpannier', ftCurrency, 0, false);
    FieldDefs.Add('cfg_grandpannier', ftCurrency, 0, false);
    FieldDefs.Add('cfg_tauxheuresup', ftCurrency, 0, false);
    FieldDefs.Add('cfg_tauxheuresupInt', ftCurrency, 0, false);
    FieldDefs.Add('cfg_facteurcharge', ftCurrency, 0, false);
    FieldDefs.Add('cfg_fraisinduits', ftCurrency, 0, false);
    FieldDefs.Add('cfg_fraisoutilsconso', ftCurrency, 0, false);

    FieldDefs.Add('cfg_nbj_jan', ftInteger, 0, false);
    FieldDefs.Add('cfg_nbj_fev', ftInteger, 0, false);
    FieldDefs.Add('cfg_nbj_mar', ftInteger, 0, false);
    FieldDefs.Add('cfg_nbj_avr', ftInteger, 0, false);
    FieldDefs.Add('cfg_nbj_mai', ftInteger, 0, false);
    FieldDefs.Add('cfg_nbj_jui', ftInteger, 0, false);
    FieldDefs.Add('cfg_nbj_jul', ftInteger, 0, false);
    FieldDefs.Add('cfg_nbj_aou', ftInteger, 0, false);
    FieldDefs.Add('cfg_nbj_sep', ftInteger, 0, false);
    FieldDefs.Add('cfg_nbj_oct', ftInteger, 0, false);
    FieldDefs.Add('cfg_nbj_nov', ftInteger, 0, false);
    FieldDefs.Add('cfg_nbj_dec', ftInteger, 0, false);

    t := 'cfg_typesemaine_';
    for i := 1 to 52 do
    begin
      temp := t + inttostr(i);
      FieldDefs.Add(temp, ftBoolean, 0, false);
    end;

    IndexDefs.Clear;
    IndexDefs.Add('', 'id_config', [ixPrimary, ixUnique]);

    CreateTable;

    /// /////////////////////////////////////////////////////////

    /// /////////////////////////////////////////////////////////
    TableName := 'employes';

    FieldDefs.Clear;
    FieldDefs.Add('id_employe', ftAutoInc, 0, true);
    FieldDefs.Add('emp_nomemploye', ftString, 30, false);
    FieldDefs.Add('emp_tauxhoraire', ftCurrency, 0, false);
    FieldDefs.Add('emp_interimaire', ftBoolean, 0, false);
    FieldDefs.Add('emp_actuel', ftBoolean, 0, false);

    IndexDefs.Clear;
    IndexDefs.Add('', 'id_employe', [ixPrimary, ixUnique]);
    IndexDefs.Add('indemp', 'emp_nomemploye', [ixCaseInsensitive]);

    CreateTable;
    /// /////////////////////////////////////////////////////////
    TableName := 'fiche_pointage';

    FieldDefs.Clear;
    FieldDefs.Add('id_pointage', ftAutoInc, 0, true);
    FieldDefs.Add('id_hebdo', ftInteger, 0, true);

    FieldDefs.Add('emp_enr_nomemploye', ftString, 30, false);
    FieldDefs.Add('emp_enr_tauxhoraire', ftCurrency, 0, false);
    FieldDefs.Add('emp_enr_interimaire', ftBoolean, 0, false);
    FieldDefs.Add('emp_enr_actuel', ftBoolean, 0, false);
    FieldDefs.Add('emp_enr_petitpannier', ftCurrency, 0, false);
    FieldDefs.Add('emp_enr_grandpannier', ftCurrency, 0, false);
    FieldDefs.Add('emp_enr_tauxheuresup', ftCurrency, 0, false);
    FieldDefs.Add('emp_enr_facteurcharge', ftCurrency, 0, false);

    FieldDefs.Add('emp_nbjourtravail', ftCurrency, 0, false);
    FieldDefs.Add('emp_heuresroute', ftCurrency, 0, false);
    FieldDefs.Add('emp_heuresjour', ftCurrency, 0, false);
    FieldDefs.Add('emp_heuressamedi', ftCurrency, 0, false);
    FieldDefs.Add('emp_heuresnuitdimanche', ftCurrency, 0, false);
    FieldDefs.Add('emp_petitdeplacement', ftCurrency, 0, false);
    FieldDefs.Add('emp_granddeplacement', ftCurrency, 0, false);
    FieldDefs.Add('emp_total', ftCurrency, 0, false);

    IndexDefs.Clear;
    IndexDefs.Add('', 'id_pointage', [ixPrimary, ixUnique]);
    IndexDefs.Add('indhebemp', 'id_hebdo', [ixCaseInsensitive]);

    CreateTable;
    /// /////////////////////////////////////////////////////////

    /// /////////////////////////////////////////////////////////
    TableName := 'materiels';

    FieldDefs.Clear;
    FieldDefs.Add('id_materiel', ftAutoInc, 0, true);
    FieldDefs.Add('mat_libellenom', ftString, 30, false);
    FieldDefs.Add('mat_cout', ftCurrency, 0, false);

    IndexDefs.Clear;
    IndexDefs.Add('', 'id_materiel', [ixPrimary, ixUnique]);
    IndexDefs.Add('indmat', 'mat_libellenom', [ixCaseInsensitive]);

    CreateTable;
    /// /////////////////////////////////////////////////////////
    TableName := 'fiche_materiel';

    FieldDefs.Clear;
    FieldDefs.Add('id_utilisation', ftAutoInc, 0, true);
    FieldDefs.Add('id_hebdo', ftInteger, 0, true);
    FieldDefs.Add('mat_enr_libellenom', ftString, 30, false);
    FieldDefs.Add('mat_enr_cout', ftCurrency, 0, false);
    FieldDefs.Add('mat_dureepret', ftCurrency, 0, false);
    FieldDefs.Add('mat_dureeutil', ftCurrency, 0, false);
    FieldDefs.Add('mat_total', ftCurrency, 0, false);

    IndexDefs.Clear;
    IndexDefs.Add('', 'id_utilisation', [ixPrimary, ixUnique]);
    IndexDefs.Add('indhebmat', 'id_hebdo', [ixCaseInsensitive]);

    CreateTable;
    /// /////////////////////////////////////////////////////////

    /// /////////////////////////////////////////////////////////
    TableName := 'fiche_hebdo';

    FieldDefs.Clear;
    FieldDefs.Add('id_hebdo', ftAutoInc, 0, true);
    FieldDefs.Add('heb_numerosemaine', ftInteger, 0, false);
    FieldDefs.Add('heb_semainebis', ftBoolean, 0, false);
    FieldDefs.Add('heb_numeromois', ftInteger, 0, false);
    FieldDefs.Add('heb_verrou', ftBoolean, 0, false);
    FieldDefs.Add('heb_impression', ftBoolean, 0, false);
    FieldDefs.Add('heb_annul', ftBoolean, 0, false);
    FieldDefs.Add('heb_annee', ftInteger, 0, false);
    FieldDefs.Add('heb_regul', ftBoolean, 0, false);
    FieldDefs.Add('heb_nbjourstravail', ftCurrency, 0, false);
    FieldDefs.Add('heb_chiffreaffairedefini', ftCurrency, 0, false);
    FieldDefs.Add('heb_totalsalarie', ftCurrency, 0, false);
    FieldDefs.Add('heb_totalinterim', ftCurrency, 0, false);
    FieldDefs.Add('heb_totalvehicule', ftCurrency, 0, false);
    FieldDefs.Add('heb_totaldiamant', ftCurrency, 0, false);
    FieldDefs.Add('heb_totalmateriel', ftCurrency, 0, false);
    FieldDefs.Add('heb_totaloutils', ftCurrency, 0, false);
    FieldDefs.Add('heb_totalexterne', ftCurrency, 0, false);
    FieldDefs.Add('heb_totalinduits', ftCurrency, 0, false);
    FieldDefs.Add('heb_totalinterne', ftCurrency, 0, false);
    FieldDefs.Add('heb_totaldeboursesec', ftCurrency, 0, false);
    FieldDefs.Add('heb_totalstructures', ftCurrency, 0, false);
    FieldDefs.Add('heb_totalprixrevient', ftCurrency, 0, false);
    FieldDefs.Add('heb_totalresultat', ftCurrency, 0, false);
    FieldDefs.Add('id_chantier', ftInteger, 0, false);

    IndexDefs.Clear;
    IndexDefs.Add('', 'id_hebdo', [ixPrimary, ixUnique]);

    IndexDefs.Add('indse', 'heb_numerosemaine', [ixCaseInsensitive]);
    IndexDefs.Add('indmo', 'heb_numeromois', [ixCaseInsensitive]);
    IndexDefs.Add('indfc', 'id_chantier', [ixCaseInsensitive]);

    CreateTable;
    /// /////////////////////////////////////////////////////////
  end;
end;

procedure CreationDonneeConfig;
var
  Present: TDateTime;
  annee: word;
  mois: word;
  Jour: word;
  t: string;
  temp: string;
  i: integer;
begin
  Present := Now;
  DecodeDate(Present, annee, mois, Jour);
  SetTableConfig('cfg_courantannee', annee);

  SetTableConfig('cfg_tauxheuresup', 0.25);
  SetTableConfig('cfg_tauxheuresupInt', 0.30);
  SetTableConfig('cfg_facteurcharge', 1.82);
  SetTableConfig('cfg_fraisinduits', 4.85);
  SetTableConfig('cfg_fraisoutilsconso', 1.1);

  SetTableConfig('cfg_coutfixevehicule', 640.00);
  SetTableConfig('cfg_coutfixemateriel', 1500.00);
  SetTableConfig('cfg_coutfixestructure', 4000.00);

  SetTableConfig('cfg_petitpannier', 14.90);
  SetTableConfig('cfg_grandpannier', 67.00);

  SetTableConfig('cfg_nbj_jan', 22);
  SetTableConfig('cfg_nbj_fev', 22);
  SetTableConfig('cfg_nbj_mar', 22);
  SetTableConfig('cfg_nbj_avr', 22);
  SetTableConfig('cfg_nbj_mai', 22);
  SetTableConfig('cfg_nbj_jui', 22);
  SetTableConfig('cfg_nbj_jul', 22);
  SetTableConfig('cfg_nbj_aou', 22);
  SetTableConfig('cfg_nbj_sep', 22);
  SetTableConfig('cfg_nbj_oct', 22);
  SetTableConfig('cfg_nbj_nov', 22);
  SetTableConfig('cfg_nbj_dec', 22);

  // definition des nouveaux parametres ajout�s a la configuration
  with FormBase.TableTemp do
  begin
    TableName := 'config';
    open;
    edit;

    t := 'cfg_typesemaine_';
    for i := 1 to 52 do
    begin
      temp := t + inttostr(i);
      FieldValues[temp] := false;
    end;

    post;
    close;
  end;
end;

function ModificationBDD: boolean;
{ modification de la base de donnees (table de configuration) }

var
  nomalias: string;
  t: string;
  repertoire: string;
  msg: string;
  sortie: integer;
  errnom: boolean;
  DelphiIni: TIniFile;

  cfgcourantannee: integer;
  cfgcoutfixevehicule: currency;
  cfgcoutfixemateriel: currency;
  cfgcoutfixestructure: currency;
  cfgpetitpannier: currency;
  cfggrandpannier: currency;
  cfgtauxheuresup: currency;
  cfgtauxheuresupInt: currency;
  cfgfacteurcharge: currency;
  cfgfraisinduits: currency;
  cfgfraisoutilsconso: currency;
  cfgnbjjan: integer;
  cfgnbjfev: integer;
  cfgnbjmar: integer;
  cfgnbjavr: integer;
  cfgnbjmai: integer;
  cfgnbjjui: integer;
  cfgnbjjul: integer;
  cfgnbjaou: integer;
  cfgnbjsep: integer;
  cfgnbjoct: integer;
  cfgnbjnov: integer;
  cfgnbjdec: integer;

  procedure SauverParametresConfigOriginale;
  begin
    with FormBase.TableTemp do
    begin
      TableName := 'config';
      open;
      edit;

      cfgcourantannee := FieldValues['cfg_courantannee'];
      cfgcoutfixevehicule := FieldValues['cfg_coutfixevehicule'];
      cfgcoutfixemateriel := FieldValues['cfg_coutfixemateriel'];
      cfgcoutfixestructure := FieldValues['cfg_coutfixestructure'];
      cfgpetitpannier := FieldValues['cfg_petitpannier'];
      cfggrandpannier := FieldValues['cfg_grandpannier'];
      cfgtauxheuresup := FieldValues['cfg_tauxheuresup'];
      cfgtauxheuresupInt := FieldValues['cfg_tauxheuresupInt'];
      cfgfacteurcharge := FieldValues['cfg_facteurcharge'];
      cfgfraisinduits := FieldValues['cfg_fraisinduits'];
      cfgfraisoutilsconso := FieldValues['cfg_fraisoutilsconso'];
      cfgnbjjan := FieldValues['cfg_nbj_jan'];
      cfgnbjfev := FieldValues['cfg_nbj_fev'];
      cfgnbjmar := FieldValues['cfg_nbj_mar'];
      cfgnbjavr := FieldValues['cfg_nbj_avr'];
      cfgnbjmai := FieldValues['cfg_nbj_mai'];
      cfgnbjjui := FieldValues['cfg_nbj_jui'];
      cfgnbjjul := FieldValues['cfg_nbj_jul'];
      cfgnbjaou := FieldValues['cfg_nbj_aou'];
      cfgnbjsep := FieldValues['cfg_nbj_sep'];
      cfgnbjoct := FieldValues['cfg_nbj_oct'];
      cfgnbjnov := FieldValues['cfg_nbj_nov'];
      cfgnbjdec := FieldValues['cfg_nbj_dec'];

      post;
      close;
    end;
  end;

  procedure RestaurerParametresConfigOriginale;
  var
    temp: string;
    t: string;
    i: integer;
  begin
    with FormBase.TableTemp do
    begin
      TableName := 'config';
      open;
      edit;

      FieldValues['cfg_courantannee'] := cfgcourantannee;
      FieldValues['cfg_coutfixevehicule'] := cfgcoutfixevehicule;
      FieldValues['cfg_coutfixemateriel'] := cfgcoutfixemateriel;
      FieldValues['cfg_coutfixestructure'] := cfgcoutfixestructure;
      FieldValues['cfg_petitpannier'] := cfgpetitpannier;
      FieldValues['cfg_grandpannier'] := cfggrandpannier;
      FieldValues['cfg_tauxheuresup'] := cfgtauxheuresup;
      FieldValues['cfg_tauxheuresupInt'] := cfgtauxheuresupInt;
      FieldValues['cfg_facteurcharge'] := cfgfacteurcharge;
      FieldValues['cfg_fraisinduits'] := cfgfraisinduits;
      FieldValues['cfg_fraisoutilsconso'] := cfgfraisoutilsconso;
      FieldValues['cfg_nbj_jan'] := cfgnbjjan;
      FieldValues['cfg_nbj_fev'] := cfgnbjfev;
      FieldValues['cfg_nbj_mar'] := cfgnbjmar;
      FieldValues['cfg_nbj_avr'] := cfgnbjavr;
      FieldValues['cfg_nbj_mai'] := cfgnbjmai;
      FieldValues['cfg_nbj_jui'] := cfgnbjjui;
      FieldValues['cfg_nbj_jul'] := cfgnbjjul;
      FieldValues['cfg_nbj_aou'] := cfgnbjaou;
      FieldValues['cfg_nbj_sep'] := cfgnbjsep;
      FieldValues['cfg_nbj_oct'] := cfgnbjoct;
      FieldValues['cfg_nbj_nov'] := cfgnbjnov;
      FieldValues['cfg_nbj_dec'] := cfgnbjdec;

      // definition des nouveaux parametres ajout�s a la configuration
      t := 'cfg_typesemaine_';
      for i := 1 to 52 do
      begin
        temp := t + inttostr(i);
        FieldValues[temp] := false;
      end;

      post;
      close;
    end;
  end;

  procedure CreationTableConfiguration;
  var
    temp: string;
    t: string;
    i: integer;
  begin
    with FormBase.TableTemp do
    begin
      { Le composant Table ne doit pas �tre actif }
      active := false;

      { En premier, description de la table qui doit �tre nomm�e }
      databasename := 'BASEGCFORA';
      TableType := ttParadox;

      TableName := 'config';

      FieldDefs.Clear;
      FieldDefs.Add('id_config', ftAutoInc, 0, true);
      FieldDefs.Add('cfg_courantannee', ftInteger, 0, false);

      FieldDefs.Add('cfg_coutfixevehicule', ftCurrency, 0, false);
      FieldDefs.Add('cfg_coutfixemateriel', ftCurrency, 0, false);
      FieldDefs.Add('cfg_coutfixestructure', ftCurrency, 0, false);

      FieldDefs.Add('cfg_petitpannier', ftCurrency, 0, false);
      FieldDefs.Add('cfg_grandpannier', ftCurrency, 0, false);
      FieldDefs.Add('cfg_tauxheuresup', ftCurrency, 0, false);
      FieldDefs.Add('cfg_tauxheuresupInt', ftCurrency, 0, false);
      FieldDefs.Add('cfg_facteurcharge', ftCurrency, 0, false);
      FieldDefs.Add('cfg_fraisinduits', ftCurrency, 0, false);
      FieldDefs.Add('cfg_fraisoutilsconso', ftCurrency, 0, false);

      FieldDefs.Add('cfg_nbj_jan', ftInteger, 0, false);
      FieldDefs.Add('cfg_nbj_fev', ftInteger, 0, false);
      FieldDefs.Add('cfg_nbj_mar', ftInteger, 0, false);
      FieldDefs.Add('cfg_nbj_avr', ftInteger, 0, false);
      FieldDefs.Add('cfg_nbj_mai', ftInteger, 0, false);
      FieldDefs.Add('cfg_nbj_jui', ftInteger, 0, false);
      FieldDefs.Add('cfg_nbj_jul', ftInteger, 0, false);
      FieldDefs.Add('cfg_nbj_aou', ftInteger, 0, false);
      FieldDefs.Add('cfg_nbj_sep', ftInteger, 0, false);
      FieldDefs.Add('cfg_nbj_oct', ftInteger, 0, false);
      FieldDefs.Add('cfg_nbj_nov', ftInteger, 0, false);
      FieldDefs.Add('cfg_nbj_dec', ftInteger, 0, false);

      t := 'cfg_typesemaine_';
      for i := 1 to 52 do
      begin
        temp := t + inttostr(i);
        FieldDefs.Add(temp, ftBoolean, 0, false);
      end;

      IndexDefs.Clear;
      IndexDefs.Add('', 'id_config', [ixPrimary, ixUnique]);

      CreateTable;
    end;
  end;

begin
  result := false;

  t := ExtractFilePath(application.exename);
  repertoire := t + 'DATA\';

  // verification existence repertoire de la base de donnees
{$I-} ChDir(repertoire); {$I+}
  sortie := IOResult;
  if sortie <> 0 then
  begin
    msg := 'Le r�pertoire qui doit contenir ' + #13 + #10;
    msg := msg + 'la base de donnees est inexistant' + #13 + #10;
    msg := msg + repertoire;
    MessageDlg(msg, mtWarning, [mbOk], 0);
    exit;
  end;

  // sortir du repertoire de la base de donnees
  ChDir('..');

  // ouvrir le fichier 'config.ini' et lecture alias
  DelphiIni := TIniFile.Create(t + 'config.ini');
  errnom := true;
  if DelphiIni.ValueExists('BASE', 'nom') then
  begin
    nomalias := DelphiIni.ReadString('BASE', 'nom', '');
    if nomalias <> '' then
      // nom alias trouve
      errnom := false;
  end;

  // si nom alias non trouve
  if errnom then
  begin
    msg := 'Erreur d''acc�s au nom d''alias de la base de donn�es';
    MessageDlg(msg, mtWarning, [mbOk], 0);
    exit;
  end;

  // tester existense alias
  if not Session.IsAlias(nomalias) then
  begin
    msg := 'Erreur d''acc�s a l''alias de la base de donn�es';
    MessageDlg(msg, mtWarning, [mbOk], 0);
    exit;
  end;

  // attribution de l'alias a la BDD
  FormBase.Database.Connected := false;
  FormBase.Database.AliasName := nomalias;
  FormBase.Database.Connected := true;

  // verification table de configuration
  if not veriftable('config') then
    exit;

  // sauver les parametres de la config originale
  SauverParametresConfigOriginale;

  // creation nouvelle table de configuration de la BDD
  CreationTableConfiguration;

  // restaurer les parametres de la config originale
  RestaurerParametresConfigOriginale;

  // modification effectuee
  result := true;
end;

function ModificationBDDajoutTablecumuls: boolean;
var
  nomalias: string;
  t: string;
  repertoire: string;
  msg: string;
  sortie: integer;
  errnom: boolean;
  DelphiIni: TIniFile;

begin
  result := false;

  t := ExtractFilePath(application.exename);
  repertoire := t + 'DATA\';

  // verification existence repertoire de la base de donnees
{$I-} ChDir(repertoire); {$I+}
  sortie := IOResult;
  if sortie <> 0 then
  begin
    msg := 'Le r�pertoire qui doit contenir ' + #13 + #10;
    msg := msg + 'la base de donnees est inexistant' + #13 + #10;
    msg := msg + repertoire;
    MessageDlg(msg, mtWarning, [mbOk], 0);
    exit;
  end;

  // sortir du repertoire de la base de donnees
  ChDir('..');

  // ouvrir le fichier 'config.ini' et lecture alias
  DelphiIni := TIniFile.Create(t + 'config.ini');
  errnom := true;
  if DelphiIni.ValueExists('BASE', 'nom') then
  begin
    nomalias := DelphiIni.ReadString('BASE', 'nom', '');
    if nomalias <> '' then
      // nom alias trouve
      errnom := false;
  end;

  // si nom alias non trouve
  if errnom then
  begin
    msg := 'Erreur d''acc�s au nom d''alias de la base de donn�es';
    MessageDlg(msg, mtWarning, [mbOk], 0);
    exit;
  end;

  // tester existense alias
  if not Session.IsAlias(nomalias) then
  begin
    msg := 'Erreur d''acc�s a l''alias de la base de donn�es';
    MessageDlg(msg, mtWarning, [mbOk], 0);
    exit;
  end;

  // attribution de l'alias a la BDD
  FormBase.Database.Connected := false;
  FormBase.Database.AliasName := nomalias;
  FormBase.Database.Connected := true;

  // verification table de configuration
  if not veriftable('config') then
    exit;

  CreationTableCumuls;
  InitTableCumuls;
  RAZTableCumuls;

  // modification effectuee
  result := true;
end;

function CreationBDD: boolean;
{ creation de la base de donnees (toutes les tables et jeu d'essai) }
var
  nomalias: string;
  t: string;
  repertoire: string;
  msg: string;
  ok: boolean;
  sortie: integer;

  DelphiIni: TIniFile;
begin
  result := false;

  t := ExtractFilePath(application.exename);
  repertoire := t + 'DATA\';

{$I-} MkDir(repertoire); {$I+}
  sortie := IOResult;
  if sortie <> 0 then
  begin
    msg := 'Impossible de cr�er le r�pertoire' + #13 + #10;
    msg := msg + repertoire;
    MessageDlg(msg, mtWarning, [mbOk], 0);
    exit;
  end;

  DelphiIni := TIniFile.Create(t + 'config.ini');
  if DelphiIni.ValueExists('BASE', 'nom') then
  begin
    nomalias := DelphiIni.ReadString('BASE', 'nom', 'gcFORA');
  end
  else
  begin
    msg := 'Entrez le nom de la base de donn�es';
    nomalias := InputBox('', msg, 'gcFORA');
    DelphiIni.writeString('BASE', 'nom', nomalias);
  end;

  with Session do
  begin
    try
      begin
        // verifie si l'alias n'existe pas
        ok := false;
        msg := 'L''alias existe d�j�';
        if not IsAlias(nomalias) then
        begin
          // il n'existe pas alors le creer
          AddStandardAlias(nomalias, repertoire, 'PARADOX');
          SaveConfigFile;

          // confirmation de la cr�ation de l'alias
          msg := 'Alias "' + nomalias;
          msg := msg + '" situ�' + #13 + #10;
          msg := msg + 'sur le repertoire ' + repertoire;
        end;

        ConfigMode := cmPersistent;

        // processus verif creation termine
        ok := true;
      end;
    finally
      begin
        ConfigMode := cmAll;
      end;
    end;
  end;

  showmessage('OK : ' + msg);

  if ok then
  begin
    // attribution de l'alias a la BDD
    FormBase.Database.Connected := false;
    FormBase.Database.AliasName := nomalias;
    FormBase.Database.Connected := true;

    CreationTables;
    CreationDonneeConfig;
    CreationBaseJeuEssai;

    CreationTableCumuls;
    InitTableCumuls;
    RAZTableCumuls;

    result := true;
  end;
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure tablemodifoui;
begin
  configtablemodifs := true;
end;

procedure tablemodifnon;
begin
  configtablemodifs := false;
end;

procedure tablemodifverif(table: TTable);
begin
  // verif si modifs effectuees
  if configtablemodifs then
  begin
    // enregistrer les modifications si necessaire
    table.edit;
    table.post;

    // confirmation ou pas remise a zero
    configtablemodifs := false;
  end;
end;

/// ///////////////////////////////////////////////////////////////////////////
/// ///////////////////////////////////////////////////////////////////////////
procedure ChargeConfiguration;
{ chargement de la configuration }
VAR
  t: string;
  temp: string;
  i: integer;
  b: boolean;
begin
  with FormBase.TableTemp do
  begin
    TableName := 'config';
    open;
    edit;

    CoutPetitPagnier := FieldByName('cfg_petitpannier').asCurrency;
    CoutGrandPagnier := FieldByName('cfg_grandpannier').asCurrency;
    TauxHoraireSup := FieldByName('cfg_tauxheuresup').asCurrency;
    TauxHoraireSupInt := FieldByName('cfg_tauxheuresupInt').asCurrency;
    TauxChargeBrut := FieldByName('cfg_facteurcharge').asCurrency;
    FraisInduits := FieldByName('cfg_fraisinduits').asCurrency;
    FraisOutilsConso := FieldByName('cfg_fraisoutilsconso').asCurrency;
    AnneeCourante := FieldByName('cfg_courantannee').asinteger;
    coutfixevehicule := FieldByName('cfg_coutfixevehicule').asCurrency;
    coutfixemateriel := FieldByName('cfg_coutfixemateriel').asCurrency;
    coutfixestructure := FieldByName('cfg_coutfixestructure').asCurrency;

    nbjoursmois[1] := FieldByName('cfg_nbj_jan').asinteger;
    nbjoursmois[2] := FieldByName('cfg_nbj_fev').asinteger;
    nbjoursmois[3] := FieldByName('cfg_nbj_mar').asinteger;
    nbjoursmois[4] := FieldByName('cfg_nbj_avr').asinteger;
    nbjoursmois[5] := FieldByName('cfg_nbj_mai').asinteger;
    nbjoursmois[6] := FieldByName('cfg_nbj_jui').asinteger;
    nbjoursmois[7] := FieldByName('cfg_nbj_jul').asinteger;
    nbjoursmois[8] := FieldByName('cfg_nbj_aou').asinteger;
    nbjoursmois[9] := FieldByName('cfg_nbj_sep').asinteger;
    nbjoursmois[10] := FieldByName('cfg_nbj_oct').asinteger;
    nbjoursmois[11] := FieldByName('cfg_nbj_nov').asinteger;
    nbjoursmois[12] := FieldByName('cfg_nbj_dec').asinteger;

    t := 'cfg_typesemaine_';
    for i := 1 to 52 do
    begin
      temp := t + inttostr(i);
      b := FieldByName(temp).asboolean;
      TypeSemaineBis[i] := b;
    end;

    post;
    close;
  end;
end;

function LireTypeSemaineBis(numsemaine: integer): boolean;
begin
  result := TypeSemaineBis[numsemaine];
end;

procedure VerrouON;
begin
  VerifierVerrou := true;
end;

procedure VerrouOFF;
begin
  VerifierVerrou := false;
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure SupprimeHebdo;
{ suppression de la fiche en cours dans le masque de base }
var
  idh: integer;
begin
  // recuperer l'identifiant dans la table hebdo
  idh := LireIdentifiantHebdo;

  // suppression dans les tables annexes
  MasquesSupprimer(idh);

  // efface enreg en cours
  FormBase.TableHebdo.edit;
  FormBase.TableHebdo.Delete;
end;

procedure SauveHebdo;
{ routine de creation ou de sauvegarde d'une fiche directement a partir du
  le masque de base }
var
  idh: integer;
  idc: integer;
  t: string;
  n: integer;
  b: boolean;
  c: currency;
begin
  // verifier si la fiche doit etre cree
  if ModeHebdoCreationOK then
  begin
    with FormBase.TableHebdo do
    begin
      close;
      open;
      insert;

      FieldByName('heb_verrou').asboolean := false;
      FieldByName('heb_impression').asboolean := true;
      FieldByName('heb_annul').asboolean := true;

      idc := LireIdentifiantChantier;
      FieldByName('id_chantier').asinteger := idc;

      // enregistrer tous les champs
      t := GetSaisieNumSemaine;
      n := strtoint(t);
      FieldByName('heb_numerosemaine').asinteger := n;

      // traite semaine bis
      b := FormBase.SaisieSemaineBis.Checked;
      FieldByName('heb_semainebis').asboolean := b;

      b := FicheRegul;
      FieldByName('heb_regul').asboolean := b;

      n := moisfichecourante;
      FieldByName('heb_numeromois').asinteger := n;

      n := AnneeCourante;
      FieldByName('heb_annee').asinteger := n;

      // enregistrer la creation
      post;
    end;
  end;

  // recuperer l'identifiant dans la table hebdo
  idh := LireIdentifiantHebdo;

  // lancer une sauvegarde de tous les masques
  MasquesSauver(idh);

  with FormBase.TableHebdo do
  begin
    // entree dans le mode modification de fiche
    edit;

    c := LiresaisieCA;
    FieldByName('heb_chiffreaffairedefini').asCurrency := c;

    FieldByName('heb_totalsalarie').asCurrency := hebdototalsalarie;
    FieldByName('heb_totalinterim').asCurrency := hebdototalinterim;
    FieldByName('heb_totalvehicule').asCurrency := hebdototalvehicule;
    FieldByName('heb_totaldiamant').asCurrency := hebdototaldiamant;
    FieldByName('heb_totalmateriel').asCurrency := hebdototalmateriel;
    FieldByName('heb_totaloutils').asCurrency := hebdototaloutils;
    FieldByName('heb_totalexterne').asCurrency := hebdototalexterne;
    FieldByName('heb_totalinduits').asCurrency := hebdototalfraisinduits;
    FieldByName('heb_totalinterne').asCurrency := hebdototalinterne;
    FieldByName('heb_totaldeboursesec').asCurrency := hebdototaldeboursesec;
    FieldByName('heb_totalstructures').asCurrency := hebdototalstructures;
    FieldByName('heb_totalprixrevient').asCurrency := hebdototalprixrevient;
    FieldByName('heb_totalresultat').asCurrency := hebdototalresultat;

    // enregistrer les modifs dans la teble
    post;
  end;

  // plus de modifs en cours
  ResetModifHebdo;
end;

procedure ChargeHebdo;
{ routine de chargement d'une fiche directement dans le masque de base }
var
  idh: integer;
  n: integer;
  ver: boolean;
  b: boolean;
  c: currency;
begin
  // pour eviter les evenements intempestifs
  ChargeEncours := true;

  // charge dans le masque la fiche hebdo
  idh := LireIdentifiantHebdo;

  with FormBase.TableHebdo do
  begin
    // charge dernieres valeurs du masque
    b := FieldByName('heb_semainebis').asboolean;
    FormBase.SaisieSemaineBis.Checked := b;

    b := FieldByName('heb_regul').asboolean;
    FicheRegul := b;

    n := FieldByName('heb_numeromois').asinteger;
    moisfichecourante := n;
    ListeMoisChoix;

    c := FieldByName('heb_chiffreaffairedefini').asCurrency;
    FormatSaisieCA(c);
  end;

  // charger les masques annexes
  MasquesCharger(idh);

  // verifier si la fiche peut etre modifiee
  if VerifierVerrou then
    ver := FormBase.TableHebdo.FieldByName('heb_verrou').asboolean
  else
    ver := false;

  if ver then
  begin
    // empecher de nouvelles modifs
    MasquesAnnexesFerme;

    // recharger les sommes de la fiche
    with FormBase.TableHebdo do
    begin
      hebdototalsalarie := FieldByName('heb_totalsalarie').asCurrency;
      hebdototalinterim := FieldByName('heb_totalinterim').asCurrency;
      hebdototalvehicule := FieldByName('heb_totalvehicule').asCurrency;
      hebdototaldiamant := FieldByName('heb_totaldiamant').asCurrency;
      hebdototalmateriel := FieldByName('heb_totalmateriel').asCurrency;
      hebdototaloutils := FieldByName('heb_totaloutils').asCurrency;
      hebdototalexterne := FieldByName('heb_totalexterne').asCurrency;
      hebdototalfraisinduits := FieldByName('heb_totalinduits').asCurrency;
      hebdototalinterne := FieldByName('heb_totalinterne').asCurrency;
      hebdototaldeboursesec := FieldByName('heb_totaldeboursesec').asCurrency;
      hebdototalstructures := FieldByName('heb_totalstructures').asCurrency;
      hebdototalprixrevient := FieldByName('heb_totalprixrevient').asCurrency;
      hebdototalresultat := FieldByName('heb_totalresultat').asCurrency;
    end;
  end
  else
  begin
    MasquesAnnexesOuvre;
  end;

  ChargeEncours := false;
end;

procedure HebdoChargeChaines;
{ routine de chargement du contenu d'une fiche dans deux variables globales de
  type tableaux (TableauTemp) (TableauValHebdo) }

  procedure chargechantiers(idch: integer);
  var
    t: string;
    n: integer;
    idage: integer;
    idcli: integer;
    idcom: integer;
  begin
    with FormBase.Tablechantiers do
    begin
      // preparer le filtre
      str(idch, t);
      filter := 'id_chantier = ' + t;
      filtered := true;

      open;
      setkey;
      if RecordCount <> 1 then
        exit;

      edit;
      n := FieldByName('cha_numchantier').asinteger;
      t := inttostr(n);
      TableauTemp[4, 0] := FillZero(6 - Length(t)) + t;

      TableauTemp[5, 0] := FieldByName('cha_nom').asstring;

      TableauTemp[6, 0] := FieldByName('cha_libelle').asstring;

      idcli := FieldByName('id_client').asinteger;
      idcom := FieldByName('id_commercial').asinteger;
      idage := FieldByName('id_agence').asinteger;

      // termine la lecture de la table chantiers
      close;
      active := true;
      filtered := false;
    end;

    with FormBase.TableClients do
    begin
      // preparer le filtre
      str(idcli, t);
      filter := 'id_client = ' + t;
      filtered := true;

      open;
      setkey;
      if RecordCount <> 1 then
        exit;

      edit;
      TableauTemp[7, 0] := FieldByName('cli_libellenom').asstring;

      // termine la lecture de la table chantiers
      close;
      active := true;
      filtered := false;
    end;

    with FormBase.TableCommerciaux do
    begin
      // preparer le filtre
      str(idcom, t);
      filter := 'id_commercial = ' + t;
      filtered := true;

      open;
      setkey;
      if RecordCount <> 1 then
        exit;

      edit;
      TableauTemp[8, 0] := FieldByName('com_libellenom').asstring;

      // termine la lecture de la table chantiers
      close;
      active := true;
      filtered := false;
    end;

    with FormBase.TableSecteurs do
    begin
      // preparer le filtre
      str(idage, t);
      filter := 'id_agence = ' + t;
      filtered := true;

      open;
      setkey;
      if RecordCount <> 1 then
        exit;

      edit;
      TableauTemp[9, 0] := FieldByName('age_libellenom').asstring;

      // termine la lecture de la table chantiers
      close;
      active := true;
      filtered := false;
    end;
  end;

var
  b: boolean;
  c: currency;
  n: integer;
  t: string;
begin
  with FormBase.TableHebdo do
  begin
    n := FieldByName('heb_numerosemaine').asinteger;
    t := inttostr(n);
    if n < 10 then
      t := '0' + t;
    TableauTemp[0, 0] := t;

    b := FieldByName('heb_semainebis').asboolean;
    t := '';
    if b then
      t := 'bis';
    TableauTemp[1, 0] := t;

    n := FieldByName('heb_numeromois').asinteger;
    TableauTemp[2, 0] := NumMois(n);

    n := FieldByName('heb_annee').asinteger;
    TableauTemp[3, 0] := inttostr(n);

    n := FieldByName('id_chantier').asinteger;
    chargechantiers(n);

    b := FieldByName('heb_regul').asboolean;
    t := TableauTemp[0, 0];
    if b then
      t := 'Regularisation';
    TableauTemp[0, 0] := t;

    c := FieldByName('heb_totalinduits').asCurrency;
    TableauValHebdo[0] := c;

    c := FieldByName('heb_chiffreaffairedefini').asCurrency;
    TableauValHebdo[1] := c;

    c := FieldByName('heb_totalsalarie').asCurrency;
    TableauValHebdo[2] := c;

    c := FieldByName('heb_totalinterim').asCurrency;
    TableauValHebdo[3] := c;

    c := FieldByName('heb_totalvehicule').asCurrency;
    TableauValHebdo[4] := c;

    c := FieldByName('heb_totaldiamant').asCurrency;
    TableauValHebdo[5] := c;

    c := FieldByName('heb_totalmateriel').asCurrency;
    TableauValHebdo[6] := c;

    c := FieldByName('heb_totaloutils').asCurrency;
    TableauValHebdo[7] := c;

    c := FieldByName('heb_totalexterne').asCurrency;
    TableauValHebdo[8] := c;

    c := FieldByName('heb_totalinterne').asCurrency;
    TableauValHebdo[9] := c;

    c := FieldByName('heb_totaldeboursesec').asCurrency;
    TableauValHebdo[10] := c;

    c := FieldByName('heb_totalstructures').asCurrency;
    TableauValHebdo[11] := c;

    c := FieldByName('heb_totalprixrevient').asCurrency;
    TableauValHebdo[12] := c;

    c := FieldByName('heb_totalresultat').asCurrency;
    TableauValHebdo[13] := c;
  end;
end;

end.
