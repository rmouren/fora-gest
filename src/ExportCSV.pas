unit ExportCSV;

interface // ------------------------------------------------------------------

procedure creerFichier(nom: string);
procedure ecrireDonnees(nbrDeLignes: Integer);
procedure fermerFichier();

var
  fichier: textfile;
  cheminFichier: string;
  nomFichier: string;

implementation // -------------------------------------------------------------

uses
  SysUtils, winbase;

// -----------------------------------------------------------------------------

// cr�e un fichier de type .csv
procedure creerFichier(nom: string);
begin
  cheminFichier := GetCurrentDir;  
  nomFichier := nom;
  assignfile(fichier, cheminFichier + '\' + nomFichier + '.csv');
  rewrite(fichier);
end;

// ecrire donn�es
procedure ecrireDonnees(nbrDeLignes: Integer);
var
  I: Integer;
begin
  for I := 0 to nbrDeLignes do
  begin
    writeln(fichier, String(TableauTemp[0]));
  end;
end;

// Fermer le fichier .
procedure fermerFichier();
begin
  closefile(fichier);
end;

end.
