unit VerifIndex;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, dbtables, db, bde;

type
  TFicheTestIndex = class(TForm)
    ProgressBar: TProgressBar;
    ListBox: TListBox;
    Button1: TButton;
    Label1: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { D�clarations priv�es }
  public
    { D�clarations publiques }
  end;

procedure Initlenbarre(nbr: integer);
procedure IncBarre;
procedure nombresfichesAtraiter(nb: integer);
procedure incfichetraitees;

var
  FicheTestIndex: TFicheTestIndex;

implementation

uses
  Clipbrd,
  winbase, utils, UtilsBDE, accesBDD, FicheEtats;

{$R *.DFM}

procedure Initlenbarre(nbr: integer);
begin
end;

procedure incfichetraitees;
begin
end;

procedure IncBarre;
begin
end;

procedure nombresfichesAtraiter(nb: integer);
begin
end;

procedure TFicheTestIndex.Button1Click(Sender: TObject);
begin
  modalresult := 1;
end;

procedure TFicheTestIndex.FormActivate(Sender: TObject);

  procedure afficheerreurDBE(msg: string);
  begin
    ListBox.items.add(msg);
  end;

  procedure traiteindexseq(table: ttable; nomindex, cleindex: string);
  var
    p: integer;
    msgsup: string;
    msgcre: string;
    t: string;
    savindex: string;
  begin
    with table do
    begin
      close;
      Exclusive := True;
      t := TableName;

      savindex := IndexName;
      IndexName := '';

      msgsup := 'Dans le fichier ''' + t + '''';
      msgsup := msgsup + ' erreur de suppression sur l''index ''';
      msgsup := msgsup + nomindex + '''.';

      msgcre := 'Dans le fichier ''' + t + '''';
      msgcre := msgcre + ' erreur de creation sur l''index ''';
      msgcre := msgcre + nomindex + '''.';

      try
        DeleteIndex(nomindex);
        try
          AddIndex(nomindex, cleindex, [ixCaseInsensitive]);
        except
          ListBox.items.add(msgcre);
        end;
      except
        on EDBEngineError do
          afficheerreurDBE(msgsup);
      end;

      IndexName := savindex;
      open;
    end;

    ListBox.Refresh;

    p := ProgressBar.Position;
    inc(p);
    ProgressBar.Position := p;
  end;

  procedure traite(nomtable: string);
  var
    msg: string;
    p: integer;
  begin
    with FormBase.TableTemp do
    begin
      open;
      Active := false;
      TableName := nomtable;
      close;
      Exclusive := True;
      open;
      msg := 'Dans la table ''' + nomtable + ''' erreur de';
      msg := msg + ' r�g�n�ration de son index primaire.';

      try
        DbiRegenIndexes(Handle);
      except
        on EDBEngineError do
          afficheerreurDBE(msg);
      end;

      close;
    end;

    ListBox.Refresh;

    p := ProgressBar.Position;
    inc(p);
    ProgressBar.Position := p;
  end;

  procedure RegenIndexSecondaire(nomtable, nomindex: string);
  var
    Handle: HDBIDB;
    nt: PAnsiChar;
    ni: PAnsiChar;
  begin
    Handle := FormBase.Database.Handle;
    nt := PAnsiChar(AnsiString(nomtable));
    ni := PAnsiChar(AnsiString(nomindex));

    Check(DbiRegenIndex(Handle, nil, nt, nil, ni, PAnsiChar(''), 0));
  end;

  procedure RegenIndex(Tbl: ttable; IndexName: string; IndexNum: Word);
  begin
    Check(DbiRegenIndex(FormBase.Database.Handle, nil, PAnsiChar(AnsiString(Tbl.TableName)),
      nil, PAnsiChar(AnsiString(IndexName)), PAnsiChar(''), IndexNum));
  end;

var
  savindex: string;
begin
  // centrer la fenetre par rapport a la fenetre principale du programme
  top := (FormBase.top + (FormBase.Height div 2)) - (Height div 2);
  left := (FormBase.left + (FormBase.width div 2)) - (width div 2);

  FicheTestIndex.ProgressBar.Min := 0;
  FicheTestIndex.ProgressBar.Max := 40;
  FicheTestIndex.ProgressBar.Position := 0;
  FicheTestIndex.ListBox.Clear;
  FicheTestIndex.repaint;

  // RegenIndexSecondaire('chantiers', 'indcom');              // OK GOOD

  traite('commercial');
  traite('secteurs');
  traite('clients');
  traite('chantiers');

  traite('materiels');
  traite('employes');
  traite('vehicules');
  traite('externe');
  traite('structure');
  traite('diamant');

  traite('fiche_pointage');
  traite('fiche_achat');
  traite('fiche_materiel');
  traite('fiche_vehicule');
  traite('fiche_externe');
  traite('fiche_structure');
  traite('fiche_diamant');

  traite('fiche_hebdo');

  savindex := FormEtats.TableClients.IndexName;
  FormEtats.TableClients.IndexName := '';
  traiteindexseq(FormBase.TableClients, 'indnom', 'cli_libellenom');
  FormEtats.TableClients.IndexName := savindex;

  traiteindexseq(FormBase.Tablechantiers, 'indcha', 'cha_numchantier');
  traiteindexseq(FormBase.Tablechantiers, 'indage', 'id_agence');
  traiteindexseq(FormBase.Tablechantiers, 'indcli', 'id_client');
  traiteindexseq(FormBase.Tablechantiers, 'indcom', 'id_commercial');

  traiteindexseq(FormBase.TabFichVehic, 'indhebveh', 'id_hebdo');
  traiteindexseq(FormBase.TabFichStruc, 'indhebstr', 'id_hebdo');
  traiteindexseq(FormBase.TabFichExter, 'indhebext', 'id_hebdo');
  traiteindexseq(FormBase.TabFichDiam, 'indhebtra', 'id_hebdo');
  traiteindexseq(FormBase.TabFichAcha, 'indhebint', 'id_hebdo');
  traiteindexseq(FormBase.TabFichPoint, 'indhebemp', 'id_hebdo');
  traiteindexseq(FormBase.TabFichMat, 'indhebmat', 'id_hebdo');

  traiteindexseq(FormBase.TableHebdo, 'indse', 'heb_numerosemaine');
  traiteindexseq(FormBase.TableHebdo, 'indmo', 'heb_numeromois');
  traiteindexseq(FormBase.TableHebdo, 'indfc', 'id_chantier');

  traiteindexseq(FormBase.TableVehicules, 'indveh', 'veh_libellenom');
  traiteindexseq(FormBase.TableEmployes, 'indemp', 'emp_nomemploye');
  traiteindexseq(FormBase.TableDiamant, 'inddia', 'tra_libellenom');
  traiteindexseq(FormBase.TableExterne, 'indext', 'ext_libellenom');
  traiteindexseq(FormBase.TableStructures, 'indstr', 'str_libellenom');
  traiteindexseq(FormBase.TableMateriels, 'indmat', 'mat_libellenom');

  if ListBox.items.Count = 0 then
    ListBox.items.add('Les index ont �t� r�g�n�r�s');
end;

end.
