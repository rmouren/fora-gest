unit fichebase;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, ExtCtrls, Db, DBTables, ColorGrd;

type
  TFichesimple = class(TForm)
    Table1: TTable;
    DataSource1: TDataSource;
    DBNavigator1: TDBNavigator;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Button1: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBEdit4: TDBEdit;
    ColorGrid1: TColorGrid;
    Label4: TLabel;
    procedure DBEdit2Change(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Table1AfterPost(DataSet: TDataSet);
    procedure DBNavigator1BeforeAction(Sender: TObject; Button: TNavigateBtn);
    procedure DBEdit4Change(Sender: TObject);
    procedure ColorGrid1Click(Sender: TObject);
    procedure DBCheckBox2Click(Sender: TObject);
    procedure DBCheckBox1Click(Sender: TObject);
  private
    { D�clarations priv�es }
  public
    { D�clarations publiques }
  end;

procedure PreparerFicheSimple(indexfiche: integer);
procedure RefreshTable(table: TTable);
procedure RefreshTables;

var
  Fichesimple: TFichesimple;

implementation

uses
  winbase, hebdo,
  MasqueEmployes, MasqueChantiers, MasqueSemaine,
  MasqueStructures, MasqueVehicules, MasqueDiamant, MasqueAchats,
  MasqueExterne, MasqueMateriels,
  accesBDD, FicheEtats;

var
  SAVTypeFichesimple: integer;

{$R *.DFM}

procedure RefreshTable(table: TTable);
begin
  table.open;
  table.Refresh;
  table.close;
  table.active := true;
end;

procedure RefreshTables;
{ routines mettant a jour les listes du masque de base suite a une eventuelle
  modification de la BDD }
begin
  RefreshTable(FormBase.TableVehicules);
  RefreshTable(FormBase.TableExterne);
  RefreshTable(FormBase.TableStructures);
  RefreshTable(FormBase.TableDiamant);
  RefreshTable(FormBase.TableMateriels);
  RefreshTable(FormBase.TableEmployes);
  RefreshTable(FormBase.TableClients);
  RefreshTable(FormBase.TableSecteurs);
  RefreshTable(FormBase.TableCommerciaux);
end;

function DeleteON: TNavButtonSet;
begin
  result := [nbFirst, nbPrior, nbNext, nbLast, nbInsert, nbDelete];
  // result := [nbFirst, nbPrior, nbNext, nbLast, nbInsert];
end;

function DeleteOFF: TNavButtonSet;
begin
  result := [nbFirst, nbPrior, nbNext, nbLast, nbInsert];
end;

procedure PreparerFicheSimple(indexfiche: integer);
{ routine permettant la creation et la modification des fiches 'vehicule'
  'externe' 'achat' 'structure' 'materiel' 'employe' et 'diamant' }
begin
  // if ProtectionFicheOuverte then exit;

  // sauver le type de la fenetre en cours d'utilisation
  SAVTypeFichesimple := indexfiche;

  with Fichesimple do
  begin
    // initialisation de la fenetre en fonction de son type et
    // de son contenu et surtout dela table quelle modifie
    case indexfiche of
      Fichevehicules:
        begin
          caption := 'Fiches Vehicules';
          width := 350;
          height := 144;
          Table1.TableName := 'vehicules.DB';
          DBNavigator1.VisibleButtons := DeleteON;

          Label1.caption := 'Numero';
          DBEdit1.Visible := true;
          Label2.caption := 'Nom v�hicule';
          DBEdit2.Visible := true;
          Label3.caption := 'Valeur';
          DBEdit3.Visible := true;
          DBCheckBox1.Visible := false;
          DBCheckBox2.Visible := false;
          ColorGrid1.Visible := false;

          DBEdit1.DataField := 'Id_vehicule';
          DBEdit2.DataField := 'Veh_libellenom';
          DBEdit3.DataField := 'Veh_cout';
          DBEdit4.DataField := '';
          DBCheckBox1.DataField := '';
          DBCheckBox2.DataField := '';
        end;
      Ficheexterne:
        begin
          caption := 'Fiches Frais Externes';
          width := 350;
          height := 144;
          Table1.TableName := 'externe.DB';
          DBNavigator1.VisibleButtons := DeleteON;

          Label1.caption := 'Numero';
          DBEdit1.Visible := true;
          Label2.caption := 'Libelle Frais';
          DBEdit2.Visible := true;
          Label3.caption := '';
          DBEdit3.Visible := false;
          DBCheckBox1.Visible := false;
          DBCheckBox2.Visible := false;
          ColorGrid1.Visible := false;

          DBEdit1.DataField := 'id_fraisexterne';
          DBEdit2.DataField := 'ext_libellenom';
          DBEdit3.DataField := '';
          DBEdit4.DataField := '';
          DBCheckBox1.DataField := '';
          DBCheckBox2.DataField := '';
        end;
      Fichestructure:
        begin
          caption := 'Fiches Frais Structure';
          width := 350;
          height := 144;
          Table1.TableName := 'structure.DB';
          DBNavigator1.VisibleButtons := DeleteON;

          Label1.caption := 'Numero';
          DBEdit1.Visible := true;
          Label2.caption := 'Libelle Frais';
          DBEdit2.Visible := true;
          Label3.caption := 'Valeur';
          DBEdit3.Visible := true;
          DBCheckBox1.Visible := false;
          DBCheckBox2.Visible := false;
          ColorGrid1.Visible := false;

          DBEdit1.DataField := 'id_fraisstructure';
          DBEdit2.DataField := 'str_libellenom';
          DBEdit3.DataField := 'str_cout';
          DBEdit4.DataField := '';
          DBCheckBox1.DataField := '';
          DBCheckBox2.DataField := '';
        end;
      Fichetravaux:
        begin
          caption := 'Fiches Diamant';
          width := 350;
          height := 144;
          Table1.TableName := 'diamant.DB';
          DBNavigator1.VisibleButtons := DeleteON;

          Label1.caption := 'Numero';
          DBEdit1.Visible := true;
          Label2.caption := 'Libelle Travail';
          DBEdit2.Visible := true;
          Label3.caption := 'Valeur';
          DBEdit3.Visible := true;
          DBCheckBox1.Visible := false;
          DBCheckBox2.Visible := false;
          ColorGrid1.Visible := false;

          DBEdit1.DataField := 'id_travail';
          DBEdit2.DataField := 'tra_libellenom';
          DBEdit3.DataField := 'tra_cout';
          DBEdit4.DataField := '';
          DBCheckBox1.DataField := '';
          DBCheckBox2.DataField := '';
        end;
      Fichemateriel:
        begin
          caption := 'Fiches Materiel';
          width := 350;
          height := 144;
          Table1.TableName := 'materiels.DB';
          DBNavigator1.VisibleButtons := DeleteON;

          Label1.caption := 'Numero';
          DBEdit1.Visible := true;
          Label2.caption := 'Libelle Outil';
          DBEdit2.Visible := true;
          Label3.caption := 'Valeur';
          DBEdit3.Visible := true;
          DBCheckBox1.Visible := false;
          DBCheckBox2.Visible := false;
          ColorGrid1.Visible := false;

          DBEdit1.DataField := 'id_materiel';
          DBEdit2.DataField := 'mat_libellenom';
          DBEdit3.DataField := 'mat_cout';
          DBEdit4.DataField := '';
          DBCheckBox1.DataField := '';
          DBCheckBox2.DataField := '';
        end;
      Ficheemploye:
        begin
          caption := 'Fiches Employ�';
          width := 350;
          height := 200;
          Table1.TableName := 'employes.DB';
          DBNavigator1.VisibleButtons := DeleteON;

          Label1.caption := 'Numero';
          DBEdit1.Visible := true;
          Label2.caption := 'Nom Employ�';
          DBEdit2.Visible := true;
          Label3.caption := 'Taux Horaire';
          DBEdit3.Visible := true;

          DBCheckBox1.caption := 'Interimaire';
          DBCheckBox1.Visible := true;
          DBCheckBox2.caption := 'Pr�sent dans l''effectif';
          DBCheckBox2.Visible := true;
          ColorGrid1.Visible := false;

          DBEdit1.DataField := 'id_employe';
          DBEdit2.DataField := 'emp_nomemploye';
          DBEdit3.DataField := 'emp_tauxhoraire';
          DBEdit4.DataField := '';
          DBCheckBox1.DataField := 'emp_interimaire';
          DBCheckBox2.DataField := 'emp_actuel';
          DBCheckBox1.Checked := false;
          DBCheckBox1.Checked := true;
        end;
      Ficheclient:
        begin
          caption := 'Fiches Client';
          width := 350;
          height := 144;
          Table1.TableName := 'clients.DB';
          DBNavigator1.VisibleButtons := DeleteON;

          Label1.caption := 'Numero';
          DBEdit1.Visible := true;
          Label2.caption := 'Nom Client';
          DBEdit2.Visible := true;
          Label3.caption := '';
          DBEdit3.Visible := false;
          DBCheckBox1.Visible := false;
          DBCheckBox2.Visible := false;
          ColorGrid1.Visible := false;

          DBEdit1.DataField := 'id_client';
          DBEdit2.DataField := 'cli_libellenom';
          DBEdit3.DataField := '';
          DBEdit4.DataField := '';
          DBCheckBox1.DataField := '';
          DBCheckBox2.DataField := '';
        end;
      Fichesecteur:
        begin
          caption := 'Fiches Secteur';
          width := 350;
          height := 144;
          Table1.TableName := 'secteurs.DB';
          DBNavigator1.VisibleButtons := DeleteOFF;

          Label1.caption := 'Numero';
          DBEdit1.Visible := true;
          Label2.caption := 'Nom Secteur';
          DBEdit2.Visible := true;
          Label3.caption := '';
          DBEdit3.Visible := false;
          DBCheckBox1.Visible := false;
          DBCheckBox2.Visible := false;
          ColorGrid1.Visible := false;

          DBEdit1.DataField := 'id_agence';
          DBEdit2.DataField := 'age_libellenom';
          DBEdit3.DataField := '';
          DBEdit4.DataField := '';
          DBCheckBox1.DataField := '';
          DBCheckBox2.DataField := '';
        end;
      Fichecommercial:
        begin
          caption := 'Fiches Commercial';
          width := 350;
          height := 190;
          Table1.TableName := 'commercial.DB';
          DBNavigator1.VisibleButtons := DeleteON;

          Label1.caption := 'Numero';
          DBEdit1.Visible := true;
          Label2.caption := 'Nom Commercial';
          DBEdit2.Visible := true;
          Label3.caption := '';
          DBEdit3.Visible := false;
          DBCheckBox1.Visible := false;
          DBCheckBox2.Visible := false;
          ColorGrid1.Visible := true;

          DBEdit1.DataField := 'id_commercial';
          DBEdit2.DataField := 'com_libellenom';
          DBEdit3.DataField := '';
          DBEdit4.DataField := 'com_couleur';
          DBCheckBox1.DataField := '';
          DBCheckBox2.DataField := '';
        end;
    end;

    // centrer la fenetre par rapport a la fenetre principale du programme
    top := (FormBase.top + (FormBase.height div 2)) - (height div 2);
    left := (FormBase.left + (FormBase.width div 2)) - (width div 2);

    // rendre active la table a la base de donnees
    Table1.active := true;

  end;

  // afficher fenetre
  Fichesimple.showmodal;

  case indexfiche of
    Fichevehicules:
      RefreshTable(FormBase.TableVehicules);
    Ficheexterne:
      RefreshTable(FormBase.TableExterne);
    Fichestructure:
      RefreshTable(FormBase.TableStructures);
    Fichetravaux:
      RefreshTable(FormBase.TableDiamant);
    Fichemateriel:
      RefreshTable(FormBase.TableMateriels);
    Ficheemploye:
      RefreshTable(FormBase.TableEmployes);
    Ficheclient:
      RefreshTable(FormBase.TableClients);
    Fichesecteur:
      RefreshTable(FormBase.TableSecteurs);
    Fichecommercial:
      RefreshTable(FormBase.TableCommerciaux);
  end;

  case indexfiche of
    Ficheclient:
      RefreshTable(FormEtats.TableClients);
    Fichesecteur:
      RefreshTable(FormEtats.TableSecteurs);
    Fichecommercial:
      RefreshTable(FormEtats.TableCommerciaux);
  end;
end;

procedure TFichesimple.DBEdit2Change(Sender: TObject);
begin
  // un changement a ete apporte a la base de donnee
  // forcer la demande de confirmation de sauvegarde
  tablemodifoui;
end;

procedure TFichesimple.DBCheckBox2Click(Sender: TObject);
begin
  // un changement a ete apporte a la base de donnee
  // forcer la demande de confirmation de sauvegarde
  tablemodifoui;
end;

procedure TFichesimple.DBCheckBox1Click(Sender: TObject);
begin
  // un changement a ete apporte a la base de donnee
  // forcer la demande de confirmation de sauvegarde
  tablemodifoui;
end;

procedure TFichesimple.Button1Click(Sender: TObject);
begin
  // verif si modifs effectuees et sauvegarde eventuelle
  tablemodifverif(Table1);

  Table1.Refresh;

  // fermer table
  Table1.close;

  // rendre inactive la table a la base de donnees
  Table1.active := false;

  // fermer fenetre
  Fichesimple.modalresult := 1;
end;

procedure TFichesimple.FormCreate(Sender: TObject);
begin
  // se rattacher a la base de donn�es
  Table1.DatabaseName := 'BASEGCFORA';

  // mise a zero de la demande de sauvegarde
  tablemodifnon;
end;

procedure TFichesimple.Table1AfterPost(DataSet: TDataSet);
begin
  // mise a zero de la demande de sauvegarde
  tablemodifnon;
end;

procedure TFichesimple.DBNavigator1BeforeAction(Sender: TObject;
  Button: TNavigateBtn);
begin
  // verification si suppression
  // suivant SAVTypeFichesimple verifier dans la table fiche_*****
  // si elle est utilis�e
end;

procedure TFichesimple.DBEdit4Change(Sender: TObject);
var
  valeur: integer;
  Code: integer;
begin
  // definition de la couleur de la palette
  Val(DBEdit4.text, valeur, Code);
  ColorGrid1.foregroundindex := valeur;
end;

procedure TFichesimple.ColorGrid1Click(Sender: TObject);
begin
  // un changement a ete apporte a la base de donnee
  // forcer la demande de confirmation de sauvegarde
  tablemodifoui;

  // recuperation de la couleur de la palette et ecriture dans la table
  // et dans la zone edit
  Table1.edit;
  Table1.FieldValues['com_couleur'] := ColorGrid1.foregroundindex;
  Table1.post;
end;

end.
