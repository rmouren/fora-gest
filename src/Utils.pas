unit Utils;

interface

function DateDuJour: string;
function MoisEnCours: integer;
function videpoint(t: string): string;
function videespace(t: string): string;
function videsnum(t: string): string;
function spctozero(t: string): string;
function FillSpaces(n: integer): string;
function NumMois(n: integer): string;
function FillZero(n: integer): string;
function JoursMois(Annee: integer; idmois: integer): integer;
procedure EcartSemaine(Mois, Annee: integer; var sd: integer; var sf: integer);
function NumeroSemaine(Jour, Mois, Annee: word): integer;
function SemaineBis(numsemaine: integer; Annee: integer): boolean;
function CalcMoisSemaine(numsemaine, Annee: integer; bis: boolean): integer;

function VerifNum(Key: Char): boolean;

function remplaceaccent(t: string): string;

implementation

uses

  SysUtils, DateUtils, accesBDD, Dialogs;


// ------------------------------------------------------------------------------

function VerifNum(Key: Char): boolean;
begin
  result := true;
  if Key = '0' then
    exit;
  if Key = '1' then
    exit;
  if Key = '2' then
    exit;
  if Key = '3' then
    exit;
  if Key = '4' then
    exit;
  if Key = '5' then
    exit;
  if Key = '6' then
    exit;
  if Key = '7' then
    exit;
  if Key = '8' then
    exit;
  if Key = '9' then
    exit;
  result := false;
end;

// ------------------------------------------------------------------------------

function JoursMois(Annee: integer; idmois: integer): integer;
{ routine calculant le nombre de jours par mois }
var
  j: integer;
begin

  j := 0;
  case idmois of
    4, 6, 9, 11:
      j := 30;
    2:
      If IsLeapYear(Annee) then
        j := 29
      else
        j := 28;

    1, 3, 5, 7, 8, 10, 12:
      j := 31;
  end;

  result := j;
end;

// ------------------------------------------------------------------------------
{
  procedure EcartSemaine(Mois, Annee: integer; var sd: integer; var sf: integer);
  // routine calculant le numero de la premiere (sd)
  // et derniere (fd) semaine d'un mois
  var
  i: integer;
  je: integer;
  jed: integer;
  jef: integer;
  rjd: integer;
  rjf: integer;
  begin
  // Initialisation :
  je := 0;

  For i := 1 to Mois - 1 do
  je := je + JoursMois(Annee, i);

  jed := je + 1;
  rjd := jed mod 7;
  sd := jed div 7;
  If rjd > 0 then
  sd := sd + 1;

  jef := je + JoursMois(Annee, Mois);
  rjf := jef mod 7;
  sf := jef div 7;
  If rjf > 0 then
  sf := sf + 1;
  end;
}
// ------------------------------------------------------------------------------

procedure EcartSemaine(Mois, Annee: integer; var sd: integer; var sf: integer);
{ routine calculant le numero de la premiere (sd) et derniere (fd) semaine
  d'un mois }

begin

  sd := NumeroSemaine(1, Mois, Annee);

  // si le premier du mois est un samedi et que le mois dure 31 jours ou si
  // le premier jour est un dimanche : la premi�re semaine commence le premier
  // lundi ( pour eviter d'avoir 6 semaines) .

  case (DayOfTheWeek(EncodeDate(Annee, Mois, 1))) of
    6:
      if (JoursMois(Annee, Mois) = 31) then
        sd := sd + 1;
    7:
      sd := sd + 1;
  end;

  sf := NumeroSemaine(JoursMois(Annee, Mois), Mois, Annee);

end;

// ------------------------------------------------------------------------------

function NumeroSemaine(Jour, Mois, Annee: word): integer;
// routine calculant a quelle semaine appartient une date
var
  jourSem1erJanvier: integer;
  duree1ereSemaine: integer;
  indexMois: integer;
  joursEcoules: integer;
  resteJoursEcoules: integer;
  NumeroSemaine: integer;
begin
  // Initialisation :
  joursEcoules := 0;

  // Calcul de la dur�e de la premi�re semaine de l'ann�e .
  jourSem1erJanvier := DayOfTheWeek(EncodeDate(Annee, 1, 1));
  duree1ereSemaine := 8 - jourSem1erJanvier;

  // calcul du nombre de jours �coul�s dans l'ann�e
  for indexMois := 1 to (Mois - 1) do
    joursEcoules := joursEcoules + JoursMois(Annee, indexMois);
  joursEcoules := joursEcoules + Jour;

  // Calcul du nombre de semaines completes depuis le debut de l'ann�e
  // ( la premiere semaine compte pour 1 m�me si elle fait moins de 7 jours )
  NumeroSemaine := ((joursEcoules - duree1ereSemaine) div 7) + 1;
  // ajout d'une semaine si une nouvelle semaine est entam�e .
  resteJoursEcoules := ((joursEcoules - duree1ereSemaine) mod 7);
  if resteJoursEcoules > 0 then
    NumeroSemaine := NumeroSemaine + 1;

  result := NumeroSemaine;
end;



// ------------------------------------------------------------------------------

function FillZero(n: integer): string;
var
  i: integer;
  t: string;
begin
  result := '';
  if n < 1 then
    exit;

  t := '';
  for i := 1 to n do
  begin
    t := t + '0';
  end;

  result := t;
end;

// ------------------------------------------------------------------------------

function remplaceaccent(t: string): string;
var
  i: integer;
  l: string;
begin
  l := '';
  for i := 1 to Length(t) do
  begin
    if (t[i] = '�') or (t[i] = '�') or (t[i] = '�') then
      l := l + 'e'
    else
      l := l + t[i];
  end;

  result := l;
end;

// ------------------------------------------------------------------------------

function FillSpaces(n: integer): string;
var
  i: integer;
  t: string;
begin
  result := '';
  if n < 1 then
    exit;

  t := '';
  for i := 1 to n do
  begin
    t := t + ' ';
  end;

  result := t;
end;

// ------------------------------------------------------------------------------

function videpoint(t: string): string;
var
  i: integer;
  l: string;
begin
  l := '';
  for i := 1 to Length(t) do
  begin
    if t[i] <> '.' then
      l := l + t[i];
  end;

  result := l;
end;

// ------------------------------------------------------------------------------

function videespace(t: string): string;
var
  i: integer;
  l: string;
begin
  l := '';
  for i := 1 to Length(t) do
  begin
    if t[i] <> ' ' then
      l := l + t[i];
  end;

  result := l;
end;

// ------------------------------------------------------------------------------

function videsnum(t: string): string;
var
  i: integer;
  l: string;
begin
  l := '';
  for i := 1 to Length(t) do
  begin
    if (t[i] = '0') or (t[i] = '1') or (t[i] = '2') or (t[i] = '3') or
      (t[i] = '4') or (t[i] = '5') or (t[i] = '6') or (t[i] = '7') or
      (t[i] = '8') or (t[i] = '9') or (t[i] = ',') or (t[i] = '-') then
      l := l + t[i];
  end;

  result := l;
end;

// ------------------------------------------------------------------------------

function spctozero(t: string): string;
var
  i: integer;
  l: string;
begin
  l := '';
  for i := 1 to Length(t) do
  begin
    if t[i] = ' ' then
      l := l + '0'
    else
      l := l + t[i];
  end;

  result := l;
end;

// ------------------------------------------------------------------------------

function MoisEnCours: integer;
var
  present: TDateTime;
  Annee: word;
  Mois: word;
  Jour: word;
begin
  present := Now;
  DecodeDate(present, Annee, Mois, Jour);
  result := Mois;
end;

// ------------------------------------------------------------------------------

function DateDuJour: string;
var
  present: TDateTime;
  Annee: word;
  Mois: word;
  Jour: word;
  n: integer;
  t: string;
begin
  present := Now;
  DecodeDate(present, Annee, Mois, Jour);

  n := Annee;
  t := IntToStr(n);
  result := t + ')';

  n := Mois;
  t := IntToStr(n);
  if n < 10 then
    t := '0' + t;

  result := '/' + t + '/' + result;

  n := Jour;
  t := IntToStr(n);
  if n < 10 then
    t := '0' + t;

  result := '(' + t + result;
end;

// ------------------------------------------------------------------------------

function NumMois(n: integer): string;
begin
  result := '';
  case n of
    1:
      result := 'JANVIER';
    2:
      result := 'FEVRIER';
    3:
      result := 'MARS';
    4:
      result := 'AVRIL';
    5:
      result := 'MAI';
    6:
      result := 'JUIN';
    7:
      result := 'JUILLET';
    8:
      result := 'AOUT';
    9:
      result := 'SEPTEMBRE';
    10:
      result := 'OCTOBRE';
    11:
      result := 'NOVEMBRE';
    12:
      result := 'DECEMBRE';
  end;
end;

// ------------------------------------------------------------------------------

function SemaineBis(numsemaine: integer; Annee: integer): boolean;
{ routine verifiant si une semaine donnee est une semaine dont le premier et
  le dernier jour de celle-ci appartiennent a deux mois differents }
var
  JourParMois: array [1 .. 12] of integer;
  PeriodeJour: array [1 .. 28] of Byte;
  j: Byte;
  Lundi: integer;
  Mois: Byte;
  nbjfev: integer;
  t: Byte;
  i: integer;
  jourdebutsemaine: integer;
  jourfinsemaine: integer;
  moisdebutsemaine: integer;
  moisfinsemaine: integer;
begin
  result := LireTypeSemaineBis(numsemaine);
  exit;

  Annee := Annee + 2;
  nbjfev := (Annee - 12) Mod 28;
  if (nbjfev = 0) Then
    nbjfev := 28;

  j := 6;
  for t := 1 To nbjfev do
  begin
    i := integer(((t - 1) Mod 4 = 0) And (t > 1));
    j := (j + 1 - i) Mod 7;
    PeriodeJour[t] := j;
  end;

  Lundi := (numsemaine + 1) * 7 - PeriodeJour[nbjfev] - 6;
  If (Lundi < 0) Then
  begin
    jourdebutsemaine := 7 - PeriodeJour[nbjfev];
    moisdebutsemaine := 11;

    jourfinsemaine := 31 - Lundi;
    moisfinsemaine := 12;
  end
  Else
  begin
    JourParMois[1] := 31;
    JourParMois[2] := 28;
    if ((nbjfev Mod 4) = 0) then
      Dec(JourParMois[2]);
    JourParMois[3] := 31;
    JourParMois[4] := 30;
    JourParMois[5] := 31;
    JourParMois[6] := 30;
    JourParMois[7] := 31;
    JourParMois[8] := 31;
    JourParMois[9] := 30;
    JourParMois[10] := 31;
    JourParMois[11] := 30;
    JourParMois[12] := 31;

    j := 1;
    Mois := 1;
    While (Lundi > JourParMois[j]) do
    begin
      Lundi := Lundi - JourParMois[j];
      j := j + 1;
      Mois := Mois + 1;
    end;

    jourfinsemaine := Lundi;
    moisfinsemaine := Mois;

    Lundi := Lundi + 6;
    If (Lundi > JourParMois[j]) Then
    begin
      jourdebutsemaine := Lundi - JourParMois[j];
      moisdebutsemaine := 1;
      if Mois <> 12 then
        moisdebutsemaine := moisdebutsemaine + Mois;
    end
    Else
    begin
      jourdebutsemaine := Lundi;
      moisdebutsemaine := Mois;
    end;
  end;

  jourdebutsemaine := jourdebutsemaine - 2;
  jourfinsemaine := jourfinsemaine - 2;

  if jourdebutsemaine <= 0 then
    if moisdebutsemaine > 1 then
      Dec(moisdebutsemaine);

  if jourfinsemaine <= 0 then
    if moisfinsemaine > 1 then
      Dec(moisfinsemaine);

  result := true;
  if moisfinsemaine = moisdebutsemaine then
    result := false;
end;

// ------------------------------------------------------------------------------

function CalcMoisSemaine(numsemaine, Annee: integer; bis: boolean): integer;
{
  routine calculant a quel mois appartient une semaine donnee en tenant
  compte du fait que cette semaine peut etre ou pas une semaine a cheval
  sur deux mois

  lorsque le parametre (bis) est faux il determine le mois de la semaine
  en question

  lorsqu'il est vrai il determine que le mois est le mois suivant la semaine
  en question
}
var
  JourParMois: array [1 .. 12] of integer;
  PeriodeJour: array [1 .. 28] of Byte;
  j: Byte;
  Lundi: integer;
  Mois: Byte;
  A: integer;
  t: Byte;
  i: integer;
  jds: integer;
  jfs: integer;
  moisdebut: integer;
  moisfin: integer;
begin
  Annee := Annee + 2;
  A := (Annee - 12) Mod 28;
  if (A = 0) Then
    A := 28;

  j := 6;
  for t := 1 To A do
  begin
    i := integer(((t - 1) Mod 4 = 0) And (t > 1));
    j := (j + 1 - i) Mod 7;
    PeriodeJour[t] := j;
  end;

  Lundi := (numsemaine + 1) * 7 - PeriodeJour[A] - 6;
  If (Lundi < 0) Then
  begin
    jds := 7 - PeriodeJour[A];
    moisdebut := 11;

    jfs := 31 - Lundi;
    moisfin := 12;
  end
  Else
  begin
    JourParMois[1] := 31;
    JourParMois[2] := 28;
    if ((A Mod 4) = 0) then
      Dec(JourParMois[2]);
    JourParMois[3] := 31;
    JourParMois[4] := 30;
    JourParMois[5] := 31;
    JourParMois[6] := 30;
    JourParMois[7] := 31;
    JourParMois[8] := 31;
    JourParMois[9] := 30;
    JourParMois[10] := 31;
    JourParMois[11] := 30;
    JourParMois[12] := 31;

    j := 1;
    Mois := 1;
    While (Lundi > JourParMois[j]) do
    begin
      Lundi := Lundi - JourParMois[j];
      j := j + 1;
      Mois := Mois + 1;
    end;

    jfs := Lundi;
    moisfin := Mois;

    Lundi := Lundi + 6;
    If (Lundi > JourParMois[j]) Then
    begin
      jds := Lundi - JourParMois[j];
      moisdebut := 1;
      if Mois <> 12 then
        moisdebut := moisdebut + Mois;
    end
    Else
    begin
      jds := Lundi;
      moisdebut := Mois;
    end;
  end;

  jds := jds - 2;
  jfs := jfs - 2;

  if jds <= 0 then
    if moisdebut > 1 then
      Dec(moisdebut);

  if jfs <= 0 then
    if moisfin > 1 then
      Dec(moisfin);

  if moisfin = moisdebut then
    // semaine non bis
    result := moisdebut
  else
  begin
    // semaine peutetre bis
    if bis then
      result := moisdebut
    else
      result := moisfin;
  end
end;

end.
