object FormBase: TFormBase
  Left = 24
  Top = 30
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Gestion de Chantiers : '
  ClientHeight = 729
  ClientWidth = 969
  Color = clBtnFace
  Constraints.MinWidth = 635
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object FicheHebdo: TPanel
    Left = 0
    Top = 0
    Width = 969
    Height = 729
    BorderWidth = 3
    BorderStyle = bsSingle
    TabOrder = 0
    object ZoneSaisieMinimum: TPanel
      Left = 1
      Top = -1
      Width = 368
      Height = 160
      BorderStyle = bsSingle
      TabOrder = 5
      object Label6: TLabel
        Left = 41
        Top = 109
        Width = 63
        Height = 13
        Caption = 'Nom chantier'
      end
      object Label1: TLabel
        Left = 41
        Top = 133
        Width = 19
        Height = 13
        Caption = 'Ville'
      end
      object ButtonExecuter: TButton
        Left = 34
        Top = 6
        Width = 53
        Height = 21
        Hint = 'Recherche de fiche existante ou cr'#233'ation d'#39'une nouvelle fiche'
        Caption = 'Executer'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 12
        TabStop = False
        OnClick = FicheExecuterClick
        OnExit = ButtonExecuterExit
        OnMouseDown = ButtonExecuterMouseDown
      end
      object PanelRegul: TPanel
        Left = 1
        Top = 33
        Width = 122
        Height = 65
        BorderStyle = bsSingle
        TabOrder = 10
        Visible = False
        object Label35: TLabel
          Left = 31
          Top = 13
          Width = 53
          Height = 13
          Caption = 'Regul Mois'
        end
        object ListeMois: TComboBox
          Left = 31
          Top = 33
          Width = 55
          Height = 21
          TabOrder = 0
          Text = 'ListeMois'
          OnClick = ListeMoisClick
          OnKeyPress = ListeMoisKeyPress
          Items.Strings = (
            'JAN'
            'FEV'
            'MAR'
            'AVR'
            'MAI'
            'JUN'
            'JUI'
            'AOU'
            'SEP'
            'OCT'
            'NOV'
            'DEC')
        end
      end
      object PanelHebdo: TPanel
        Left = 1
        Top = 33
        Width = 122
        Height = 65
        BorderStyle = bsSingle
        TabOrder = 11
        Visible = False
        object Label36: TLabel
          Left = 6
          Top = 6
          Width = 81
          Height = 13
          Caption = 'Numero Semaine'
        end
        object saisieNumSemaineMsk: TMaskEdit
          Left = 92
          Top = 34
          Width = 20
          Height = 21
          Hint = 'Numero de semaine de la fiche '#224' charger ou '#224' modifier'
          TabStop = False
          EditMask = '!00;1;_'
          MaxLength = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Text = '00'
          Visible = False
        end
        object SaisieSemaineBis: TCheckBox
          Left = 6
          Top = 34
          Width = 80
          Height = 17
          Hint = 'Definition de semaine de debut de mois suivant'
          TabStop = False
          Caption = 'Semaine Bis'
          Enabled = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SaisieSemaineBisClick
        end
        object saisieNumSemaine: TEdit
          Left = 91
          Top = 4
          Width = 21
          Height = 21
          TabOrder = 2
          Text = '00'
          OnChange = saisieNumSemaineChange
          OnClick = saisieNumSemaineClick
          OnEnter = saisieNumSemaineEnter
          OnKeyPress = saisieNumSemaineKeyPress
        end
      end
      object SaisieNumChantier: TEdit
        Left = 292
        Top = 5
        Width = 48
        Height = 21
        TabOrder = 16
        Text = '000000'
        OnChange = SaisieNumChantierChange
        OnClick = SaisieNumChantierClick
        OnKeyPress = SaisieNumChantierKeyPress
        OnMouseDown = SaisieNumChantierMouseDown
      end
      object SaisieNumChantierMsk: TMaskEdit
        Left = 9
        Top = 106
        Width = 43
        Height = 21
        Hint = 'Numero de chantier en cours'
        TabStop = False
        EditMask = '!000000;1;_'
        MaxLength = 6
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Text = '      '
        Visible = False
      end
      object StaticText11: TStaticText
        Left = 154
        Top = 60
        Width = 58
        Height = 17
        Caption = 'Commercial'
        TabOrder = 1
      end
      object StaticText12: TStaticText
        Left = 158
        Top = 37
        Width = 41
        Height = 17
        Caption = 'Agence'
        TabOrder = 2
      end
      object StaticText13: TStaticText
        Left = 154
        Top = 83
        Width = 30
        Height = 17
        Caption = 'Client'
        TabOrder = 3
      end
      object ListeSecteurs: TDBLookupComboBox
        Left = 216
        Top = 33
        Width = 145
        Height = 21
        Hint = 'Liste des agences enregistr'#233'es'
        KeyField = 'id_agence'
        ListField = 'age_libellenom'
        ListSource = DataSourceSecteurs
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        TabStop = False
        OnClick = ListeSecteursClick
      end
      object ListeCommerciaux: TDBLookupComboBox
        Left = 216
        Top = 57
        Width = 145
        Height = 21
        Hint = 'Liste des commerciaux enregistr'#233's'
        KeyField = 'id_commercial'
        ListField = 'com_libellenom'
        ListSource = DataSourceCommerciaux
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        TabStop = False
        OnClick = ListeCommerciauxClick
      end
      object ListeClient: TDBLookupComboBox
        Left = 216
        Top = 81
        Width = 145
        Height = 21
        Hint = 'Liste des clients enregistr'#233's'
        KeyField = 'id_client'
        ListField = 'cli_libellenom'
        ListSource = DataSourceClients
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        TabStop = False
        OnClick = ListeClientClick
      end
      object StaticText14: TStaticText
        Left = 245
        Top = 10
        Width = 43
        Height = 17
        Caption = 'Chantier'
        TabOrder = 7
      end
      object BoutonConsultation: TButton
        Left = 137
        Top = 4
        Width = 48
        Height = 25
        Hint = 'Enregistrement du chantier en cours de saisie'
        Caption = 'Modifier'
        Enabled = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 8
        TabStop = False
        OnClick = BoutonConsultationClick
      end
      object BoutonCreation: TButton
        Left = 188
        Top = 4
        Width = 49
        Height = 25
        Hint = 'Cr'#233'ation du chantier en cours de saisie'
        Caption = 'Creer'
        Enabled = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 9
        TabStop = False
        OnClick = BoutonCreationClick
      end
      object NomChantier: TEdit
        Left = 136
        Top = 106
        Width = 222
        Height = 21
        Hint = 'Nom du chantier en cours'
        TabStop = False
        AutoSize = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 13
        Text = 'NomChantier'
        OnClick = NomChantierClick
        OnKeyPress = NomChantierKeyPress
      end
      object LibelleChantier: TEdit
        Left = 136
        Top = 131
        Width = 222
        Height = 21
        Hint = 'Ville du chantier en cours'
        TabStop = False
        AutoSize = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 14
        Text = 'LibelleChantier'
        OnClick = LibelleChantierClick
      end
      object ListeChantiers: TDBLookupComboBox
        Left = 291
        Top = 6
        Width = 70
        Height = 21
        Hint = 'Liste des chantiers enregistr'#233's'
        KeyField = 'cha_numchantier'
        ListField = 'cha_numchantier'
        ListSource = DataSourcechantiers
        ParentShowHint = False
        ShowHint = True
        TabOrder = 15
        TabStop = False
        Visible = False
        OnClick = ListeChantiersClick
        OnExit = ListeChantiersExit
      end
    end
    object Panel11: TPanel
      Left = 1
      Top = 500
      Width = 368
      Height = 221
      BorderStyle = bsSingle
      TabOrder = 10
      object Label49: TLabel
        Left = 23
        Top = 46
        Width = 121
        Height = 13
        Caption = 'Frais induits (ass + tel cht)'
      end
      object LabelFraisInduits: TLabel
        Left = 230
        Top = 46
        Width = 65
        Height = 18
        Alignment = taRightJustify
        AutoSize = False
        Caption = '000.00'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 263
        Top = 31
        Width = 22
        Height = 13
        Caption = 'Co'#251't'
      end
      object Label8: TLabel
        Left = 49
        Top = 29
        Width = 66
        Height = 13
        Caption = 'Frais Externes'
      end
      object ListeExterne: TDBLookupComboBox
        Left = 4
        Top = 4
        Width = 208
        Height = 21
        Hint = 'Liste des frais externes enregistr'#233's'
        KeyField = 'id_fraisexterne'
        ListField = 'ext_libellenom'
        ListSource = DataSourceExterne
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TabStop = False
        OnClick = ListeEmployesClick
        OnEnter = ListeEmployesEnter
      end
      object ListeUtilisationEXT: TStringGrid
        Left = 2
        Top = 63
        Width = 359
        Height = 146
        Hint = 'Liste des frais externes de la fiche en cours'
        TabStop = False
        ColCount = 13
        DefaultColWidth = 50
        FixedCols = 0
        RowCount = 1
        FixedRows = 0
        Options = [goDrawFocusSelected, goTabs, goAlwaysShowEditor]
        ParentShowHint = False
        ScrollBars = ssVertical
        ShowHint = True
        TabOrder = 1
        OnClick = ListePointageEMPClick
        OnDblClick = ListePointageEMPDblClick
        OnEnter = ListePointageEMPEnter
        OnGetEditMask = ListePointageEMPGetEditMask
        OnKeyPress = ListePointageEMPKeyPress
        OnKeyUp = ListePointageEMPKeyUp
        OnMouseUp = ListePointageEMPMouseUp
        OnSelectCell = ListePointageEMPSelectCell
        ColWidths = (
          50
          52
          50
          50
          50
          50
          50
          50
          50
          50
          50
          50
          50)
      end
    end
    object BoutonEnregistrer: TButton
      Left = 39
      Top = 165
      Width = 56
      Height = 25
      Hint = 'Sauvegarde fiche en cours'
      Caption = 'Sauver'
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      TabStop = False
      OnClick = BoutonEnregistrerClick
    end
    object BoutonImprimer: TButton
      Left = 117
      Top = 165
      Width = 56
      Height = 25
      Hint = 'Imprimer fiche en cours'
      Caption = 'Imprimer'
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      TabStop = False
      OnClick = BoutonImprimerClick
    end
    object BoutonAnnuler: TButton
      Left = 196
      Top = 165
      Width = 56
      Height = 25
      Hint = 'Fermeture fiche en cours'
      Caption = 'Fermer'
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      TabStop = False
      OnClick = BoutonAnnulerClick
    end
    object BoutonSupprimer: TButton
      Left = 274
      Top = 165
      Width = 56
      Height = 25
      Hint = 'Supprimer fiche en cours'
      Caption = 'Effacer'
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 6
      TabStop = False
      OnClick = BoutonSupprimerClick
    end
    object Panel14: TPanel
      Left = 370
      Top = 1
      Width = 594
      Height = 300
      BorderStyle = bsSingle
      TabOrder = 3
      DesignSize = (
        590
        296)
      object Label13: TLabel
        Left = 40
        Top = 34
        Width = 40
        Height = 13
        Caption = 'Employ'#233
      end
      object Label14: TLabel
        Left = 274
        Top = 34
        Width = 16
        Height = 13
        Caption = 'HR'
      end
      object Label15: TLabel
        Left = 316
        Top = 34
        Width = 13
        Height = 13
        Caption = 'HJ'
      end
      object Label16: TLabel
        Left = 355
        Top = 34
        Width = 15
        Height = 13
        Caption = 'HS'
      end
      object Label17: TLabel
        Left = 234
        Top = 34
        Width = 20
        Height = 13
        Caption = 'NBJ'
      end
      object Label18: TLabel
        Left = 196
        Top = 34
        Width = 15
        Height = 13
        Caption = 'TH'
      end
      object Label19: TLabel
        Left = 389
        Top = 34
        Width = 24
        Height = 13
        Caption = 'HND'
      end
      object Label20: TLabel
        Left = 435
        Top = 34
        Width = 7
        Height = 13
        Caption = 'P'
      end
      object Label21: TLabel
        Left = 474
        Top = 34
        Width = 8
        Height = 13
        Caption = 'G'
      end
      object Label22: TLabel
        Left = 517
        Top = 34
        Width = 35
        Height = 13
        Caption = 'TOTAL'
      end
      object ListePointageEMP: TStringGrid
        Left = 3
        Top = 53
        Width = 583
        Height = 161
        Hint = 'Liste des pointages des employ'#233's de la fiche en cours'
        Anchors = [akTop, akRight]
        ColCount = 13
        DefaultColWidth = 100
        FixedCols = 0
        RowCount = 1
        FixedRows = 0
        Options = [goDrawFocusSelected, goColSizing, goEditing, goTabs, goAlwaysShowEditor]
        ParentShowHint = False
        ScrollBars = ssVertical
        ShowHint = True
        TabOrder = 1
        OnClick = ListePointageEMPClick
        OnDblClick = ListePointageEMPDblClick
        OnEnter = ListePointageEMPEnter
        OnGetEditMask = ListePointageEMPGetEditMask
        OnKeyPress = ListePointageEMPKeyPress
        OnKeyUp = ListePointageEMPKeyUp
        OnMouseUp = ListePointageEMPMouseUp
        OnSelectCell = ListePointageEMPSelectCell
      end
      object ListePointageINT: TStringGrid
        Left = 3
        Top = 216
        Width = 583
        Height = 77
        Hint = 'Liste des pointages des interimaires de la fiche en cours'
        TabStop = False
        ColCount = 13
        DefaultColWidth = 60
        FixedCols = 0
        RowCount = 1
        FixedRows = 0
        Options = [goTabs, goAlwaysShowEditor]
        ParentShowHint = False
        ScrollBars = ssVertical
        ShowHint = True
        TabOrder = 2
        OnClick = ListePointageEMPClick
        OnDblClick = ListePointageEMPDblClick
        OnEnter = ListePointageEMPEnter
        OnGetEditMask = ListePointageEMPGetEditMask
        OnKeyPress = ListePointageEMPKeyPress
        OnKeyUp = ListePointageEMPKeyUp
        OnMouseUp = ListePointageEMPMouseUp
        OnSelectCell = ListePointageEMPSelectCell
      end
      object ListeEmployes: TDBLookupComboBox
        Left = 3
        Top = 4
        Width = 201
        Height = 21
        Hint = 'Liste des employ'#233's enregistr'#233's'
        KeyField = 'id_employe'
        ListField = 'emp_nomemploye'
        ListSource = DataSourceEmployes
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TabStop = False
        OnClick = ListeEmployesClick
        OnEnter = ListeEmployesEnter
      end
    end
    object Panel5: TPanel
      Left = 370
      Top = 302
      Width = 328
      Height = 275
      BorderStyle = bsSingle
      TabOrder = 7
      object Label7: TLabel
        Left = 61
        Top = 29
        Width = 37
        Height = 13
        Caption = 'Materiel'
      end
      object Label11: TLabel
        Left = 252
        Top = 30
        Width = 35
        Height = 13
        Caption = 'TOTAL'
      end
      object Label23: TLabel
        Left = 216
        Top = 30
        Width = 17
        Height = 13
        Caption = 'Nbr'
      end
      object Label37: TLabel
        Left = 173
        Top = 30
        Width = 22
        Height = 13
        Caption = 'Co'#251't'
      end
      object ListeMateriels: TDBLookupComboBox
        Left = 3
        Top = 2
        Width = 201
        Height = 21
        Hint = 'Liste des materiels enregistr'#233's'
        KeyField = 'id_materiel'
        ListField = 'mat_libellenom'
        ListSource = DataSourceMateriels
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TabStop = False
        OnClick = ListeEmployesClick
        OnEnter = ListeEmployesEnter
      end
      object ListeUtilisationMAT: TStringGrid
        Left = 3
        Top = 50
        Width = 317
        Height = 218
        Hint = 'Liste des materiels de la fiche en cours'
        TabStop = False
        ColCount = 13
        DefaultColWidth = 50
        FixedCols = 0
        RowCount = 1
        FixedRows = 0
        Options = [goDrawFocusSelected, goTabs, goAlwaysShowEditor]
        ParentShowHint = False
        ScrollBars = ssVertical
        ShowHint = True
        TabOrder = 1
        OnClick = ListePointageEMPClick
        OnDblClick = ListePointageEMPDblClick
        OnEnter = ListePointageEMPEnter
        OnGetEditMask = ListePointageEMPGetEditMask
        OnKeyPress = ListePointageEMPKeyPress
        OnKeyUp = ListePointageEMPKeyUp
        OnMouseUp = ListePointageEMPMouseUp
        OnSelectCell = ListePointageEMPSelectCell
        ColWidths = (
          50
          52
          50
          50
          50
          50
          50
          50
          50
          50
          50
          50
          50)
      end
    end
    object Panel16: TPanel
      Left = 1
      Top = 355
      Width = 368
      Height = 145
      BorderStyle = bsSingle
      TabOrder = 11
      object Label54: TLabel
        Left = 60
        Top = 30
        Width = 41
        Height = 13
        Caption = 'Vehicule'
      end
      object Label55: TLabel
        Left = 286
        Top = 31
        Width = 35
        Height = 13
        Caption = 'TOTAL'
      end
      object Label57: TLabel
        Left = 240
        Top = 31
        Width = 17
        Height = 13
        Caption = 'Nbr'
      end
      object Label58: TLabel
        Left = 203
        Top = 31
        Width = 22
        Height = 13
        Caption = 'Co'#251't'
      end
      object ListeVehicules: TDBLookupComboBox
        Left = 6
        Top = 3
        Width = 206
        Height = 21
        Hint = 'Liste des v'#233'hicules enregistr'#233's'
        KeyField = 'id_vehicule'
        ListField = 'veh_libellenom'
        ListSource = DataSourceVehicule
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TabStop = False
        OnClick = ListeEmployesClick
        OnEnter = ListeEmployesEnter
      end
      object ListeUtilisationVEH: TStringGrid
        Left = 1
        Top = 47
        Width = 360
        Height = 90
        Hint = 'Liste des pointages des v'#233'hicules de la fiche en cours'
        TabStop = False
        ColCount = 13
        DefaultColWidth = 50
        FixedCols = 0
        RowCount = 1
        FixedRows = 0
        Options = [goDrawFocusSelected, goTabs, goAlwaysShowEditor]
        ParentShowHint = False
        ScrollBars = ssVertical
        ShowHint = True
        TabOrder = 1
        OnClick = ListePointageEMPClick
        OnDblClick = ListePointageEMPDblClick
        OnEnter = ListePointageEMPEnter
        OnGetEditMask = ListePointageEMPGetEditMask
        OnKeyPress = ListePointageEMPKeyPress
        OnKeyUp = ListePointageEMPKeyUp
        OnMouseUp = ListePointageEMPMouseUp
        OnSelectCell = ListePointageEMPSelectCell
        ColWidths = (
          50
          52
          50
          50
          50
          50
          50
          50
          50
          50
          50
          50
          50)
      end
    end
    object Panel17: TPanel
      Left = 1
      Top = 194
      Width = 368
      Height = 162
      BorderStyle = bsSingle
      TabOrder = 12
      object Label59: TLabel
        Left = 46
        Top = 30
        Width = 39
        Height = 13
        Caption = 'Diamant'
      end
      object Label60: TLabel
        Left = 240
        Top = 31
        Width = 25
        Height = 13
        Caption = 'Coeff'
      end
      object Label61: TLabel
        Left = 286
        Top = 31
        Width = 35
        Height = 13
        Caption = 'TOTAL'
      end
      object Label62: TLabel
        Left = 203
        Top = 31
        Width = 19
        Height = 13
        Caption = 'NbJ'
      end
      object Label63: TLabel
        Left = 162
        Top = 31
        Width = 22
        Height = 13
        Caption = 'Co'#251't'
      end
      object ListeDiamant: TDBLookupComboBox
        Left = 4
        Top = 3
        Width = 208
        Height = 21
        Hint = 'Liste des interventions enregistr'#233'es'
        KeyField = 'id_travail'
        ListField = 'tra_libellenom'
        ListSource = DataSourceDiamant
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TabStop = False
        OnClick = ListeEmployesClick
        OnEnter = ListeEmployesEnter
      end
      object ListeUtilisationDIA: TStringGrid
        Left = 1
        Top = 46
        Width = 360
        Height = 111
        Hint = 'Liste des pointages d'#39'interventions de la fiche en cours'
        TabStop = False
        ColCount = 13
        DefaultColWidth = 50
        FixedCols = 0
        RowCount = 1
        FixedRows = 0
        Options = [goDrawFocusSelected, goTabs, goAlwaysShowEditor]
        ParentShowHint = False
        ScrollBars = ssVertical
        ShowHint = True
        TabOrder = 1
        OnClick = ListePointageEMPClick
        OnDblClick = ListePointageEMPDblClick
        OnEnter = ListePointageEMPEnter
        OnGetEditMask = ListePointageEMPGetEditMask
        OnKeyPress = ListePointageEMPKeyPress
        OnKeyUp = ListePointageEMPKeyUp
        OnMouseUp = ListePointageEMPMouseUp
        OnSelectCell = ListePointageEMPSelectCell
        ColWidths = (
          50
          52
          50
          50
          50
          50
          50
          50
          50
          50
          50
          50
          50)
      end
    end
    object Panel13: TPanel
      Left = 698
      Top = 455
      Width = 266
      Height = 266
      BorderStyle = bsSingle
      TabOrder = 1
      object Label25: TLabel
        Left = 5
        Top = 31
        Width = 109
        Height = 12
        AutoSize = False
        Caption = 'SALARIE'
      end
      object Label26: TLabel
        Left = 5
        Top = 50
        Width = 110
        Height = 12
        AutoSize = False
        Caption = 'INTERIM'
      end
      object Label27: TLabel
        Left = 5
        Top = 69
        Width = 110
        Height = 13
        AutoSize = False
        Caption = 'VEHICULE'
      end
      object Label28: TLabel
        Left = 5
        Top = 88
        Width = 110
        Height = 13
        AutoSize = False
        Caption = 'DIAMANT'
      end
      object Label29: TLabel
        Left = 5
        Top = 108
        Width = 110
        Height = 11
        AutoSize = False
        Caption = 'MATERIEL'
      end
      object Label30: TLabel
        Left = 5
        Top = 182
        Width = 110
        Height = 15
        AutoSize = False
        Caption = 'DEBOURSE SEC'
      end
      object Label31: TLabel
        Left = 5
        Top = 126
        Width = 110
        Height = 12
        AutoSize = False
        Caption = 'OUTILLAGE + CONS'
      end
      object Label32: TLabel
        Left = 5
        Top = 201
        Width = 110
        Height = 13
        AutoSize = False
        Caption = 'FRAIS DE STRUC'
      end
      object Label33: TLabel
        Left = 5
        Top = 221
        Width = 110
        Height = 25
        AutoSize = False
        Caption = 'PRIX DE REVIENT'
      end
      object Label34: TLabel
        Left = 5
        Top = 240
        Width = 110
        Height = 15
        AutoSize = False
        Caption = 'RESULTAT'
      end
      object Label38: TLabel
        Left = 6
        Top = 145
        Width = 110
        Height = 14
        AutoSize = False
        Caption = 'FRAIS EXTERNES'
      end
      object Label39: TLabel
        Left = 5
        Top = 164
        Width = 110
        Height = 17
        AutoSize = False
        Caption = 'ACHATS TRX INTER'
      end
      object ListeCalculs: TStringGrid
        Left = 113
        Top = 26
        Width = 145
        Height = 232
        Hint = 'Totaux de la fiche en cours'
        TabStop = False
        ColCount = 3
        DefaultColWidth = 83
        DefaultRowHeight = 19
        Enabled = False
        FixedCols = 0
        RowCount = 12
        FixedRows = 0
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Pitch = fpFixed
        Font.Style = []
        Options = []
        ParentFont = False
        ParentShowHint = False
        ScrollBars = ssNone
        ShowHint = True
        TabOrder = 1
      end
      object StaticText8: TStaticText
        Left = 4
        Top = 7
        Width = 67
        Height = 17
        Caption = 'Chiffre Affaire'
        TabOrder = 2
      end
      object SaisieCA: TEdit
        Left = 113
        Top = 1
        Width = 121
        Height = 21
        Hint = 'Chiffre d'#39'affaire de la fiche en cours'
        TabStop = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = SaisieCAClick
        OnEnter = SaisieCAEnter
        OnKeyPress = SaisieCAKeyPress
      end
    end
    object Panel6: TPanel
      Left = 698
      Top = 302
      Width = 266
      Height = 152
      BorderStyle = bsSingle
      TabOrder = 8
      object Label40: TLabel
        Left = 49
        Top = 30
        Width = 64
        Height = 13
        Caption = 'Achat Interne'
      end
      object Label44: TLabel
        Left = 201
        Top = 30
        Width = 22
        Height = 13
        Caption = 'Co'#251't'
      end
      object ListeAchats: TDBLookupComboBox
        Left = 4
        Top = 3
        Width = 201
        Height = 21
        Hint = 'Listes des agences acheteuses enregistr'#233'es'
        KeyField = 'id_agence'
        ListField = 'age_libellenom'
        ListSource = DataSourceSecteurs
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TabStop = False
        OnClick = ListeEmployesClick
        OnEnter = ListeEmployesEnter
      end
      object ListeUtilisationACH: TStringGrid
        Left = 2
        Top = 46
        Width = 256
        Height = 97
        Hint = 'Liste des achats aux agences de la fiche en cours'
        TabStop = False
        ColCount = 13
        DefaultColWidth = 50
        FixedCols = 0
        RowCount = 1
        FixedRows = 0
        Options = [goDrawFocusSelected, goTabs, goAlwaysShowEditor]
        ParentShowHint = False
        ScrollBars = ssVertical
        ShowHint = True
        TabOrder = 1
        OnClick = ListePointageEMPClick
        OnDblClick = ListePointageEMPDblClick
        OnEnter = ListePointageEMPEnter
        OnGetEditMask = ListePointageEMPGetEditMask
        OnKeyPress = ListePointageEMPKeyPress
        OnKeyUp = ListePointageEMPKeyUp
        OnMouseUp = ListePointageEMPMouseUp
        OnSelectCell = ListePointageEMPSelectCell
        ColWidths = (
          50
          52
          50
          50
          50
          50
          50
          50
          50
          50
          50
          50
          50)
      end
    end
    object Panel9: TPanel
      Left = 370
      Top = 576
      Width = 328
      Height = 145
      BorderStyle = bsSingle
      TabOrder = 9
      object Label12: TLabel
        Left = 40
        Top = 31
        Width = 80
        Height = 13
        Caption = 'Frais de Sructure'
      end
      object Label45: TLabel
        Left = 254
        Top = 32
        Width = 35
        Height = 13
        Caption = 'TOTAL'
      end
      object Label47: TLabel
        Left = 215
        Top = 32
        Width = 17
        Height = 13
        Caption = 'Nbr'
      end
      object Label48: TLabel
        Left = 172
        Top = 32
        Width = 22
        Height = 13
        Caption = 'Co'#251't'
      end
      object ListeStructures: TDBLookupComboBox
        Left = 3
        Top = 5
        Width = 201
        Height = 21
        Hint = 'Liste des frais de structure enregistr'#233's'
        KeyField = 'id_fraisstructure'
        ListField = 'str_libellenom'
        ListSource = DataSourceStructure
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TabStop = False
        OnClick = ListeEmployesClick
        OnEnter = ListeEmployesEnter
      end
      object ListeUtilisationSTR: TStringGrid
        Left = 3
        Top = 48
        Width = 317
        Height = 88
        Hint = 'Liste des frais de stucture de la fiche en cours'
        TabStop = False
        ColCount = 13
        DefaultColWidth = 50
        FixedCols = 0
        RowCount = 1
        FixedRows = 0
        Options = [goDrawFocusSelected, goTabs, goAlwaysShowEditor]
        ParentShowHint = False
        ScrollBars = ssVertical
        ShowHint = True
        TabOrder = 1
        OnClick = ListePointageEMPClick
        OnDblClick = ListePointageEMPDblClick
        OnEnter = ListePointageEMPEnter
        OnGetEditMask = ListePointageEMPGetEditMask
        OnKeyPress = ListePointageEMPKeyPress
        OnKeyUp = ListePointageEMPKeyUp
        OnMouseUp = ListePointageEMPMouseUp
        OnSelectCell = ListePointageEMPSelectCell
        ColWidths = (
          50
          52
          50
          50
          50
          50
          50
          50
          50
          50
          50
          50
          50)
      end
    end
    object ListeFiches: TComboBox
      Left = 9
      Top = 7
      Width = 111
      Height = 21
      Sorted = True
      TabOrder = 13
      Visible = False
      OnClick = ListeFichesClick
      OnExit = ListeFichesExit
    end
  end
  object MainMenu: TMainMenu
    Left = 1000
    Top = 8
    object Saisie1: TMenuItem
      Caption = '&Saisie'
      object FicheHebdomadaire: TMenuItem
        Caption = 'Fiche &Hebdomadaire'
        ShortCut = 16456
        OnClick = FicheHebdomadaireClick
      end
      object FichedeRegularisation: TMenuItem
        Caption = 'Fiche de &Regularisation'
        ShortCut = 16466
        OnClick = FichedeRegularisationClick
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object ImpressionEtats: TMenuItem
        Caption = 'Impression Etats'
        ShortCut = 16453
        OnClick = ImpressionEtatsClick
      end
      object Imprimante: TMenuItem
        Caption = 'Configuration imprimante'
        OnClick = ImprimanteClick
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object Quitter: TMenuItem
        Caption = '&Quitter'
        OnClick = QuitterClick
      end
    end
    object Fiches1: TMenuItem
      Caption = '&Fiches'
      object Materiels: TMenuItem
        Caption = '&Materiels'
        OnClick = MaterielsClick
      end
      object Employes: TMenuItem
        Caption = '&Employ'#233's'
        OnClick = EmployesClick
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object Secteurs: TMenuItem
        Caption = '&Agences'
        OnClick = SecteursClick
      end
      object Commercial: TMenuItem
        Caption = 'C&ommerciaux'
        OnClick = CommercialClick
      end
      object Clients: TMenuItem
        Caption = 'C&lients'
        OnClick = ClientsClick
      end
      object N7: TMenuItem
        Caption = '-'
      end
      object Vehicules: TMenuItem
        Caption = '&Vehicules'
        OnClick = VehiculesClick
      end
      object TravauxDiamant: TMenuItem
        Caption = '&Travaux (Diamant)'
        OnClick = TravauxDiamantClick
      end
      object N5: TMenuItem
        Caption = '-'
      end
      object FraisExternes: TMenuItem
        Caption = 'Frais E&xternes'
        OnClick = FraisExternesClick
      end
      object FraisStructures: TMenuItem
        Caption = 'Frais Str&uctures'
        OnClick = FraisStructuresClick
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Verrou1: TMenuItem
        Caption = 'Verrou'
        ShortCut = 16470
        Visible = False
        OnClick = Verrou1Click
      end
      object Configuration: TMenuItem
        Caption = '&Configuration'
        OnClick = ConfigurationClick
      end
      object Exportation: TMenuItem
        Caption = 'Exportation Clients'
        ShortCut = 16472
        OnClick = ExportationClick
      end
      object VerificationIndex: TMenuItem
        Caption = 'Verification des index'
        OnClick = VerificationIndexClick
      end
    end
  end
  object TableTemp: TTable
    DatabaseName = 'BASEGCFORA'
    TableType = ttParadox
    Left = 840
    Top = 120
  end
  object DataSourcechantiers: TDataSource
    DataSet = Tablechantiers
    Left = 1128
    Top = 16
  end
  object Tablechantiers: TTable
    DatabaseName = 'BASEGCFORA'
    TableName = 'chantiers.DB'
    Left = 1256
    Top = 8
  end
  object DataSourceSecteurs: TDataSource
    DataSet = TableSecteurs
    Left = 1128
    Top = 64
  end
  object TableSecteurs: TTable
    DatabaseName = 'BASEGCFORA'
    TableName = 'secteurs.DB'
    Left = 1256
    Top = 56
  end
  object DataSourceCommerciaux: TDataSource
    DataSet = TableCommerciaux
    Left = 1128
    Top = 176
  end
  object TableCommerciaux: TTable
    DatabaseName = 'BASEGCFORA'
    TableName = 'commercial.DB'
    Left = 1256
    Top = 104
  end
  object DataSourceClients: TDataSource
    DataSet = TableClients
    Left = 1128
    Top = 120
  end
  object TableClients: TTable
    DatabaseName = 'BASEGCFORA'
    IndexName = 'indnom'
    TableName = 'clients.DB'
    Left = 1256
    Top = 152
  end
  object DataSourceHebdo: TDataSource
    DataSet = TableHebdo
    Left = 1128
    Top = 232
  end
  object TableHebdo: TTable
    DatabaseName = 'BASEGCFORA'
    TableName = 'fiche_hebdo.DB'
    Left = 1256
    Top = 200
  end
  object DataSourceEmployes: TDataSource
    DataSet = TableEmployes
    Left = 1126
    Top = 580
  end
  object TableEmployes: TTable
    DatabaseName = 'BASEGCFORA'
    TableName = 'employes'
    TableType = ttParadox
    Left = 838
    Top = 68
  end
  object DataSourceMateriels: TDataSource
    DataSet = TableMateriels
    Left = 1128
    Top = 408
  end
  object TableMateriels: TTable
    DatabaseName = 'BASEGCFORA'
    SessionName = 'Default'
    TableName = 'materiels'
    TableType = ttParadox
    Left = 1256
    Top = 768
  end
  object DataSourceVehicule: TDataSource
    DataSet = TableVehicules
    Left = 1128
    Top = 288
  end
  object DataSourceStructure: TDataSource
    DataSet = TableStructures
    Left = 1128
    Top = 520
  end
  object DataSourceExterne: TDataSource
    DataSet = TableExterne
    Left = 1128
    Top = 464
  end
  object DataSourceDiamant: TDataSource
    DataSet = TableDiamant
    Left = 1128
    Top = 352
  end
  object TableVehicules: TTable
    DatabaseName = 'BASEGCFORA'
    SessionName = 'Default'
    TableName = 'vehicules'
    TableType = ttParadox
    Left = 1254
    Top = 682
  end
  object TableStructures: TTable
    DatabaseName = 'BASEGCFORA'
    SessionName = 'Default'
    TableName = 'structure'
    TableType = ttParadox
    Left = 1256
    Top = 584
  end
  object TableExterne: TTable
    DatabaseName = 'BASEGCFORA'
    SessionName = 'Default'
    TableName = 'externe'
    TableType = ttParadox
    Left = 1256
    Top = 344
  end
  object TableDiamant: TTable
    DatabaseName = 'BASEGCFORA'
    SessionName = 'Default'
    TableName = 'diamant'
    TableType = ttParadox
    Left = 1256
    Top = 632
  end
  object SouFichPoint: TDataSource
    DataSet = TabFichPoint
    Left = 999
    Top = 453
  end
  object TabFichPoint: TTable
    DatabaseName = 'BASEGCFORA'
    TableName = 'fiche_pointage.DB'
    Left = 1256
    Top = 440
  end
  object TabFichMat: TTable
    DatabaseName = 'BASEGCFORA'
    TableName = 'fiche_materiel'
    TableType = ttParadox
    Left = 1256
    Top = 724
  end
  object TabFichVehic: TTable
    DatabaseName = 'BASEGCFORA'
    TableName = 'fiche_vehicule'
    TableType = ttParadox
    Left = 1256
    Top = 296
  end
  object TabFichStruc: TTable
    DatabaseName = 'BASEGCFORA'
    TableName = 'fiche_structure'
    TableType = ttParadox
    Left = 1256
    Top = 392
  end
  object TabFichExter: TTable
    DatabaseName = 'BASEGCFORA'
    TableName = 'fiche_externe'
    TableType = ttParadox
    Left = 1256
    Top = 488
  end
  object TabFichDiam: TTable
    DatabaseName = 'BASEGCFORA'
    TableName = 'fiche_diamant'
    TableType = ttParadox
    Left = 1256
    Top = 248
  end
  object TabFichAcha: TTable
    DatabaseName = 'BASEGCFORA'
    TableName = 'fiche_achat'
    TableType = ttParadox
    Left = 1256
    Top = 536
  end
  object SouFichMat: TDataSource
    DataSet = TabFichMat
    Left = 1000
    Top = 283
  end
  object SouFichVeh: TDataSource
    DataSet = TabFichVehic
    Left = 1000
    Top = 217
  end
  object SouFichStruc: TDataSource
    DataSet = TabFichStruc
    Left = 1000
    Top = 394
  end
  object SouFichExter: TDataSource
    DataSet = TabFichExter
    Left = 1000
    Top = 338
  end
  object SouFichDiam: TDataSource
    DataSet = TabFichDiam
    Left = 1000
    Top = 168
  end
  object SouFichAcha: TDataSource
    DataSet = TabFichAcha
    Left = 1000
    Top = 511
  end
  object PrinterSetupDialog: TPrinterSetupDialog
    Left = 1000
    Top = 80
  end
  object TabCha: TTable
    DatabaseName = 'BASEGCFORA'
    TableName = 'chantiers'
    TableType = ttParadox
    Left = 837
    Top = 174
  end
  object TabFich: TTable
    DatabaseName = 'BASEGCFORA'
    IndexName = 'indse'
    TableName = 'fiche_hebdo'
    TableType = ttParadox
    Left = 764
    Top = 70
  end
  object Database: TDatabase
    DatabaseName = 'BASEGCFORA'
    DriverName = 'STANDARD'
    SessionName = 'Default'
    Left = 772
    Top = 140
  end
end
