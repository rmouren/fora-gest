unit MasqueSemaine;

interface

uses

  windows, SysUtils, Dialogs, Controls;

procedure TraiteNumSemaine;
procedure setSaisieNumSemaine(txt: string);
function GetSaisieNumSemaine: string;

implementation

uses

  MasqueChantiers, MasqueEmployes, Hebdo, accesBDD,
  winbase;

function GetSaisieNumSemaine: string;
begin
  result := FormBase.saisieNumSemaine.Text;
end;

procedure setSaisieNumSemaine(txt: string);
begin
  FormBase.saisieNumSemaine.Text := txt;
end;

procedure TraiteNumSemaine;

{
  cette routine opere plusieurs taches
  - lecture saisie num semaine et chantier
  - suivant le mode en cours hebdo ou regul verifie l'existence d'une fiche
  - en cas d'abscence en creer une ou alors la charge a partir de la BDD
  - bascule le soft dans le mode adequat creation ou modif ou consultation
}

  function ChoixRegul: boolean;
  begin
    result := FicheRegul;
  end;

  function LireNumMois: integer;
  begin
    result := FormBase.ListeMois.ItemIndex + 1;
  end;

  function LireNumSemaine: integer;
  begin
    result := strtoint(GetSaisieNumSemaine);
  end;

  function ErreurNumMois(nummois: integer): boolean;
  begin
    result := false;
    if (nummois < 1) or (nummois > 12) then
    begin
      // erreur de saisie de numero de semaine
      showmessage('Erreur de numero de mois recommencez.');
      result := true;
    end;
  end;

  function ErreurNumSemaine(numsemaine: integer): boolean;
  begin
    result := false;
    if (numsemaine < 1) or (numsemaine > 52) then
    begin
      // erreur de saisie de numero de semaine
      showmessage('Erreur de numero de semaine recommencez.');
      result := true;
    end;
  end;

  function ErreurChoixChantier: boolean;
  begin
    result := false;
    if ChoixChantierOK = false then
    begin
      // erreur de saisie de numero de semaine
      showmessage('Erreur de choix de numero de chantier recommencez.');
      result := true;
    end;
  end;

var
  n: integer;
  nc: integer;
  c: boolean;
  ver: boolean;
  b: boolean;
begin
  // pour eviter les evenements intempestifs
  if ChargeEncours then
    exit;

  // traite eventuellement le chantier
  TraiteNumChantier;

  // suivant type de fiche regul ou non verifier la saisie
  c := ChoixRegul;
  b := false;
  if c then
  begin
    // verifie si le numero de mois est correct sinon message
    n := LireNumMois;
    if ErreurNumMois(n) then
      exit;
  end
  else
  begin
    // verifie si le numero de semaine est correct sinon message
    n := LireNumSemaine;
    if ErreurNumSemaine(n) then
      exit;

    b := FormBase.SaisieSemaineBis.Checked;
  end;

  // verifie saisie de numero de chantier
  if ErreurChoixChantier then
    exit;

  // lire identifiant de chantier
  nc := LireIdentifiantChantier;

  // effacer les listes annexes
  MasquesViderListe;

  // cherche si un hebdo existe
  if ChercheHebdo(c, b, nc, n) then
  begin
    // charger fiche hebdo trouvee
    ChargeHebdo;

    ver := FormBase.TableHebdo.FieldByName('heb_verrou').asboolean;
    if ver then
    begin
      // mise en mode seulement de consultation
      SetModeHebdoConsult;

      // afficher les sommes de l'hebdo
      AfficheSommesHebdo;

      if c then
        AfficheTitre(3)
      else
        AfficheTitre(4);
    end
    else
    begin
      // mise en mode modifs possible
      SetModeHebdoModif;

      // recalculer les sommes de l'hebdo et les afficher
      CalculGeneralHebdo;

      if c then
        AfficheTitre(1)
      else
        AfficheTitre(2);
    end;

    exit;
  end;

  // mise en mode creation
  SetModeHebdoCreation;

  if c then
    AfficheTitre(5)
  else
    AfficheTitre(6);
end;

end.
