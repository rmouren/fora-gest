unit MasqueMateriels;

interface

procedure ResetCumulsMateriel;
procedure CumulsMaterielLire(var nbr: integer);
procedure CumulsMateriel(idh: integer);
procedure MaterielViderListe;
procedure MaterielInitMasque;
procedure MaterielExecuterCalcul;
function MaterielLireCalcul: currency;
procedure MaterielEnreg(numhebdo: integer);
procedure MaterielCharge(index: integer);
procedure MaterielChargeChaines(index: integer; var nbrec: integer);
procedure MaterielSupprime(index: integer);
procedure MaterielModifsNon;
procedure MaterielModifsOui;
function MaterielLireModifsEtat: boolean;

function traiteMaterielLireFormatCellules(ACol: integer): string;
procedure traiteMaterielsouris;
procedure traiteMaterielInitialiserProcEffaceLigne(X, Y: integer);
function TraiteMaterielSelection(ACol, ARow: integer): boolean;
procedure TraiteMaterielValide(var Key: Char);
procedure traiteMaterieltouches(Key: word);
procedure traiteMaterieltouchesFIN();
procedure TraiteMaterielChargeLigne;
procedure TraiteMaterielEffacerLigne;

function Veriflignematerieldiversexiste: integer;
procedure AffecterNombreMaterieldivers(ligne: integer; Valeur: currency);

implementation

uses

  SysUtils, grids, Dialogs, windows,
  MasqueVehicules,
  winbase, accesBDD, Hebdo, impression, utils;

const

  colindex = 0;
  colNom = colindex;
  colcout = 1;
  colnombre = 2;
  colsommeligne = 3;

  coldeselection = 10;
  colautoselection = colnombre;

type

  TlargeurLigne = array [colNom .. colsommeligne] of integer;

const

  Largeur: TlargeurLigne = (160, 40, 40, 60);

var

  TblNomMAT: array of array of string;
  TblValMAT: array of array of currency;
  nbMAT: integer;

  positioncolonne: integer = 1;
  positionligne: integer;
  LigneEnCours: boolean = true;
  TableauMateriels: array of array of currency;
  CelluleEnCours: boolean;
  EditionEnCours: boolean;
  CelluleEnCoursCol: integer;
  CelluleEnCoursRow: integer;
  Suppressionencours: boolean;
  ModeLigneAppend: boolean;

  ModifsEtat: boolean;
  AutoriseModifs: boolean;

  SommeMAT: currency;

{$H-}

  /// ///////////////////////////////////////////////////////////////////////////
procedure unselectMAT;
begin
  Suppressionencours := true;
  FormBase.ListeUtilisationMAT.Col := coldeselection;
  FormBase.ListeUtilisationMAT.Row := 0;
  Suppressionencours := false;
end;

procedure MaterielInitMasque;
var
  i: integer;
begin
  for i := colNom to colsommeligne do
    FormBase.ListeUtilisationMAT.ColWidths[i] := Largeur[i];

  for i := colsommeligne + 1 to coldeselection do
    FormBase.ListeUtilisationMAT.ColWidths[i] := 0;

  unselectMAT;
end;

procedure MaterielModifsOui;
begin
  AutoriseModifs := true;
  FormBase.ListeMateriels.Enabled := true;
end;

procedure MaterielModifsNon;
begin
  unselectMAT;
  AutoriseModifs := false;
  FormBase.ListeMateriels.Enabled := false;
  FormBase.ListeMateriels.KeyValue := -1;
end;

function MaterielLireModifsEtat: boolean;
begin
  result := ModifsEtat;
end;

procedure SetModifsEtat;
begin
  ModifsEtat := true;

  // prevenir qu'il y a eu une modif d'un masque
  EvenementModifMasqueAnnexe;
end;

procedure ResetModifsEtat;
begin
  ModifsEtat := false;
end;

procedure MaterielViderListe;

  procedure viderligne(fin: integer);
  var
    C: integer;
  begin
    for C := colindex to colsommeligne do
      FormBase.ListeUtilisationMAT.Cells[C, fin - 1] := '';
  end;

begin
  // pour ne pas repasser par le traitement evenement masque
  LigneEnCours := true;

  while FormBase.ListeUtilisationMAT.RowCount > 1 do
  begin
    positioncolonne := 0;
    positionligne := FormBase.ListeUtilisationMAT.RowCount - 2;
    TraiteMaterielEffacerLigne;
  end;

  positioncolonne := 1;
  LigneEnCours := false;
end;

function VerifMaxiOK(ACol: integer; Valeur: currency): boolean;
begin
  result := false;
  case ACol of
    colnombre:
      if (Valeur < -999.9) or (Valeur > 999.9) then
        exit;
  end;
  result := true;
end;

function traiteMaterielLireFormatCellules(ACol: integer): string;
begin
  case ACol of
    colNom:
      result := '';
    colcout:
      result := '999,99';
    colnombre:
      result := '';
    colsommeligne:
      result := '#####,##';
  end;
end;

function FormatSortie(Col: integer): string;
begin
  case Col of
    colcout:
      result := '000.00';
    colnombre:
      result := '000.00';
    colsommeligne:
      result := '00000.00';
  else
    result := '';
  end;
end;

Procedure RechargerCellule(Col, lig: integer);
var
  Valeur: currency;
  temp: string;
begin
  Valeur := TableauMateriels[Col, lig];
  temp := FormatCurr(FormatSortie(Col), Valeur);
  FormBase.ListeUtilisationMAT.Cells[Col, lig] := temp;
end;

function MaterielLireCalcul: currency;
begin
  result := SommeMAT
end;

procedure CalculMateriel(lig: integer);
var
  C, N, Somme: currency;
  temp: string;
begin
  C := TableauMateriels[colcout, lig];
  N := TableauMateriels[colnombre, lig];

  Somme := (C * N);

  // somme du Materiel
  TableauMateriels[colsommeligne, lig] := Somme;
  temp := FormatCurr(FormatSortie(colsommeligne), Somme);
  FormBase.ListeUtilisationMAT.Cells[colsommeligne, lig] := temp;
end;
// colNom

function Veriflignematerieldiversexiste: integer;

  function trouve(t: string): boolean;
  begin
    result := true;
    t := trimleft(t);
    t := trimright(t);
    t := LowerCase(t);
    t := remplaceaccent(t);

    if t = 'materiel divers' then
      exit;
    if t = 'materiels divers' then
      exit;
    if t = 'materiel diver' then
      exit;
    if t = 'materiels diver' then
      exit;

    result := false;
  end;

var
  nbl: integer;
  N: integer;
  i: integer;
  t: string;
begin
  N := -1;
  nbl := FormBase.ListeUtilisationMAT.RowCount - 1;
  if nbl >= 0 then
  begin
    for i := 0 to (nbl - 1) do
    begin
      // t := TableauMateriels[colNom, i];
      t := FormBase.ListeUtilisationMAT.Cells[colNom, i];

      if trouve(t) then
      begin
        N := i;
        break;
      end;
    end;
  end;

  result := N;
end;

procedure MaterielExecuterCalcul;
var
  nbl: integer;
  t: currency;
  i: integer;
begin
  t := 0;
  nbl := FormBase.ListeUtilisationMAT.RowCount - 1;
  if nbl >= 0 then
  begin
    for i := 0 to (nbl - 1) do
      t := t + TableauMateriels[colsommeligne, i];
  end;

  SommeMAT := t;

  // lance le calcul generalde la fiche Hebdo;
  CalculGeneralHebdo;
end;

function CurrToInt(Valeur: currency): integer;
begin
  result := StrToInt(CurrToStr(Valeur));
end;

function IntToCurr(Valeur: integer): currency;
begin
  result := StrToCurr(IntToStr(Valeur));
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure MaterielEnreg(numhebdo: integer);
var
  nbe: integer;
  C: currency;
  l: integer;
  s: string;

  procedure ChercheEnreg(ligne: integer);
  var
    C: currency;
    id: integer;
  begin
    C := TableauMateriels[colindex, ligne];

    id := CurrToInt(C);
    FormBase.TabFichMat.editkey;
    FormBase.TabFichMat.FieldByName('id_Materiel').asinteger := id;
    FormBase.TabFichMat.gotokey;
    FormBase.TabFichMat.edit;
  end;

begin
  // supprimer les enregs precedents
  MaterielSupprime(numhebdo);
  ModeLigneAppend := false;

  nbe := FormBase.ListeUtilisationMAT.RowCount - 1;
  if (nbe = 0) then
    exit;

  // pas de mofifs en cours
  ResetModifsEtat;

  with FormBase.TabFichMat do
  begin
    Open;

    if nbe <> 0 then
    begin
      // creer chaque ligne et la remplir
      for l := 0 to (nbe - 1) do
      begin
        if ModeLigneAppend = false then
          // creer nouveau enregistrement de ligne
          insert
        else
          // chercher enreg existant
          ChercheEnreg(l);

        s := FormBase.ListeUtilisationMAT.Cells[colNom, l];
        FieldByName('mat_enr_libellenom').AsString := s;

        C := TableauMateriels[colcout, l];
        FieldByName('mat_enr_cout').Ascurrency := C;

        C := TableauMateriels[colnombre, l];
        FieldByName('mat_dureeutil').Ascurrency := C;

        C := TableauMateriels[colsommeligne, l];
        FieldByName('mat_total').Ascurrency := C;

        FieldByName('id_hebdo').asinteger := numhebdo;

        // valider nouvelle ligne avec ces valeurs
        post;
      end;
    end;

    // fermeture de la table apres ajouts
    close;
  end;

  // sortie du mode de modification
  ModeLigneAppend := false;
end;

procedure ChargeLigneExiste;
var
  nc: integer;
  nblig: integer;

  idemp: integer;
  // inter:     boolean;
  monaie: currency;
  Valeur: currency;
  nomemp: string;

  procedure Ecrire(Col, lig: integer; Valeur: currency);
  var
    t: string;
  begin
    t := FormatCurr(FormatSortie(Col), Valeur);
    TableauMateriels[Col, lig] := Valeur;
    FormBase.ListeUtilisationMAT.Cells[Col, lig] := t;
  end;

begin
  with FormBase.TabFichMat do
  begin
    // definition de la ligne de Materiel
    nblig := FormBase.ListeUtilisationMAT.RowCount - 1;

    SetLength(TableauMateriels, 10, nblig + 1);

    idemp := FieldByName('id_utilisation').asinteger;
    TableauMateriels[colindex, nblig] := IntToCurr(idemp);

    nomemp := FieldByName('mat_enr_libellenom').AsString;
    FormBase.ListeUtilisationMAT.Cells[colNom, nblig] := nomemp;

    // debut chargement liste ecran
    LigneEnCours := true;

    monaie := FieldByName('mat_enr_cout').Ascurrency;
    Ecrire(colcout, nblig, monaie);

    Valeur := FieldByName('mat_dureeutil').Ascurrency;
    Ecrire(colnombre, nblig, Valeur);

    // calculer la ligne
    CalculMateriel(nblig);

    // fin chargement liste ecran
    LigneEnCours := false;

    // defintion de la prochaine ligne
    nc := FormBase.ListeUtilisationMAT.RowCount;
    inc(nc);
    FormBase.ListeUtilisationMAT.RowCount := nc;

    // selection de la 1ere colonne a editer
    FormBase.ListeUtilisationMAT.SetFocus;
    FormBase.ListeUtilisationMAT.Col := colnombre;
    FormBase.ListeUtilisationMAT.Row := nblig;
  end;
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure MaterielCharge(index: integer);
var
  nbrec: integer;
  t: string;
  nblig: integer;
begin
  ChargeConfiguration;

  // pas de mofifs en cours
  ResetModifsEtat;

  // definition des lignes de Materiel
  FormBase.ListeUtilisationMAT.RowCount := 1;

  with FormBase.TabFichMat do
  begin
    // recherche des enreg contenant 'index'

    // preparer le filtre
    str(index, t);
    filter := 'id_hebdo = ' + t;
    filtered := true;

    Open;
    setkey;

    nbrec := recordcount;
    if nbrec <> 0 then
    begin
      // entree dans le mode de modification
      ModeLigneAppend := true;
      edit;

      for nblig := 0 to (nbrec - 1) do
      begin
        // se positionner sur 1er enreg ou sur suivant
        if nblig = 0 then
          first
        else
          next;

        // charger liste de Materiel
        ChargeLigneExiste;
      end;
    end;

    // termine la lecture de la table
    close;
    filtered := false;

    // recalcul tout
    MaterielExecuterCalcul;
  end;
end;

procedure GetEnregMaterielCourant(var i: integer; var N: string;
  var C: currency);
begin
  with FormBase.TableMateriels do
  begin
    edit;
    i := FieldValues['id_materiel'];
    N := FieldByName('mat_libellenom').AsString;
    C := FieldByName('mat_cout').Ascurrency;
    post;
  end;
end;

procedure TraiteMaterielChargeLigne;
var
  nc: integer;
  nblig: integer;

  idemp: integer;
  monaie: currency;
  nomemp: string;

  numlig: integer;
  nbv: currency;

  function ChercheExiste(nblig: integer; nomemp: string): boolean;
  var
    i: integer;
    t: string;
  begin
    result := false;
    for i := 0 to nblig - 1 do
    begin
      t := FormBase.ListeUtilisationMAT.Cells[colNom, i];
      if nomemp = trim(t) then
        result := true;
    end;
  end;

  procedure Ecrire(Col, lig: integer; Valeur: currency);
  var
    t: string;
  begin
    t := FormatCurr(FormatSortie(Col), Valeur);
    TableauMateriels[Col, lig] := Valeur;
    FormBase.ListeUtilisationMAT.Cells[Col, lig] := t;
  end;

begin
  // inclure un materiel dans la liste de Materiel

  if FormBase.TableMateriels.recordcount = 0 then
    exit;

  // modifs en cours
  SetModifsEtat;

  // lire l'enreg correspondant de l'employe modifier
  GetEnregMaterielCourant(idemp, nomemp, monaie);

  ChargeConfiguration;

  // definition de la ligne de Materiel
  nblig := FormBase.ListeUtilisationMAT.RowCount - 1;

  // verifier si maxi du nb lignes atteint alors sortie
  if nblig = nbligmat then
    exit;

  if nblig <> 0 then
  begin
    // verifier si le nom existe dans liste alors ne pas charger
    if ChercheExiste(nblig, nomemp) then
      exit;
  end;

  SetLength(TableauMateriels, 10, nblig + 1);

  TableauMateriels[colindex, nblig] := IntToCurr(idemp);
  FormBase.ListeUtilisationMAT.Cells[colNom, nblig] := nomemp;

  LigneEnCours := true; // debut chargement liste ecran

  Ecrire(colcout, nblig, monaie);
  Ecrire(colnombre, nblig, 0);

  // calculer la ligne
  CalculMateriel(nblig);

  LigneEnCours := false; // fin chargement liste ecran

  // defintion de la prochaine ligne
  nc := FormBase.ListeUtilisationMAT.RowCount;
  inc(nc);
  FormBase.ListeUtilisationMAT.RowCount := nc;

  // selection de la 1ere colonne a editer
  FormBase.ListeUtilisationMAT.SetFocus;
  FormBase.ListeUtilisationMAT.Col := colnombre;
  FormBase.ListeUtilisationMAT.Row := nblig;

  // recalcul tout
  MaterielExecuterCalcul;

  numlig := Veriflignematerieldiversexiste;
  if numlig <> -1 then
  begin
    // recuperer le Nombre de Vehicule
    nbv := CalculNombreVehicule;

    // l'affecter a la ligne materiels divers
    AffecterNombreMaterieldivers(numlig, nbv);
  end;
end;

function TraiteMaterielSelection(ACol, ARow: integer): boolean;
var
  OK: boolean;
  i: integer;
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    result := false;
    exit;
  end;

  result := true;
  OK := true;

  // utile pour la suppression de la derniere ligne
  if Suppressionencours and (ACol = 10) then
    exit;

  // verifier si depassement nbr de lignes
  i := FormBase.ListeUtilisationMAT.RowCount - 1;
  if ARow >= i then
    OK := false;

  if (ACol = colnombre) and OK then
  begin
    // autoriser l'edition de la cellule
    with FormBase.ListeUtilisationMAT do
      options := options + [goEditing];

    RechargerCellule(ACol, ARow);
  end
  else
  begin
    // interdire l'edition et la selection de la cellule
    with FormBase.ListeUtilisationMAT do
      options := options - [goEditing];
    result := false;
    exit;
  end;
end;

procedure AffecterNombreMaterieldivers(ligne: integer; Valeur: currency);
var
  temp: string;
begin
  try
    begin
      // test maxi
      if VerifMaxiOK(colnombre, Valeur) then
      begin
        TableauMateriels[colnombre, ligne] := Valeur;
        temp := FormatCurr(FormatSortie(colnombre), Valeur);
        FormBase.ListeUtilisationMAT.Cells[colnombre, ligne] := temp;
      end
      else
        // valeur hors de la borne maximum donc recharge
        RechargerCellule(colnombre, ligne);

      // calculer la ligne
      CalculMateriel(ligne);
    end;

    // recalcul tout
    MaterielExecuterCalcul;

  except
    // si erreur de saisie dans la cellule
    on EConvertError do
      RechargerCellule(colnombre, ligne);
  end;
end;

procedure TraiteMaterielValide(var Key: Char);
var
  ACol: integer;
  ARow: integer;
  temp: string;
  Valeur: currency;
  N: integer;
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    Key := chr(0);
    exit;
  end;

  if (Key = ' ') then
  begin
    // ne traite pas ces touches
    Key := chr(0);
    exit;
  end;

  if (Key = '.') then
  begin;
    // modifs en cours
    SetModifsEtat;

    // pour faciliter la saisie des nombres decimaux
    Key := ',';

    exit;
  end;

  // traitement materiel divers
  N := Veriflignematerieldiversexiste;
  if N <> -1 then
  begin
    // ACol := FormBase.ListeUtilisationMAT.Col;
    ARow := FormBase.ListeUtilisationMAT.Row;

    if ARow = N then
    begin
      // ne traite pas ces touches
      Key := chr(0);
      exit;
    end;
  end;

  // valide la saisie
  if Key = chr(13) then
  begin
    // modifs en cours
    SetModifsEtat;

    // Stocker la saisie entree au clavier
    ACol := FormBase.ListeUtilisationMAT.Col;
    ARow := FormBase.ListeUtilisationMAT.Row;

    try
      begin
        // Stocker la saisie entree au clavier
        temp := FormBase.ListeUtilisationMAT.Cells[ACol, ARow];
        Valeur := StrToCurr(trim(temp));

        // test maxi
        if VerifMaxiOK(ACol, Valeur) then
        begin
          TableauMateriels[ACol, ARow] := Valeur;
          temp := FormatCurr(FormatSortie(ACol), Valeur);
          FormBase.ListeUtilisationMAT.Cells[ACol, ARow] := temp;
        end
        else
          // valeur hors de la borne maximum donc recharge
          RechargerCellule(ACol, ARow);

        // calculer la ligne
        CalculMateriel(ARow);
      end;

      // recalcul tout
      MaterielExecuterCalcul;

    except
      // si erreur de saisie dans la cellule
      on EConvertError do
        RechargerCellule(ACol, ARow);
    end;
  end;

  // si touche numerique
  CelluleEnCours := false;
  EditionEnCours := false;
  if ((ord(Key) > 47) and (ord(Key) < 58)) or (Key = '-') or (Key = '+') then
  begin
    // modifs en cours
    SetModifsEtat;

    CelluleEnCours := true;
    EditionEnCours := true;

    // Stocker la saisie entree au clavier
    CelluleEnCoursCol := FormBase.ListeUtilisationMAT.Col;
    CelluleEnCoursRow := FormBase.ListeUtilisationMAT.Row;
  end;
end;

procedure traiteMaterielInitialiserProcEffaceLigne(X, Y: integer);
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  with FormBase.ListeUtilisationMAT do
    MouseToCell(X, Y, positioncolonne, positionligne)
end;

procedure TraiteMaterielEffacerLigne;
var
  nbl: integer;
  i: integer;

  procedure transfert(deb, fin: integer);
  var
    l: integer;
    C: integer;
    t: currency;
  begin
    for l := deb to fin - 2 do
    begin
      for C := colindex to colsommeligne do
      begin
        with FormBase.ListeUtilisationMAT do
          Cells[C, l] := Cells[C, l + 1];

        t := TableauMateriels[C, l + 1];
        TableauMateriels[C, l] := t;
      end;
    end;
  end;

  procedure viderligne(fin: integer);
  var
    C: integer;
  begin
    for C := colindex to colsommeligne do
      FormBase.ListeUtilisationMAT.Cells[C, fin - 1] := '';
  end;

begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  // suppression ligne de Materiel selectionnée
  if (positioncolonne = colNom) then
  begin
    // modifs en cours
    SetModifsEtat;

    // diminuer d'une ligne verifier si depassement nbr de lignes
    nbl := FormBase.ListeUtilisationMAT.RowCount - 1;
    if positionligne >= nbl then
      exit;

    // remonter d'une unite toutes les lignes
    if positionligne < nbl then
      transfert(positionligne, nbl);

    if nbl = 1 then
      unselectMAT
    else
    begin
      FormBase.ListeUtilisationMAT.Col := colautoselection;
      FormBase.ListeUtilisationMAT.Row := 0;
    end;

    // effacer derniere ligne
    viderligne(nbl);

    i := FormBase.ListeUtilisationMAT.RowCount;
    dec(i);
    FormBase.ListeUtilisationMAT.RowCount := i;

    // pour eviter des suppression intenpestives
    positioncolonne := 1;
  end;

  // recalcul tout
  MaterielExecuterCalcul;
end;

procedure traiteMaterieltouchesFIN();
begin
  CelluleEnCours := false;
end;

procedure traiteMaterieltouches(Key: word);
var
  ACol: integer;
  ARow: integer;

  // recupere position cellule en cours
  procedure Cellule(var ACol: integer; var ARow: integer);
  begin
    ACol := FormBase.ListeUtilisationMAT.Col;
    ARow := FormBase.ListeUtilisationMAT.Row;
  end;

  function lignes: boolean;
  begin
    result := false;
    if FormBase.ListeUtilisationMAT.RowCount > 2 then
      result := true;
  end;

begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  // sortie si 0 lignes
  if not lignes then
    exit;

  if CelluleEnCours = true then
    exit;

  if TestTouchesFleches(Key) or (VK_TAB = Key) then
  begin
    if LigneEnCours = false then
    begin
      // retour en arriere d'une cellule suivant touche
      Cellule(ACol, ARow);
      case Key of
        VK_TAB:
          dec(ACol);
        VK_LEFT:
          inc(ACol);
        VK_RIGHT:
          dec(ACol);
        VK_DOWN:
          if lignes then
            dec(ARow);
        VK_UP:
          if lignes then
            inc(ARow);
      end;

      // reaffichage suite a entree non validee par CRLF
      if CelluleEnCours = false then
        if colsommeligne <> ACol then
        begin
          // modifs en cours
          SetModifsEtat;

          RechargerCellule(ACol, ARow);
        end;
    end;
  end;
end;

procedure traiteMaterielsouris;
var
  ACol: integer;
  ARow: integer;
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  if EditionEnCours = false then
    exit;

  ACol := FormBase.ListeUtilisationMAT.Col;
  ARow := FormBase.ListeUtilisationMAT.Row;

  if not ForcerRestitution then
    if (CelluleEnCoursCol = ACol) and (CelluleEnCoursRow = ARow) then
      exit;

  EditionEnCours := false;

  // modifs en cours
  SetModifsEtat;

  RechargerCellule(CelluleEnCoursCol, CelluleEnCoursRow);
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure MaterielChargeChaines(index: integer; var nbrec: integer);

  function LireChaine(param: string): string;
  begin
    result := FormBase.TabFichMat.FieldByName(param).AsString;
  end;

  function LireValeur(param: string): currency;
  begin
    result := FormBase.TabFichMat.FieldByName(param).Ascurrency;
  end;

  procedure Charge(nblig: integer);
  var
    temp: string;
    Valeur: currency;
  begin
    TableauTemp[0, nblig] := '';

    TableauTemp[1, nblig] := LireChaine('mat_enr_libellenom');

    temp := FormatSortie(colcout);
    Valeur := LireValeur('mat_enr_cout');
    temp := FormatCurr('0.00', Valeur);
    TableauTemp[2, nblig] := temp;
    TableauvalTemp[2, nblig] := Valeur;

    temp := FormatSortie(colnombre);
    Valeur := LireValeur('mat_dureeutil');
    temp := FormatCurr('0.00', Valeur);
    TableauTemp[3, nblig] := temp;
    TableauvalTemp[3, nblig] := Valeur;

    temp := FormatSortie(colsommeligne);
    Valeur := LireValeur('mat_total');
    temp := FormatCurr('0.00', Valeur);
    TableauTemp[4, nblig] := temp;
    TableauvalTemp[4, nblig] := Valeur;
  end;

var
  t: string;
  nblig: integer;

begin
  with FormBase.TabFichMat do
  begin
    // preparer le filtre recherche des enreg contenant 'index'
    str(index, t);
    filter := 'id_hebdo = ' + t;
    filtered := true;

    Open;
    setkey;

    nbrec := recordcount;
    if nbrec <> 0 then
    begin
      // entree dans le mode de modification
      SetLength(TableauTemp, 11, nbrec + 1);
      SetLength(TableauvalTemp, 11, nbrec + 1);
      edit;

      for nblig := 0 to (nbrec - 1) do
      begin
        // se positionner sur 1er enreg ou sur suivant
        if nblig = 0 then
          first
        else
          next;

        // charger liste de Materiel
        Charge(nblig);
      end;
    end;

    // termine la lecture de la table
    close;
    filtered := false;
  end;
end;

procedure MaterielSupprime(index: integer);
var
  t: string;
begin
  with FormBase.TabFichMat do
  begin
    // preparer le filtre recherche des enreg contenant 'index'
    str(index, t);
    filter := 'id_hebdo = ' + t;
    filtered := true;

    Open;
    setkey;

    // supprimer tous les enregs
    while recordcount <> 0 do
      delete;

    // termine la suppression dans la table
    close;
    filtered := false;
  end;
end;

procedure ResetCumulsMateriel;
begin
  nbMAT := 0;

  SetLength(TblNomMAT, 2, nbMAT);
  SetLength(TblValMAT, 3, nbMAT);
end;

procedure CumulsMateriel(idh: integer);

  function ChercheAjoute(nom: string; var ind: integer): boolean;
  var
    i: integer;
    t: string;
  begin
    result := true;

    if nbMAT = 0 then
      exit;
    for i := 0 to (nbMAT - 1) do
    begin
      t := TblNomMAT[1, i];
      if t = nom then
      begin
        ind := i;
        result := false;
        exit;
      end;
    end;
  end;

var
  ind: integer;
  nbrec: integer;
  i: integer;
  N: integer;
  C: currency;
begin
  // charger les pointages
  MaterielChargeChaines(idh, nbrec);

  // si aucuns pointage sortir
  if nbrec = 0 then
    exit;

  // traiter tous les pointages
  for i := 0 to (nbrec - 1) do
  begin
    // verifier si le pointage existe deja
    if ChercheAjoute(TableauTemp[1, i], ind) then
    begin
      // ajoute une ligne au tableau
      inc(nbMAT);
      SetLength(TblNomMAT, 2, nbMAT);
      SetLength(TblValMAT, 3, nbMAT);

      // stocker nom et type ligne de pointage
      ind := nbMAT - 1;
      TblNomMAT[0, ind] := '';
      TblNomMAT[1, ind] := TableauTemp[1, i];

      // raz valeurs nouvelle ligne
      for N := 1 to 2 do
        TblValMAT[N, ind] := 0;

      // stocker le cout
      TblValMAT[0, ind] := TableauvalTemp[2, i];
    end;

    // cumuler a la ligne en cours la nouvelle ligne
    for N := 1 to 2 do
    begin
      C := TableauvalTemp[N + 2, i];
      C := TblValMAT[N, ind] + C;
      TblValMAT[N, ind] := C;
    end;
  end;
end;

procedure CumulsMaterielLire(var nbr: integer);
var
  i: integer;
  t: string;
begin
  nbr := nbMAT;
  if nbMAT = 0 then
    exit;

  // pour stocker tous les pointages
  SetLength(TableauTemp, 11, nbMAT);

  for i := 0 to (nbMAT - 1) do
  begin
    TableauTemp[0, i] := TblNomMAT[0, i];
    TableauTemp[1, i] := TblNomMAT[1, i];

    t := FormatSortie(colcout);
    t := FormatCurr('0.00', TblValMAT[0, i]);
    TableauTemp[2, i] := t;

    t := FormatSortie(colnombre);
    t := FormatCurr('0.00', TblValMAT[1, i]);
    TableauTemp[3, i] := t;

    t := FormatSortie(colsommeligne);
    t := FormatCurr('0.00', TblValMAT[2, i]);
    TableauTemp[4, i] := t;
  end;
end;
/// ///////////////////////////////////////////////////////////////////////////

end.
