unit Hebdo;

interface

uses UITypes, forms;

function ProtectionFicheOuverte: boolean;

procedure TraiteListeFiches;
procedure CreationListeFiches;
procedure TraiteSortieSimple;
function TraiteSortieApplication: TCloseAction;
procedure FormatSaisieCA(valeur: currency);
function LiresaisieCA: currency;
procedure TraiteSaisieCA(var Key: Char);

function CalcSemaineBis(numsemaine: integer; Annee: integer): boolean;
procedure ListeMoisChoix;
procedure TraiteListeMois;
procedure AfficheTitre(num: integer);
function ModifHebdoOK: boolean;
function ModeHebdoModificationOK: boolean;
procedure SetModeHebdoInitial;
procedure SetModeHebdoCreation;
procedure SetModeHebdoConsult;
procedure SetModeHebdoModif;
procedure SetModeHebdoEnreg;
procedure EvenementModifMasqueAnnexe;
procedure EvenementModifMasqueHebdo;

procedure PrepareInterface(mode: boolean);
procedure HEBDOAnnuler;
procedure HEBDOEnregistrer;
procedure HEBDOImprimer;
procedure HEBDOSupprimer;
procedure ImpressionEnCours;

procedure CalculGeneralHebdo;
procedure ResetModifHebdo;
procedure verifieSemaineBis;
procedure afficheMoisSemaineBis;

function LireIdentifiantHebdo: integer;
function ModeHebdoCreationOK: boolean;
procedure AfficheSommesHebdo;
procedure EvenementModifHebdo;

procedure InitMasqueBase;
procedure MasquesAnnexesFerme;
procedure MasquesAnnexesOuvre;
procedure MasquesViderListe;
procedure MasquesRestitueValeurs;
procedure MasquesCharger(id_hebdo: integer);
procedure MasquesSauver(id_hebdo: integer);
procedure MasquesSupprimer(id_hebdo: integer);

var

  hebdochiffreaffaire: currency;
  hebdototalsalarie: currency;
  hebdototalinterim: currency;
  hebdototalvehicule: currency;
  hebdototaldiamant: currency;
  hebdototalmateriel: currency;
  hebdototaloutils: currency;
  hebdototalexterne: currency;
  hebdototalinterne: currency;
  hebdototaldeboursesec: currency;
  hebdototalstructures: currency;
  hebdototalprixrevient: currency;
  hebdototalresultat: currency;
  hebdototalfraisinduits: currency;

  jourfinsemaine: integer;
  jourdebutsemaine: integer;
  moisfinsemaine: integer;
  moisdebutsemaine: integer;
  ForcerRestitution: boolean;

implementation

uses

  windows, Dialogs, Controls, SysUtils, Mask, Printers,
  Utils, UtilsBDE, etats,
  accesBDD, fichebase,
  MasqueEmployes, MasqueChantiers, MasqueSemaine,
  MasqueStructures, MasqueVehicules, MasqueDiamant, MasqueAchats,
  MasqueExterne, MasqueMateriels, Impression,
  winbase;

var

  ModeHebdoCreation: boolean;
  ModifEnCours: boolean;
  EvenementEnCours: boolean;

function ProtectionFicheOuverte: boolean;
var
  t: string;
begin
  result := false;

  // empecher si en cours de modif ou de creation d'une fiche
  if FormBase.BoutonEnregistrer.enabled then
  begin
    result := true;

    t := 'Une fiche est en cours de modification, veuillez la';
    t := t + chr(13) + chr(10);
    t := t + 'sauver puis la fermer, et ensuite vous pourrez';
    t := t + chr(13) + chr(10);
    t := t + 'relancer l''operation.';
    MessageDlg(t, mtWarning, [mbOk], 0);

    exit;
  end;

  // idem plus haut si modif deja sauvee
  if FormBase.BoutonAnnuler.enabled then // ModeHebdoCreation
  begin
    result := true;

    t := 'Une fiche est en cours de consultation, veuillez la';
    t := t + chr(13) + chr(10);
    t := t + 'fermer, et ensuite vous pourrez relancer l''operation.';
    MessageDlg(t, mtWarning, [mbOk], 0);

    exit;
  end;
end;

{ ----------------------------------------------------------------------------- }
function TraiteSortieApplication: TCloseAction;
{ traite eventuellement la sortie du logiciel verifie l'etat du soft (modif
  en cours) et suvegarde possible
}
  function MessCreat: word;
  var
    t: string;
  begin
    t := 'Fiche en cours de cr�ation. Voulez vous la sauver,';
    t := t + chr(13);
    t := t + 'l'' ignorer,  ou annuler la  sortie du  programme.';
    result := MessageDlg(t, mtConfirmation, [mbYes, mbIgnore, mbCancel], 0);
  end;

  function MessModif: word;
  var
    t: string;
  begin
    t := 'Fiche  en  cours  de  modification.   Voulez  vous  la';
    t := t + chr(13);
    t := t + 'sauver, l''ignorer, ou annuler la sortie du programme.';
    result := MessageDlg(t, mtConfirmation, [mbYes, mbIgnore, mbCancel], 0);
  end;

var
  s: word;
begin
  // verifier si en cours de creation
  if ModeHebdoCreationOK then
  begin
    s := MessCreat;

    // si annuler alors sortir sans fermer
    if s = mrCancel then
    begin
      result := caNone;
      exit;
    end;

    // si sauver alors sauver
    if s = mrYes then
      HEBDOEnregistrer;

    // fermer
    result := caFree;
    exit;
  end;

  // verifier si en cours de modification
  if ModeHebdoModificationOK then
  begin
    s := MessModif;

    // si annuler alors sortir sans fermer
    if s = mrCancel then
    begin
      result := caNone;
      exit;
    end;

    // si sauver alors sauver
    if s = mrYes then
      HEBDOEnregistrer;

    // fermer
    result := caFree;
    exit;
  end;

  // revoie fermeture autorisee
  result := caFree;
end;

procedure TraiteSortieSimple;
var
  Action: TCloseAction;
begin
  // traite eventuel de la sortie
  Action := TraiteSortieApplication;

  // quitter application si demande
  if Action = caFree then
    application.Terminate;
end;

procedure FormatSaisieCA(valeur: currency);
var
  t: string;
begin
  t := FormatCurr('00,000,000.00', valeur);
  FormBase.saisieCA.Text := t;
end;

function LiresaisieCA: currency;
var
  t: string;
begin
  t := FormBase.saisieCA.Text;
  t := videsnum(t);
  result := strtocurr(t);
end;

procedure TraiteSaisieCA(var Key: Char);
var
  c: currency;
begin
  if Key = chr(13) then
  begin
    c := LiresaisieCA;
    FormatSaisieCA(c);

    EvenementModifMasqueHebdo;

    exit;
  end;

  if (Key = '.') then
  begin
    // pour faciliter la saisie des nombres decimaux
    Key := ',';

    exit;
  end;
end;

procedure MasquesRestitueValeurs;
{ cette routine retablit une valeur lorsque dans une grille une saisie n'a pas
  ete validee par la touche entree }
begin
  // forcer le la restitution d'une valeur dans un masque
  ForcerRestitution := true;

  traitepointagesouris(true);
  traitepointagesouris(false);
  traiteAchatsouris;
  traiteStructuresouris;
  traiteVehiculesouris;
  traiteDiamantsouris;
  traiteExternesouris;
  traiteMaterielsouris;

  // terminer le forcage de restitution
  ForcerRestitution := false;
end;

procedure MasquesViderListe;
{ vider le contenu des masques annexes (grilles de saisie) }
begin
  PointageViderListe;
  AchatViderListe;
  StructureViderListe;
  VehiculeViderListe;
  DiamantViderListe;
  ExterneViderListe;
  MaterielViderListe;
end;

procedure MasquesAnnexesFerme;
{ bloquer l'acces aux masques annexes }
begin
  PointageModifsNon;
  AchatModifsNon;
  StructureModifsNon;
  VehiculeModifsNon;
  DiamantModifsNon;
  ExterneModifsNon;
  MaterielModifsNon;
end;

procedure MasquesAnnexesOuvre;
{ ouvrir l'acces aux masques annexes }
begin
  PointageModifsOui;
  AchatModifsOui;
  StructureModifsOui;
  VehiculeModifsOui;
  DiamantModifsOui;
  ExterneModifsOui;
  MaterielModifsOui;
end;

procedure MasquesInit;
{ initialisations diverses de l'interface }
begin
  PointageInitMasque;
  AchatInitMasque;
  StructureInitMasque;
  VehiculeInitMasque;
  DiamantInitMasque;
  ExterneInitMasque;
  MaterielInitMasque;
end;

procedure InitMasqueBase;
{ prepare le masque de saisie principal }
begin
  // utilisee par les masques pour restituer une saisie non validee
  ForcerRestitution := false;

  with FormBase do
  begin
    // SetSaisieNumSemaine('00');
    SaisieSemaineBis.Checked := false;
    FicheRegul := false;
    ListeMois.ItemIndex := 0;
    setSaisieNumCha('000000');
    FormatSaisieCA(0);
  end;

  // vide les listes des grilles de saisie
  MasquesViderListe;

  // autoriser le traitements des masques
  MasquesAnnexesOuvre;

  // initialise chaque masque de saisie
  MasquesInit;
end;

procedure MasquesSupprimer(id_hebdo: integer);
{ supprime les enregistrements relies a la fiche }
begin
  PointageSupprime(id_hebdo);
  MaterielSupprime(id_hebdo);
  ExterneSupprime(id_hebdo);
  DiamantSupprime(id_hebdo);
  AchatSupprime(id_hebdo);
  StructureSupprime(id_hebdo);
  VehiculeSupprime(id_hebdo);
end;

procedure MasquesSauver(id_hebdo: integer);
{ sauvegarde les enregistrements relies a la fiche }
begin
  PointageEnreg(id_hebdo);
  MaterielEnreg(id_hebdo);
  ExterneEnreg(id_hebdo);
  DiamantEnreg(id_hebdo);
  AchatEnreg(id_hebdo);
  StructureEnreg(id_hebdo);
  VehiculeEnreg(id_hebdo);
end;

procedure MasquesCharger(id_hebdo: integer);
{ charger les enregistrements relies a la fiche }
begin
  PointageCharge(id_hebdo);
  MaterielCharge(id_hebdo);
  ExterneCharge(id_hebdo);
  DiamantCharge(id_hebdo);
  AchatCharge(id_hebdo);
  StructureCharge(id_hebdo);
  VehiculeCharge(id_hebdo);
end;
{ ----------------------------------------------------------------------------- }

{ ----------------------------------------------------------------------------- }
procedure AfficheTitre(num: integer);
var
  t: string;
  c: string;
begin
  t := FormBase.Caption;
  t := Copy(t, 1, 23);
  case num of
    1:
      c := 'Fiche de Regularisation charg�e';
    2:
      c := 'Fiche Hebdomadaire charg�e';
    3:
      c := 'Fiche de Regularisation verouill�e charg�e';
    4:
      c := 'Fiche Hebdomadaire verouill�e charg�e';
    5:
      c := 'Fiche de Regularisation en cr�ation';
    6:
      c := 'Fiche Hebdomadaire en cr�ation';
    7:
      c := 'Fiche de Regularisation en modification';
    8:
      c := 'Fiche Hebdomadaire en modification';
    9:
      c := 'Fiche de Regularisation enregistr�e';
    10:
      c := 'Fiche Hebdomadaire enregistr�e';

  else
    c := '';
  end;

  t := t + c;
  FormBase.Caption := t;
end;

function CalcSemaineBis(numsemaine: integer; Annee: integer): boolean;
var
  JourParMois: array [1 .. 12] of integer;
  PeriodeJour: array [1 .. 28] of Byte;
  j: Byte;
  Lundi: integer;
  Mois: Byte;
  A: integer;
  t: Byte;
  i: integer;
begin
  result := LireTypeSemaineBis(numsemaine);
  exit;

  Annee := Annee + 2;
  A := (Annee - 12) Mod 28;
  if (A = 0) Then
    A := 28;

  j := 6;
  for t := 1 To A do
  begin
    i := integer(((t - 1) Mod 4 = 0) And (t > 1));
    j := (j + 1 - i) Mod 7;
    PeriodeJour[t] := j;
  end;

  Lundi := (numsemaine + 1) * 7 - PeriodeJour[A] - 6;
  If (Lundi < 0) Then
  begin
    jourdebutsemaine := 7 - PeriodeJour[A];
    moisdebutsemaine := 11;

    jourfinsemaine := 31 - Lundi;
    moisfinsemaine := 12;
  end
  Else
  begin
    JourParMois[1] := 31;
    JourParMois[2] := 28;
    if ((A Mod 4) = 0) then
      Dec(JourParMois[2]);
    JourParMois[3] := 31;
    JourParMois[4] := 30;
    JourParMois[5] := 31;
    JourParMois[6] := 30;
    JourParMois[7] := 31;
    JourParMois[8] := 31;
    JourParMois[9] := 30;
    JourParMois[10] := 31;
    JourParMois[11] := 30;
    JourParMois[12] := 31;

    j := 1;
    Mois := 1;
    While (Lundi > JourParMois[j]) do
    begin
      Lundi := Lundi - JourParMois[j];
      j := j + 1;
      Mois := Mois + 1;
    end;

    jourfinsemaine := Lundi;
    moisfinsemaine := Mois;

    Lundi := Lundi + 6;
    If (Lundi > JourParMois[j]) Then
    begin
      jourdebutsemaine := Lundi - JourParMois[j];
      moisdebutsemaine := 1;
      if Mois <> 12 then
        moisdebutsemaine := moisdebutsemaine + Mois;
    end
    Else
    begin
      jourdebutsemaine := Lundi;
      moisdebutsemaine := Mois;
    end;
  end;

  jourdebutsemaine := jourdebutsemaine - 2;
  jourfinsemaine := jourfinsemaine - 2;

  if jourdebutsemaine <= 0 then
    if moisdebutsemaine > 1 then
      Dec(moisdebutsemaine);

  if jourfinsemaine <= 0 then
    if moisfinsemaine > 1 then
      Dec(moisfinsemaine);

  result := true;
  if moisfinsemaine = moisdebutsemaine then
    result := false;
end;

procedure ListeMoisChoix;
begin
  // defini le mois dans la liste
  FormBase.ListeMois.ItemIndex := moisfichecourante - 1;
end;

procedure TraiteListeMois;
begin
  // defini le mois dans la liste
  moisfichecourante := FormBase.ListeMois.ItemIndex + 1;
end;

procedure afficheMoisSemaineBis;
var
  Mois: integer;
begin
  if FormBase.SaisieSemaineBis.Checked then
    Mois := moisdebutsemaine
  else
    Mois := moisfinsemaine;

  moisfichecourante := Mois;
end;

procedure verifieSemaineBis;
{ verifie la saisie du numero de semaine et traite la semaine bis }
var
  ns: integer;
  t: string;
begin
  // lire numero de semaine
  t := GetSaisieNumSemaine;
  ns := strtoint(t);

  if CalcSemaineBis(ns, AnneeCourante) then
    FormBase.SaisieSemaineBis.enabled := true
  else
  begin
    FormBase.SaisieSemaineBis.enabled := false;
    FormBase.SaisieSemaineBis.Checked := false;
  end;
end;

function LireIdentifiantHebdo: integer;
begin
  result := FormBase.TableHebdo.FieldByName('id_hebdo').asinteger;
end;

function ModifHebdoOK: boolean;
begin
  result := true;

  // test si en cours de modification
  if ModifEnCours then
    exit;

  // pas de modification en cours
  result := false;
end;

procedure EvenementModifHebdo;
{ traitement a effectuer lors de toute modif dans l'interface de base }
var
  c: boolean;
begin
  // pour eviter des evenements intenpestifs
  if EvenementEnCours then
    exit;

  // modifs en cours
  ModifEnCours := true;

  // autorise la sauvegarde et l'annulation
  SetModeHebdoEnreg;

  // suite a une modification recalcul general
  CalculGeneralHebdo;

  c := FicheRegul;
  if c then
    AfficheTitre(7)
  else
    AfficheTitre(8);
end;

procedure EvenementModifMasqueAnnexe;
{ routine executee lors d'une modif (clavier ou souris) dans un
  champ d'un masque annexe }
begin
  EvenementModifHebdo;
end;

procedure EvenementModifMasqueHebdo;
{ routine executee lors d'une modif (clavier ou souris) dans un
  champ du masque de base }
begin
  // prevenir qu'il y a eu une modif du masque principal
  EvenementModifHebdo;
end;

procedure ResetModifHebdo;
begin
  ModifEnCours := false;
end;

function ModeHebdoModificationOK: boolean;
begin
  result := ModifEnCours;
end;

function ModeHebdoCreationOK: boolean;
begin
  result := ModeHebdoCreation;
end;

procedure AfficheSommesHebdo;
{ affiche les calculs lies a la fiche en cours dans l'interface }

  procedure AfficheTotal(l: integer; valeur: currency);
  var
    temp: string;
    n: integer;
    c: currency;
  begin
    temp := FormatCurr(' 00,000,000.00', valeur);
    n := length(temp);
    if n < 13 then
      n := 13 - n
    else
      n := 0;

    FormBase.ListeCalculs.Cells[0, l] := FillSpaces(n) + temp;

    temp := ' XXX %';
    if hebdochiffreaffaire <> 0 then
    begin
      c := (valeur / hebdochiffreaffaire) * 100;
      temp := FormatCurr('000.00', c);

      n := length(temp);
      if n < 12 then
        n := 6 - n
      else
        n := 0;

      temp := ' ' + FillSpaces(n) + temp + ' %';
    end;

    // remplir les pourcentages
    FormBase.ListeCalculs.Cells[1, l] := temp;
  end;

var
  t: string;
begin
  // affichage des fraisinduits
  t := FormatCurr('000,000.00', hebdototalfraisinduits);
  FormBase.LabelFraisInduits.Caption := t;

  // Affichage des totaux
  AfficheTotal(0, hebdototalsalarie);
  AfficheTotal(1, hebdototalinterim);
  AfficheTotal(2, hebdototalvehicule);
  AfficheTotal(3, hebdototaldiamant);
  AfficheTotal(4, hebdototalmateriel);
  AfficheTotal(5, hebdototaloutils);
  AfficheTotal(6, hebdototalexterne);
  AfficheTotal(7, hebdototalinterne);
  AfficheTotal(8, hebdototaldeboursesec);
  AfficheTotal(9, hebdototalstructures);
  AfficheTotal(10, hebdototalprixrevient);
  AfficheTotal(11, hebdototalresultat);
end;

procedure CalculGeneralHebdo;
{ execute les calculs lies a la fiche en cours et les affiche dans l'interface }
var
  sommeV: currency;
  sommeS: currency;
begin
  // calcul des sommes generales de la fiche hebdo

  // lire le chiffre d'affaire
  hebdochiffreaffaire := LiresaisieCA;

  // lire les sommes des masques annexe
  hebdototalsalarie := PointageLireCalcul(false);
  hebdototalinterim := PointageLireCalcul(true);

  sommeS := StructureLireSomme;
  sommeV := VehiculeLireSomme;

  hebdototalvehicule := VehiculeLireCalcul;
  hebdototaldiamant := diamantLireCalcul;
  hebdototalmateriel := materielLireCalcul;
  hebdototalinterne := AchatLireCalcul;
  hebdototalstructures := StructureLireCalcul;

  hebdototalfraisinduits := sommeS * FraisInduits;
  hebdototalexterne := hebdototalfraisinduits + externeLireCalcul;

  hebdototaloutils := (sommeS * 6) + (sommeV * 12);
  hebdototaloutils := hebdototaloutils * FraisOutilsConso;

  hebdototaldeboursesec := hebdototalsalarie;
  hebdototaldeboursesec := hebdototaldeboursesec + hebdototalinterim;
  hebdototaldeboursesec := hebdototaldeboursesec + hebdototalvehicule;
  hebdototaldeboursesec := hebdototaldeboursesec + hebdototaldiamant;
  hebdototaldeboursesec := hebdototaldeboursesec + hebdototalmateriel;
  hebdototaldeboursesec := hebdototaldeboursesec + hebdototaloutils;
  hebdototaldeboursesec := hebdototaldeboursesec + hebdototalexterne;
  hebdototaldeboursesec := hebdototaldeboursesec + hebdototalinterne;

  hebdototalprixrevient := hebdototaldeboursesec + hebdototalstructures;
  hebdototalresultat := hebdochiffreaffaire - hebdototalprixrevient;

  // AFFICHER RESULTAT DES CALCULS
  AfficheSommesHebdo;
end;

procedure SetModeHebdoCreation;
{ mode utilise lorsque la fiche est au debut de sa creation mais pas sauvee }
begin
  ModeHebdoCreation := true;

  FormBase.BoutonEnregistrer.enabled := false;
  FormBase.BoutonImprimer.enabled := false;
  FormBase.BoutonAnnuler.enabled := true;
  FormBase.BoutonSupprimer.enabled := false;

  FormBase.saisieCA.enabled := true;
  FormatSaisieCA(0);

  FormBase.ZoneSaisieMinimum.enabled := false;

  MasquesAnnexesOuvre;
end;

procedure SetModeHebdoEnreg;
{ mode utilise lorsque la fiche est en cours de modification }
begin
  // autorise la sauvegarde et l'annulation
  FormBase.BoutonEnregistrer.enabled := true;
  FormBase.BoutonAnnuler.enabled := true;
  FormBase.BoutonSupprimer.enabled := false;
  FormBase.BoutonImprimer.enabled := false;

  FormBase.ZoneSaisieMinimum.enabled := false;
end;

procedure SetModeHebdoModif;
{ mode utilise lorsque la fiche est valide et enregistree dans la BDD }
begin
  ModeHebdoCreation := false;

  // zone minimum de saisie toujours valide
  FormBase.ZoneSaisieMinimum.enabled := false;

  // la fiche peut etre modifiee
  FormBase.saisieCA.enabled := true;

  FormBase.BoutonEnregistrer.enabled := false;
  FormBase.BoutonAnnuler.enabled := true;
  FormBase.BoutonSupprimer.enabled := true;
  FormBase.BoutonImprimer.enabled := true;

  // autorise les modifs des masques annexes
  MasquesAnnexesOuvre;
end;

procedure SetModeHebdoConsult;
{ mode utilise lorsque la fiche est verouillee }
begin
  // zone minimum de saisie toujours valide
  FormBase.ZoneSaisieMinimum.enabled := false;

  // la fiche ne peut pas etre modifiee
  FormBase.saisieCA.enabled := false;

  FormBase.BoutonEnregistrer.enabled := false;
  FormBase.BoutonAnnuler.enabled := true;
  FormBase.BoutonSupprimer.enabled := false;
  FormBase.BoutonImprimer.enabled := true;

  // interdire modifs masque annexes
  MasquesAnnexesFerme;
end;

procedure SetModeHebdoInitial;
{ mode d'attente de base du masque de saisie principal }
begin
  // sortir du mode creation au cas ou
  ModeHebdoCreation := false;

  // interdire modifs masque annexes
  MasquesAnnexesFerme;

  // revenir a la saisie minimum de hebdo
  FormBase.ZoneSaisieMinimum.enabled := true;

  FormBase.saisieCA.enabled := false;

  FormBase.BoutonEnregistrer.enabled := false;
  FormBase.BoutonImprimer.enabled := false;
  FormBase.BoutonAnnuler.enabled := false;
  FormBase.BoutonSupprimer.enabled := false;

  FormatSaisieCA(0);

  SetSaisieNumSemaine('00');
  setSaisieNumCha('000000');
  FormBase.ListeMois.ItemIndex := -1;
  FormBase.SaisieSemaineBis.Checked := false;
  FormBase.SaisieSemaineBis.enabled := false;

  CalculGeneralHebdo;

  AfficheTitre(0)
end;

procedure PrepareInterface(mode: boolean);
{ bascule en mode saisie de fiche hebdo ou regul }
begin
  if ProtectionFicheOuverte then
    exit;

  if mode then
  begin
    // afficher outils fiche regul
    FicheRegul := true;
    FormBase.PanelHebdo.Visible := false;
    FormBase.PanelRegul.Visible := true;
    moisfichecourante := 1;
    ListeMoisChoix;
  end
  else
  begin
    // afficher outils fiche hebdo
    FicheRegul := false;
    FormBase.PanelHebdo.Visible := true;
    FormBase.PanelRegul.Visible := false;
  end;

  // test si en cours consul modif ou creation
  if FormBase.BoutonAnnuler.enabled then
    // oui alors annuler en cours
    HEBDOAnnuler;
end;

{ ----------------------------------------------------------------------------- }
procedure HEBDOAnnuler;
{ fermeture de la fiche en cours }
begin
  EvenementEnCours := true;

  // ignorer les modifs en cours
  ResetModifHebdo;

  fermehebdo;

  // ouvrir l'acces aux masques annexes et effacer les listes annexes
  MasquesAnnexesOuvre;
  MasquesViderListe;

  // initialisation du mode hebdo de base
  SetModeHebdoInitial;

  EvenementEnCours := false;
end;

procedure TraiteListeFiches;
{ installe dans le champ numero de semaine et dans les champs numero de chantier
  , champ bis et eventuellement le champ mois les valeurs correspondantes
  choisies dans la fiche de la liste }
var
  t: string;
begin
  // lire contenu liste hebdos
  t := FormBase.ListeFiches.Text;

  // recuperer num chantier
  setSaisieNumCha(Copy(t, 7, 6));

  // basculer en regul ou hebdo
  if Copy(t, 1, 1) = 'R' then
  begin
    // traitement des fiches de regularisation
    PrepareInterface(true);

    // lire le mois et le definir dans la liste des mois
    FormBase.ListeMois.ItemIndex := strtoint(Copy(t, 2, 2)) - 1;
  end
  else
  begin
    // traitement des fiches hebdomadaire
    PrepareInterface(false);

    // lire la semaine et definir le champ semaine
    SetSaisieNumSemaine(Copy(t, 2, 2));

    // traiter semaine bis
    if Copy(t, 4, 1) = 'b' then
      FormBase.SaisieSemaineBis.Checked := true
    else
      FormBase.SaisieSemaineBis.Checked := false;

  end;

  // cacher liste fiches
  FormBase.ListeFiches.Visible := false;

  // simuler appui sur executer
  TraiteNumSemaine;
end;

procedure CreationListeFiches;
{ creation d'une liste de fiches enregistrees }

  function TestAnneeEnCours: boolean;
  var
    Annee: integer;
  begin
    result := false;
    Annee := FormBase.TabFich.FieldByName('heb_annee').asinteger;
    if Annee = AnneeCourante then
      result := true;
  end;

  function Regul: boolean;
  begin
    result := false;
    if FormBase.TabFich.FieldByName('heb_regul').asBoolean then
      result := true;
  end;

  function Bis: boolean;
  begin
    result := false;
    if FormBase.TabFich.FieldByName('heb_semainebis').asBoolean then
      result := true;
  end;

  function Mois: string;
  var
    n: integer;
    t: string;
  begin
    n := FormBase.TabFich.FieldByName('heb_numeromois').asinteger;
    t := inttostr(n);
    if n < 10 then
      t := '0' + t;

    result := t;
  end;

  function Semaine: string;
  var
    n: integer;
    t: string;
  begin
    n := FormBase.TabFich.FieldByName('heb_numerosemaine').asinteger;
    t := inttostr(n);
    if n < 10 then
      t := '0' + t;

    result := t;
  end;

  function IDchantier: integer;
  begin
    result := FormBase.TabFich.FieldByName('id_chantier').asinteger;
  end;

  function NumeroChantier(idc: integer): string;
  var
    n: integer;
    t: string;
  begin
    FormBase.TabCha.EditKey;
    FormBase.TabCha.FieldByName('id_chantier').asinteger := idc;
    FormBase.TabCha.gotokey;

    n := FormBase.TabCha.FieldByName('cha_numchantier').asinteger;
    t := inttostr(n);
    result := FillZero(6 - length(t)) + t;
  end;

var
  nbf: integer;
  f: integer;
  idc: integer;
  temp: string;
begin
  // creation contenu de la liste des fiches

  // vider liste et lire nb fiches
  FormBase.ListeFiches.Clear;
  FormBase.TabFich.Open;

  nbf := FormBase.TabFich.RecordCount;
  if nbf = 0 then
  begin
    FormBase.TabFich.close;
    exit;
  end;

  // ouvrir chantiers
  FormBase.TabCha.Open;

  // pointer sur premiere fiche
  FormBase.TabFich.first;

  for f := 1 to nbf do
  begin
    if TestAnneeEnCours then
    begin
      temp := '';
      if Regul then
        temp := temp + 'R' + Mois + '   '
      else
      begin
        temp := temp + ' ' + Semaine;
        if Bis then
          temp := temp + 'b  '
        else
          temp := temp + '   ';
      end;

      // lire pointeur dans table fiches
      idc := IDchantier;

      // chercher le chantier
      temp := temp + NumeroChantier(idc);

      FormBase.ListeFiches.Items.Add(temp);
    end;

    // pointer sur fiche suivante
    FormBase.TabFich.next;
  end;

  FormBase.TabFich.close;
  FormBase.TabFich.Active;

  FormBase.TabCha.close;
  FormBase.TabCha.Active;
end;

procedure HEBDOSupprimer;
{ supression de la fiche en cours de traitement }

  function VerifSuppressionOK: boolean;
  var
    t: string;
    s: word;
  begin
    result := false;
    t := 'Voulez vous supprimer la fiche en cours ?';
    s := MessageDlg(t, mtWarning, [mbYes, mbNo], 0);

    if s = mrNo then
      exit;
    result := true;
  end;

begin
  // verifier si reele volonte de supprimer la fiche en cours
  if not VerifSuppressionOK then
    exit;

  EvenementEnCours := true;

  // ignorer les modifs en cours
  ResetModifHebdo;

  // supprime les enregs concernant l'hebdo en cours
  SupprimeHebdo;

  fermehebdo;

  // effacer les listes annexes
  MasquesViderListe;

  // initialisation du mode hebdo de base
  SetModeHebdoInitial;

  EvenementEnCours := false;

  // traitement de la liste de choix des hebdos
  CreationListeFiches;
end;

procedure HEBDOEnregistrer;
{ enregistrement de la fiche en cours }
var
  c: boolean;
  // idh:   integer;
begin
  // sauvegarde fiche en cours
  SauveHebdo;

  // mise en mode consultation modification
  SetModeHebdoModif;

  c := FicheRegul;
  if c then
    AfficheTitre(9)
  else
    AfficheTitre(10);

  // traitement de la liste de choix des fiches
  CreationListeFiches;
end;

{
  // -----------------------------------------------------------------------------
  procedure HEBDOImprimer;
  // impression de la fiche en cours

  function Totalstr(valeur: currency): string;
  var
  temp: string;
  n: integer;
  begin
  temp := FormatCurr('##,###,##0.00', valeur);
  n := length(temp);
  if n < 13 then
  n := 13 - n
  else
  n := 0;

  result := FillSpaces(n) + temp;
  end;

  procedure resetimpression;
  begin
  with FormBase.TableHebdo do
  begin
  // desormais fiche plus imprimable
  edit;
  FieldByName('heb_impression').asBoolean := false;
  post;
  end;
  end;

  var
  indexhebdo: integer;
  nbrec: integer;
  begin
  // reset impression
  resetimpression;

  // demarre impression
  Printer.Orientation := poPortrait;
  Printer.BeginDoc;

  // imprimer le cadre de l'etat et les grilles des masques
  imprimefondhebdo;

  // imprimer entete de fiche hebdo
  SetLength(Tableautemp, 12, 1);
  SetLength(TableauValHebdo, 14);
  HebdoChargeChaines;

  imprimeentete;

  // recuperer le pointeur de fiche hebdo
  indexhebdo := LireIdentifiantHebdo;

  // impression de la grille des empployes et des interimaires
  SetLength(Tableautemp, 11, 0);
  PointageChargeChaines(indexhebdo, nbrec);
  ImprimePointage(nbrec);

  SetLength(Tableautemp, 11, 0);
  VehiculeChargeChaines(indexhebdo, nbrec);
  ImprimeVehicule(nbrec);

  SetLength(Tableautemp, 11, 0);
  StructureChargeChaines(indexhebdo, nbrec);
  ImprimeStructure(nbrec);

  SetLength(Tableautemp, 11, 0);
  MaterielChargeChaines(indexhebdo, nbrec);
  ImprimeMateriel(nbrec);

  SetLength(Tableautemp, 11, 0);
  AchatChargeChaines(indexhebdo, nbrec);
  ImprimeAchat(nbrec);

  SetLength(Tableautemp, 11, 0);
  DiamantChargeChaines(indexhebdo, nbrec);
  ImprimeDiamant(nbrec);

  SetLength(Tableautemp, 11, 0);
  ExterneChargeChaines(indexhebdo, nbrec);
  ImprimeExterne(nbrec);

  // impression des totaux
  ImprimeTotaux;

  // fin de l'impression
  Printer.EndDoc
  end;
}
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
procedure HEBDOImprimer;
// impression de la fiche en cours

  function Totalstr(valeur: currency): string;
  var
    temp: string;
    n: integer;
  begin
    temp := FormatCurr('##,###,##0.00', valeur);
    n := length(temp);
    if n < 13 then
      n := 13 - n
    else
      n := 0;

    result := FillSpaces(n) + temp;
  end;

  procedure resetimpression;
  begin
    with FormBase.TableHebdo do
    begin
      // desormais fiche plus imprimable
      edit;
      FieldByName('heb_impression').asBoolean := false;
      post;
    end;
  end;

var
  indexhebdo: integer;
  nbrec: integer;
  nbligempTemp: integer;
  nbligintTemp: integer;
  posYintTemp: integer;

  doublePage: boolean;
  i: integer;
begin
  posYintTemp := posYint;
  nbrec := 0;
  nbligempTemp := 0;
  nbligintTemp := 0;

  doublePage := false;

  // lock sur l'impression
  resetimpression;

  // demarre impression
  Printer.Orientation := poPortrait;
  Printer.BeginDoc;

  // Charge les donn�es
  SetLength(Tableautemp, 12, 1);
  SetLength(TableauValHebdo, 14);

  // Charge les donn�es
  HebdoChargeChaines;

  // imprimer entete de fiche hebdo
  imprimeentete;

  // recuperer le pointeur de fiche hebdo
  indexhebdo := LireIdentifiantHebdo;

  // Compte le nombre de salari�es
  SetLength(Tableautemp, 11, 0);
  PointageChargeChaines(indexhebdo, nbrec);
  if (nbrec > 16) then
    doublePage := true;

  for i := 0 to nbrec - 1 do
  begin
    if (Tableautemp[0, i] = 'I') then
      nbligintTemp := nbligintTemp + 1
    else
      nbligempTemp := nbligempTemp + 1;
  end;
  if nbligempTemp > nbligemp then
    doublePage := true;
  if nbligintTemp > nbligint then
    doublePage := true;

  // imprimer le cadre de l'etat et les grilles des masques
  if (doublePage = false) then
    imprimefondhebdo(nbligempTemp, nbligintTemp)
  else
    imprimefondhebdoSansEmployes;

  if (doublePage = false) then
  begin
    // impression de la grille des employ�s et des interimaires
    SetLength(Tableautemp, 11, 0);
    PointageChargeChaines(indexhebdo, nbrec);
    ImprimePointage(nbrec);
  end;

  SetLength(Tableautemp, 11, 0);
  VehiculeChargeChaines(indexhebdo, nbrec);
  ImprimeVehicule(nbrec);

  SetLength(Tableautemp, 11, 0);
  StructureChargeChaines(indexhebdo, nbrec);
  ImprimeStructure(nbrec);

  SetLength(Tableautemp, 11, 0);
  MaterielChargeChaines(indexhebdo, nbrec);
  ImprimeMateriel(nbrec);

  SetLength(Tableautemp, 11, 0);
  AchatChargeChaines(indexhebdo, nbrec);
  ImprimeAchat(nbrec);

  SetLength(Tableautemp, 11, 0);
  DiamantChargeChaines(indexhebdo, nbrec);
  ImprimeDiamant(nbrec);

  SetLength(Tableautemp, 11, 0);
  ExterneChargeChaines(indexhebdo, nbrec);
  ImprimeExterne(nbrec);

  // impression des totaux
  ImprimeTotaux;

  // Impression de la page 2 si il y en a une .

  if (doublePage) then
  begin
    Printer.NewPage;

    // Charge les donn�es
    SetLength(Tableautemp, 12, 1);
    SetLength(TableauValHebdo, 14);

    // Charge les donn�es
    HebdoChargeChaines;

    imprimefondhebdoPage2(nbligempTemp, nbligintTemp);

    // imprimer entete de fiche hebdo
    imprimeentete;

    // impression de la grille des employ�s et des interimaires
    SetLength(Tableautemp, 11, 0);
    PointageChargeChaines(indexhebdo, nbrec);
    ImprimePointage(nbrec);

  end;

  // fin de l'impression
  Printer.EndDoc;

  posYint := posYintTemp;
end;
{ ----------------------------------------------------------------------------- }
{ ----------------------------------------------------------------------------- }

procedure ImpressionEnCours;
{ execute l'impression de toutes les fiches non encore imprimees }
var
  nbe: integer;
  i: integer;
  b: boolean;
begin
  with FormBase.TableHebdo do
  begin
    Open;
    edit;
    nbe := RecordCount;
    if nbe = 0 then
    begin
      ShowMessage('Aucune impression � effectuer');
      exit;
    end;

    // pointe sur premier enreg
    first;

    // trace toute la table
    for i := 0 to (nbe + 1) do
    begin
      // lire etat impression
      b := FieldByName('heb_impression').asBoolean;

      if b then
      begin
        // imprime fiche en cours
        HEBDOImprimer;
      end;

      next;
    end;

    close;
    Active := true;
  end;
end;

{ ----------------------------------------------------------------------------- }
end.
