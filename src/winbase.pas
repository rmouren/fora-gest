unit winbase;

interface

uses
  Windows, UITypes, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  clipbrd, Menus, ExtCtrls, StdCtrls, Mask, ComCtrls, DBCtrls, Grids,
  DBGrids, Db, DBTables, ActnList, Tabnotbk, printers, ColorGrd, UtilsBDE,
  bde;

type
  TFormBase = class(TForm)
    MainMenu: TMainMenu;
    Saisie1: TMenuItem;
    FicheHebdomadaire: TMenuItem;
    FichedeRegularisation: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    Quitter: TMenuItem;
    Fiches1: TMenuItem;
    Materiels: TMenuItem;
    Employes: TMenuItem;
    N4: TMenuItem;
    Vehicules: TMenuItem;
    TravauxDiamant: TMenuItem;
    N5: TMenuItem;
    FraisExternes: TMenuItem;
    FraisStructures: TMenuItem;
    N7: TMenuItem;
    TableTemp: TTable;
    Secteurs: TMenuItem;
    Commercial: TMenuItem;
    Clients: TMenuItem;
    DataSourcechantiers: TDataSource;
    Tablechantiers: TTable;
    DataSourceSecteurs: TDataSource;
    TableSecteurs: TTable;
    DataSourceCommerciaux: TDataSource;
    TableCommerciaux: TTable;
    DataSourceClients: TDataSource;
    TableClients: TTable;
    DataSourceHebdo: TDataSource;
    TableHebdo: TTable;
    DataSourceEmployes: TDataSource;
    TableEmployes: TTable;
    Configuration: TMenuItem;
    N1: TMenuItem;
    DataSourceMateriels: TDataSource;
    TableMateriels: TTable;
    DataSourceVehicule: TDataSource;
    DataSourceStructure: TDataSource;
    DataSourceExterne: TDataSource;
    DataSourceDiamant: TDataSource;
    TableVehicules: TTable;
    TableStructures: TTable;
    TableExterne: TTable;
    TableDiamant: TTable;
    SouFichPoint: TDataSource;
    TabFichPoint: TTable;
    TabFichMat: TTable;
    TabFichVehic: TTable;
    TabFichStruc: TTable;
    TabFichExter: TTable;
    TabFichDiam: TTable;
    TabFichAcha: TTable;
    SouFichMat: TDataSource;
    SouFichVeh: TDataSource;
    SouFichStruc: TDataSource;
    SouFichExter: TDataSource;
    SouFichDiam: TDataSource;
    SouFichAcha: TDataSource;
    FicheHebdo: TPanel;
    Panel11: TPanel;
    Label49: TLabel;
    LabelFraisInduits: TLabel;
    Label9: TLabel;
    Label8: TLabel;
    ListeExterne: TDBLookupComboBox;
    ListeUtilisationEXT: TStringGrid;
    Panel13: TPanel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    ListeCalculs: TStringGrid;
    ZoneSaisieMinimum: TPanel;
    Label6: TLabel;
    Label1: TLabel;
    PanelHebdo: TPanel;
    Label36: TLabel;
    saisieNumSemaineMsk: TMaskEdit;
    SaisieSemaineBis: TCheckBox;
    PanelRegul: TPanel;
    Label35: TLabel;
    ListeMois: TComboBox;
    StaticText11: TStaticText;
    StaticText12: TStaticText;
    StaticText13: TStaticText;
    ListeSecteurs: TDBLookupComboBox;
    ListeCommerciaux: TDBLookupComboBox;
    ListeClient: TDBLookupComboBox;
    StaticText14: TStaticText;
    SaisieNumChantierMsk: TMaskEdit;
    BoutonConsultation: TButton;
    BoutonCreation: TButton;
    ButtonExecuter: TButton;
    NomChantier: TEdit;
    LibelleChantier: TEdit;
    BoutonEnregistrer: TButton;
    BoutonImprimer: TButton;
    BoutonAnnuler: TButton;
    BoutonSupprimer: TButton;
    Panel14: TPanel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    ListePointageEMP: TStringGrid;
    ListePointageINT: TStringGrid;
    ListeEmployes: TDBLookupComboBox;
    Panel5: TPanel;
    Label7: TLabel;
    Label11: TLabel;
    Label23: TLabel;
    Label37: TLabel;
    ListeMateriels: TDBLookupComboBox;
    ListeUtilisationMAT: TStringGrid;
    Panel6: TPanel;
    Label40: TLabel;
    Label44: TLabel;
    ListeAchats: TDBLookupComboBox;
    ListeUtilisationACH: TStringGrid;
    Panel9: TPanel;
    Label12: TLabel;
    Label45: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    ListeStructures: TDBLookupComboBox;
    ListeUtilisationSTR: TStringGrid;
    Panel16: TPanel;
    Label54: TLabel;
    Label55: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    ListeVehicules: TDBLookupComboBox;
    ListeUtilisationVEH: TStringGrid;
    Panel17: TPanel;
    Label59: TLabel;
    Label60: TLabel;
    Label61: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    ListeDiamant: TDBLookupComboBox;
    ListeUtilisationDIA: TStringGrid;
    StaticText8: TStaticText;
    ImpressionEtats: TMenuItem;
    ListeChantiers: TDBLookupComboBox;
    PrinterSetupDialog: TPrinterSetupDialog;
    Imprimante: TMenuItem;
    SaisieCA: TEdit;
    ListeFiches: TComboBox;
    TabCha: TTable;
    TabFich: TTable;
    Exportation: TMenuItem;
    Database: TDatabase;
    SaisieNumChantier: TEdit;
    saisieNumSemaine: TEdit;
    Verrou1: TMenuItem;
    VerificationIndex: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QuitterClick(Sender: TObject);
    procedure ClientsClick(Sender: TObject);
    procedure FicheHebdomadaireClick(Sender: TObject);
    procedure FichedeRegularisationClick(Sender: TObject);
    procedure MaterielsClick(Sender: TObject);
    procedure VehiculesClick(Sender: TObject);
    procedure EmployesClick(Sender: TObject);
    procedure CommercialClick(Sender: TObject);
    procedure SecteursClick(Sender: TObject);
    procedure TravauxDiamantClick(Sender: TObject);
    procedure FraisExternesClick(Sender: TObject);
    procedure FraisStructuresClick(Sender: TObject);
    procedure ListePointageEMPGetEditMask(Sender: TObject; ACol, ARow: Integer;
      var Value: String);
    procedure ListePointageEMPKeyPress(Sender: TObject; var Key: Char);
    procedure ListePointageEMPSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure ListePointageEMPDblClick(Sender: TObject);
    procedure ListePointageEMPMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ConfigurationClick(Sender: TObject);
    procedure ListePointageEMPKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ListePointageEMPClick(Sender: TObject);
    procedure BoutonConsultationClick(Sender: TObject);
    procedure BoutonCreationClick(Sender: TObject);
    procedure ListeSecteursClick(Sender: TObject);
    procedure ListeCommerciauxClick(Sender: TObject);
    procedure ListeClientClick(Sender: TObject);
    procedure FicheExecuterClick(Sender: TObject);
    procedure SaisieSemaineBisClick(Sender: TObject);
    procedure SaisieRegulClick(Sender: TObject);
    procedure BoutonEnregistrerClick(Sender: TObject);
    procedure BoutonImprimerClick(Sender: TObject);
    procedure BoutonAnnulerClick(Sender: TObject);
    procedure BoutonSupprimerClick(Sender: TObject);
    procedure ListeMoisClick(Sender: TObject);
    procedure saisieNBJChange(Sender: TObject);
    procedure LibelleChantierKeyPress(Sender: TObject; var Key: Char);
    procedure ListeEmployesEnter(Sender: TObject);
    procedure ListeEmployesClick(Sender: TObject);
    procedure ListePointageEMPEnter(Sender: TObject);
    procedure ImpressionEtatsClick(Sender: TObject);
    procedure ListeChantiersExit(Sender: TObject);
    procedure ListeChantiersClick(Sender: TObject);
    procedure ImprimanteClick(Sender: TObject);
    procedure SaisieCAKeyPress(Sender: TObject; var Key: Char);
    procedure SaisieCAEnter(Sender: TObject);
    procedure ListeMoisKeyPress(Sender: TObject; var Key: Char);
    procedure NomChantierKeyPress(Sender: TObject; var Key: Char);
    procedure SaisieCAClick(Sender: TObject);
    procedure ListeFichesExit(Sender: TObject);
    procedure ListeFichesClick(Sender: TObject);
    procedure ButtonExecuterMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ButtonExecuterExit(Sender: TObject);
    procedure ExportationClick(Sender: TObject);
    procedure SaisieNumChantierMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SaisieNumChantierChange(Sender: TObject);
    procedure saisieNumSemaineChange(Sender: TObject);
    procedure saisieNumSemaineEnter(Sender: TObject);
    procedure saisieNumSemaineKeyPress(Sender: TObject; var Key: Char);
    procedure SaisieNumChantierKeyPress(Sender: TObject; var Key: Char);
    procedure SaisieNumChantierClick(Sender: TObject);
    procedure saisieNumSemaineClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Verrou1Click(Sender: TObject);
    procedure VerificationIndexClick(Sender: TObject);
    procedure NomChantierClick(Sender: TObject);
    procedure LibelleChantierClick(Sender: TObject);

  private
    { D�clarations priv�es }
  public
    { D�clarations publiques }
  end;

function TestTouchesFleches(Key: Word): Boolean;

type
  TPublicCustomGrid = class(TCustomGrid);

const
  Fichemateriel = 0;
  Ficheemploye = 1;
  Fichesecteur = 2;
  Fichecommercial = 3;
  Ficheclient = 4;
  Fichevehicules = 5;
  Fichetravaux = 6;
  Ficheexterne = 8;
  Fichestructure = 9;

var

  FormBase: TFormBase;
  TableauTemp: array of array of string;
  TableauValHebdo: array of currency;
  TableauValTemp: array of array of currency;

implementation

uses
  applicbase, accesBDD, fichebase, Ficheconfig, Hebdo, FicheEtats, etats,
  MasqueChantiers, MasqueSemaine, Impression, utils,
  MasqueStructures, MasqueVehicules, MasqueDiamant, MasqueAchats,
  MasqueExterne, MasqueMateriels,
  VerifIndex,

  MasqueEmployes;

var

  HandleMutex: THandle;

{$R *.DFM}
{$H-}

  /// ///////////////////////////////////////////////////////////////////////////
function TesterInitialisation: Boolean;
{ routine d'initialisation de la base de donn�es lors du lancement de
  l'executable avec le parametre /C ou /c }
var
  t: string;
  ok: Boolean;
begin
  VerrouON;

  // si parametre de lancement
  if paramcount <> 0 then
  begin
    t := ParamStr(1);

    // creation de la base de donnees
    if (t = '/C') or (t = '/c') then
    begin
      // lancement cr�ation effective de la base de donn�es
      ok := CreationBDD;
      if ok then
      begin
        // creation vaiment effectu�e
        t := 'Config BDD r�alis�e';
        MessageDlg(t, mtInformation, [mbOk], 0);
        result := true;
        exit;
      end;
    end;

    // modification de la base de donnees
    if (t = '/P1') or (t = '/p1') then
    begin
      // lancement modification effective de la base de donn�es
      ok := ModificationBDD;
      if ok then
      begin
        // creation vaiment effectu�e
        t := 'Config BDD modif�e';
        MessageDlg(t, mtInformation, [mbOk], 0);
        result := true;
        exit;
      end;
    end;

    // modification de la base de donnees
    if (t = '/P2') or (t = '/p2') then
    begin
      // lancement modification effective de la base de donn�es
      ok := ModificationBDDajoutTablecumuls;
      if ok then
      begin
        // creation vaiment effectu�e
        t := 'Table de cumuls ajout�e � la BDD';
        MessageDlg(t, mtInformation, [mbOk], 0);
        result := true;
        exit;
      end;
    end;

    // empecher le traitement du verrou
    if (t = '/v') or (t = '/V') then
    begin
      VerrouOFF;
    end;

    // Reindexation de la BDD
    if (t = '/i') or (t = '/I') then
    begin
      // verification presence base de donnees
      if not VerificationBDD then
      begin
        // probleme avec la base de donnees
        application.Terminate;
      end;

      if ReindexationBDD then
      begin
        t := 'Reindexation de la BDD effectu�e';
        MessageDlg(t, mtInformation, [mbOk], 0);
        application.Terminate;
      end;
    end;

    // modification de la base de donnees
    if (t = '/P3') or (t = '/p3') then
    begin
      // verification presence base de donnees
      if not VerificationBDD then
      begin
        // probleme avec la base de donnees
        application.Terminate;
      end;

      PatchEmployesP3;
      result := true;
      exit;
    end;

    // modification de la base de donnees
    if (t = '/P4') or (t = '/p4') then
    begin
      // verification presence base de donnees
      if not VerificationBDD then
      begin
        // probleme avec la base de donnees
        application.Terminate;
      end;

      PatcheIndexSupp;
      result := true;
      exit;
    end;

  end;

  result := false;
end;

function TestTouchesFleches(Key: Word): Boolean;
begin
  result := true;

  if (Key = VK_LEFT) then
    exit;
  if (Key = VK_UP) then
    exit;
  if (Key = VK_RIGHT) then
    exit;
  if (Key = VK_DOWN) then
    exit;

  result := false;
end;

function ExecuteDeja: Boolean;
var
  t: string;
begin
  SetLastError(NO_ERROR);
  result := true;

  t := ExtractFileName(application.ExeName);
  if OpenMutex(MUTEX_ALL_ACCESS, false, pChar(t)) <> 0 then
    exit;

  HandleMutex := CreateMutex(Nil, true, pChar(t));
  if HandleMutex = 0 then
    exit;

  result := false;
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure TFormBase.FormCreate(Sender: TObject);
var
  t: string;

begin
  // traitement lancement unique de l'application
  if ExecuteDeja then
  begin
    t := 'L''application a d�j� �t� lan��e !';
    MessageDlg(t, mtInformation, [mbOk], 0);
    application.Terminate;
  end;

  // changement de l'emplacement de PDOXUSRS.NET : repertoire courant .
  Session.NetFileDir := GetCurrentDir;

  InitImpression;

  // ouverture application
  if TesterInitialisation then
  begin
    // initialisation r�alis�e donc arret du programme
    application.Terminate;
  end;

  // verification presence base de donnees
  if not VerificationBDD then
  begin
    // probleme avec la base de donnees
    application.Terminate;
  end;

  // deselection grille de calcul
  FormBase.ListeCalculs.ColWidths[1] := 53;
  FormBase.ListeCalculs.ColWidths[2] := 0;
  FormBase.ListeCalculs.Col := 2;

  if TesterNecessiteP3 then
  begin
    t := 'Le patche /P3 doit etre lan��';
    MessageDlg(t, mtInformation, [mbOk], 0);
    application.Terminate;
  end;

  if TesterNecessiteP4 then
  begin
    t := 'Le patche /P4 doit etre lan��';
    MessageDlg(t, mtInformation, [mbOk], 0);
    application.Terminate;
  end;

  // initialisation fenetre de base et masques
  InitMasqueBase;

  // chargement des valeurs de la configuration
  ChargeConfiguration;

  // initialisation des tables
  RefreshTables;

  // mise en mode de creation du masque chantier
  SetModeCreationChantier;

  // initialisation du mode hebdo de base
  SetModeHebdoInitial;
  ResetModifHebdo;

  // traitement de la liste de choix des hebdos
  CreationListeFiches;

  // traitement des fiches hebdomadaire
  PrepareInterface(false);

  // analyse la saisie du numero de chantier
  TraiteNumChantier;
end;

procedure TFormBase.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  // traitement lancement unique de l'application
  if HandleMutex <> 0 then
    ReleaseMutex(HandleMutex);

  // traite eventuellement la sortie du logiciel
  Action := TraiteSortieApplication;
end;

procedure TFormBase.QuitterClick(Sender: TObject);
begin
  // traite la sortie
  TraiteSortieSimple;
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure TFormBase.FicheHebdomadaireClick(Sender: TObject);
begin
  // traitement des fiches hebdomadaire
  PrepareInterface(false);
end;

procedure TFormBase.FichedeRegularisationClick(Sender: TObject);
begin
  // traitement des fiches de regularisation
  PrepareInterface(true);
end;

procedure TFormBase.ImpressionEtatsClick(Sender: TObject);
begin
  PreparerFicheImpressionEtats;
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure TFormBase.MaterielsClick(Sender: TObject);
begin
  PreparerFicheSimple(Fichemateriel);
end;

procedure TFormBase.EmployesClick(Sender: TObject);
begin
  PreparerFicheSimple(Ficheemploye);
end;

procedure TFormBase.SecteursClick(Sender: TObject);
begin
  PreparerFicheSimple(Fichesecteur);
end;

procedure TFormBase.CommercialClick(Sender: TObject);
begin
  PreparerFicheSimple(Fichecommercial);
end;

procedure TFormBase.ClientsClick(Sender: TObject);
begin
  PreparerFicheSimple(Ficheclient);
end;

procedure TFormBase.VehiculesClick(Sender: TObject);
begin
  PreparerFicheSimple(Fichevehicules);
end;

procedure TFormBase.TravauxDiamantClick(Sender: TObject);
begin
  PreparerFicheSimple(Fichetravaux);
end;

procedure TFormBase.FraisExternesClick(Sender: TObject);
begin
  PreparerFicheSimple(Ficheexterne);
end;

procedure TFormBase.FraisStructuresClick(Sender: TObject);
begin
  PreparerFicheSimple(Fichestructure);
end;

procedure TFormBase.ConfigurationClick(Sender: TObject);
begin
  if ProtectionFicheOuverte then
    exit;

  PreparerFicheConfig;

  CreationListeFiches;
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure TFormBase.FicheExecuterClick(Sender: TObject);
begin
  // lancement traitement saisie masque de base
  TraiteNumSemaine;
end;

procedure TFormBase.ButtonExecuterMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    ListeFiches.Visible := true;

  ListeChantiers.Visible := false;
end;

procedure TFormBase.ListeChantiersExit(Sender: TObject);
begin
  ListeChantiers.Visible := false;
end;

procedure TFormBase.ButtonExecuterExit(Sender: TObject);
begin
  ListeFiches.Visible := false;
end;

procedure TFormBase.ListeFichesExit(Sender: TObject);
begin
  ListeFiches.Visible := false;
end;

procedure TFormBase.ListeChantiersClick(Sender: TObject);
begin
  ListeFiches.Visible := false;
  TraiteListeChantiers;
end;

procedure TFormBase.ListeFichesClick(Sender: TObject);
begin
  TraiteListeFiches;
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure TFormBase.ListeSecteursClick(Sender: TObject);
begin
  ListeFiches.Visible := false;
  VerifieListesChantier;
end;

procedure TFormBase.ListeCommerciauxClick(Sender: TObject);
begin
  ListeFiches.Visible := false;
  VerifieListesChantier;
end;

procedure TFormBase.ListeClientClick(Sender: TObject);
begin
  ListeFiches.Visible := false;
  VerifieListesChantier;
end;

procedure TFormBase.NomChantierKeyPress(Sender: TObject; var Key: Char);
begin
  VerifieListesChantier;
end;

procedure TFormBase.LibelleChantierKeyPress(Sender: TObject; var Key: Char);
begin
  VerifieListesChantier;
end;

procedure TFormBase.BoutonConsultationClick(Sender: TObject);
begin
  TraiteFicheChantier;
end;

procedure TFormBase.BoutonCreationClick(Sender: TObject);
begin
  TraiteFicheChantier;
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure TFormBase.SaisieNumChantierClick(Sender: TObject);
begin
  ListeFiches.Visible := false;
  SaisieNumChantier.SelectAll;
end;

procedure TFormBase.SaisieNumChantierChange(Sender: TObject);
begin
  TraiteNumChantier;
end;

procedure TFormBase.SaisieNumChantierMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    ListeChantiers.Visible := true;

  ListeFiches.Visible := false;
end;

procedure TFormBase.SaisieNumChantierKeyPress(Sender: TObject; var Key: Char);
begin
  // sortie si chiffre
  if VerifNum(Key) then
    exit;

  // sinon annuler
  Key := chr(0);
end;

procedure TFormBase.saisieNumSemaineClick(Sender: TObject);
begin
  ListeFiches.Visible := false;
  saisieNumSemaine.SelectAll;
end;

procedure TFormBase.saisieNumSemaineEnter(Sender: TObject);
begin
  ListeChantiers.Visible := false;
  ListeFiches.Visible := false;
end;

procedure TFormBase.saisieNumSemaineChange(Sender: TObject);
begin
  // traitemant semaine bis
  verifieSemaineBis;
  afficheMoisSemaineBis;
end;

procedure TFormBase.saisieNumSemaineKeyPress(Sender: TObject; var Key: Char);
begin
  // sortie si chiffre
  if VerifNum(Key) then
    exit;

  if Key = chr(13) then
  begin
    TraiteNumSemaine;
    exit
  end;

  // sinon annuler
  Key := chr(0);
end;

procedure TFormBase.SaisieSemaineBisClick(Sender: TObject);
begin
  ListeFiches.Visible := false;
  afficheMoisSemaineBis;
end;

procedure TFormBase.SaisieRegulClick(Sender: TObject);
begin
  TraiteNumSemaine;
end;

procedure TFormBase.ListeMoisClick(Sender: TObject);
begin
  TraiteListeMois;
end;

procedure TFormBase.ListeMoisKeyPress(Sender: TObject; var Key: Char);
begin
  // lancement traitement saisie masque de base
  if Key = chr(13) then
    TraiteNumSemaine;
end;

procedure TFormBase.SaisieCAClick(Sender: TObject);
begin
  SaisieCA.SelectAll;
end;

procedure TFormBase.SaisieCAEnter(Sender: TObject);
begin
  MasquesRestitueValeurs;
end;

procedure TFormBase.SaisieCAKeyPress(Sender: TObject; var Key: Char);
begin
  TraiteSaisieCA(Key);
end;

procedure TFormBase.saisieNBJChange(Sender: TObject);
begin
  EvenementModifMasqueHebdo;
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure TFormBase.ImprimanteClick(Sender: TObject);
begin
  // configuration de l'imprimante
  PrinterSetupDialog.execute;

  // initialisation de l'imprimante
  InitImpression;
end;

procedure TFormBase.BoutonEnregistrerClick(Sender: TObject);
begin
  HEBDOEnregistrer;

  /// //////////////////////////
  // ImprimeDiamantCoeffON;
  // HEBDOImprimer;
end;

procedure TFormBase.BoutonImprimerClick(Sender: TObject);
begin
  ImprimeDiamantCoeffON;
  HEBDOImprimer;
end;

procedure TFormBase.BoutonAnnulerClick(Sender: TObject);
begin
  HEBDOAnnuler;
end;

procedure TFormBase.BoutonSupprimerClick(Sender: TObject);
begin
  HEBDOSupprimer;
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure TFormBase.ListeEmployesClick(Sender: TObject);
{ routine executee lors du choix d'un element dans les listes des masques.
  celle ci ajoute dans la grille de saisie l'element choisi
  (tous les masque fonctionnent selon le meme principe) }
begin
  if Sender = ListeEmployes then
    TraitePointageChargeLigne;
  if Sender = ListeMateriels then
    TraiteMaterielChargeLigne;
  if Sender = ListeExterne then
    TraiteExterneChargeLigne;
  if Sender = ListeAchats then
    TraiteAchatChargeLigne;
  if Sender = ListeStructures then
    TraiteStructureChargeLigne;
  if Sender = ListeDiamant then
    TraiteDiamantChargeLigne;
  if Sender = ListeVehicules then
    TraiteVehiculeChargeLigne;
end;

procedure TFormBase.ListeEmployesEnter(Sender: TObject);
{ routine executee lorsqu'une liste est manipulee par l'utilisateur }
begin
  MasquesRestitueValeurs;
end;

procedure TFormBase.ListePointageEMPEnter(Sender: TObject);
{ routine executee lorsqu'une grille de saisie recupere le focus }
begin
  MasquesRestitueValeurs;
end;

procedure TFormBase.ListePointageEMPClick(Sender: TObject);
{ routine executee lorsqu'une grille de saisie recoit un click de la souris }
begin
  if Sender = ListePointageEMP then
    traitepointagesouris(false);
  if Sender = ListePointageINT then
    traitepointagesouris(true);
  if Sender = ListeUtilisationVEH then
    traiteVehiculesouris;
  if Sender = ListeUtilisationSTR then
    traiteStructuresouris;
  if Sender = ListeUtilisationMAT then
    traiteMaterielsouris;
  if Sender = ListeUtilisationEXT then
    traiteExternesouris;
  if Sender = ListeUtilisationDIA then
    traiteDiamantsouris;
  if Sender = ListeUtilisationACH then
    traiteAchatsouris;
end;

procedure TFormBase.ListePointageEMPGetEditMask(Sender: TObject;
  ACol, ARow: Integer; var Value: String);
{ routine executee lorsqu'une grille de saisie opere une entree au clavier et
  a besoin d'une chaine representant le masque de saisie de la cellule en cours
  au coordonnees (ACol, ARow) }
begin
  if (Sender = ListePointageEMP) or (Sender = ListePointageINT) then
    Value := traitePointageLireFormatCellules(ACol);
  if Sender = ListeUtilisationVEH then
    Value := traiteVehiculeLireFormatCellules(ACol);
  if Sender = ListeUtilisationSTR then
    Value := traiteStructureLireFormatCellules(ACol);
  if Sender = ListeUtilisationMAT then
    Value := traiteMaterielLireFormatCellules(ACol);
  if Sender = ListeUtilisationEXT then
    Value := traiteExterneLireFormatCellules(ACol);
  if Sender = ListeUtilisationDIA then
    Value := traiteDiamantLireFormatCellules(ACol);
  if Sender = ListeUtilisationACH then
    Value := traiteAchatLireFormatCellules(ACol);
end;

procedure TFormBase.ListePointageEMPSelectCell(Sender: TObject;
  ACol, ARow: Integer; var CanSelect: Boolean);
{ routine executee lorsqu'une cellule de la grille de saisie est selectionnee
  au coordonnees (ACol, ARow) }
begin
  if Sender = ListePointageEMP then
    CanSelect := TraitePointageSelection(false, ACol, ARow);
  if Sender = ListePointageINT then
    CanSelect := TraitePointageSelection(true, ACol, ARow);

  if Sender = ListeUtilisationVEH then
    CanSelect := TraiteVehiculeSelection(ACol, ARow);
  if Sender = ListeUtilisationSTR then
    CanSelect := TraiteStructureSelection(ACol, ARow);
  if Sender = ListeUtilisationMAT then
    CanSelect := TraiteMaterielSelection(ACol, ARow);
  if Sender = ListeUtilisationEXT then
    CanSelect := TraiteExterneSelection(ACol, ARow);
  if Sender = ListeUtilisationDIA then
    CanSelect := TraiteDiamantSelection(ACol, ARow);
  if Sender = ListeUtilisationACH then
    CanSelect := TraiteAchatSelection(ACol, ARow);
end;

procedure TFormBase.ListePointageEMPKeyPress(Sender: TObject; var Key: Char);
{ routine executee lorsqu'une touche du clavier est pressee dans la cellule
  selectionnee de la grille de saisie }
begin
  if Sender = ListePointageEMP then
    TraitePointageValide(false, Key);
  if Sender = ListePointageINT then
    TraitePointageValide(true, Key);
  if Sender = ListeUtilisationVEH then
    TraiteVehiculeValide(Key);
  if Sender = ListeUtilisationSTR then
    TraiteStructureValide(Key);
  if Sender = ListeUtilisationMAT then
    TraiteMaterielValide(Key);
  if Sender = ListeUtilisationEXT then
    TraiteExterneValide(Key);
  if Sender = ListeUtilisationDIA then
    TraiteDiamantValide(Key);
  if Sender = ListeUtilisationACH then
    TraiteAchatValide(Key);
end;

procedure TFormBase.ListePointageEMPDblClick(Sender: TObject);
{ routine executee lorsqu'une cellule de la grille de saisie est
  selectionnee par un double click de la souris }
begin
  // lancement effacement de la ligne
  if Sender = ListePointageEMP then
    TraitePointageEffacerLigne(false);
  if Sender = ListePointageINT then
    TraitePointageEffacerLigne(true);
  if Sender = ListeUtilisationVEH then
    TraiteVehiculeEffacerLigne;
  if Sender = ListeUtilisationSTR then
    TraiteStructureEffacerLigne;
  if Sender = ListeUtilisationMAT then
    TraiteMaterielEffacerLigne;
  if Sender = ListeUtilisationEXT then
    TraiteExterneEffacerLigne;
  if Sender = ListeUtilisationDIA then
    TraiteDiamantEffacerLigne;
  if Sender = ListeUtilisationACH then
    TraiteAchatEffacerLigne;
end;

procedure TFormBase.ListePointageEMPMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
{ routine executee lorsque la cellule selectionnee de la grille de saisie
  voit le bouton de la souris remonter }
begin
  // sauvegarde position colonne ligne dans Liste Pointage
  if Sender = ListePointageEMP then
    traitePointageInitialiserProcEffaceLigne(false, X, Y);
  if Sender = ListePointageINT then
    traitePointageInitialiserProcEffaceLigne(true, X, Y);
  if Sender = ListeUtilisationVEH then
    traiteVehiculeInitialiserProcEffaceLigne(X, Y);
  if Sender = ListeUtilisationSTR then
    traiteStructureInitialiserProcEffaceLigne(X, Y);
  if Sender = ListeUtilisationMAT then
    traiteMaterielInitialiserProcEffaceLigne(X, Y);
  if Sender = ListeUtilisationEXT then
    traiteExterneInitialiserProcEffaceLigne(X, Y);
  if Sender = ListeUtilisationDIA then
    traiteDiamantInitialiserProcEffaceLigne(X, Y);
  if Sender = ListeUtilisationACH then
    traiteAchatInitialiserProcEffaceLigne(X, Y);
end;

procedure TFormBase.ListePointageEMPKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
{ routine executee lorsque la cellule selectionnee de la grille de saisie
  voit la touche du clavier remonter }

begin
  OutputDebugStringA(PAnsiChar('cellule = ' + AnsiString(Sender.ToString)));

  if Sender = ListePointageEMP then
  begin
    traitepointagetouches(false, Key);
    traitepointagetouchesFIN();
  end;

  if Sender = ListePointageINT then
  begin
    traitepointagetouches(true, Key);
    traitepointagetouchesFIN();
  end;

  if Sender = ListeUtilisationVEH then
  begin
    traiteVehiculetouches(Key);
    traiteVehiculetouchesFIN;
  end;

  if Sender = ListeUtilisationSTR then
  begin
    traiteStructuretouches(Key);
    traiteStructuretouchesFIN;
  end;

  if Sender = ListeUtilisationMAT then
  begin
    traiteMaterieltouches(Key);
    traiteMaterieltouchesFIN;
  end;

  if Sender = ListeUtilisationEXT then
  begin
    traiteExternetouches(Key);
    traiteExternetouchesFIN;
  end;

  if Sender = ListeUtilisationDIA then
  begin
    traiteDiamanttouches(Key);
    traiteDiamanttouchesFIN;
  end;

  if Sender = ListeUtilisationACH then
  begin
    traiteAchattouches(Key);
    traiteAchattouchesFIN;
  end;
end;
/// ///////////////////////////////////////////////////////////////////////////

procedure TFormBase.ExportationClick(Sender: TObject);
begin
  if Exportation.Checked then
    Exportation.Checked := false
  else
    Exportation.Checked := true;
end;

procedure TFormBase.Button1Click(Sender: TObject);
begin
  printer.Orientation := poLandscape;
  printer.BeginDoc;
  tracefondETAT5;
  printer.EndDoc;
end;

procedure TFormBase.Button2Click(Sender: TObject);
begin
  printer.Orientation := poLandscape;
  printer.BeginDoc;
  tracefondETAT6;
  printer.EndDoc;
end;

procedure TFormBase.Button3Click(Sender: TObject);
begin
  printer.Orientation := poLandscape;
  printer.BeginDoc;
  tracefondETATRECAPCLI;
  printer.EndDoc;
end;

procedure TFormBase.Verrou1Click(Sender: TObject);
begin
  showmessage('essai')
end;

procedure TFormBase.VerificationIndexClick(Sender: TObject);
var
  t: string;
begin
  if ProtectionFicheOuverte then
    exit;

  t := 'Veuillez comfirmer la reindexation des fiches ';
  if MessageDlg(t, mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    FicheTestIndex.ShowModal;
end;

procedure TFormBase.NomChantierClick(Sender: TObject);
begin
  ListeFiches.Visible := false;
end;

procedure TFormBase.LibelleChantierClick(Sender: TObject);
begin
  ListeFiches.Visible := false;
end;

end.
