�
 TFORMCONFIG 0c<  TPF0TFormConfig
FormConfigLeftTop�BorderIcons BorderStylebsDialogCaptionConfigurationClientHeight.ClientWidth>Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPosition	poDefaultOnCreate
FormCreatePixelsPerInch`
TextHeight TLabelLabel1LeftTophWidth_HeightCaptionTaux heures samedi  TLabelLabel2LeftTop� WidthMHeightCaptionFacteur charges  TLabelLabel3LeftTop-WidthUHeightCaption   Petit déplacement  TLabelLabel4LeftTopEWidth]HeightCaption   Grand déplacement  TLabelLabel5LeftTop� Width7HeightCaptionFrais induits  TLabelLabel6LeftTop� Width� HeightCaptionFrais outils et consommables  TLabelLabel7LeftTopWidthKHeightCaption   Année en cours  TLabelLabel8LeftTop� Width^HeightCaptionFrais fixes vehicules  TLabelLabel9LeftTop� WidthZHeightCaptionFrais fixes materiels  TLabelLabel10LeftTopWidth_HeightCaptionFrais fixes structures  TLabelLabel11Left� TopNWidth"HeightCaptionJanvier  TLabelLabel12LeftTop:WidthMHeightCaptionNombre de jours  TLabelLabel13Left� Top`Width HeightCaptionFevrier  TLabelLabel14Left� ToprWidthHeightCaptionMars  TLabelLabel15Left� Top� WidthHeightCaptionAvril  TLabelLabel16Left� Top� WidthHeightCaptionMai  TLabelLabel17Left� Top� WidthHeightCaptionJuin  TLabelLabel18Left� Top� WidthHeightCaptionJuillet  TLabelLabel19Left� Top� WidthHeightCaptionAout  TLabelLabel20Left� Top� Width3HeightCaption	Septembre  TLabelLabel21Left� Top� Width&HeightCaptionOctobre  TLabelLabel22Left� TopWidth1HeightCaptionNovembre  TLabelLabel23Left� TopWidth1HeightCaptionDecembre  TLabelLabel24LeftTop}WidthrHeightCaption   Taux heures intérimaires  TButtonButton1LeftTopWidthKHeightCancel	CaptionFermerDefault	TabOrder OnClickButton1Click  TDBEditDBEdit1Left� TopeWidthFHeight	DataFieldcfg_tauxheuresup
DataSourceDataSourceConfigTabOrder  TDBEditDBEdit2Left� Top� WidthFHeight	DataFieldcfg_facteurcharge
DataSourceDataSourceConfigTabOrder  TDBEditDBEdit3Left� Top+WidthFHeight	DataFieldcfg_petitpannier
DataSourceDataSourceConfigTabOrder  TDBEditDBEdit4Left� TopCWidthFHeight	DataFieldcfg_grandpannier
DataSourceDataSourceConfigTabOrder  TDBEditDBEdit5Left� Top� WidthFHeight	DataFieldcfg_fraisinduits
DataSourceDataSourceConfigTabOrder  TDBEditDBEdit6Left� Top� WidthFHeight	DataFieldcfg_fraisoutilsconso
DataSourceDataSourceConfigTabOrder  TDBEditDBEdit7Left� Top
WidthFHeight	DataFieldcfg_courantannee
DataSourceDataSourceConfigTabOrderOnChangeDBEdit7Change  TDBEditDBEdit8Left� Top� WidthGHeight	DataFieldCfg_coutfixevehicule
DataSourceDataSourceConfigTabOrder  TDBEditDBEdit9Left� Top� WidthGHeight	DataFieldCfg_coutfixemateriel
DataSourceDataSourceConfigTabOrder	  TDBEditDBEdit10Left� TopWidthGHeight	DataFieldCfg_coutfixestructure
DataSourceDataSourceConfigTabOrder
  TDBEditDBEdit11Left3TopMWidth*HeightAutoSize	DataFieldCfg_nbj_jan
DataSourceDataSourceConfigTabOrder  TDBEditDBEdit12Left3Top_Width*HeightAutoSize	DataFieldCfg_nbj_fev
DataSourceDataSourceConfigTabOrder  TDBEditDBEdit13Left3TopqWidth*HeightAutoSize	DataFieldCfg_nbj_mar
DataSourceDataSourceConfigTabOrder  TDBEditDBEdit14Left3Top� Width*HeightAutoSize	DataFieldCfg_nbj_avr
DataSourceDataSourceConfigTabOrder  TDBEditDBEdit15Left3Top� Width*HeightAutoSize	DataFieldCfg_nbj_mai
DataSourceDataSourceConfigTabOrder  TDBEditDBEdit16Left3Top� Width*HeightAutoSize	DataFieldCfg_nbj_jui
DataSourceDataSourceConfigTabOrder  TDBEditDBEdit17Left3Top� Width*HeightAutoSize	DataFieldCfg_nbj_jul
DataSourceDataSourceConfigTabOrder  TDBEditDBEdit18Left3Top� Width*HeightAutoSize	DataFieldCfg_nbj_aou
DataSourceDataSourceConfigTabOrder  TDBEditDBEdit19Left3Top� Width*HeightAutoSize	DataFieldCfg_nbj_sep
DataSourceDataSourceConfigTabOrder  TDBEditDBEdit20Left3Top� Width*HeightAutoSize	DataFieldCfg_nbj_oct
DataSourceDataSourceConfigTabOrder  TDBEditDBEdit21Left3TopWidth*HeightAutoSize	DataFieldCfg_nbj_nov
DataSourceDataSourceConfigTabOrder  TDBEditDBEdit22Left3TopWidth*HeightAutoSize	DataFieldCfg_nbj_dec
DataSourceDataSourceConfigTabOrder  TDBEditDBEdit23Left� TopzWidthFHeight	DataFieldcfg_tauxheuresupInt
DataSourceDataSourceConfigTabOrder  	TGroupBox	GroupBox1LeftpTop� Width� Height� Caption   Définition des semaines bisTabOrder TLabelLabel25Left'TopWidth� HeightCaptionx0 x1 x2 x3 x4 x5 x6 x7 x8 x9   TLabelLabel26LeftTop-WidthHeightCaption0 X  TLabelLabel27LeftTop=WidthHeightCaption1 X  TLabelLabel28LeftTopMWidthHeightCaption2 X  TLabelLabel29LeftTop]WidthHeightCaption3 X  TLabelLabel30LeftTopmWidthHeightCaption4 X  TLabelLabel31LeftTopWidthHeightCaption5 X  TDBCheckBoxDBCheckBox47Left&TopLWidthHeightCaptionDBCheckBox9	DataFieldcfg_typesemaine_20
DataSourceDataSourceConfigTabOrder.ValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox48Left&Top\WidthHeightCaptionDBCheckBox9	DataFieldcfg_typesemaine_30
DataSourceDataSourceConfigTabOrder/ValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox49Left&ToplWidthHeightCaptionDBCheckBox9	DataFieldcfg_typesemaine_40
DataSourceDataSourceConfigTabOrder0ValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox50Left&Top|WidthHeightCaptionDBCheckBox9	DataFieldcfg_typesemaine_50
DataSourceDataSourceConfigTabOrder1ValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox46Left&Top<WidthHeightCaptionDBCheckBox9	DataFieldcfg_typesemaine_10
DataSourceDataSourceConfigTabOrder-ValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox1Left4Top,WidthHeight	DataFieldcfg_typesemaine_1
DataSourceDataSourceConfigTabOrder ValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox2LeftBTop,WidthHeightCaptionDBCheckBox2	DataFieldcfg_typesemaine_2
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox3LeftPTop,WidthHeightCaptionDBCheckBox3	DataFieldcfg_typesemaine_3
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox4Left^Top,WidthHeightCaptionDBCheckBox4	DataFieldcfg_typesemaine_4
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox5LeftlTop,WidthHeightCaptionDBCheckBox5	DataFieldcfg_typesemaine_5
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox6LeftzTop,WidthHeightCaptionDBCheckBox6	DataFieldcfg_typesemaine_6
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox7Left� Top,WidthHeightCaptionDBCheckBox7	DataFieldcfg_typesemaine_7
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox8Left� Top,WidthHeightCaptionDBCheckBox8	DataFieldcfg_typesemaine_8
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox9Left� Top,WidthHeightCaptionDBCheckBox9	DataFieldcfg_typesemaine_9
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox10Left4Top<WidthHeightCaptionDBCheckBox10	DataFieldcfg_typesemaine_11
DataSourceDataSourceConfigTabOrder	ValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox11LeftBTop<WidthHeightCaptionDBCheckBox2	DataFieldcfg_typesemaine_12
DataSourceDataSourceConfigTabOrder
ValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox12LeftPTop<WidthHeightCaptionDBCheckBox3	DataFieldcfg_typesemaine_13
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox13Left^Top<WidthHeightCaptionDBCheckBox4	DataFieldcfg_typesemaine_14
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox14LeftlTop<WidthHeightCaptionDBCheckBox5	DataFieldcfg_typesemaine_15
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox15LeftzTop<WidthHeightCaptionDBCheckBox6	DataFieldcfg_typesemaine_16
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox16Left� Top<WidthHeightCaptionDBCheckBox7	DataFieldcfg_typesemaine_17
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox17Left� Top<WidthHeightCaptionDBCheckBox8	DataFieldcfg_typesemaine_18
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox18Left� Top<WidthHeightCaptionDBCheckBox9	DataFieldcfg_typesemaine_19
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox19Left4TopLWidthHeightCaptionDBCheckBox10	DataFieldcfg_typesemaine_21
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox20LeftBTopLWidthHeightCaptionDBCheckBox2	DataFieldcfg_typesemaine_22
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox21LeftPTopLWidthHeightCaptionDBCheckBox3	DataFieldcfg_typesemaine_23
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox22Left^TopLWidthHeightCaptionDBCheckBox4	DataFieldcfg_typesemaine_24
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox23LeftlTopLWidthHeightCaptionDBCheckBox5	DataFieldcfg_typesemaine_25
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox24LeftzTopLWidthHeightCaptionDBCheckBox6	DataFieldcfg_typesemaine_26
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox25Left� TopLWidthHeightCaptionDBCheckBox7	DataFieldcfg_typesemaine_27
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox26Left� TopLWidthHeightCaptionDBCheckBox8	DataFieldcfg_typesemaine_28
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox27Left� TopLWidthHeightCaptionDBCheckBox9	DataFieldcfg_typesemaine_29
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox28Left4Top\WidthHeightCaptionDBCheckBox10	DataFieldcfg_typesemaine_31
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox29LeftBTop\WidthHeightCaptionDBCheckBox2	DataFieldcfg_typesemaine_32
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox30LeftPTop\WidthHeightCaptionDBCheckBox3	DataFieldcfg_typesemaine_33
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox31Left^Top\WidthHeightCaptionDBCheckBox4	DataFieldcfg_typesemaine_34
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox32LeftlTop\WidthHeightCaptionDBCheckBox5	DataFieldcfg_typesemaine_35
DataSourceDataSourceConfigTabOrderValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox33LeftzTop\WidthHeightCaptionDBCheckBox6	DataFieldcfg_typesemaine_36
DataSourceDataSourceConfigTabOrder ValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox34Left� Top\WidthHeightCaptionDBCheckBox7	DataFieldcfg_typesemaine_37
DataSourceDataSourceConfigTabOrder!ValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox35Left� Top\WidthHeightCaptionDBCheckBox8	DataFieldcfg_typesemaine_38
DataSourceDataSourceConfigTabOrder"ValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox36Left� Top\WidthHeightCaptionDBCheckBox9	DataFieldcfg_typesemaine_39
DataSourceDataSourceConfigTabOrder#ValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox37Left4ToplWidthHeightCaptionDBCheckBox10	DataFieldcfg_typesemaine_41
DataSourceDataSourceConfigTabOrder$ValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox38LeftBToplWidthHeightCaptionDBCheckBox2	DataFieldcfg_typesemaine_42
DataSourceDataSourceConfigTabOrder%ValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox39LeftPToplWidthHeightCaptionDBCheckBox3	DataFieldcfg_typesemaine_43
DataSourceDataSourceConfigTabOrder&ValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox40Left^ToplWidthHeightCaptionDBCheckBox4	DataFieldcfg_typesemaine_44
DataSourceDataSourceConfigTabOrder'ValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox41LeftlToplWidthHeightCaptionDBCheckBox5	DataFieldcfg_typesemaine_45
DataSourceDataSourceConfigTabOrder(ValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox42LeftzToplWidthHeightCaptionDBCheckBox6	DataFieldcfg_typesemaine_46
DataSourceDataSourceConfigTabOrder)ValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox43Left� ToplWidthHeightCaptionDBCheckBox7	DataFieldcfg_typesemaine_47
DataSourceDataSourceConfigTabOrder*ValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox44Left� ToplWidthHeightCaptionDBCheckBox8	DataFieldcfg_typesemaine_48
DataSourceDataSourceConfigTabOrder+ValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox45Left� ToplWidthHeightCaptionDBCheckBox9	DataFieldcfg_typesemaine_49
DataSourceDataSourceConfigTabOrder,ValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox51Left4Top|WidthHeightCaptionDBCheckBox9	DataFieldcfg_typesemaine_51
DataSourceDataSourceConfigTabOrder2ValueCheckedVraiValueUncheckedFaux  TDBCheckBoxDBCheckBox52LeftBTop|WidthHeightCaptionDBCheckBox9	DataFieldcfg_typesemaine_52
DataSourceDataSourceConfigTabOrder3ValueCheckedVraiValueUncheckedFaux   TDataSourceDataSourceConfigDataSetTableconfigLeft�Top  TTableTableconfigDatabaseNamegcFORA	TableNameconfig	TableType	ttParadoxLeft�Top   