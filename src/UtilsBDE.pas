unit UtilsBDE;

interface

function TesterIndexFiche(ligne: string): boolean;
function ReindexationBDD: boolean;
procedure RecreerBDD;

implementation

uses
  winbase, UITypes, hebdo, accesBDD, VerifIndex,
  dbtables, db, SysUtils, bde, Dialogs,
  DbiProcs, DbiTypes, DBIErrs;

var
  semaine: integer;
  bis: boolean;
  mois: integer;
  annee: integer;
  idcha: integer;
  nbjours: integer;
  verrou: boolean;
  impression: boolean;
  annul: boolean;
  regul: boolean;
  totaux: array [0 .. 13] of currency;

  nbligEMP: integer;
  nbligINT: integer;
  inter: boolean;
  tINT: array of array of currency;
  tEMP: array of array of currency;
  tnomEMP: array of string;
  tnomINT: array of string;
  CPP: currency;
  CGP: currency;
  THS: currency;
  TCB: currency;

  nbligDIA: integer;
  TDIA: array of array of currency;
  TnomDIA: array of string;

  nbligVEH: integer;
  TVEH: array of array of currency;
  TnomVEH: array of string;

  nbligMAT: integer;
  TMAT: array of array of currency;
  TnomMAT: array of string;

  nbligEXT: integer;
  TEXT: array of array of currency;
  TnomEXT: array of string;

  nbligSTR: integer;
  TSTR: array of array of currency;
  TnomSTR: array of string;

  nbligACH: integer;
  TACH: array of array of currency;
  TnomACH: array of string;

  { ----------------------------------------------------------------------------- }

function TesterIndexFiche(ligne: string): boolean;
var
  c: boolean;
  b: boolean;
  idcha: integer;
  num: integer;
  n: integer;
begin
  result := true;

  if Copy(ligne, 1, 1) = 'R' then
    c := true
  else
    c := false;

  if Copy(ligne, 4, 1) = 'b' then
    b := true
  else
    b := false;

  with Formbase.Tablechantiers do
  begin
    close;
    indexname := 'indcha';
    open;
    setkey;
    fieldbyname('cha_numchantier').asstring := Copy(ligne, 7, 6);
    gotokey;
    idcha := fieldbyname('id_chantier').asinteger;
  end;

  num := strtoint(Copy(ligne, 2, 2));
  ChercheHebdo(c, b, idcha, num);

  if c then
    n := Formbase.TableHebdo.fieldbyname('heb_numeromois').asinteger
  else
    n := Formbase.TableHebdo.fieldbyname('heb_numerosemaine').asinteger;

  if n <> num then
    result := false;
end;

{ ----------------------------------------------------------------------------- }

function ReindexationBDD: boolean;
{ cette routine opere une reindexation complete de toutes les tables de la
  base de donnees renvoie faux dans le cas d'une erreur sinon renvoie vrai }

  function traite(nomtable: string): boolean;
  var
    t: string;
    n: word;
  begin
    result := false;

    with Formbase.TableTemp do
    begin
      open;
      Active := false;
      TableName := nomtable;
      close;
      exclusive := true;
      open;

      n := DbiRegenIndexes(Handle);
      if n = DBIERR_NONE then
      begin
        result := true;
        close;
        exit;
      end
      else
      begin
        t := 'Probleme de reindexation de la BDD ';
        t := t + 'dans la table ' + nomtable;
        t := t + ': ';

        case n of
          DBIERR_INVALIDHNDL:
            t := t + 'The specified cursor handle is invalid or NULL.';

          DBIERR_NEEDEXCLACCESS:
            t := t + 'The table associated with hCursor is opened in open shared mode.';

          DBIERR_NOTSUPPORTED:
            t := t + 'SQL indexes cannot be regenerated.';
        end;

        MessageDlg(t, mtInformation, [mbOk], 0);
      end;

      close;
    end;
  end;

begin
  result := true;

  if not traite('commercial') then
    result := false;
  if not traite('secteurs') then
    result := false;
  if not traite('clients') then
    result := false;
  if not traite('chantiers') then
    result := false;

  if not traite('materiels') then
    result := false;
  if not traite('employes') then
    result := false;
  if not traite('vehicules') then
    result := false;
  if not traite('externe') then
    result := false;
  if not traite('structure') then
    result := false;
  if not traite('diamant') then
    result := false;

  if not traite('fiche_pointage') then
    result := false;
  if not traite('fiche_achat') then
    result := false;
  if not traite('fiche_materiel') then
    result := false;
  if not traite('fiche_vehicule') then
    result := false;
  if not traite('fiche_externe') then
    result := false;
  if not traite('fiche_structure') then
    result := false;
  if not traite('fiche_diamant') then
    result := false;

  if not traite('fiche_hebdo') then
    result := false;
end;

{ ----------------------------------------------------------------------------- }

procedure recreationtablehebdo;
begin
  with Formbase.TableHebdo do
  begin
    Active := false;
    databasename := 'BASEGCFORA';
    TableType := ttParadox;
    TableName := 'fiche_hebdo';

    FieldDefs.Clear;
    FieldDefs.Add('id_hebdo', ftAutoInc, 0, true);
    FieldDefs.Add('heb_numerosemaine', ftInteger, 0, false);
    FieldDefs.Add('heb_semainebis', ftBoolean, 0, false);
    FieldDefs.Add('heb_numeromois', ftInteger, 0, false);
    FieldDefs.Add('heb_verrou', ftBoolean, 0, false);
    FieldDefs.Add('heb_impression', ftBoolean, 0, false);
    FieldDefs.Add('heb_annul', ftBoolean, 0, false);
    FieldDefs.Add('heb_annee', ftInteger, 0, false);
    FieldDefs.Add('heb_regul', ftBoolean, 0, false);
    FieldDefs.Add('heb_nbjourstravail', ftCurrency, 0, false);
    FieldDefs.Add('heb_chiffreaffairedefini', ftCurrency, 0, false);
    FieldDefs.Add('heb_totalsalarie', ftCurrency, 0, false);
    FieldDefs.Add('heb_totalinterim', ftCurrency, 0, false);
    FieldDefs.Add('heb_totalvehicule', ftCurrency, 0, false);
    FieldDefs.Add('heb_totaldiamant', ftCurrency, 0, false);
    FieldDefs.Add('heb_totalmateriel', ftCurrency, 0, false);
    FieldDefs.Add('heb_totaloutils', ftCurrency, 0, false);
    FieldDefs.Add('heb_totalexterne', ftCurrency, 0, false);
    FieldDefs.Add('heb_totalinduits', ftCurrency, 0, false);
    FieldDefs.Add('heb_totalinterne', ftCurrency, 0, false);
    FieldDefs.Add('heb_totaldeboursesec', ftCurrency, 0, false);
    FieldDefs.Add('heb_totalstructures', ftCurrency, 0, false);
    FieldDefs.Add('heb_totalprixrevient', ftCurrency, 0, false);
    FieldDefs.Add('heb_totalresultat', ftCurrency, 0, false);
    FieldDefs.Add('id_chantier', ftInteger, 0, false);

    IndexDefs.Clear;
    IndexDefs.Add('', 'id_hebdo', [ixPrimary, ixUnique]);

    IndexDefs.Add('indse', 'heb_numerosemaine', [ixCaseInsensitive]);
    IndexDefs.Add('indmo', 'heb_numeromois', [ixCaseInsensitive]);
    IndexDefs.Add('indfc', 'id_chantier', [ixCaseInsensitive]);

    CreateTable;
  end;
end;

{ ----------------------------------------------------------------------------- }

procedure LireLignePointage;
{ execute le chargement a partir de la BDD du contenu de la grille de saisie }
var
  valeur: currency;
  nomemp: string;
begin
  with Formbase.TabFichPoint do
  begin
    CPP := fieldbyname('emp_enr_petitpannier').Ascurrency;
    CGP := fieldbyname('emp_enr_grandpannier').Ascurrency;
    THS := fieldbyname('emp_enr_tauxheuresup').Ascurrency;
    TCB := fieldbyname('emp_enr_facteurcharge').Ascurrency;

    inter := fieldbyname('emp_enr_interimaire').asboolean;
    if inter then
    begin
      SetLength(tnomINT, nbligINT + 1);
      SetLength(tINT, 9, nbligINT + 1);

      nomemp := fieldbyname('emp_enr_nomemploye').asstring;
      tnomINT[nbligINT] := nomemp;

      valeur := fieldbyname('emp_enr_tauxhoraire').Ascurrency;
      tINT[0, nbligINT] := valeur;

      valeur := fieldbyname('emp_nbjourtravail').Ascurrency;
      tINT[1, nbligINT] := valeur;

      valeur := fieldbyname('emp_heuresroute').Ascurrency;
      tINT[2, nbligINT] := valeur;

      valeur := fieldbyname('emp_heuresjour').Ascurrency;
      tINT[3, nbligINT] := valeur;

      valeur := fieldbyname('emp_heuressamedi').Ascurrency;
      tINT[4, nbligINT] := valeur;

      valeur := fieldbyname('emp_heuresnuitdimanche').Ascurrency;
      tINT[5, nbligINT] := valeur;

      valeur := fieldbyname('emp_petitdeplacement').Ascurrency;
      tINT[6, nbligINT] := valeur;

      valeur := fieldbyname('emp_granddeplacement').Ascurrency;
      tINT[7, nbligINT] := valeur;

      valeur := fieldbyname('emp_total').Ascurrency;
      tINT[8, nbligINT] := valeur;

      inc(nbligINT);
    end
    else
    begin
      SetLength(tnomEMP, nbligEMP + 1);
      SetLength(tEMP, 9, nbligEMP + 1);

      nomemp := fieldbyname('emp_enr_nomemploye').asstring;
      tnomEMP[nbligEMP] := nomemp;

      valeur := fieldbyname('emp_enr_tauxhoraire').Ascurrency;
      tEMP[0, nbligEMP] := valeur;

      valeur := fieldbyname('emp_nbjourtravail').Ascurrency;
      tEMP[1, nbligEMP] := valeur;

      valeur := fieldbyname('emp_heuresroute').Ascurrency;
      tEMP[2, nbligEMP] := valeur;

      valeur := fieldbyname('emp_heuresjour').Ascurrency;
      tEMP[3, nbligEMP] := valeur;

      valeur := fieldbyname('emp_heuressamedi').Ascurrency;
      tEMP[4, nbligEMP] := valeur;

      valeur := fieldbyname('emp_heuresnuitdimanche').Ascurrency;
      tEMP[5, nbligEMP] := valeur;

      valeur := fieldbyname('emp_petitdeplacement').Ascurrency;
      tEMP[6, nbligEMP] := valeur;

      valeur := fieldbyname('emp_granddeplacement').Ascurrency;
      tEMP[7, nbligEMP] := valeur;

      valeur := fieldbyname('emp_total').Ascurrency;
      tEMP[8, nbligEMP] := valeur;

      inc(nbligEMP);
    end;
  end;
end;

{ ----------------------------------------------------------------------------- }

procedure lirelignediamant;
var
  valeur: currency;
  nomdia: string;
begin
  with Formbase.TabFichDiam do
  begin
    SetLength(TnomDIA, nbligDIA + 1);
    SetLength(TDIA, 4, nbligDIA + 1);

    nomdia := fieldbyname('tra_enr_libellenom').asstring;
    TnomDIA[nbligDIA] := nomdia;

    valeur := fieldbyname('tra_enr_cout').Ascurrency;
    TDIA[0, nbligDIA] := valeur;

    valeur := fieldbyname('tra_nombre').Ascurrency;
    TDIA[1, nbligDIA] := valeur;

    valeur := fieldbyname('tra_coeffficient').Ascurrency;
    TDIA[2, nbligDIA] := valeur;

    valeur := fieldbyname('tra_total').Ascurrency;
    TDIA[3, nbligDIA] := valeur;

    inc(nbligDIA);
  end;
end;

{ ----------------------------------------------------------------------------- }

procedure lirelignevehicule;
var
  valeur: currency;
  nomveh: string;
begin
  with Formbase.TabFichVehic do
  begin
    SetLength(TnomVEH, nbligVEH + 1);
    SetLength(TVEH, 4, nbligVEH + 1);

    nomveh := fieldbyname('veh_enr_libellenom').asstring;
    TnomVEH[nbligVEH] := nomveh;

    valeur := fieldbyname('veh_enr_cout').Ascurrency;
    TVEH[0, nbligVEH] := valeur;

    valeur := fieldbyname('veh_nombre').Ascurrency;
    TVEH[1, nbligVEH] := valeur;

    valeur := fieldbyname('veh_total').Ascurrency;
    TVEH[2, nbligVEH] := valeur;

    inc(nbligVEH);
  end;
end;

{ ----------------------------------------------------------------------------- }

procedure LireLignemateriel;
var
  valeur: currency;
  nommat: string;
begin
  with Formbase.TabFichMat do
  begin
    SetLength(TnomMAT, nbligMAT + 1);
    SetLength(TMAT, 3, nbligMAT + 1);

    nommat := fieldbyname('mat_enr_libellenom').asstring;
    TnomMAT[nbligMAT] := nommat;

    valeur := fieldbyname('mat_enr_cout').Ascurrency;
    TMAT[0, nbligMAT] := valeur;

    valeur := fieldbyname('mat_dureeutil').Ascurrency;
    TMAT[1, nbligMAT] := valeur;

    valeur := fieldbyname('mat_total').Ascurrency;
    TMAT[2, nbligMAT] := valeur;

    inc(nbligMAT);
  end;
end;

procedure lireligneexterne;
var
  valeur: currency;
  nomext: string;
begin
  with Formbase.TabFichExter do
  begin
    SetLength(TnomEXT, nbligEXT + 1);
    SetLength(TEXT, 1, nbligEXT + 1);

    nomext := fieldbyname('ext_enr_libellenom').asstring;
    TnomEXT[nbligEXT] := nomext;

    valeur := fieldbyname('ext_cout').Ascurrency;
    TEXT[0, nbligEXT] := valeur;

    inc(nbligEXT);
  end;
end;

procedure lirelignestructure;
var
  valeur: currency;
  nomstr: string;
begin
  with Formbase.TabFichStruc do
  begin
    SetLength(TnomSTR, nbligSTR + 1);
    SetLength(TSTR, 3, nbligSTR + 1);

    nomstr := fieldbyname('str_enr_libellenom').asstring;
    TnomSTR[nbligSTR] := nomstr;

    valeur := fieldbyname('str_enr_cout').Ascurrency;
    TSTR[0, nbligSTR] := valeur;

    valeur := fieldbyname('str_nombre').Ascurrency;
    TSTR[1, nbligSTR] := valeur;

    valeur := fieldbyname('str_total').Ascurrency;
    TSTR[2, nbligSTR] := valeur;

    inc(nbligSTR);
  end;
end;

{ ----------------------------------------------------------------------------- }

procedure lireligneachat;
var
  valeur: currency;
  nomach: string;
begin
  with Formbase.TabFichAcha do
  begin
    SetLength(TnomACH, nbligACH + 1);
    SetLength(TACH, 1, nbligACH + 1);

    nomach := fieldbyname('int_enr_libellenom').asstring;
    TnomACH[nbligACH] := nomach;

    valeur := fieldbyname('int_cout').Ascurrency;
    TACH[0, nbligACH] := valeur;

    inc(nbligACH);
  end;
end;

{ ----------------------------------------------------------------------------- }
function lireficheencoursdanstempo: integer;
var
  t: string;
  idh: integer;
  i: integer;
  nbr: integer;
begin
  with Formbase.TableTemp do
  begin
    idh := fieldbyname('id_hebdo').asinteger;
    semaine := fieldbyname('heb_numerosemaine').asinteger;
    bis := fieldbyname('heb_semainebis').asboolean;
    mois := fieldbyname('heb_numeromois').asinteger;
    annee := fieldbyname('heb_annee').asinteger;
    idcha := fieldbyname('id_chantier').asinteger;
    verrou := fieldbyname('heb_verrou').asboolean;
    impression := fieldbyname('heb_impression').asboolean;
    annul := fieldbyname('heb_annul').asboolean;
    regul := fieldbyname('heb_regul').asboolean;
    nbjours := fieldbyname('heb_nbjourstravail').asinteger;
    totaux[0] := fieldbyname('heb_totalinduits').Ascurrency;
    totaux[1] := fieldbyname('heb_chiffreaffairedefini').Ascurrency;
    totaux[2] := fieldbyname('heb_totalsalarie').Ascurrency;
    totaux[3] := fieldbyname('heb_totalinterim').Ascurrency;
    totaux[4] := fieldbyname('heb_totalvehicule').Ascurrency;
    totaux[5] := fieldbyname('heb_totaldiamant').Ascurrency;
    totaux[6] := fieldbyname('heb_totalmateriel').Ascurrency;
    totaux[7] := fieldbyname('heb_totaloutils').Ascurrency;
    totaux[8] := fieldbyname('heb_totalexterne').Ascurrency;
    totaux[9] := fieldbyname('heb_totalinterne').Ascurrency;
    totaux[10] := fieldbyname('heb_totaldeboursesec').Ascurrency;
    totaux[11] := fieldbyname('heb_totalstructures').Ascurrency;
    totaux[12] := fieldbyname('heb_totalprixrevient').Ascurrency;
    totaux[13] := fieldbyname('heb_totalresultat').Ascurrency;
  end;

  // preparer le filtre
  t := 'id_hebdo = ' + inttostr(idh);

  result := idh;

  nbligEMP := 0;
  nbligINT := 0;
  nbligDIA := 0;
  nbligACH := 0;
  nbligSTR := 0;
  nbligEXT := 0;
  nbligMAT := 0;
  nbligVEH := 0;

  with Formbase.TabFichPoint do
  begin
    filter := t;
    filtered := true;
    open;
    setkey;

    nbr := recordcount;
    if nbr <> 0 then
    begin
      first;
      for i := 1 to nbr do
      begin
        LireLignePointage;
        next;
      end;
    end;

    close;
    filtered := false;
  end;

  with Formbase.TabFichDiam do
  begin
    filter := t;
    filtered := true;
    open;
    setkey;

    nbr := recordcount;
    if nbr <> 0 then
    begin
      first;
      for i := 1 to nbr do
      begin
        lirelignediamant;
        next;
      end;
    end;

    close;
    filtered := false;
  end;

  with Formbase.TabFichVehic do
  begin
    filter := t;
    filtered := true;
    open;
    setkey;

    nbr := recordcount;
    if nbr <> 0 then
    begin
      first;
      for i := 1 to nbr do
      begin
        lirelignevehicule;
        next;
      end;
    end;

    close;
    filtered := false;
  end;

  with Formbase.TabFichMat do
  begin
    filter := t;
    filtered := true;
    open;
    setkey;

    nbr := recordcount;
    if nbr <> 0 then
    begin
      first;
      for i := 1 to nbr do
      begin
        LireLignemateriel;
        next;
      end;
    end;

    close;
    filtered := false;
  end;

  with Formbase.TabFichExter do
  begin
    filter := t;
    filtered := true;
    open;
    setkey;

    nbr := recordcount;
    if nbr <> 0 then
    begin
      first;
      for i := 1 to nbr do
      begin
        lireligneexterne;
        next;
      end;
    end;

    close;
    filtered := false;
  end;

  with Formbase.TabFichStruc do
  begin
    filter := t;
    filtered := true;
    open;
    setkey;

    nbr := recordcount;
    if nbr <> 0 then
    begin
      first;
      for i := 1 to nbr do
      begin
        lirelignestructure;
        next;
      end;
    end;

    close;
    filtered := false;
  end;

  with Formbase.TabFichAcha do
  begin
    filter := t;
    filtered := true;
    open;
    setkey;

    nbr := recordcount;
    if nbr <> 0 then
    begin
      first;
      for i := 1 to nbr do
      begin
        lireligneachat;
        next;
      end;
    end;

    close;
    filtered := false;
  end;
end;

{ ----------------------------------------------------------------------------- }

procedure sauverficheluedansnouvelletable;
var
  idh: integer;
  l: integer;
begin
  with Formbase.TableHebdo do
  begin
    open;
    insert;
    fieldbyname('heb_numerosemaine').asinteger := semaine;
    fieldbyname('heb_semainebis').asboolean := bis;
    fieldbyname('heb_numeromois').asinteger := mois;
    fieldbyname('heb_annee').asinteger := annee;
    fieldbyname('id_chantier').asinteger := idcha;
    fieldbyname('heb_verrou').asboolean := verrou;
    fieldbyname('heb_impression').asboolean := impression;
    fieldbyname('heb_annul').asboolean := annul;
    fieldbyname('heb_regul').asboolean := regul;
    fieldbyname('heb_nbjourstravail').asinteger := nbjours;
    fieldbyname('heb_totalinduits').Ascurrency := totaux[0];
    fieldbyname('heb_chiffreaffairedefini').Ascurrency := totaux[1];
    fieldbyname('heb_totalsalarie').Ascurrency := totaux[2];
    fieldbyname('heb_totalinterim').Ascurrency := totaux[3];
    fieldbyname('heb_totalvehicule').Ascurrency := totaux[4];
    fieldbyname('heb_totaldiamant').Ascurrency := totaux[5];
    fieldbyname('heb_totalmateriel').Ascurrency := totaux[6];
    fieldbyname('heb_totaloutils').Ascurrency := totaux[7];
    fieldbyname('heb_totalexterne').Ascurrency := totaux[8];
    fieldbyname('heb_totalinterne').Ascurrency := totaux[9];
    fieldbyname('heb_totaldeboursesec').Ascurrency := totaux[10];
    fieldbyname('heb_totalstructures').Ascurrency := totaux[11];
    fieldbyname('heb_totalprixrevient').Ascurrency := totaux[12];
    fieldbyname('heb_totalresultat').Ascurrency := totaux[13];
    post;
    idh := fieldbyname('id_hebdo').asinteger;
    close;
  end;

  with Formbase.TabFichPoint do
  begin
    if nbligINT <> 0 then
    begin
      open;
      for l := 0 to (nbligINT - 1) do
      begin
        insert;
        fieldbyname('id_hebdo').asinteger := idh;
        fieldbyname('emp_enr_nomemploye').asstring := tnomINT[l];
        fieldbyname('emp_enr_tauxhoraire').Ascurrency := tINT[0, l];
        fieldbyname('emp_nbjourtravail').Ascurrency := tINT[1, l];
        fieldbyname('emp_heuresroute').Ascurrency := tINT[2, l];
        fieldbyname('emp_heuresjour').Ascurrency := tINT[3, l];
        fieldbyname('emp_heuressamedi').Ascurrency := tINT[4, l];
        fieldbyname('emp_heuresnuitdimanche').Ascurrency := tINT[5, l];
        fieldbyname('emp_petitdeplacement').Ascurrency := tINT[6, l];
        fieldbyname('emp_granddeplacement').Ascurrency := tINT[7, l];
        fieldbyname('emp_total').Ascurrency := tINT[8, l];
        post;
      end;
      close;
    end;

    if nbligEMP <> 0 then
    begin
      open;
      for l := 0 to (nbligEMP - 1) do
      begin
        insert;
        fieldbyname('id_hebdo').asinteger := idh;
        fieldbyname('emp_enr_nomemploye').asstring := tnomEMP[l];
        fieldbyname('emp_enr_tauxhoraire').Ascurrency := tEMP[0, l];
        fieldbyname('emp_nbjourtravail').Ascurrency := tEMP[1, l];
        fieldbyname('emp_heuresroute').Ascurrency := tEMP[2, l];
        fieldbyname('emp_heuresjour').Ascurrency := tEMP[3, l];
        fieldbyname('emp_heuressamedi').Ascurrency := tEMP[4, l];
        fieldbyname('emp_heuresnuitdimanche').Ascurrency := tEMP[5, l];
        fieldbyname('emp_petitdeplacement').Ascurrency := tEMP[6, l];
        fieldbyname('emp_granddeplacement').Ascurrency := tEMP[7, l];
        fieldbyname('emp_total').Ascurrency := tEMP[8, l];
        post;
      end;
      close;
    end;
  end;

  with Formbase.TabFichDiam do
  begin
    if nbligDIA <> 0 then
    begin
      open;
      for l := 0 to (nbligDIA - 1) do
      begin
        insert;
        fieldbyname('id_hebdo').asinteger := idh;
        fieldbyname('tra_enr_libellenom').asstring := TnomDIA[l];
        fieldbyname('tra_enr_cout').Ascurrency := TDIA[0, l];
        fieldbyname('tra_nombre').Ascurrency := TDIA[1, l];
        fieldbyname('tra_coeffficient').Ascurrency := TDIA[2, l];
        fieldbyname('tra_total').Ascurrency := TDIA[3, l];
        post;
      end;
      close;
    end;
  end;

  with Formbase.TabFichVehic do
  begin
    if nbligVEH <> 0 then
    begin
      open;
      for l := 0 to (nbligVEH - 1) do
      begin
        insert;
        fieldbyname('id_hebdo').asinteger := idh;
        fieldbyname('veh_enr_libellenom').asstring := TnomVEH[l];
        fieldbyname('veh_enr_cout').Ascurrency := TVEH[0, l];
        fieldbyname('veh_nombre').Ascurrency := TVEH[1, l];
        fieldbyname('veh_total').Ascurrency := TVEH[2, l];
        post;
      end;
      close;
    end;
  end;

  with Formbase.TabFichExter do
  begin
    if nbligEXT <> 0 then
    begin
      open;
      for l := 0 to (nbligEXT - 1) do
      begin
        insert;
        fieldbyname('id_hebdo').asinteger := idh;
        fieldbyname('ext_enr_libellenom').asstring := TnomEXT[l];
        fieldbyname('ext_cout').Ascurrency := TEXT[0, l];
        post;
      end;
      close;
    end;
  end;

  with Formbase.TabFichMat do
  begin
    if nbligMAT <> 0 then
    begin
      open;
      for l := 0 to (nbligMAT - 1) do
      begin
        insert;
        fieldbyname('id_hebdo').asinteger := idh;
        fieldbyname('mat_enr_libellenom').asstring := TnomMAT[l];
        fieldbyname('mat_enr_cout').Ascurrency := TMAT[0, l];
        fieldbyname('mat_dureeutil').Ascurrency := TMAT[1, l];
        fieldbyname('mat_total').Ascurrency := TMAT[2, l];
        post;
      end;
      close;
    end;
  end;

  with Formbase.TabFichStruc do
  begin
    if nbligSTR <> 0 then
    begin
      open;
      for l := 0 to (nbligSTR - 1) do
      begin
        insert;
        fieldbyname('id_hebdo').asinteger := idh;
        fieldbyname('str_enr_libellenom').asstring := TnomSTR[l];
        fieldbyname('str_enr_cout').Ascurrency := TSTR[0, l];
        fieldbyname('str_nombre').Ascurrency := TSTR[1, l];
        fieldbyname('str_total').Ascurrency := TSTR[2, l];
        post;
      end;
    end;

    close;
  end;

  with Formbase.TabFichAcha do
  begin
    if nbligACH <> 0 then
    begin
      open;
      for l := 0 to (nbligACH - 1) do
      begin
        insert;
        fieldbyname('id_hebdo').asinteger := idh;
        fieldbyname('int_enr_libellenom').asstring := TnomACH[l];
        fieldbyname('int_cout').Ascurrency := TACH[0, l];
        post;
      end;
      close;
    end;
  end;
end;

{ ----------------------------------------------------------------------------- }

function NbrEnregMasques: integer;

  function compteenregtable(table: ttable): integer;
  begin
    with table do
    begin
      filter := '';
      filtered := false;
      open;
      setkey;
      result := recordcount;
      close;
    end;
  end;

var
  nb: integer;
begin
  nb := 0;
  nb := nb + compteenregtable(Formbase.TabFichPoint);
  nb := nb + compteenregtable(Formbase.TabFichMat);
  nb := nb + compteenregtable(Formbase.TabFichExter);
  nb := nb + compteenregtable(Formbase.TabFichDiam);
  nb := nb + compteenregtable(Formbase.TabFichAcha);
  nb := nb + compteenregtable(Formbase.TabFichStruc);
  nb := nb + compteenregtable(Formbase.TabFichVehic);
  result := nb;
end;

{ ----------------------------------------------------------------------------- }

procedure SuppressionEnregMasque(idh: integer);

  procedure resetenregtable(table: ttable; idh: integer);
  var
    t: string;
  begin
    t := 'id_hebdo = ' + inttostr(idh);

    with table do
    begin
      // preparer le filtre recherche des enreg contenant 'index'
      filter := t;
      filtered := true;

      open;
      setkey;

      // supprimer tous les enregs
      while recordcount <> 0 do
        delete;

      close;
      filtered := false;
    end;
  end;

begin
  resetenregtable(Formbase.TabFichPoint, idh);
  resetenregtable(Formbase.TabFichMat, idh);
  resetenregtable(Formbase.TabFichExter, idh);
  resetenregtable(Formbase.TabFichDiam, idh);
  resetenregtable(Formbase.TabFichAcha, idh);
  resetenregtable(Formbase.TabFichStruc, idh);
  resetenregtable(Formbase.TabFichVehic, idh);
end;

{ ----------------------------------------------------------------------------- }

procedure RecreerBDD;
var
  idh: integer;
  nbf: integer;
begin

  nbf := LireNombreFiches(0, 0);
  Initlenbarre(nbf);
  nombresfichesAtraiter(nbf);
  if nbf = 0 then
    exit;

  with Formbase.TableTemp do
  begin
    TableName := 'fiche_hebdo';
    RenameTable('tempo');
  end;

  recreationtablehebdo;

  with Formbase.TableTemp do
  begin
    // preparer lecture table d'origine
    filter := '';
    filtered := false;
    indexname := '';
    open;
    setkey;
    first;

    // traiter toutes les fiches table d'origine
    while recordcount <> 0 do
    begin
      IncBarre;

      // lire fiche en cours dans tempo
      idh := lireficheencoursdanstempo;

      // suppression dans les tables annexes
      SuppressionEnregMasque(idh);

      // sauver fiche lue dans nouvelle table
      sauverficheluedansnouvelletable;

      // traitement barre de progression
      incfichetraitees;

      // effacer fiche lue
      delete;

      // fiche suivante s'il y a lieu
      next;
    end;

    // suppression table tempo
    close;
    DeleteTable;
  end;
end;

{ ----------------------------------------------------------------------------- }

{ ----------------------------------------------------------------------------- }
end.
