unit MasqueStructures;

interface

procedure ResetCumulsStructure;
procedure CumulsStructure(idh: integer);
procedure CumulsStructureLire(var nbr: integer);
procedure StructureViderListe;
procedure StructureInitMasque;
procedure StructureExecuterCalcul;
function StructureLireCalcul: currency;
function StructureLireSomme: currency;
procedure StructureEnreg(numhebdo: integer);
procedure StructureCharge(index: integer);
procedure StructureChargeChaines(index: integer; var nbrec: integer);
procedure StructureSupprime(index: integer);
procedure StructureModifsNon;
procedure StructureModifsOui;
function StructureLireModifsEtat: boolean;

function traiteStructureLireFormatCellules(ACol: integer): string;
procedure traiteStructuresouris;
procedure traiteStructureInitialiserProcEffaceLigne(X, Y: integer);
function TraiteStructureSelection(ACol, ARow: integer): boolean;
procedure TraiteStructureValide(var Key: Char);
procedure traiteStructuretouches(Key: word);
procedure traiteStructuretouchesFIN();
procedure TraiteStructureChargeLigne;
procedure TraiteStructureEffacerLigne;

implementation

uses

  SysUtils, grids, Dialogs, windows,
  winbase, accesBDD, Hebdo, impression;

const

  colindex = 0;
  colNom = 0;
  colcout = 1;
  colnombre = 2;
  colsommeligne = 3;

  coldeselection = 10;
  colautoselection = colnombre;

type

  TlargeurLigne = array [colNom .. colsommeligne] of integer;

const

  Largeur: TlargeurLigne = (160, 40, 40, 60);

var

  TblNomSTR: array of array of string;
  TblValSTR: array of array of currency;
  nbSTR: integer;

  positioncolonne: integer = 1;
  positionligne: integer;
  LigneEnCours: boolean = true;
  TableauStructures: array of array of currency;
  CelluleEnCours: boolean;
  EditionEnCours: boolean;
  CelluleEnCoursCol: integer;
  CelluleEnCoursRow: integer;
  Suppressionencours: boolean;
  ModeLigneAppend: boolean;

  ModifsEtat: boolean;
  AutoriseModifs: boolean;

  SommeSTR: currency;
  SommeNombre: currency;

  /// ///////////////////////////////////////////////////////////////////////////
procedure unselectSTR;
begin
  Suppressionencours := true;
  FormBase.ListeUtilisationSTR.Col := coldeselection;
  FormBase.ListeUtilisationSTR.Row := 0;
  Suppressionencours := false;
end;

procedure StructureInitMasque;
var
  i: integer;
begin
  for i := colindex to colsommeligne do
    FormBase.ListeUtilisationSTR.ColWidths[i] := Largeur[i];

  for i := colsommeligne + 1 to coldeselection do
    FormBase.ListeUtilisationSTR.ColWidths[i] := 0;

  unselectSTR;
end;

procedure StructureModifsOui;
begin
  AutoriseModifs := true;
  FormBase.ListeStructures.Enabled := true;
end;

procedure StructureModifsNon;
begin
  unselectSTR;
  AutoriseModifs := false;
  FormBase.ListeStructures.Enabled := false;
  FormBase.ListeStructures.KeyValue := -1;
end;

function StructureLireModifsEtat: boolean;
begin
  result := ModifsEtat;
end;

procedure SetModifsEtat;
begin
  ModifsEtat := true;

  // prevenir qu'il y a eu une modif d'un masque
  EvenementModifMasqueAnnexe;
end;

procedure ResetModifsEtat;
begin
  ModifsEtat := false;
end;

procedure StructureViderListe;

  procedure viderligne(fin: integer);
  var
    C: integer;
  begin
    for C := colindex to colsommeligne do
      FormBase.ListeUtilisationSTR.Cells[C, fin - 1] := '';
  end;

begin
  // pour ne pas repasser par le traitement evenement masque
  LigneEnCours := true;

  while FormBase.ListeUtilisationSTR.RowCount > 1 do
  begin
    positioncolonne := 0;
    positionligne := FormBase.ListeUtilisationSTR.RowCount - 2;
    TraiteStructureEffacerLigne;
  end;

  positioncolonne := 1;
  LigneEnCours := false;
end;

function VerifMaxiOK(ACol: integer; valeur: currency): boolean;
begin
  result := false;
  case ACol of
    colnombre:
      if (valeur < -999.9) or (valeur > 999.9) then
        exit;
  end;
  result := true;
end;

function traiteStructureLireFormatCellules(ACol: integer): string;
begin
  case ACol of
    colNom:
      result := '';
    colcout:
      result := '999,99';
    colnombre:
      result := '';
    colsommeligne:
      result := '#####,##';
  end;
end;

function FormatSortie(Col: integer): string;
begin
  case Col of
    colcout:
      result := '000.00';
    colnombre:
      result := '000.00';
    colsommeligne:
      result := '00000.00';
  else
    result := '';
  end;
end;

Procedure RechargerCellule(Col, lig: integer);
var
  valeur: currency;
  temp: string;
begin
  valeur := TableauStructures[Col, lig];
  temp := FormatCurr(FormatSortie(Col), valeur);
  FormBase.ListeUtilisationSTR.Cells[Col, lig] := temp;
end;

function StructureLireCalcul: currency;
begin
  result := SommeSTR
end;

function StructureLireSomme: currency;
begin
  result := SommeNombre
end;

procedure CalculStructure(lig: integer);
var
  C, N, Somme: currency;
  temp: string;
begin
  C := TableauStructures[colcout, lig];
  N := TableauStructures[colnombre, lig];

  Somme := (C * N);

  // somme du Structure
  TableauStructures[colsommeligne, lig] := Somme;
  temp := FormatCurr(FormatSortie(colsommeligne), Somme);
  FormBase.ListeUtilisationSTR.Cells[colsommeligne, lig] := temp;
end;

procedure StructureExecuterCalcul;
var
  nbl: integer;
  t, N: currency;
  i: integer;
begin
  t := 0;
  N := 0;
  nbl := FormBase.ListeUtilisationSTR.RowCount - 1;
  if nbl >= 0 then
  begin
    for i := 0 to (nbl - 1) do
    begin
      t := t + TableauStructures[colsommeligne, i];
      N := N + TableauStructures[colnombre, i];
    end;
  end;

  SommeSTR := t;
  SommeNombre := N;

  // lance le calcul generalde la fiche Hebdo;
  CalculGeneralHebdo;
end;

function CurrToInt(valeur: currency): integer;
begin
  result := StrToInt(CurrToStr(valeur));
end;

function IntToCurr(valeur: integer): currency;
begin
  result := StrToCurr(IntToStr(valeur));
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure StructureEnreg(numhebdo: integer);
var
  nbe: integer;
  C: currency;
  l: integer;
  s: string;

  procedure ChercheEnreg(ligne: integer);
  var
    C: currency;
    id: integer;
  begin
    C := TableauStructures[colindex, ligne];

    id := CurrToInt(C);
    FormBase.TabFichStruc.editkey;
    FormBase.TabFichStruc.FieldByName('id_Structure').asinteger := id;
    FormBase.TabFichStruc.gotokey;
    FormBase.TabFichStruc.edit;
  end;

begin
  // supprimer les enregs precedents
  StructureSupprime(numhebdo);
  ModeLigneAppend := false;

  nbe := FormBase.ListeUtilisationSTR.RowCount - 1;
  if (nbe = 0) then
    exit;

  // pas de mofifs en cours
  ResetModifsEtat;

  with FormBase.TabFichStruc do
  begin
    Open;

    if nbe <> 0 then
    begin
      // creer chaque ligne et la remplir
      for l := 0 to (nbe - 1) do
      begin
        if ModeLigneAppend = false then
          // creer nouveau enregistrement de ligne
          insert
        else
          // chercher enreg existant
          ChercheEnreg(l);

        s := FormBase.ListeUtilisationSTR.Cells[colNom, l];
        FieldByName('str_enr_libellenom').AsString := s;

        C := TableauStructures[colcout, l];
        FieldByName('str_enr_cout').Ascurrency := C;

        C := TableauStructures[colnombre, l];
        FieldByName('str_nombre').Ascurrency := C;

        C := TableauStructures[colsommeligne, l];
        FieldByName('str_total').Ascurrency := C;

        FieldByName('id_hebdo').asinteger := numhebdo;

        // valider nouvelle ligne avec ces valeurs
        post;
      end;
    end;

    // fermeture de la table apres ajouts
    close;
  end;

  // sortie du mode de modification
  ModeLigneAppend := false;
end;

procedure ChargeLigneExiste;
var
  nc: integer;
  nblig: integer;

  idemp: integer;
  monaie: currency;
  valeur: currency;
  nomemp: string;

  procedure Ecrire(Col, lig: integer; valeur: currency);
  var
    t: string;
  begin
    t := FormatCurr(FormatSortie(Col), valeur);
    TableauStructures[Col, lig] := valeur;
    FormBase.ListeUtilisationSTR.Cells[Col, lig] := t;
  end;

begin
  with FormBase.TabFichStruc do
  begin
    // definition de la ligne de Structure
    nblig := FormBase.ListeUtilisationSTR.RowCount - 1;

    SetLength(TableauStructures, 10, nblig + 1);

    idemp := FieldByName('id_Structure').asinteger;
    TableauStructures[colindex, nblig] := IntToCurr(idemp);

    nomemp := FieldByName('str_enr_libellenom').AsString;
    FormBase.ListeUtilisationSTR.Cells[colNom, nblig] := nomemp;

    // debut chargement liste ecran
    LigneEnCours := true;

    monaie := FieldByName('str_enr_cout').Ascurrency;
    Ecrire(colcout, nblig, monaie);

    valeur := FieldByName('str_nombre').Ascurrency;
    Ecrire(colnombre, nblig, valeur);

    // calculer la ligne
    CalculStructure(nblig);

    // fin chargement liste ecran
    LigneEnCours := false;

    // defintion de la prochaine ligne
    nc := FormBase.ListeUtilisationSTR.RowCount;
    inc(nc);
    FormBase.ListeUtilisationSTR.RowCount := nc;

    // selection de la 1ere colonne a editer
    FormBase.ListeUtilisationSTR.SetFocus;
    FormBase.ListeUtilisationSTR.Col := colnombre;
    FormBase.ListeUtilisationSTR.Row := nblig;
  end;
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure StructureCharge(index: integer);
var
  nbrec: integer;
  t: string;
  nblig: integer;
begin
  ChargeConfiguration;

  // pas de mofifs en cours
  ResetModifsEtat;

  // definition des lignes de Structure
  FormBase.ListeUtilisationSTR.RowCount := 1;

  with FormBase.TabFichStruc do
  begin
    // recherche des enreg contenant 'index'

    // preparer le filtre
    str(index, t);
    filter := 'id_hebdo = ' + t;
    filtered := true;

    Open;
    setkey;

    nbrec := recordcount;
    if nbrec <> 0 then
    begin
      // entree dans le mode de modification
      ModeLigneAppend := true;
      edit;

      for nblig := 0 to (nbrec - 1) do
      begin
        // se positionner sur 1er enreg ou sur suivant
        if nblig = 0 then
          first
        else
          next;

        // charger liste de Structure
        ChargeLigneExiste;
      end;
    end;

    // termine la lecture de la table
    close;
    filtered := false;

    // recalcul tout
    StructureExecuterCalcul;
  end;
end;

procedure GetEnregStructureCourant(var i: integer; var N: string;
  var C: currency);
begin
  with FormBase.TableStructures do
  begin
    edit;
    i := FieldValues['id_fraisstructure'];
    N := FieldByName('str_libellenom').AsString;
    C := FieldByName('str_cout').Ascurrency;
    post;
  end;
end;

procedure TraiteStructureChargeLigne;
var
  nc: integer;
  nblig: integer;

  idemp: integer;
  monaie: currency;
  nomemp: string;

  function ChercheExiste(nblig: integer; nomemp: string): boolean;
  var
    i: integer;
    t: string;
  begin
    result := false;
    for i := 0 to nblig - 1 do
    begin
      t := FormBase.ListeUtilisationSTR.Cells[colNom, i];
      if nomemp = trim(t) then
        result := true;
    end;
  end;

  procedure Ecrire(Col, lig: integer; valeur: currency);
  var
    t: string;
  begin
    t := FormatCurr(FormatSortie(Col), valeur);
    TableauStructures[Col, lig] := valeur;
    FormBase.ListeUtilisationSTR.Cells[Col, lig] := t;
  end;

begin
  // inclure un employ� dans la liste de Structure

  if FormBase.TableStructures.recordcount = 0 then
    exit;

  // modifs en cours
  SetModifsEtat;

  // lire l'enreg correspondant de l'employe modifier
  GetEnregStructureCourant(idemp, nomemp, monaie);

  ChargeConfiguration;

  // definition de la ligne de Structure
  nblig := FormBase.ListeUtilisationSTR.RowCount - 1;

  // verifier si nb maxi de lignes atteint
  if nblig = nbligstr then
    exit;

  if nblig <> 0 then
  begin
    // verifier si le nom existe dans liste alors ne pas charger
    if ChercheExiste(nblig, nomemp) then
      exit;
  end;

  SetLength(TableauStructures, 10, nblig + 1);

  TableauStructures[colindex, nblig] := IntToCurr(idemp);
  FormBase.ListeUtilisationSTR.Cells[colNom, nblig] := nomemp;

  LigneEnCours := true; // debut chargement liste ecran

  Ecrire(colcout, nblig, monaie);
  Ecrire(colnombre, nblig, 0);

  // calculer la ligne
  CalculStructure(nblig);

  LigneEnCours := false; // fin chargement liste ecran

  // defintion de la prochaine ligne
  nc := FormBase.ListeUtilisationSTR.RowCount;
  inc(nc);
  FormBase.ListeUtilisationSTR.RowCount := nc;

  // selection de la 1ere colonne a editer
  FormBase.ListeUtilisationSTR.SetFocus;
  FormBase.ListeUtilisationSTR.Col := colnombre;
  FormBase.ListeUtilisationSTR.Row := nblig;

  // recalcul tout
  StructureExecuterCalcul;
end;

function TraiteStructureSelection(ACol, ARow: integer): boolean;
var
  OK: boolean;
  i: integer;
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    result := false;
    exit;
  end;

  result := true;
  OK := true;

  // utile pour la suppression de la derniere ligne
  if Suppressionencours and (ACol = 10) then
    exit;

  // verifier si depassement nbr de lignes
  i := FormBase.ListeUtilisationSTR.RowCount - 1;
  if ARow >= i then
    OK := false;

  if (ACol = colnombre) and OK then
  begin
    // autoriser l'edition de la cellule
    with FormBase.ListeUtilisationSTR do
      options := options + [goEditing];

    RechargerCellule(ACol, ARow);
  end
  else
  begin
    // interdire l'edition et la selection de la cellule
    with FormBase.ListeUtilisationSTR do
      options := options - [goEditing];
    result := false;
    exit;
  end;
end;

procedure TraiteStructureValide(var Key: Char);
var
  ACol: integer;
  ARow: integer;
  temp: string;
  valeur: currency;
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    Key := chr(0);
    exit;
  end;

  if (Key = ' ') then
  begin
    // ne traite pas ces touches
    Key := chr(0);
    exit;
  end;

  if (Key = '.') then
  begin;
    // modifs en cours
    SetModifsEtat;

    // pour faciliter la saisie des nombres decimaux
    Key := ',';

    exit;
  end;

  // valide la saisie
  if Key = chr(13) then
  begin
    // modifs en cours
    SetModifsEtat;

    // Stocker la saisie entree au clavier
    ACol := FormBase.ListeUtilisationSTR.Col;
    ARow := FormBase.ListeUtilisationSTR.Row;

    try
      begin
        // Stocker la saisie entree au clavier
        temp := FormBase.ListeUtilisationSTR.Cells[ACol, ARow];
        valeur := StrToCurr(trim(temp));

        // test maxi
        if VerifMaxiOK(ACol, valeur) then
        begin
          TableauStructures[ACol, ARow] := valeur;
          temp := FormatCurr(FormatSortie(ACol), valeur);
          FormBase.ListeUtilisationSTR.Cells[ACol, ARow] := temp;
        end
        else
          // valeur hors de la bone maximum donc recharge
          RechargerCellule(ACol, ARow);

        // calculer la ligne
        CalculStructure(ARow);
      end;

      // recalcul tout
      StructureExecuterCalcul;

    except
      // si erreur de saisie dans la cellule
      on EConvertError do
        RechargerCellule(ACol, ARow);
    end;
  end;

  // si touche numerique
  CelluleEnCours := false;
  EditionEnCours := false;
  if ((ord(Key) > 47) and (ord(Key) < 58)) or (Key = '-') or (Key = '+') then
  begin
    // modifs en cours
    SetModifsEtat;

    CelluleEnCours := true;
    EditionEnCours := true;

    // Stocker la saisie entree au clavier
    CelluleEnCoursCol := FormBase.ListeUtilisationSTR.Col;
    CelluleEnCoursRow := FormBase.ListeUtilisationSTR.Row;
  end;
end;

procedure traiteStructureInitialiserProcEffaceLigne(X, Y: integer);
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  with FormBase.ListeUtilisationSTR do
    MouseToCell(X, Y, positioncolonne, positionligne)
end;

procedure TraiteStructureEffacerLigne;
var
  nbl: integer;
  i: integer;

  procedure transfert(deb, fin: integer);
  var
    l: integer;
    C: integer;
    t: currency;
  begin
    for l := deb to fin - 2 do
    begin
      for C := colindex to colsommeligne do
      begin
        with FormBase.ListeUtilisationSTR do
          Cells[C, l] := Cells[C, l + 1];

        t := TableauStructures[C, l + 1];
        TableauStructures[C, l] := t;
      end;
    end;
  end;

  procedure viderligne(fin: integer);
  var
    C: integer;
  begin
    for C := colindex to colsommeligne do
      FormBase.ListeUtilisationSTR.Cells[C, fin - 1] := '';
  end;

begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  // suppression ligne de Structure selectionn�e
  if (positioncolonne = colNom) then
  begin
    // modifs en cours
    SetModifsEtat;

    // diminuer d'une ligne verifier si depassement nbr de lignes
    nbl := FormBase.ListeUtilisationSTR.RowCount - 1;
    if positionligne >= nbl then
      exit;

    // remonter d'une unite toutes les lignes
    if positionligne < nbl then
      transfert(positionligne, nbl);

    if nbl = 1 then
      unselectSTR
    else
    begin
      FormBase.ListeUtilisationSTR.Col := colautoselection;
      FormBase.ListeUtilisationSTR.Row := 0;
    end;

    // effacer derniere ligne
    viderligne(nbl);

    i := FormBase.ListeUtilisationSTR.RowCount;
    dec(i);
    FormBase.ListeUtilisationSTR.RowCount := i;

    // pour eviter des suppression intenpestives
    positioncolonne := 1;
  end;

  // recalcul tout
  StructureExecuterCalcul;
end;

procedure traiteStructuretouchesFIN();
begin
  CelluleEnCours := false;
end;

procedure traiteStructuretouches(Key: word);
var
  ACol: integer;
  ARow: integer;

  // recupere position cellule en cours
  procedure Cellule(var ACol: integer; var ARow: integer);
  begin
    ACol := FormBase.ListeUtilisationSTR.Col;
    ARow := FormBase.ListeUtilisationSTR.Row;
  end;

  function lignes: boolean;
  begin
    result := false;
    if FormBase.ListeUtilisationSTR.RowCount > 2 then
      result := true;
  end;

begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  // sortie si 0 lignes
  if not lignes then
    exit;

  if CelluleEnCours = true then
    exit;

  if TestTouchesFleches(Key) or (VK_TAB = Key) then
  begin
    if LigneEnCours = false then
    begin
      // retour en arriere d'une cellule suivant touche
      Cellule(ACol, ARow);
      case Key of
        VK_TAB:
          dec(ACol);
        VK_LEFT:
          inc(ACol);
        VK_RIGHT:
          dec(ACol);
        VK_DOWN:
          if lignes then
            dec(ARow);
        VK_UP:
          if lignes then
            inc(ARow);
      end;

      // reaffichage suite a entree non validee par CRLF
      if CelluleEnCours = false then
        if colsommeligne <> ACol then
        begin
          // modifs en cours
          SetModifsEtat;

          RechargerCellule(ACol, ARow);
        end;
    end;
  end;
end;

procedure traiteStructuresouris;
var
  ACol: integer;
  ARow: integer;
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  if EditionEnCours = false then
    exit;

  ACol := FormBase.ListeUtilisationSTR.Col;
  ARow := FormBase.ListeUtilisationSTR.Row;

  if not ForcerRestitution then
    if (CelluleEnCoursCol = ACol) and (CelluleEnCoursRow = ARow) then
      exit;

  EditionEnCours := false;

  // modifs en cours
  SetModifsEtat;

  RechargerCellule(CelluleEnCoursCol, CelluleEnCoursRow);
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure StructureChargeChaines(index: integer; var nbrec: integer);

  function LireChaine(param: string): string;
  begin
    result := FormBase.TabFichStruc.FieldByName(param).AsString;
  end;

  function LireValeur(param: string): currency;
  begin
    result := FormBase.TabFichStruc.FieldByName(param).Ascurrency;
  end;

  procedure Charge(nblig: integer);
  var
    temp: string;
    valeur: currency;
  begin
    TableauTemp[0, nblig] := '';

    TableauTemp[1, nblig] := LireChaine('str_enr_libellenom');

    temp := FormatSortie(colcout);
    valeur := LireValeur('str_enr_cout');
    temp := FormatCurr('0.00', valeur);
    TableauTemp[2, nblig] := temp;
    TableauvalTemp[2, nblig] := valeur;

    temp := FormatSortie(colnombre);
    valeur := LireValeur('str_nombre');
    temp := FormatCurr('0.00', valeur);
    TableauTemp[3, nblig] := temp;
    TableauvalTemp[3, nblig] := valeur;

    temp := FormatSortie(colsommeligne);
    valeur := LireValeur('str_total');
    temp := FormatCurr('0.00', valeur);
    TableauTemp[4, nblig] := temp;
    TableauvalTemp[4, nblig] := valeur;
  end;

var
  t: string;
  nblig: integer;

begin
  with FormBase.TabFichStruc do
  begin
    // preparer le filtre recherche des enreg contenant 'index'
    str(index, t);
    filter := 'id_hebdo = ' + t;
    filtered := true;

    Open;
    setkey;

    nbrec := recordcount;
    if nbrec <> 0 then
    begin
      // entree dans le mode de modification
      SetLength(TableauTemp, 11, nbrec + 1);
      SetLength(TableauvalTemp, 11, nbrec + 1);
      edit;

      for nblig := 0 to (nbrec - 1) do
      begin
        // se positionner sur 1er enreg ou sur suivant
        if nblig = 0 then
          first
        else
          next;

        // charger liste de Structure
        Charge(nblig);
      end;
    end;

    // termine la lecture de la table
    close;
    filtered := false;
  end;
end;

procedure StructureSupprime(index: integer);
var
  t: string;
begin
  with FormBase.TabFichStruc do
  begin
    // preparer le filtre recherche des enreg contenant 'index'
    str(index, t);
    filter := 'id_hebdo = ' + t;
    filtered := true;

    Open;
    setkey;

    // supprimer tous les enregs
    while recordcount <> 0 do
      delete;

    // termine la suppression dans la table
    close;
    filtered := false;
  end;
end;

procedure ResetCumulsStructure;
begin
  nbSTR := 0;

  SetLength(TblNomSTR, 2, nbSTR);
  SetLength(TblValSTR, 3, nbSTR);
end;

procedure CumulsStructure(idh: integer);

  function ChercheAjoute(nom: string; var ind: integer): boolean;
  var
    i: integer;
    t: string;
  begin
    result := true;

    if nbSTR = 0 then
      exit;
    for i := 0 to (nbSTR - 1) do
    begin
      t := TblNomSTR[1, i];
      if t = nom then
      begin
        ind := i;
        result := false;
        exit;
      end;
    end;
  end;

var
  ind: integer;
  nbrec: integer;
  i: integer;
  N: integer;
  C: currency;
begin
  // charger les pointages
  StructureChargeChaines(idh, nbrec);

  // si aucuns pointage sortir
  if nbrec = 0 then
    exit;

  // traiter tous les pointages
  for i := 0 to (nbrec - 1) do
  begin
    // verifier si le pointage existe deja
    if ChercheAjoute(TableauTemp[1, i], ind) then
    begin
      // ajoute une ligne au tableau
      inc(nbSTR);
      SetLength(TblNomSTR, 2, nbSTR);
      SetLength(TblValSTR, 3, nbSTR);

      // stocker nom et type ligne de pointage
      ind := nbSTR - 1;
      TblNomSTR[0, ind] := '';
      TblNomSTR[1, ind] := TableauTemp[1, i];

      // raz valeurs nouvelle ligne
      for N := 1 to 2 do
        TblValSTR[N, ind] := 0;

      // stocker le cout
      TblValSTR[0, ind] := TableauvalTemp[2, i];
    end;

    // cumuler a la ligne en cours la nouvelle ligne
    for N := 1 to 2 do
    begin
      C := TableauvalTemp[N + 2, i];
      C := TblValSTR[N, ind] + C;
      TblValSTR[N, ind] := C;
    end;
  end;
end;

procedure CumulsStructureLire(var nbr: integer);
var
  i: integer;
  t: string;
begin
  nbr := nbSTR;
  if nbSTR = 0 then
    exit;

  // pour stocker tous les pointages
  SetLength(TableauTemp, 11, nbSTR);

  for i := 0 to (nbSTR - 1) do
  begin
    TableauTemp[0, i] := TblNomSTR[0, i];
    TableauTemp[1, i] := TblNomSTR[1, i];

    t := FormatSortie(colcout);
    t := FormatCurr('0.00', TblValSTR[0, i]);
    TableauTemp[2, i] := t;

    t := FormatSortie(colnombre);
    t := FormatCurr('0.00', TblValSTR[1, i]);
    TableauTemp[3, i] := t;

    t := FormatSortie(colsommeligne);
    t := FormatCurr('0.00', TblValSTR[2, i]);
    TableauTemp[4, i] := t;
  end;
end;
/// ///////////////////////////////////////////////////////////////////////////

end.
