unit Masque;

interface

         procedure PointageViderListe;
         procedure PointageInitMasque;
         procedure PointageExecuterCalcul;
         function  PointageLireCalcul: currency;
         function  PointageLireSomme: currency;
         procedure PointageEnreg(numhebdo: integer);
         procedure PointageCharge(index: integer);
         procedure PointageChargeChaines(index: integer; var nbrec: integer);
         procedure PointageSupprime(index: integer);
         procedure PointageModifsNon;
         procedure PointageModifsOui;
         function  PointageLireModifsEtat: boolean;

         function  traitePointageLireFormatCellules(ACol: integer): string;
         procedure traitepointagesouris;
         procedure traitePointageInitialiserProcEffaceLigne(X, Y: Integer);
         function  TraitePointageSelection(ACol, ARow: Integer): Boolean;
         procedure TraitePointageValide(var Key: Char);
         procedure traitepointagetouches(key: word);
         procedure traitepointagetouchesFIN();
         procedure TraitePointageChargeLigne;
         procedure TraitePointageEffacerLigne;

implementation

   uses

         SysUtils, grids, Dialogs, windows,
         winbase, accesBDD, Hebdo;

   const

         colindex                  = 0;
         colNom                    = 0;
         coltauxhoraire            = 1;
         colnombrejours            = 2;
         colheuresroute            = 3;
         colheuresjour             = 4;
         colheuressamedi           = 5;
         colheuresnuitcoldimanche  = 6;
         colpetitpagnier           = 7;
         colgrandpagnier           = 8;
         colsommepointage          = 9;

   type

         TlargeurPointage = array[colNom..colsommepointage] of Integer;

   const

         Largeur: TlargeurPointage
                  = (160, 50, 35, 35, 35, 35, 35, 35, 35, 50);

   var

         positioncolonne:       integer = 1;
         positionligne:         integer;
         LigneEnCours:          boolean  = true;
         TableEmployes:         array of array of currency;
         CelluleEnCours:        boolean;
         EditionEnCours:        boolean;
         CelluleEnCoursCol:     integer;
         CelluleEnCoursRow:     integer;
         Suppressionencours:    boolean;
         ModeLigneAppend:       boolean;

         ModifsEtat:            boolean;
         AutoriseModifs:        boolean;

         SommeEMP:              currency;
         SommeNombre:           currency;

//////////////////////////////////////////////////////////////////////////////
procedure unselectEMP;
begin
     Suppressionencours := true;
     FormBase.ListePointageEMP.Col := 10;
     FormBase.ListePointageEMP.Row := 0;
     Suppressionencours := false;
end;

procedure PointageModifsOui;
begin
     AutoriseModifs := true;
     FormBase.ListeEmployes.Enabled := true;
end;

procedure PointageModifsNon;
begin
     unselectEMP;
     AutoriseModifs := false;
     FormBase.ListeEmployes.Enabled := false;
end;

function PointageLireModifsEtat: boolean;
begin
     result := ModifsEtat;
end;

procedure SetModifsEtat;
begin
     ModifsEtat := true;

     // prevenir qu'il y a eu une modif d'un masque
     EvenementModifMasqueAnnexe;
end;

procedure ResetModifsEtat;
begin
     ModifsEtat := false;
end;

procedure PointageViderListe;

   procedure viderligne(fin: integer);
   var
      C:     integer;
   begin
        for C := colindex to colsommepointage do
            FormBase.ListePointageEMP.Cells[C, fin - 1] := '';
   end;

begin
     // pour ne pas repasser par le traitement evenement masque
     LigneEnCours := true;

     while FormBase.ListePointageEMP.RowCount > 1 do
           begin
                positioncolonne := 0;
                positionligne   := FormBase.ListePointageEMP.RowCount - 2;
                TraitePointageEffacerLigne;
           end;

     positioncolonne := 1;
     LigneEnCours := false;
end;

procedure PointageInitMasque;
var
   i:     integer;
begin
     for I := colindex to colsommepointage do
         FormBase.ListePointageEMP.ColWidths[i] := largeur[i];

     FormBase.ListePointageEMP.ColWidths[10] := 0;

     unselectEMP;
end;

function VerifMaxiOK(ACol: integer; valeur: currency): boolean;
begin
     result := false ;
     case ACol of
          2,3,4,5,6: if (valeur > 99.9) then exit;
          7,8:       if (valeur > 7) then exit;
     end;
     result := true;
end;

function traitePointageLireFormatCellules(ACol: integer): string;
begin
     case ACol of
          0:         result :=     '';
          1:         result :=    '99,99';
          2,3,4,5,6: result :=     '';
          7,8:       result :=     '9';
          9:         result := '#####,##';
     end;
end;

function FormatSortie(col: integer): string;
begin
     case col of
          coltauxhoraire:           result := '00.00';
          colnombrejours:           result :=  '0.00';
          colheuresroute:           result := '00.0';
          colheuresjour:            result := '00.0';
          colheuressamedi:          result := '00.0';
          colheuresnuitcoldimanche: result := '00.0';
          colpetitpagnier:          result := '0';
          colgrandpagnier:          result := '0';
          colsommepointage:         result := '00000.00';
          else
              result := '00.00';
     end;
end;

Procedure RechargerCellule(col, lig: integer);
var
   valeur: currency;
   temp:   string;
begin
     valeur := TableEmployes[col, lig];
     temp   := FormatCurr(FormatSortie(col), valeur);
     FormBase.ListePointageEMP.Cells[col, lig] := temp;
end;

function PointageLireCalcul: currency;
begin
     result := SommeEMP
end;

function PointageLireSomme: currency;
begin
     result := SommeNombre
end;

procedure CalculPointageEMP(lig: integer);
var
   G, D, H, I, J, L, Somme:  currency;
   temp:                     string;
begin
     G  := TableEmployes[colheuresjour, lig];
     D  := TableEmployes[coltauxhoraire, lig];
     H  := TableEmployes[colheuressamedi, lig];
     I  := TableEmployes[colheuresnuitcoldimanche, lig];
     J  := TableEmployes[colpetitpagnier, lig];
     L  := TableEmployes[colgrandpagnier, lig];

     Somme := (G * D);
     Somme := Somme + (H * D * TauxHoraireSup);
     Somme := Somme + (I * D);
     Somme := Somme + (J * CoutPetitPagnier);
     Somme := Somme * TauxChargeBrut;
     Somme := Somme + (L * CoutGrandPagnier);

     // somme du pointage
     TableEmployes[colsommepointage, lig] := Somme;
     temp := FormatCurr(FormatSortie(colsommepointage), Somme);
     FormBase.ListePointageEMP.Cells[colsommepointage, lig] := temp;
end;

procedure PointageExecuterCalcul;
var
   nbl:   integer;
   t:     currency;
   i:     integer;
begin
     t := 0;
     nbl := FormBase.ListePointageEMP.RowCount - 1;
     if nbl >= 0 then
        begin
             for i := 0 to (nbl - 1) do
                 t := t + TableEmployes[colsommepointage, i];
        end;

     SommeEMP := t;

     // lance le calcul generalde la fiche Hebdo;
     CalculGeneralHebdo;
end;

function CurrToInt(valeur: currency): integer;
begin
     result := StrToInt(CurrToStr(valeur));
end;

function IntToCurr(valeur: integer): currency;
begin
     result := StrToCurr(IntToStr(valeur));
end;

//////////////////////////////////////////////////////////////////////////////
procedure PointageEnreg(numhebdo: integer);
var
   nbe:   integer;
   c:     currency;
   l:     integer;
   s:     string;

   procedure ChercheEnreg(ligne: integer);
   var
      c:     currency;
      id:    integer;
   begin
        c := TableEmployes[colindex, ligne];

        id := CurrToInt(c);
        FormBase.TabFichPoint.editkey;
        FormBase.TabFichPoint.FieldByName('id_pointage').asinteger := id;
        FormBase.TabFichPoint.gotokey;
        FormBase.TabFichPoint.edit;
   end;

begin
     // supprimer les enregs precedents
     PointageSupprime(numhebdo);
     ModeLigneAppend := false;

     nbe := FormBase.ListePointageEMP.RowCount - 1;
     if (nbe = 0) then exit;

     // pas de mofifs en cours
     ResetModifsEtat;

     with FormBase.TabFichPoint do
          begin
               Open;

               if nbe <> 0 then
                  begin
                       // creer chaque ligne et la remplir
                       for l := 0 to (nbe - 1) do
                           begin
                                if ModeLigneAppend = false then
                                   // creer nouveau enregistrement de ligne
                                   insert
                                else
                                   // chercher enreg existant
                                   ChercheEnreg(l);

                                s := FormBase.ListePointageEMP.Cells[colNom, l];
                                FieldByName('emp_enr_nomemploye').AsString := s;

                                c := TableEmployes[coltauxhoraire, l];
                                FieldByName('emp_enr_tauxhoraire').Ascurrency := c;

                                FieldByName('emp_enr_interimaire').AsBoolean := false;
                                FieldByName('emp_enr_actuel').AsBoolean := true;
                                FieldByName('emp_enr_petitpannier').Ascurrency := CoutPetitPagnier;
                                FieldByName('emp_enr_grandpannier').Ascurrency := CoutGrandPagnier;
                                FieldByName('emp_enr_tauxheuresup').Ascurrency := TauxHoraireSup;
                                FieldByName('emp_enr_facteurcharge').Ascurrency := TauxChargeBrut;

                                c := TableEmployes[colnombrejours, l];
                                FieldByName('emp_nbjourtravail').Ascurrency := c;

                                c := TableEmployes[colheuresroute, l];
                                FieldByName('emp_heuresroute').Ascurrency := c;

                                c := TableEmployes[colheuresjour, l];
                                FieldByName('emp_heuresjour').Ascurrency := c;

                                c := TableEmployes[colheuressamedi, l];
                                FieldByName('emp_heuressamedi').Ascurrency := c;

                                c := TableEmployes[colheuresnuitcoldimanche, l];
                                FieldByName('emp_heuresnuitdimanche').Ascurrency := c;

                                c := TableEmployes[colpetitpagnier, l];
                                FieldByName('emp_petitdeplacement').Ascurrency := c;

                                c := TableEmployes[colgrandpagnier, l];
                                FieldByName('emp_granddeplacement').Ascurrency := c;

                                c := TableEmployes[colsommepointage, l];
                                FieldByName('emp_total').Ascurrency := c;

                                FieldByName('id_hebdo').Asinteger := numhebdo;

                                // valider nouvelle ligne avec ces valeurs
                                post;
                           end;
                  end;

               // fermeture de la table apres ajouts
               close;
          end;

     // sortie du mode de modification
     ModeLigneAppend := false;
end;

procedure ChargeLigneExiste;
var
   nc:        integer;
   nblig:     integer;

   idemp:     integer;
   //inter:     boolean;
   monaie:    Currency;
   valeur:    currency;
   nomemp:    string;

   procedure Ecrire(col, lig: integer; valeur: currency);
   var
      t: string;
   begin
        t := FormatCurr(FormatSortie(col), valeur);
        TableEmployes[col, lig] := valeur;
        FormBase.ListePointageEMP.Cells[col, lig] := t;
   end;

begin
     with FormBase.TabFichPoint do
          begin
               CoutPetitPagnier := FieldByName('emp_enr_petitpannier').Ascurrency;
               CoutGrandPagnier := FieldByName('emp_enr_grandpannier').Ascurrency;
               TauxHoraireSup   := FieldByName('emp_enr_tauxheuresup').Ascurrency;
               TauxChargeBrut   := FieldByName('emp_enr_facteurcharge').Ascurrency;

               // definition de la ligne de pointage
               nblig := FormBase.ListePointageEMP.RowCount - 1;

               SetLength(TableEmployes, 10, nblig + 1);

               idemp := FieldByName('id_pointage').asinteger;
               TableEmployes[colindex, nblig] := IntToCurr(idemp);

               nomemp := FieldByName('emp_enr_nomemploye').AsString;
               FormBase.ListePointageEMP.Cells[colNom, nblig] := nomemp;

               // debut chargement liste ecran
               LigneEnCours := true;

               monaie := FieldByName('emp_enr_tauxhoraire').Ascurrency;
               Ecrire(coltauxhoraire, nblig, monaie);

               valeur := FieldByName('emp_nbjourtravail').Ascurrency;
               Ecrire(colnombrejours, nblig, valeur);

               valeur := FieldByName('emp_heuresroute').Ascurrency;
               Ecrire(colheuresroute, nblig, valeur);

               valeur := FieldByName('emp_heuresjour').Ascurrency;
               Ecrire(colheuresjour, nblig, valeur);

               valeur := FieldByName('emp_heuressamedi').Ascurrency;
               Ecrire(colheuressamedi, nblig, valeur);

               valeur := FieldByName('emp_heuresnuitdimanche').Ascurrency;
               Ecrire(colheuresnuitcoldimanche, nblig, valeur);

               valeur := FieldByName('emp_petitdeplacement').Ascurrency;
               Ecrire(colpetitpagnier, nblig, valeur);

               valeur := FieldByName('emp_granddeplacement').Ascurrency;
               Ecrire(colgrandpagnier, nblig, valeur);

               // calculer la ligne
               CalculPointageEMP(nblig);

               // fin chargement liste ecran
               LigneEnCours := false;

               // defintion de la prochaine ligne
               nc := FormBase.ListePointageEMP.RowCount;
               inc(nc);
               FormBase.ListePointageEMP.RowCount := nc;

               // selection de la 1ere colonne a editer
               FormBase.ListePointageEMP.SetFocus;
               FormBase.ListePointageEMP.col := colnombrejours;
               FormBase.ListePointageEMP.row := nblig;
          end;
end;
//////////////////////////////////////////////////////////////////////////////
procedure PointageCharge(index: integer);
var
   nbrec:     integer;
   t:         string;
   nblig:     integer;
begin
     ChargeConfiguration;

     // pas de mofifs en cours
     ResetModifsEtat;

     // definition des lignes de pointage
     FormBase.ListePointageEMP.RowCount := 1;

     with FormBase.TabFichPoint do
          begin
               // recherche des enreg contenant 'index'

               // preparer le filtre
               str(index, t);
               filter := 'id_hebdo = ' + t;
               filtered := true;

               open;
               setkey;

               nbrec := recordcount;
               if nbrec <> 0 then
                  begin
                       // entree dans le mode de modification
                       ModeLigneAppend := true;
                       Edit;

                       for nblig := 0 to (nbrec - 1) do
                           begin
                                // se positionner sur 1er enreg ou sur suivant
                                if nblig = 0 then first else next;

                                // charger liste de pointage
                                ChargeLigneExiste;
                           end;
                  end;

               // termine la lecture de la table
               close;
               filtered := false;

               // recalcul tout
               PointageExecuterCalcul;
          end;
end;

procedure GetEnregEmployeCourant(var i: integer;
                                 var a: boolean;
                                 var m: currency;
                                 var n: string);
begin
     with FormBase.TableEmployes do
     begin
          Edit;
          i := FieldValues['id_employe'];
          a := Fieldbyname('emp_actuel').asboolean;
          m := Fieldbyname('emp_tauxhoraire').ascurrency;
          n := Fieldbyname('emp_nomemploye').asstring;
          post;
     end;
end;

procedure TraitePointageChargeLigne;
var
   nc:        integer;
   nblig:     integer;

   idemp:     integer;
   monaie:    Currency;
   actuel:    Boolean;
   nomemp:    string;

   function ChercheExiste(nblig: integer; nomemp: string): boolean;
   var
      i:         integer;
      t:         string;
   begin
        result := false;
        for I := 0 to nblig - 1 do
            begin
                 t := FormBase.ListePointageEMP.Cells[colNom, I];
                 if nomemp = trim(t) then result := true;
            end;
   end;

   procedure Ecrire(col, lig: integer; valeur: currency);
   var
      t: string;
   begin
        t := FormatCurr(FormatSortie(col), valeur);
        TableEmployes[col, lig] := valeur;
        FormBase.ListePointageEMP.Cells[col, lig] := t;
   end;

begin
     // inclure un employ� dans la liste de pointage

     if FormBase.TableEmployes.RecordCount = 0 then
        exit;

     // modifs en cours
     SetModifsEtat;

     // lire l'enreg correspondant de l'employe modifier
     GetEnregEmployeCourant(idemp, actuel, monaie, nomemp);

     ChargeConfiguration;

     // definition de la ligne de pointage
     nblig := FormBase.ListePointageEMP.RowCount - 1;
     if nblig <> 0 then
        begin
             // verifier si le nom existe dans liste alors ne pas charger
             if ChercheExiste(nblig, nomemp) then
                exit;
        end;

     SetLength(TableEmployes, 10, nblig + 1);

     TableEmployes[colindex, nblig] := IntToCurr(idemp);
     FormBase.ListePointageEMP.Cells[colNom, nblig] := nomemp;

     LigneEnCours := true;         // debut chargement liste ecran

     Ecrire(coltauxhoraire, nblig, monaie);
     for nc := colnombrejours to colsommepointage do
         Ecrire(nc, nblig, 0);

     // calculer la ligne
     CalculPointageEMP(nblig);

     LigneEnCours := false;        // fin chargement liste ecran

     // defintion de la prochaine ligne
     nc := FormBase.ListePointageEMP.RowCount;
     inc(nc);
     FormBase.ListePointageEMP.RowCount := nc;

     // selection de la 1ere colonne a editer
     FormBase.ListePointageEMP.SetFocus;
     FormBase.ListePointageEMP.col := colnombrejours;
     FormBase.ListePointageEMP.row := nblig;

     // recalcul tout
     PointageExecuterCalcul;
end;

function TraitePointageSelection(ACol, ARow: Integer): Boolean;
var
   OK:       boolean;
   i:        integer;
begin
     // verifier si modifs autorisees
     if AutoriseModifs = false then
        begin
             // modifs interdites
             result := false;
             exit;
        end;

     result := true;
     OK     := true;

     // utile pour la suppression de la derniere ligne
     if Suppressionencours and (ACol = 10) then exit;

     // verifier si depassement nbr de lignes
     i := FormBase.ListePointageEMP.RowCount - 1;
     if ARow >= i then
        OK := false;

     if (ACol > coltauxhoraire) and (ACol <= colgrandpagnier) and OK then
        begin
             // autoriser l'edition de la cellule
             with FormBase.ListePointageEMP do
                  options := options + [goEditing];

             RechargerCellule(Acol, ARow);
        end
     else
         begin
              // interdire l'edition et la selection de la cellule
              with FormBase.ListePointageEMP do
                   options := options - [goEditing];
              result := false;
              exit;
         end;
end;

procedure TraitePointageValide(var Key: Char);
var
   ACol:     Integer;
   ARow:     Integer;
   temp:     string;
   valeur:   currency;
begin
     // verifier si modifs autorisees
     if AutoriseModifs = false then
        begin
             // modifs interdites
             key := chr(0);
             exit;
        end;

     if (key = ' ') or (key = '-') or(key = '+') then
        begin
              // ne traite pas ces touches
              key := chr(0);
              exit;
        end;

     if (key = '.') then
         begin;
               // modifs en cours
               SetModifsEtat;

               // pour faciliter la saisie des nombres decimaux
               key := ',';

               exit;
         end;

     // valide la saisie
     if Key = chr(13) then
        begin
             // modifs en cours
             SetModifsEtat;

             // Stocker la saisie entree au clavier
             ACol := FormBase.ListePointageEMP.Col;
             ARow := FormBase.ListePointageEMP.Row;

             try
                   begin
                        // Stocker la saisie entree au clavier
                        temp := FormBase.ListePointageEMP.Cells[ACol, ARow];
                        valeur := strtocurr(trim(temp));

                        // test maxi
                        if VerifMaxiOK(ACol, valeur) then
                           begin
                                TableEmployes[ACol, ARow] := valeur;
                                temp := FormatCurr(FormatSortie(Acol), valeur);
                                FormBase.ListePointageEMP.Cells[ACol, ARow] := temp;
                           end
                        else
                           // valeur hors de la bone maximum donc recharge
                           RechargerCellule(Acol, ARow);

                        // calculer la ligne
                        CalculPointageEMP(ARow);
                   end;

                   // recalcul tout
                   PointageExecuterCalcul;

             except
                // si erreur de saisie dans la cellule
                on EConvertError do
                   RechargerCellule(ACol, ARow);
             end;
        end;

     // si touche numerique
     CelluleEnCours := false;
     EditionEnCours := false;
     if (ord(key) > 47) and (ord(key) < 58) then
         begin
              // modifs en cours
              SetModifsEtat;

              CelluleEnCours      := true;
              EditionEnCours      := true;

              // Stocker la saisie entree au clavier
              CelluleEnCoursCol := FormBase.ListePointageEMP.Col;
              CelluleEnCoursRow := FormBase.ListePointageEMP.Row;
         end;
end;

procedure traitePointageInitialiserProcEffaceLigne(X, Y: Integer);
begin
     // verifier si modifs autorisees
     if AutoriseModifs = false then
        begin
             // modifs interdites
             exit;
        end;

     with FormBase.ListePointageEMP do
          MouseToCell(X, Y, positioncolonne, positionligne)
end;

procedure TraitePointageEffacerLigne;
var
   nbl:   integer;
   i:     integer;

   procedure transfert(deb, fin: integer);
   var
      L:     integer;
      C:     integer;
      t:     currency;
   begin
        for L := deb to fin - 2 do
            begin
                 for C := colindex to colsommepointage do
                     begin
                          with FormBase.ListePointageEMP do
                               Cells[C, L] := Cells[C, L + 1];

                               t := TableEmployes[C, L + 1];
                               TableEmployes[C, L] := t;
                     end;
            end;
   end;

   procedure viderligne(fin: integer);
   var
      C:     integer;
   begin
        for C := colindex to colsommepointage do
            FormBase.ListePointageEMP.Cells[C, fin - 1] := '';
   end;

begin
     // verifier si modifs autorisees
     if AutoriseModifs = false then
        begin
             // modifs interdites
             exit;
        end;

     // suppression ligne de pointage selectionn�e
     if (positioncolonne = colNom) then
         begin
              // modifs en cours
              SetModifsEtat;

              // diminuer d'une ligne verifier si depassement nbr de lignes
              nbl := FormBase.ListePointageEMP.RowCount - 1;
              if positionligne >= nbl then exit;

              // remonter d'une unite toutes les lignes
              if positionligne < nbl then
                 transfert(positionligne, nbl);

              if nbl = 1 then
                 unselectEMP
              else
                 begin
                      FormBase.ListePointageEMP.Col := 2;
                      FormBase.ListePointageEMP.Row := 0;
                 end;

              //effacer derniere ligne
              viderligne(nbl);

              i := FormBase.ListePointageEMP.RowCount;
              dec(i);
              FormBase.ListePointageEMP.RowCount := i;

              // pour eviter des suppression intenpestives
              positioncolonne := 1;
         end;

     // recalcul tout
     PointageExecuterCalcul;
end;

procedure traitepointagetouchesFIN();
begin
     CelluleEnCours := false;
end;

procedure traitepointagetouches(key: word);
var
   ACol:     Integer;
   ARow:     Integer;

   // recupere position cellule en cours
   procedure Cellule(var ACol: Integer; var ARow: Integer);
   begin
        ACol := FormBase.ListePointageEMP.Col;
        ARow := FormBase.ListePointageEMP.Row;
   end;

   function lignes: boolean;
   begin
        result := false;
        if FormBase.ListePointageEMP.RowCount > 2 then
           result := true;
   end;

begin
     // verifier si modifs autorisees
     if AutoriseModifs = false then
        begin
             // modifs interdites
             exit;
        end;

     // sortie si 0 lignes
     if not lignes then exit;

     if CelluleEnCours = true then exit;

     if TestTouchesFleches(key) or (VK_TAB = key) then
        begin
             if LigneEnCours = false then
                begin
                     // retour en arriere d'une cellule suivant touche
                     Cellule(ACol, ARow);
                     case key of
                          VK_TAB:   dec(Acol);
                          VK_LEFT:  inc(Acol);
                          VK_RIGHT: dec(Acol);
                          VK_DOWN:  if lignes then dec(ARow);
                          VK_UP:    if lignes then inc(ARow);
                     end;

                     // reaffichage suite a entree non validee par CRLF
                     if CelluleEnCours = false then
                        if colsommepointage <> Acol then
                           begin
                                // modifs en cours
                                SetModifsEtat;

                                RechargerCellule(Acol, ARow);
                           end;
                end;
        end;
end;

procedure traitepointagesouris;
var
   ACol:     Integer;
   ARow:     Integer;
begin
     // verifier si modifs autorisees
     if AutoriseModifs = false then
        begin
             // modifs interdites
             exit;
        end;

     if EditionEnCours = false then exit;

     ACol := FormBase.ListePointageEMP.Col;
     ARow := FormBase.ListePointageEMP.Row;

     if (CelluleEnCoursCol = ACol) and (CelluleEnCoursRow = ARow) then
        exit;

     EditionEnCours := false;

     // modifs en cours
     SetModifsEtat;

     RechargerCellule(CelluleEnCoursCol, CelluleEnCoursRow);
end;
//////////////////////////////////////////////////////////////////////////////
procedure PointageChargeChaines(index: integer; var nbrec: integer);

   function LireChaine(param: string): string;
   begin
        result := FormBase.TabFichPoint.FieldByName(param).AsString;
   end;

   function LireValeur(Param: string): currency;
   begin
        result := FormBase.TabFichPoint.FieldByName(param).Ascurrency;
   end;

   procedure Charge(nblig: integer);
      var
         i:         boolean;
         temp:      string;

   begin
        // recuperer le type de ligne
        i := FormBase.TabFichPoint.FieldByName('emp_enr_interimaire').asboolean;
        if i then
           temp := 'I'
        else
           temp := '';

        TableauTemp[0, nblig] := temp;

        TableauTemp[1, nblig] := LireChaine('emp_enr_nomemploye');

        temp := FormatSortie(coltauxhoraire);
        temp := FormatCurr(temp, LireValeur('emp_enr_tauxhoraire'));
        TableauTemp[2, nblig] := temp;

        temp := FormatSortie(colnombrejours);
        temp := FormatCurr(temp, LireValeur('emp_nbjourtravail'));
        TableauTemp[3, nblig] := temp;

        temp := FormatSortie(colheuresroute);
        temp := FormatCurr(temp, LireValeur('emp_heuresroute'));
        TableauTemp[4, nblig] := temp;

        temp := FormatSortie(colheuresjour);
        temp := FormatCurr(temp, LireValeur('emp_heuresjour'));
        TableauTemp[5, nblig] := temp;

        temp := FormatSortie(colheuressamedi);
        temp := FormatCurr(temp, LireValeur('emp_heuressamedi'));
        TableauTemp[6, nblig] := temp;

        temp := FormatSortie(colheuresnuitcoldimanche);
        temp := FormatCurr(temp, LireValeur('emp_heuresnuitdimanche'));
        TableauTemp[7, nblig] := temp;

        temp := FormatSortie(colpetitpagnier);
        temp := FormatCurr(temp, LireValeur('emp_petitdeplacement'));
        TableauTemp[8, nblig] := temp;

        temp := FormatSortie(colgrandpagnier);
        temp := FormatCurr(temp, LireValeur('emp_granddeplacement'));
        TableauTemp[9, nblig] := temp;

        temp := FormatSortie(colsommepointage);
        temp := FormatCurr(temp, LireValeur('emp_total'));
        TableauTemp[10, nblig] := temp;
   end;

var
   t:         string;
   nblig:     integer;

begin
     with FormBase.TabFichPoint do
          begin
               // preparer le filtre recherche des enreg contenant 'index'
               str(index, t);
               filter := 'id_hebdo = ' + t;
               filtered := true;

               open;
               setkey;

               nbrec := recordcount;
               if nbrec <> 0 then
                  begin
                       // entree dans le mode de modification
                       SetLength(TableauTemp, 11, nbrec + 1);
                       Edit;

                       for nblig := 0 to (nbrec - 1) do
                           begin
                                // se positionner sur 1er enreg ou sur suivant
                                if nblig = 0 then
                                   first
                                else
                                   next;

                                // charger liste de pointage
                                Charge(nblig);
                           end;
                  end;

               // termine la lecture de la table
               close;
               filtered := false;
          end;
end;

procedure PointageSupprime(index: integer);
var
   t:         string;
begin
     with FormBase.TabFichPoint do
          begin
               // preparer le filtre recherche des enreg contenant 'index'
               str(index, t);
               filter := 'id_hebdo = ' + t;
               filtered := true;

               open;
               setkey;

               // supprimer tous les enregs
               while recordcount <> 0 do
                     delete;

               // termine la suppression dans la table
               close;
               filtered := false;
          end;
end;

//////////////////////////////////////////////////////////////////////////////
procedure RefreshPointage;
begin

end;

end.
