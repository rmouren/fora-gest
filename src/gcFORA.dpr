program gcFORA;

uses
  Forms,
  UtilsBDE in 'UtilsBDE.pas',
  applicbase in 'applicbase.pas',
  winbase in 'winbase.pas' {FormBase},
  fichebase in 'fichebase.pas' {Fichesimple},
  Ficheconfig in 'Ficheconfig.pas' {FormConfig},
  FicheEtats in 'FicheEtats.pas' {FormEtats},
  VerifIndex in 'VerifIndex.pas' {FicheTestIndex},
  Hebdo in 'Hebdo.pas',
  accesBDD in 'accesBDD.pas',
  Utils in 'Utils.pas',
  Etats in 'Etats.pas',
  Impression in 'Impression.pas',
  MasqueEmployes in 'MasqueEmployes.pas',
  MasqueAchats in 'MasqueAchats.pas',
  MasqueChantiers in 'MasqueChantiers.pas',
  MasqueDiamant in 'MasqueDiamant.pas',
  MasqueExterne in 'MasqueExterne.pas',
  MasqueMateriels in 'MasqueMateriels.pas',
  MasqueSemaine in 'MasqueSemaine.pas',
  MasqueStructures in 'MasqueStructures.pas',
  MasqueVehicules in 'MasqueVehicules.pas';

{$R *.RES }
{$H-}

begin

  Application.Initialize;
  Application.CreateForm(TFormBase, FormBase);
  Application.CreateForm(TFichesimple, Fichesimple);
  Application.CreateForm(TFormConfig, FormConfig);
  Application.CreateForm(TFormEtats, FormEtats);
  Application.CreateForm(TFicheTestIndex, FicheTestIndex);
  Application.Run;

end.
