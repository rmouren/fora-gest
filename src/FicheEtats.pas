unit FicheEtats;

interface

uses
  Windows, UITypes, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs,
  StdCtrls, ComCtrls, Mask, ExtCtrls, Db, DBTables, DBCtrls;

type

  TFormEtats = class(TForm)
    Button1: TButton;
    PageControlEtats: TPageControl;
    OngletHebdo: TTabSheet;
    TabSheet2: TTabSheet;
    PanelFiltre: TPanel;
    ListeSecteurs: TDBLookupComboBox;
    ListeCommerciaux: TDBLookupComboBox;
    ListeClients: TDBLookupComboBox;
    BFiltreAgence: TRadioButton;
    BFiltreCommercial: TRadioButton;
    BFiltreClient: TRadioButton;
    DataSourceSecteurs: TDataSource;
    TableSecteurs: TTable;
    DataSourceCommerciaux: TDataSource;
    TableCommerciaux: TTable;
    DataSourceClients: TDataSource;
    TableClients: TTable;
    PrintEtat: TButton;
    Label3: TLabel;
    TabSheet3: TTabSheet;
    Panel2: TPanel;
    Panel3: TPanel;
    Label4: TLabel;
    HebdoSaisies: TRadioButton;
    HebdoSemaine: TRadioButton;
    HebdoRegul: TRadioButton;
    SaisieNumMois: TComboBox;
    CBFiches: TCheckBox;
    CBReguls: TCheckBox;
    ChoixMulti: TRadioButton;
    ListeChantiers: TDBLookupComboBox;
    ChoixUnique: TRadioButton;
    Panel4: TPanel;
    CumulHebdo: TRadioButton;
    CumulMensuel: TRadioButton;
    SaisieNumMoisCum: TComboBox;
    saisieBis: TCheckBox;
    CumulAnnuel: TRadioButton;
    Panel5: TPanel;
    RecapGeneral: TRadioButton;
    RecapSimple: TRadioButton;
    RecapClients: TRadioButton;
    RecapChantiers: TRadioButton;
    Panel6: TPanel;
    Panel7: TPanel;
    ChoixAnnee: TCheckBox;
    S1: TLabel;
    S2: TLabel;
    S3: TLabel;
    S4: TLabel;
    S5: TLabel;
    Label9: TLabel;
    ME1: TMaskEdit;
    ME2: TMaskEdit;
    ME3: TMaskEdit;
    ME4: TMaskEdit;
    ME5: TMaskEdit;
    ChoixImpre1: TCheckBox;
    ChoixImpre2: TCheckBox;
    ChoixImpre3: TCheckBox;
    ChoixImpre4: TCheckBox;
    ChoixImpre5: TCheckBox;
    Label5: TLabel;
    ChoixImpreRegul: TCheckBox;
    Label1: TLabel;
    Tablechantiers: TTable;
    BFiltre: TRadioButton;
    HebdoUnique: TRadioButton;
    UniqueBis: TCheckBox;
    TabSheet4: TTabSheet;
    Panel1: TPanel;
    ChoixClotureHebdo: TRadioButton;
    ChoixClotureMois: TRadioButton;
    saisieClotureMois: TComboBox;
    SaisieClotureBis: TCheckBox;
    ChoixClotureAnnee: TRadioButton;
    ExporteDialog: TSaveDialog;
    ParCommercial: TCheckBox;
    PanelClotSem: TPanel;
    PanelClotMoi: TPanel;
    PanelClotAnn: TPanel;
    SemFich: TCheckBox;
    SemCum: TCheckBox;
    SemRec: TCheckBox;
    Label2: TLabel;
    MoiReg: TCheckBox;
    Label6: TLabel;
    MoiCumM: TCheckBox;
    MoiCumA: TCheckBox;
    MoiCha: TCheckBox;
    MoiCli: TCheckBox;
    Label7: TLabel;
    AnnCum: TCheckBox;
    AnnCli: TCheckBox;
    SaisieNumMoisRecap: TComboBox;
    Label8: TLabel;
    saisieClotureSemaine: TEdit;
    saisieNumSemaine: TEdit;
    SaisieNum: TEdit;
    saisieNumDeb: TEdit;
    saisieNumFin: TEdit;
    procedure Button1Click(Sender: TObject);
    procedure CumulHebdoClick(Sender: TObject);
    procedure CumulMensuelClick(Sender: TObject);
    procedure SaisieNumMoisCumClick(Sender: TObject);
    procedure RecapGeneralClick(Sender: TObject);
    procedure RecapSimpleClick(Sender: TObject);
    procedure RecapClientsClick(Sender: TObject);
    procedure SaisieNumMoisRecapClick(Sender: TObject);
    procedure ListeSecteursClick(Sender: TObject);
    procedure ListeCommerciauxClick(Sender: TObject);
    procedure ListeClientsClick(Sender: TObject);
    procedure PrintEtatClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PageControlEtatsChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure saisieBisClick(Sender: TObject);
    procedure ME1Change(Sender: TObject);
    procedure ME2Change(Sender: TObject);
    procedure ME3Change(Sender: TObject);
    procedure ME4Change(Sender: TObject);
    procedure ME5Change(Sender: TObject);
    procedure CumulAnnuelClick(Sender: TObject);
    procedure ChoixMultiClick(Sender: TObject);
    procedure ListeChantiersClick(Sender: TObject);
    procedure ChoixUniqueClick(Sender: TObject);
    procedure ChoixAnneeClick(Sender: TObject);
    procedure ChoixImpre1Click(Sender: TObject);
    procedure ChoixImpre2Click(Sender: TObject);
    procedure ChoixImpre3Click(Sender: TObject);
    procedure ChoixImpre4Click(Sender: TObject);
    procedure ChoixImpre5Click(Sender: TObject);
    procedure ChoixImpreRegulClick(Sender: TObject);
    procedure CBFichesClick(Sender: TObject);
    procedure CBRegulsClick(Sender: TObject);
    procedure SaisieNumMoisClick(Sender: TObject);
    procedure HebdoSaisiesClick(Sender: TObject);
    procedure HebdoSemaineClick(Sender: TObject);
    procedure HebdoRegulClick(Sender: TObject);
    procedure BFiltreMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure HebdoUniqueClick(Sender: TObject);
    procedure UniqueBisClick(Sender: TObject);
    procedure ChoixClotureHebdoClick(Sender: TObject);
    procedure ChoixClotureMoisClick(Sender: TObject);
    procedure ChoixClotureAnneeClick(Sender: TObject);
    procedure SaisieClotureBisClick(Sender: TObject);
    procedure saisieClotureMoisClick(Sender: TObject);
    procedure ListeSecteursKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ListeSecteursEnter(Sender: TObject);
    procedure ListeSecteursMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure RecapChantiersClick(Sender: TObject);
    procedure ParCommercialClick(Sender: TObject);
    procedure saisieClotureSemaineChange(Sender: TObject);
    procedure saisieClotureSemaineClick(Sender: TObject);
    procedure saisieClotureSemaineKeyPress(Sender: TObject; var Key: Char);
    procedure saisieNumSemaineChange(Sender: TObject);
    procedure saisieNumSemaineClick(Sender: TObject);
    procedure saisieNumSemaineKeyPress(Sender: TObject; var Key: Char);
    procedure SaisieNumChange(Sender: TObject);
    procedure SaisieNumClick(Sender: TObject);
    procedure SaisieNumKeyPress(Sender: TObject; var Key: Char);
    procedure saisieNumDebChange(Sender: TObject);
    procedure saisieNumDebClick(Sender: TObject);
    procedure saisieNumDebKeyPress(Sender: TObject; var Key: Char);
    procedure saisieNumFinChange(Sender: TObject);
    procedure saisieNumFinClick(Sender: TObject);
    procedure saisieNumFinKeyPress(Sender: TObject; var Key: Char);

  private
    { D�clarations priv�es }
  public
    { D�clarations publiques }
  end;

function ValideImprimeRecap(iDsemaine: Integer): boolean;
procedure PreparerFicheImpressionEtats;
procedure CalcSemaine;
function LireNBJSaisie(iDsemaine: Integer): Integer;
function GetChoixImpreSemaine(num: Integer): boolean;
procedure SetChoixImpre(num: Integer; impr: boolean);
function LireChaineChantier: string;

const

  hebdosaisis = 10;
  hebdoecartsemaines = 11;
  hebdoecartmois = 12;
  hebdosimple = 14;

  EtatHebdo = 0;
  EtatRegul = 1;
  Etat2 = 2;
  Etat3 = 3;
  Etat4 = 4;
  Etat5 = 5;
  Etat6 = 6;
  Etat7 = 7;
  Etat8 = 8;
  Etat9 = 20;
  Etat10 = 21;
  Etat11 = 22;

  SansFiltre = 0;
  FiltreAgence = 1;
  FiltreCommercial = 2;
  FiltreClient = 3;

var

  FormEtats: TFormEtats;

implementation

uses Ficheconfig, winbase, fichebase, accesBDD, Etats, utils, hebdo, Impression,
  DateUtils;

{$R *.DFM}

var

  ChoixEtat: Integer = Etat2;
  NumSemaine: Integer = 1;
  SemaineBis: boolean = false;
  NumMois: Integer = 1;
  ClientCommercial: boolean;
  ChaineChantier: string;

  NumSemaineCloture: Integer = 1;
  SemaineBisCloture: boolean = false;
  NumMoisCloture: Integer = 1;

  RecapAnnuel: boolean;

  RecapS1: boolean = true;
  RecapS2: boolean = true;
  RecapS3: boolean = true;
  RecapS4: boolean = true;
  RecapS5: boolean = true;
  RecapRegul: boolean = true;

  nbj1: Integer = 1;
  nbj2: Integer = 1;
  nbj3: Integer = 1;
  nbj4: Integer = 1;
  nbj5: Integer = 1;

  ChoixChantiers: boolean = true;
  NumChantier: Integer;

  ChoixFiltre: Integer = FiltreAgence;
  NumAgence: Integer;
  NumCommercial: Integer;
  NumClient: Integer;

  HebdoSem: Integer = 1;
  HebdoBis: boolean = false;
  HebdoSemDeb: Integer = 1;
  HebdoSemFin: Integer = 1;
  HebdoMois: Integer = 1;
  HebdoFiches: boolean = true;
  HebdoReguls: boolean = true;

  { ----------------------------------------------------------------------------- }
procedure CalcSemaine;
var
  n: string;
  ok: boolean;
  semdeb: Integer;
  semfin: Integer;
  ns: Integer;
begin
  // recuperer numero mois
  NumMois := FormEtats.SaisieNumMoisRecap.ItemIndex + 1;

  // calculer les semaines
  EcartSemaine(NumMois, AnneeCourante, semdeb, semfin);
  // if ((DayOfTheWeek(EncodeDate(AnneeCourante, NumMois, 1)) >= 6) AND (JoursMois(AnneeCourante, NumMois) = 31) AND (semdeb > 1)) then  semdeb := semdeb + 1;

  ns := (semfin - semdeb + 1);

  ok := false;
  if ns > 4 then
    ok := true;

  // si semaine 5 valider les nbj
  FormEtats.S5.Visible := ok;
  FormEtats.ME5.Visible := ok;

  n := 'Semaine ';
  FormEtats.S1.Caption := n + inttostr(semdeb);
  FormEtats.S2.Caption := n + inttostr(semdeb + 1);
  FormEtats.S3.Caption := n + inttostr(semdeb + 2);
  FormEtats.S4.Caption := n + inttostr(semdeb + 3);

  if ns > 4 then
    FormEtats.S5.Caption := n + inttostr(semdeb + 4);

  case ChoixEtat of
    Etat5: // , Etat6, Etat7
      FormEtats.Panel6.Enabled := true;
  else
    FormEtats.Panel6.Enabled := false;
  end;

  {
    case ChoixEtat of
    Etat5:
    begin
    FormEtats.ChoixImpre1.Enabled := true;
    FormEtats.ChoixImpre2.Enabled := true;
    FormEtats.ChoixImpre3.Enabled := true;
    FormEtats.ChoixImpre4.Enabled := true;
    FormEtats.ChoixImpre5.Enabled := true;
    FormEtats.ChoixImpreRegul.Enabled := true;
    end;
    else
    begin
    FormEtats.ChoixImpre1.Enabled := false;
    FormEtats.ChoixImpre2.Enabled := false;
    FormEtats.ChoixImpre3.Enabled := false;
    FormEtats.ChoixImpre4.Enabled := false;
    FormEtats.ChoixImpre5.Enabled := false;
    FormEtats.ChoixImpreRegul.Enabled := false;
    end;
    end;
  }

end;

procedure traiteboutonimprimer;
var
  ok: boolean;
begin
  ok := false;

  case ChoixFiltre of
    SansFiltre:
      ok := true;
    FiltreAgence:
      if FormEtats.ListeSecteurs.KeyValue <> -1 then
        ok := true;
    FiltreCommercial:
      if FormEtats.ListeCommerciaux.KeyValue <> -1 then
        ok := true;
    FiltreClient:
      if FormEtats.ListeClients.KeyValue <> -1 then
        ok := true;
  end;

  if ok = true then
    if FormEtats.PageControlEtats.ActivePage.Caption = 'Cumul' then
      if FormEtats.ChoixUnique.Checked = true then
        if Not FormEtats.ListeChantiers.KeyValue then
          ok := false;

  FormEtats.PrintEtat.Enabled := ok;
end;

procedure SetChoixImpre(num: Integer; impr: boolean);
begin
  case num of
    1:
      RecapS1 := impr;
    2:
      RecapS2 := impr;
    3:
      RecapS3 := impr;
    4:
      RecapS4 := impr;
    5:
      RecapS5 := impr;
    0:
      RecapRegul := impr;
  end;
end;

function ValideImprimeRecap(iDsemaine: Integer): boolean;
begin
  result := true;
  case iDsemaine of
    0:
      result := RecapRegul;
    1:
      result := RecapS1;
    2:
      result := RecapS2;
    3:
      result := RecapS3;
    4:
      result := RecapS4;
    5:
      result := RecapS5;
  end;
end;

function LireNBJSaisie(iDsemaine: Integer): Integer;
begin
  result := 0;
  case iDsemaine of
    1:
      result := nbj1;
    2:
      result := nbj2;
    3:
      result := nbj3;
    4:
      result := nbj4;
    5:
      result := nbj5;
  end;
end;

procedure verifnumsemaine;
var
  t: string;
  n: Integer;
begin
  t := FormEtats.saisieNumSemaine.Text;
  n := strtoint(t);
  if (n > 0) and (n < 53) then
    // stocker numero semaine
    NumSemaine := n
  else
  begin
    // restituer numero semaine sauv�
    t := inttostr(NumSemaine);
    if NumSemaine < 10 then
      t := '0' + t;
    FormEtats.saisieNumSemaine.Text := t;
  end;
end;

procedure TraiteChoixFiltre(Sender: TObject);
begin
  with FormEtats do
  begin
    if Sender = BFiltre then
    begin
      ChoixFiltre := SansFiltre;
      ListeSecteurs.Enabled := false;
      ListeCommerciaux.Enabled := false;
      ListeClients.Enabled := false;
      ListeSecteurs.KeyValue := -1;
      ListeCommerciaux.KeyValue := -1;
      ListeClients.KeyValue := -1;
    end
    else if Sender = FormEtats.BFiltreAgence then
    begin
      ChoixFiltre := FiltreAgence;
      ListeSecteurs.Enabled := true;
      ListeCommerciaux.Enabled := false;
      ListeClients.Enabled := false;
      ListeCommerciaux.KeyValue := -1;
      ListeClients.KeyValue := -1;
    end
    else if Sender = BFiltreCommercial then
    begin
      ChoixFiltre := FiltreCommercial;
      ListeSecteurs.Enabled := false;
      ListeCommerciaux.Enabled := true;
      ListeClients.Enabled := false;
      ListeSecteurs.KeyValue := -1;
      ListeClients.KeyValue := -1;
    end
    else if Sender = BFiltreClient then
    begin
      ChoixFiltre := FiltreClient;
      ListeSecteurs.Enabled := false;
      ListeCommerciaux.Enabled := false;
      ListeClients.Enabled := true;
      ListeSecteurs.KeyValue := -1;
      ListeCommerciaux.KeyValue := -1;
    end;
  end;

  traiteboutonimprimer;
end;

procedure TraiteChoixFiltreTousChantiers;
begin
  // mettre en sans filtre
  FormEtats.BFiltre.Checked := true;

  TraiteChoixFiltre(FormEtats.BFiltre);
end;

procedure PreparerFicheImpressionEtats;

  function VerifSauveOK: boolean;
  var
    t: string;
    s: Word;
  begin
    result := false;
    t := 'Voulez vous sauver la fiche en cours ?';
    s := MessageDlg(t, mtWarning, [mbYes, mbNo], 0);

    if s = mrNo then
      exit;
    result := true;
  end;

begin
  if ProtectionFicheOuverte then
    exit;

  with FormEtats do
  begin
    // centrer la fenetre par rapport a la fenetre principale
    top := (FormBase.top + (FormBase.Height div 2));
    top := top - (Height div 2);
    left := (FormBase.left + (FormBase.width div 2));
    left := left - (width div 2);
  end;

  // verifier si fiche en cours modif
  if ModifHebdoOK then
  begin
    // message de demande de sauvegarde
    if VerifSauveOK then
      HEBDOEnregistrer;
  end;

  // remettre en mode initial
  HEBDOAnnuler;

  // afficher fenetre
  FormEtats.showmodal;
end;

procedure setFicheHebdo(mode: Integer);
begin
  ChoixEtat := mode;

  case ChoixEtat of
    hebdosaisis:
      begin
        FormEtats.saisieNumDeb.Enabled := false;
        FormEtats.saisieNumFin.Enabled := false;
        FormEtats.SaisieNumMois.Enabled := false;
      end;
    hebdoecartsemaines:
      begin
        FormEtats.saisieNumDeb.Enabled := true;
        FormEtats.saisieNumFin.Enabled := true;
        FormEtats.SaisieNumMois.Enabled := false;
      end;
    hebdoecartmois:
      begin
        FormEtats.saisieNumDeb.Enabled := false;
        FormEtats.saisieNumFin.Enabled := false;
        FormEtats.SaisieNumMois.Enabled := true;
        FormEtats.SaisieNumMois.ItemIndex := -1;
      end;
  end;
end;

function LireChaineChantier: string;
begin
  result := ChaineChantier;
end;

function ChoixChantierUnique: boolean;
var
  t: string;
begin
  result := false;

  t := FormEtats.ListeChantiers.Text;
  ChaineChantier := t;
  if t = '' then
  begin
    // empecher impression
    FormEtats.PrintEtat.Enabled := false;
    exit;
  end;

  NumChantier := FormEtats.ListeChantiers.KeyValue;
  result := true;
end;

procedure SetChoixChantiers(choix: boolean);
begin
  FormEtats.BFiltre.Checked := true;
  TraiteChoixFiltre(FormEtats.BFiltre);

  if choix then
  begin
    // init cumuls de tous les chantiers
    ChoixChantiers := true;
    FormEtats.PanelFiltre.Enabled := true;
    FormEtats.ListeChantiers.Enabled := false;
  end
  else
  begin
    // init cumuls d'un seul chantier
    ChoixChantiers := false;
    FormEtats.PanelFiltre.Enabled := false;
    FormEtats.ListeChantiers.Enabled := true;

    // tester si un chantier est choisi et enregistrer si OK
    if not ChoixChantierUnique then
      exit;
  end;

  traiteboutonimprimer;
end;

procedure setCumulHebdo;
begin
  // definir etat cumul semaine
  ChoixEtat := Etat2;

  // preparer saisie num semaine
  FormEtats.saisieNumSemaine.Enabled := true;
  FormEtats.saisieBis.Enabled := true;
  FormEtats.SaisieNumMoisCum.Enabled := false;

  FormEtats.ChoixMulti.Checked := true;
  SetChoixChantiers(true);
  FormEtats.Panel2.Enabled := false;

  // lire saisie num semaine cumul
  verifnumsemaine;
end;

procedure setCumulMensuel;
begin
  // definir etat cumul mensuel
  ChoixEtat := Etat3;

  // preparer saisie num mois
  FormEtats.saisieNumSemaine.Enabled := false;
  FormEtats.saisieBis.Enabled := false;
  FormEtats.SaisieNumMoisCum.Enabled := true;

  FormEtats.ChoixMulti.Checked := true;
  SetChoixChantiers(true);
  FormEtats.Panel2.Enabled := false;

  // lire saisie num mois cumul
  NumMois := FormEtats.SaisieNumMoisCum.ItemIndex + 1;
end;

procedure setCumulAnnuel;
begin
  // definir etat cumul annuel
  ChoixEtat := Etat4;

  // empecher saisie num mois
  FormEtats.saisieNumSemaine.Enabled := false;
  FormEtats.saisieBis.Enabled := false;
  FormEtats.SaisieNumMoisCum.Enabled := true;

  FormEtats.ChoixMulti.Checked := true;
  FormEtats.Panel2.Enabled := true;

  // lire saisie num mois cumul
  NumMois := FormEtats.SaisieNumMoisCum.ItemIndex + 1;
end;

procedure SetHebdo(mode: Integer);
begin
  ChoixEtat := mode;

  case mode of
    hebdosaisis:
      begin
        FormEtats.saisieNumDeb.Enabled := false;
        FormEtats.saisieNumFin.Enabled := false;
        FormEtats.SaisieNumMois.Enabled := false;
        FormEtats.CBFiches.Enabled := false;
        FormEtats.CBReguls.Enabled := false;
        FormEtats.SaisieNum.Enabled := false;
        FormEtats.UniqueBis.Enabled := false;
      end;
    hebdosimple:
      begin
        FormEtats.saisieNumDeb.Enabled := false;
        FormEtats.saisieNumFin.Enabled := false;
        FormEtats.SaisieNumMois.Enabled := false;
        FormEtats.CBFiches.Enabled := false;
        FormEtats.CBReguls.Enabled := false;
        FormEtats.SaisieNum.Enabled := true;
        FormEtats.UniqueBis.Enabled := true;
      end;
    hebdoecartsemaines:
      begin
        FormEtats.saisieNumDeb.Enabled := true;
        FormEtats.saisieNumFin.Enabled := true;
        FormEtats.SaisieNumMois.Enabled := false;
        FormEtats.CBFiches.Enabled := false;
        FormEtats.CBReguls.Enabled := false;
        FormEtats.SaisieNum.Enabled := false;
        FormEtats.UniqueBis.Enabled := false;
      end;
    hebdoecartmois:
      begin
        FormEtats.saisieNumDeb.Enabled := false;
        FormEtats.saisieNumFin.Enabled := false;
        FormEtats.SaisieNumMois.Enabled := true;
        FormEtats.CBFiches.Enabled := true;
        FormEtats.CBReguls.Enabled := true;
        FormEtats.SaisieNum.Enabled := false;
        FormEtats.UniqueBis.Enabled := false;
      end;
  end;
end;

procedure SetRecap(mode: Integer);
begin
  ChoixEtat := mode;

  // rendre le filtre accessible suivant etat
  case ChoixEtat of
    Etat5:
      begin
        FormEtats.PanelFiltre.Enabled := true;
        // FormEtats.printEtat.Enabled := false;
        FormEtats.ParCommercial.Enabled := false;
      end;
    Etat6:
      begin
        FormEtats.PanelFiltre.Enabled := true;
        // FormEtats.printEtat.Enabled := false;
        FormEtats.ParCommercial.Enabled := false;
      end;
    Etat7:
      begin
        FormEtats.PanelFiltre.Enabled := false;
        FormEtats.PrintEtat.Enabled := true;
        FormEtats.ParCommercial.Enabled := true;

        // mettre en sans filtre
        TraiteChoixFiltreTousChantiers;
      end;
    Etat8:
      begin
        FormEtats.PanelFiltre.Enabled := true;
        // FormEtats.printEtat.Enabled := false;
        FormEtats.ParCommercial.Enabled := false;
      end;
  end;

  // lire saisie num mois recap
  NumMois := FormEtats.SaisieNumMoisRecap.ItemIndex + 1;

  // calculer les semaine et si etat 5 6 7 valider les nbj
  CalcSemaine;
end;

procedure SetCloture(mode: Integer);
begin
  ChoixEtat := mode;

  // cacher composants inutiles
  case ChoixEtat of
    Etat9:
      begin
        FormEtats.saisieClotureSemaine.Enabled := true;
        FormEtats.SaisieClotureBis.Enabled := true;
        FormEtats.saisieClotureMois.Enabled := false;

        FormEtats.PanelClotSem.Visible := true;
        FormEtats.PanelClotMoi.Visible := false;
        FormEtats.PanelClotAnn.Visible := false;

        FormEtats.SemFich.Checked := true;
        FormEtats.SemCum.Checked := true;
        FormEtats.SemRec.Checked := true;
      end;
    Etat10:
      begin
        FormEtats.saisieClotureSemaine.Enabled := false;
        FormEtats.SaisieClotureBis.Enabled := false;
        FormEtats.saisieClotureMois.Enabled := true;

        FormEtats.PanelClotSem.Visible := false;
        FormEtats.PanelClotMoi.Visible := true;
        FormEtats.PanelClotAnn.Visible := false;

        FormEtats.MoiReg.Checked := true;
        FormEtats.MoiCumM.Checked := true;
        FormEtats.MoiCumA.Checked := true;
        FormEtats.MoiCha.Checked := true;
        FormEtats.MoiCli.Checked := true;
      end;
    Etat11:
      begin
        FormEtats.saisieClotureSemaine.Enabled := false;
        FormEtats.SaisieClotureBis.Enabled := false;
        FormEtats.saisieClotureMois.Enabled := false;

        FormEtats.PanelClotSem.Visible := false;
        FormEtats.PanelClotMoi.Visible := false;
        FormEtats.PanelClotAnn.Visible := true;

        FormEtats.AnnCum.Checked := true;
        FormEtats.AnnCli.Checked := true;
      end;
  end;

  // verifier saisie
  traiteboutonimprimer;
end;

procedure VerifieOngletTypeEdition;
begin
  if FormEtats.PageControlEtats.ActivePage.Caption = 'Fiches hebdomadaires' then
  begin
    // rendre le filtre inaccessible
    FormEtats.PanelFiltre.Enabled := false;
    FormEtats.PrintEtat.Enabled := true;

    // effectuer premier choix
    FormEtats.HebdoSaisies.Checked := true;
    SetHebdo(hebdosaisis);
  end
  else if FormEtats.PageControlEtats.ActivePage.Caption = 'Etat Cumuls' then
  begin
    // rendre le filtre accessible et chantiers multiples
    FormEtats.ChoixMulti.Checked := true;
    SetChoixChantiers(true);
    FormEtats.PrintEtat.Enabled := false;

    // effectuer premier choix
    FormEtats.CumulHebdo.Checked := true;
    setCumulHebdo;
  end
  else if FormEtats.PageControlEtats.ActivePage.Caption = 'Recapitulatifs' then
  begin
    // rendre le filtre accessible
    FormEtats.PanelFiltre.Enabled := true;
    FormEtats.PrintEtat.Enabled := false;

    // effectuer premier choix
    FormEtats.RecapGeneral.Checked := true;
    SetRecap(Etat5);
  end
  else
  begin
    // rendre le filtre inaccessible
    FormEtats.PanelFiltre.Enabled := false;
    FormEtats.PrintEtat.Enabled := false;

    // effectuer premier choix
    FormEtats.ChoixClotureHebdo.Checked := true;
    SetCloture(Etat9);

    FormEtats.PanelClotSem.Visible := true;
    FormEtats.PanelClotMoi.Visible := false;
    FormEtats.PanelClotAnn.Visible := false;

    FormEtats.SemFich.Checked := true;
    FormEtats.SemCum.Checked := true;
    FormEtats.SemRec.Checked := true;
  end;

  // mettre en sans filtre
  TraiteChoixFiltreTousChantiers;
end;

procedure TFormEtats.Button1Click(Sender: TObject);
begin
  // sortir de l'impression des etats
  FormEtats.modalresult := 1;
end;

procedure TFormEtats.FormShow(Sender: TObject);
begin
  FormEtats.PageControlEtats.ActivePage := PageControlEtats.pages[0];

  FormEtats.SaisieNumMoisCum.ItemIndex := 0;
  FormEtats.SaisieNumMoisRecap.ItemIndex := 0;
  FormEtats.SaisieNumMois.ItemIndex := 0;

  ChoixImpre1.Checked := true;
  ChoixImpre2.Checked := true;
  ChoixImpre3.Checked := true;
  ChoixImpre4.Checked := true;
  ChoixImpre5.Checked := true;
  ChoixImpreRegul.Checked := true;

  VerifieOngletTypeEdition;

  CalcSemaine;
end;

{ ----------------------------------------------------------------------------- }
procedure TFormEtats.FormCreate(Sender: TObject);
begin
  SetChoixImpre(1, ChoixImpre1.Checked);
  SetChoixImpre(2, ChoixImpre2.Checked);
  SetChoixImpre(3, ChoixImpre3.Checked);
  SetChoixImpre(4, ChoixImpre4.Checked);
  SetChoixImpre(5, ChoixImpre5.Checked);
  SetChoixImpre(0, ChoixImpreRegul.Checked);

  // nb jours par defaut des semaines
  ME1.Text := '5';
  ME2.Text := '5';
  ME3.Text := '5';
  ME4.Text := '5';
  ME5.Text := '5';

  // forcer la page hebdo
  PageControlEtats.ActivePage.PageIndex := 0;

  // remise a jour en cas de modif de ces tables
  RefreshTable(TableSecteurs);
  RefreshTable(TableCommerciaux);
  RefreshTable(TableClients);
end;

procedure TFormEtats.CumulHebdoClick(Sender: TObject);
begin
  setCumulHebdo;
end;

procedure TFormEtats.CumulMensuelClick(Sender: TObject);
begin
  setCumulMensuel;
end;

procedure TFormEtats.CumulAnnuelClick(Sender: TObject);
begin
  setCumulAnnuel;
end;

{ ----------------------------------------------------------------------------- }
procedure TFormEtats.BFiltreMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  TraiteChoixFiltre(Sender);
end;

{ ----------------------------------------------------------------------------- }
procedure TFormEtats.saisieNumSemaineChange(Sender: TObject);
begin
  // verif si 1 a 52
  verifnumsemaine;

  traiteboutonimprimer;
end;

procedure TFormEtats.saisieNumSemaineClick(Sender: TObject);
begin
  saisieNumSemaine.SelectAll;
end;

procedure TFormEtats.saisieNumSemaineKeyPress(Sender: TObject; var Key: Char);
begin
  // sortie si chiffre
  if VerifNum(Key) then
    exit;

  // sinon annuler
  Key := chr(0);
end;

procedure TFormEtats.saisieBisClick(Sender: TObject);
begin
  SemaineBis := FormEtats.saisieBis.Checked;

  traiteboutonimprimer;
end;

procedure TFormEtats.SaisieNumMoisCumClick(Sender: TObject);
begin
  // recuperer numero mois
  NumMois := FormEtats.SaisieNumMoisCum.ItemIndex + 1;

  traiteboutonimprimer;
end;

procedure TFormEtats.ChoixMultiClick(Sender: TObject);
begin
  SetChoixChantiers(true);
end;

procedure TFormEtats.ChoixUniqueClick(Sender: TObject);
begin
  SetChoixChantiers(false);
end;

procedure TFormEtats.ListeChantiersClick(Sender: TObject);
begin
  // tester si un chantier est choisi et enregistrer si OK
  if not ChoixChantierUnique then
    exit;

  traiteboutonimprimer;
end;

procedure TFormEtats.ListeSecteursClick(Sender: TObject);
begin
  NumAgence := ListeSecteurs.KeyValue;
  traiteboutonimprimer;
end;

procedure TFormEtats.ListeCommerciauxClick(Sender: TObject);
begin
  NumCommercial := ListeCommerciaux.KeyValue;
  traiteboutonimprimer;
end;

procedure TFormEtats.ListeClientsClick(Sender: TObject);
begin
  NumClient := ListeClients.KeyValue;
  traiteboutonimprimer;
end;

{ ----------------------------------------------------------------------------- }
procedure AfficheEtatxxx;
var
  t: string;
begin
  t := 'Impression Etat ';
  case ChoixEtat of
    Etat2:
      t := t + 'cumul hebdomadaire : semaine ';
    Etat3:
      t := t + 'cumul mensuel : mois ';

    Etat5:
      t := t + 'recapitulatif mensuel general: mois ';
    Etat6:
      t := t + 'recapitulatif mensuel simple: mois ';
    Etat7:
      t := t + 'recapitulatif mensuel chantiers: mois ';
    Etat8:
      t := t + 'recapitulatif mensuel CA: mois ';
  end;

  case ChoixEtat of
    Etat2:
      t := t + inttostr(NumSemaine) + ' ';
  else
    t := t + inttostr(NumMois) + ' ';
  end;

  if ChoixEtat = Etat2 then
    if SemaineBis then
      t := t + 'bis ';

  // choix filtre
  case ChoixFiltre of
    FiltreAgence:
      t := t + 'par agence';
    FiltreCommercial:
      t := t + 'par commercial';
    FiltreClient:
      t := t + 'par client';
  end;

  FormEtats.Caption := t;
end;

procedure ClotureSemaine(NumS: Integer; bis: boolean);
{ execute une cloture des fiches hebdomadaires d'une semaine donnee
  NumS          numero de semaine pour les fiches traitees
  bis           vrai  fiches bis traitees appartenants au mois suivant la semaine traitee
  faux          fiches traitees appartenants au mois de la semainetraitee
}
var
  tempbis: boolean;
  NumM: Integer;
  sd, sf: Integer;
  idsem: Integer;
  i: Integer;
  b: boolean;
begin
  tempbis := bis;
  b := utils.SemaineBis(NumS, AnneeCourante);
  if not b then
    tempbis := false;

  // verrouiller les fiches
  VerrouHebdo(NumS, tempbis, true);

  // edition des fiches correspondant a la semaine
  ImprimeDiamantCoeffON;
  if FormEtats.SemFich.Checked then
    ImprimerHebdoUnique(NumS, tempbis);

  // impression du cumul de la semanine
  ImprimeDiamantCoeffOFF;
  if FormEtats.SemCum.Checked then
    ImprimerCumulHebdo(tempbis, NumS, SansFiltre, 0, true, 0);

  if FormEtats.SemRec.Checked then
  begin
    // preparation impression du recap
    NumM := CalcMoisSemaine(NumS, AnneeCourante, tempbis);
    EcartSemaine(NumM, AnneeCourante, sd, sf);
    for i := sd to sf do
    begin
      // impression seulement de la bonne semaine
      idsem := i - sd + 1;
      if i = NumS then
        SetChoixImpre(idsem, true)
      else
        SetChoixImpre(idsem, false);
    end;

    // pas d'imression des reguls
    SetChoixImpre(0, false);

    // impression du recap sans filtre de la semaine
    ImprimerRecap(false, NumM, SansFiltre, 0);
  end;
end;

procedure ClotureMois(NumM: Integer);
{ execute une cloture des fiches hebdomadaires et reguls d'un mois donne

  NumM          numero de mois pour les fiches traitees
}
begin
  // verrouiller les fiches
  VerrouMois(NumM, true);

  // impression des fiche de regul du mois
  ImprimeDiamantCoeffON;
  if FormEtats.MoiReg.Checked then
    ImprimerHebdoMois(NumM, false, true);

  // impression des cumuls du mois
  ImprimeDiamantCoeffOFF;
  if FormEtats.MoiCumM.Checked then
    ImprimerCumulMois(NumM, SansFiltre, 0, true, 0);

  // impression du cumul des mois deja traites
  ImprimeDiamantCoeffOFF;
  if FormEtats.MoiCumA.Checked then
    ImprimerCumulAnnee(NumM, SansFiltre, 0, true, 0);

  // impression recap chantiers
  if FormEtats.MoiCha.Checked then
    ImprimerRecapChantier(false, NumM, SansFiltre, 0);

  // impression du recap clients du mois
  if FormEtats.MoiCli.Checked then
    ImprimerRecapClients(false, NumM, 0);
end;

procedure ClotureAnnee;
{ execute une cloture des fiches hebdomadaires et reguls de l'annee en cours }
begin
  // impression des cumuls de l'annee
  ImprimeDiamantCoeffOFF;
  if FormEtats.AnnCum.Checked then
    ImprimerCumulAnnee(12, SansFiltre, 0, true, 0);

  // impression du recap clients
  if FormEtats.AnnCli.Checked then
    ImprimerRecapClients(true, 0, 0);
end;

procedure TFormEtats.PrintEtatClick(Sender: TObject);

  function MessageExporte: boolean;
  var
    t: string;
    s: Word;
  begin
    t := 'Voulez vous exporter le recap client ?';
    s := MessageDlg(t, mtConfirmation, [mbYes, mbCancel], 0);

    result := false;
    if s = mrYes then
      result := true;
  end;

  procedure ImprimerRecapClientsCommerciaux;
  var
    nbc: Integer;
    i: Integer;
    idcom: Integer;
  begin
    if not ClientCommercial then
    begin
      // imprimer tous commerciaux confondus
      ImprimerRecapClients(RecapAnnuel, NumMois, 0);
    end
    else
    begin

      // imprimer une liste par commercial
      nbc := LireNombreCommerciaux;

      // ShowMessage('Nombre ce Commerciaux : ' + inttostr(nbc));
      if nbc <> 0 then
      begin
        // pointer sur premier commercial
        CommercialPremier;

        // traiter tous les commerciaux
        for i := 1 to nbc do
        begin
          // lire
          idcom := LireIdentifiantCommercial;
          // ShowMessage('Commercial ' + inttostr(i));
          // imprimer tous commerciaux confondus
          ImprimerRecapClients(RecapAnnuel, NumMois, idcom);

          // commercial suivant
          CommercialSuivant;
        end;
      end;

    end;
  end;

var
  filtre: Integer;
begin

  // choix filtre
  TButton(Sender).Enabled := false;

  case ChoixFiltre of
    FiltreAgence:
      filtre := NumAgence;
    FiltreCommercial:
      filtre := NumCommercial;
    FiltreClient:
      filtre := NumClient;
  else
    filtre := 0;
  end;

  case ChoixEtat of
    hebdosaisis:
      ImprimeDiamantCoeffON;
    hebdosimple:
      ImprimeDiamantCoeffON;
    hebdoecartsemaines:
      ImprimeDiamantCoeffON;
    hebdoecartmois:
      ImprimeDiamantCoeffON;

    Etat2:
      ImprimeDiamantCoeffOFF;
    Etat3:
      ImprimeDiamantCoeffOFF;
    Etat4:
      ImprimeDiamantCoeffOFF;
  end;

  // ShowMessage('debut de l''impression');

  case ChoixEtat of
    hebdosaisis:
      ImprimerHebdoSaisies;

    hebdosimple:
      ImprimerHebdoUnique(HebdoSem, HebdoBis);

    hebdoecartsemaines:
      ImprimerHebdoEcart(HebdoSemDeb, HebdoSemFin);

    hebdoecartmois:
      ImprimerHebdoMois(HebdoMois, HebdoFiches, HebdoReguls);

    Etat2:
      ImprimerCumulHebdo(SemaineBis, NumSemaine, ChoixFiltre, filtre,
        ChoixChantiers, NumChantier);

    Etat3:
      ImprimerCumulMois(NumMois, ChoixFiltre, filtre, ChoixChantiers,
        NumChantier);

    Etat4:
      ImprimerCumulAnnee(NumMois, ChoixFiltre, filtre, ChoixChantiers,
        NumChantier);

    Etat5:
      ImprimerRecap(RecapAnnuel, NumMois, ChoixFiltre, filtre);

    Etat6:
      ImprimerRecapSimple(RecapAnnuel, NumMois, ChoixFiltre, filtre);

    Etat7:
      begin
        if FormBase.Exportation.Checked then
        begin
          if MessageExporte then
            ExporteRecapClients(true, NumMois);
        end
        else
          ImprimerRecapClientsCommerciaux;
      end;

    Etat8:
      ImprimerRecapChantier(RecapAnnuel, NumMois, ChoixFiltre, filtre);

    Etat9:
      ClotureSemaine(NumSemaineCloture, SemaineBisCloture);

    Etat10:
      ClotureMois(NumMoisCloture);

    Etat11:
      ClotureAnnee;
  end;

  RefreshTable(FormBase.TableClients);
  RefreshTable(FormBase.TableSecteurs);
  RefreshTable(FormBase.TableCommerciaux);

  TButton(Sender).Enabled := true;
  self.Close;

end;

{ ----------------------------------------------------------------------------- }
procedure TFormEtats.PageControlEtatsChange(Sender: TObject);
begin
  VerifieOngletTypeEdition;
end;

procedure SelectMoisRecap;
begin
  RecapAnnuel := false;
  FormEtats.ChoixAnnee.Checked := RecapAnnuel;

  // recuperer numero mois
  NumMois := FormEtats.SaisieNumMoisRecap.ItemIndex + 1;

  traiteboutonimprimer;

  // calculer les semaine et si etat 5 6 7 valider les nbj
  CalcSemaine;
end;

procedure TFormEtats.ChoixAnneeClick(Sender: TObject);
begin
  RecapAnnuel := ChoixAnnee.Checked;

  FormEtats.Panel6.Enabled := not RecapAnnuel;

  if not RecapAnnuel then
    SelectMoisRecap;
end;

procedure TFormEtats.SaisieNumMoisRecapClick(Sender: TObject);
begin
  SelectMoisRecap;
end;

function GetChoixImpreSemaine(num: Integer): boolean;
begin
  result := false;
  case num of
    1:
      result := RecapS1;
    2:
      result := RecapS2;
    3:
      result := RecapS3;
    4:
      result := RecapS4;
    5:
      result := RecapS5;
    0:
      result := RecapRegul;
  end;
end;

procedure TFormEtats.ChoixImpre1Click(Sender: TObject);
begin
  SetChoixImpre(1, ChoixImpre1.Checked);
end;

procedure TFormEtats.ChoixImpre2Click(Sender: TObject);
begin
  SetChoixImpre(2, ChoixImpre2.Checked);
end;

procedure TFormEtats.ChoixImpre3Click(Sender: TObject);
begin
  SetChoixImpre(3, ChoixImpre3.Checked);
end;

procedure TFormEtats.ChoixImpre4Click(Sender: TObject);
begin
  SetChoixImpre(4, ChoixImpre4.Checked);
end;

procedure TFormEtats.ChoixImpre5Click(Sender: TObject);
begin
  SetChoixImpre(5, ChoixImpre5.Checked);
end;

procedure TFormEtats.ChoixImpreRegulClick(Sender: TObject);
begin
  SetChoixImpre(0, ChoixImpreRegul.Checked);
end;

procedure TFormEtats.ParCommercialClick(Sender: TObject);
begin
  ClientCommercial := FormEtats.ParCommercial.Checked;
end;

procedure TFormEtats.RecapGeneralClick(Sender: TObject);
begin
  SetRecap(Etat5);
end;

procedure TFormEtats.RecapSimpleClick(Sender: TObject);
begin
  SetRecap(Etat6);
end;

procedure TFormEtats.RecapClientsClick(Sender: TObject);
begin
  SetRecap(Etat7);
end;

procedure TFormEtats.RecapChantiersClick(Sender: TObject);
begin
  SetRecap(Etat8);
end;

procedure TFormEtats.ME1Change(Sender: TObject);
var
  n: Integer;
begin
  n := strtoint(FormEtats.ME1.Text);
  if (n >= 0) and (n <= 7) then
    nbj1 := n
  else
    FormEtats.ME1.Text := inttostr(nbj1);
end;

procedure TFormEtats.ME2Change(Sender: TObject);
var
  n: Integer;
begin
  n := strtoint(FormEtats.ME2.Text);
  if (n >= 0) and (n <= 7) then
    nbj2 := n
  else
    FormEtats.ME2.Text := inttostr(nbj2);
end;

procedure TFormEtats.ME3Change(Sender: TObject);
var
  n: Integer;
begin
  n := strtoint(FormEtats.ME3.Text);
  if (n >= 0) and (n <= 7) then
    nbj3 := n
  else
    FormEtats.ME3.Text := inttostr(nbj3);
end;

procedure TFormEtats.ME4Change(Sender: TObject);
var
  n: Integer;
begin
  n := strtoint(FormEtats.ME4.Text);
  if (n >= 0) and (n <= 7) then
    nbj4 := n
  else
    FormEtats.ME4.Text := inttostr(nbj4);
end;

procedure TFormEtats.ME5Change(Sender: TObject);
var
  n: Integer;
begin
  n := strtoint(FormEtats.ME5.Text);
  if (n >= 0) and (n <= 7) then
    nbj5 := n
  else
    FormEtats.ME5.Text := inttostr(nbj5);
end;

procedure TFormEtats.HebdoSaisiesClick(Sender: TObject);
begin
  SetHebdo(hebdosaisis);
end;

procedure TFormEtats.HebdoUniqueClick(Sender: TObject);
begin
  SetHebdo(hebdosimple);
end;

procedure TFormEtats.HebdoSemaineClick(Sender: TObject);
begin
  SetHebdo(hebdoecartsemaines);
end;

procedure TFormEtats.HebdoRegulClick(Sender: TObject);
begin
  SetHebdo(hebdoecartmois);
end;

procedure TFormEtats.CBFichesClick(Sender: TObject);
begin
  HebdoFiches := CBFiches.Checked;

  traiteboutonimprimer;
end;

procedure TFormEtats.CBRegulsClick(Sender: TObject);
begin
  HebdoReguls := CBReguls.Checked;

  traiteboutonimprimer;
end;

procedure TFormEtats.SaisieNumMoisClick(Sender: TObject);
begin
  HebdoMois := FormEtats.SaisieNumMois.ItemIndex + 1;

  traiteboutonimprimer;
end;

procedure verifecartnumsemaine;

  function FormatSemaine(num: Integer): string;
  begin
    result := inttostr(num);
    if num < 10 then
      result := '0' + result;
  end;

  procedure restituernumsemaines;
  begin
    // restituer numero semaine debut sauv�
    FormEtats.saisieNumDeb.Text := FormatSemaine(HebdoSemDeb);
    FormEtats.saisieNumFin.Text := FormatSemaine(HebdoSemFin);
  end;

var
  td: string;
  tf: string;
  nd: Integer;
  nf: Integer;
begin
  td := FormEtats.saisieNumDeb.Text;
  tf := FormEtats.saisieNumFin.Text;
  nd := strtoint(td);
  nf := strtoint(tf);
  if (nd > 0) and (nd < 53) and (nf > 0) and (nf < 53) and (nd <= nf) then
  begin
    // stocker numeros semaine
    HebdoSemDeb := nd;
    HebdoSemFin := nf;
  end
  else
    restituernumsemaines;

  traiteboutonimprimer;
end;

procedure TFormEtats.saisieNumDebChange(Sender: TObject);
begin
  verifecartnumsemaine;
end;

procedure TFormEtats.saisieNumDebClick(Sender: TObject);
begin
  saisieNumDeb.SelectAll;
end;

procedure TFormEtats.saisieNumDebKeyPress(Sender: TObject; var Key: Char);
begin
  // sortie si chiffre
  if VerifNum(Key) then
    exit;

  // sinon annuler
  Key := chr(0);
end;

procedure TFormEtats.saisieNumFinChange(Sender: TObject);
begin
  verifecartnumsemaine;
end;

procedure TFormEtats.saisieNumFinClick(Sender: TObject);
begin
  saisieNumFin.SelectAll;
end;

procedure TFormEtats.saisieNumFinKeyPress(Sender: TObject; var Key: Char);
begin
  // sortie si chiffre
  if VerifNum(Key) then
    exit;

  // sinon annuler
  Key := chr(0);
end;

procedure verifnumsemaineunique;

  function FormatSemaine(num: Integer): string;
  begin
    result := inttostr(num);
    if num < 10 then
      result := '0' + result;
  end;

var
  t: string;
  n: Integer;
begin
  t := FormEtats.SaisieNum.Text;
  n := strtoint(t);
  if (n > 0) and (n < 53) then
    HebdoSem := n
  else
    FormEtats.saisieNumDeb.Text := FormatSemaine(HebdoSem);
end;

procedure TFormEtats.SaisieNumChange(Sender: TObject);
begin
  verifnumsemaineunique;

  traiteboutonimprimer;
end;

procedure TFormEtats.SaisieNumClick(Sender: TObject);
begin
  SaisieNum.SelectAll;
end;

procedure TFormEtats.SaisieNumKeyPress(Sender: TObject; var Key: Char);
begin
  // sortie si chiffre
  if VerifNum(Key) then
    exit;

  // sinon annuler
  Key := chr(0);
end;

procedure TFormEtats.UniqueBisClick(Sender: TObject);
begin
  HebdoBis := FormEtats.UniqueBis.Checked;

  traiteboutonimprimer;
end;

procedure TFormEtats.ChoixClotureHebdoClick(Sender: TObject);
begin
  SetCloture(Etat9);
end;

procedure TFormEtats.ChoixClotureMoisClick(Sender: TObject);
begin
  SetCloture(Etat10);
end;

procedure TFormEtats.ChoixClotureAnneeClick(Sender: TObject);
begin
  SetCloture(Etat11);
end;

procedure verifnumsemainecloture;
var
  t: string;
  n: Integer;
begin
  t := FormEtats.saisieClotureSemaine.Text;
  n := strtoint(t);
  if (n > 0) and (n < 53) then
    // stocker numero semaine
    NumSemaineCloture := n
  else
  begin
    // restituer numero semaine sauv�
    t := inttostr(NumSemaineCloture);
    if NumSemaineCloture < 10 then
      t := '0' + t;
    FormEtats.saisieClotureSemaine.Text := t;
  end;
end;

procedure TFormEtats.saisieClotureSemaineChange(Sender: TObject);
begin
  verifnumsemainecloture;

  traiteboutonimprimer;
end;

procedure TFormEtats.saisieClotureSemaineClick(Sender: TObject);
begin
  saisieClotureSemaine.SelectAll;
end;

procedure TFormEtats.saisieClotureSemaineKeyPress(Sender: TObject;
  var Key: Char);
begin
  // sortie si chiffre
  if VerifNum(Key) then
    exit;

  // sinon annuler
  Key := chr(0);
end;

procedure TFormEtats.SaisieClotureBisClick(Sender: TObject);
begin
  SemaineBisCloture := FormEtats.SaisieClotureBis.Checked;

  traiteboutonimprimer;
end;

procedure TFormEtats.saisieClotureMoisClick(Sender: TObject);
begin
  NumMoisCloture := FormEtats.saisieClotureMois.ItemIndex + 1;

  traiteboutonimprimer;
end;

procedure TFormEtats.ListeSecteursKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  // showmessage('key reussi')
end;

procedure TFormEtats.ListeSecteursEnter(Sender: TObject);
begin
  // showmessage('enter reussi')
end;

procedure TFormEtats.ListeSecteursMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  // showmessage('mouse reussi')
end;

end.
