unit MasqueEmployes;

interface

procedure PointageCumulsReset;
procedure CumulsPointage(idh: integer);
procedure PointageCumulsLire(var nbr: integer);
procedure PointageViderListe;
procedure PointageInitMasque;
procedure PointageExecuterCalcul;
function PointageLireCalcul(inter: boolean): currency;
function PointageLireSomme: currency;
procedure PointageEnreg(numhebdo: integer);
procedure PointageCharge(index: integer);
procedure PointageChargeChaines(index: integer; var nbrec: integer);
procedure PointageSupprime(index: integer);
procedure PointageModifsNon;
procedure PointageModifsOui;
function PointageLireModifsEtat: boolean;

function traitePointageLireFormatCellules(ACol: integer): string;
procedure traitepointagesouris(inter: boolean);
procedure traitePointageInitialiserProcEffaceLigne(inter: boolean;
  X, Y: integer);
function TraitePointageSelection(inter: boolean; ACol, ARow: integer): boolean;
procedure TraitePointageValide(inter: boolean; var Key: Char);
procedure traitepointagetouches(inter: boolean; Key: word);
procedure traitepointagetouchesFIN();
procedure TraitePointageChargeLigne;
procedure TraitePointageEffacerLigne(inter: boolean);

implementation

uses

  SysUtils, grids, Dialogs, windows,
  winbase, accesBDD, Hebdo, impression;

const

  colindex = 0;
  colNom = 0;
  coltauxhoraire = 1;
  colnombrejours = 2;
  colheuresroute = 3;
  colheuresjour = 4;
  colheuressamedi = 5;
  colheuresnuitcoldimanche = 6;
  colpetitpagnier = 7;
  colgrandpagnier = 8;
  colsommepointage = 9;

  coldeselection = 10;
  colautoselection = 2;

type

  TlargeurPointage = array [colNom .. colsommepointage] of integer;

const

  // Largeur: TlargeurPointage
  // = (160, 40, 30, 30, 30, 30, 30, 15, 15, 50);
  Largeur: TlargeurPointage = (180, 40, 40, 40, 40, 40, 40, 40, 40, 60);

var

  TblNomPNT: array of array of string;
  TblValPNT: array of array of currency;
  nbPNT: integer;

  positioncolonne: integer = 1;
  positionligne: integer;
  LigneEnCours: boolean = true;
  TableEmployes: array of array of currency;
  TableInterimaires: array of array of currency;
  CelluleEnCours: boolean;
  EditionEnCours: boolean;
  CelluleEnCoursInter: boolean;
  CelluleEnCoursCol: integer;
  CelluleEnCoursRow: integer;
  Suppressionencours: boolean;
  ModeLigneAppend: boolean;

  ModifsEtat: boolean;
  AutoriseModifs: boolean;

  SommeEMP: currency;
  SommeINT: currency;
  SommeNombre: currency;

  /// ///////////////////////////////////////////////////////////////////////////
procedure unselectEMP;
begin
  Suppressionencours := true;
  FormBase.ListePointageEMP.Col := coldeselection;
  FormBase.ListePointageEMP.Row := 0;
  Suppressionencours := false;
end;

procedure unselectINT;
begin
  Suppressionencours := true;
  FormBase.ListePointageINT.Col := coldeselection;
  FormBase.ListePointageINT.Row := 0;
  Suppressionencours := false;
end;

procedure PointageModifsOui;
begin
  AutoriseModifs := true;
  FormBase.ListeEmployes.Enabled := true;
end;

procedure PointageModifsNon;
begin
  unselectINT;
  unselectEMP;
  AutoriseModifs := false;
  FormBase.ListeEmployes.Enabled := false;
  FormBase.ListeEmployes.KeyValue := -1;
end;

function PointageLireModifsEtat: boolean;
begin
  result := ModifsEtat;
end;

procedure SetModifsEtat;
begin
  ModifsEtat := true;

  // prevenir qu'il y a eu une modif d'un masque
  EvenementModifMasqueAnnexe;
end;

procedure ResetModifsEtat;
begin
  ModifsEtat := false;
end;

procedure PointageViderListe;
{ effacement complet de la grille de saisie et de ses donnees }
  procedure viderligne(inter: boolean; fin: integer);
  var
    C: integer;
  begin
    if inter then
      for C := colindex to colsommepointage do
        FormBase.ListePointageINT.Cells[C, fin - 1] := ''
    else
      for C := colindex to colsommepointage do
        FormBase.ListePointageEMP.Cells[C, fin - 1] := '';
  end;

begin
  // pour ne pas repasser par le traitement evenement masque
  LigneEnCours := true;

  while FormBase.ListePointageINT.RowCount > 1 do
  begin
    positioncolonne := 0;
    positionligne := FormBase.ListePointageINT.RowCount - 2;
    TraitePointageEffacerLigne(true);
  end;

  while FormBase.ListePointageEMP.RowCount > 1 do
  begin
    positioncolonne := 0;
    positionligne := FormBase.ListePointageEMP.RowCount - 2;
    TraitePointageEffacerLigne(false);
  end;

  positioncolonne := 1;
  LigneEnCours := false;
end;

procedure PointageInitMasque;
{ routine preparant le format d'affichage de la grille de saisie }
var
  i: integer;
begin
  for i := colindex to colsommepointage do
  begin
    FormBase.ListePointageINT.ColWidths[i] := Largeur[i];
    FormBase.ListePointageEMP.ColWidths[i] := Largeur[i];
  end;

  FormBase.ListePointageEMP.ColWidths[coldeselection] := 0;
  FormBase.ListePointageINT.ColWidths[coldeselection] := 0;

  unselectINT;
  unselectEMP;
end;

function VerifMaxiOK(ACol: integer; valeur: currency): boolean;
begin
  result := false;
  case ACol of
    2, 3, 5, 6:
      if (valeur < -999.9) or (valeur > 999.9) then
        exit;
    4:
      if (valeur < -9999.9) or (valeur > 9999.9) then
        exit;
    7, 8:
      if (valeur < -999) or (valeur > 999) then
        exit;
  end;
  result := true;
end;

function traitePointageLireFormatCellules(ACol: integer): string;
{ routine renvoyant le masque de saisie suivant une colonne donnee }
begin
  case ACol of
    0:
      result := '';
    1:
      result := '999,99';
    2, 3, 4, 5, 6:
      result := '';
    7, 8:
      result := '999';
    9:
      result := '#####,##';
  end;
end;

function FormatSortie(Col: integer): string;
{ routine renvoyant le masque le format de sortie suivant une colonne donnee }
begin
  case Col of
    coltauxhoraire:
      result := '00.00';
    colnombrejours:
      result := '00.00';
    colheuresroute:
      result := '00.0';
    colheuresjour:
      result := '00.0';
    colheuressamedi:
      result := '00.0';
    colheuresnuitcoldimanche:
      result := '00.0';
    // colpetitpagnier:          result := '0';
    colpetitpagnier:
      result := '00';
    // colgrandpagnier:          result := '0';
    colgrandpagnier:
      result := '00';
    colsommepointage:
      result := '00000.00';
  else
    result := '00.00';
  end;
end;

Procedure RechargerCellule(inter: boolean; Col, lig: integer);
var
  valeur: currency;
  temp: string;
begin
  if inter then
  begin
    valeur := TableInterimaires[Col, lig];
    temp := FormatCurr(FormatSortie(Col), valeur);
    FormBase.ListePointageINT.Cells[Col, lig] := temp;
  end
  else
  begin
    valeur := TableEmployes[Col, lig];
    temp := FormatCurr(FormatSortie(Col), valeur);
    FormBase.ListePointageEMP.Cells[Col, lig] := temp;
  end;
end;

function PointageLireCalcul(inter: boolean): currency;
begin
  if inter then
    result := SommeINT
  else
    result := SommeEMP;
end;

function PointageLireSomme: currency;
begin
  result := SommeNombre
end;

procedure PointageCalculINT(lig: integer);
{ execute le calcul dans la grille de saisie pour une ligne donnee }
var
  G, D, H, i, J, L, Somme: currency;
  temp: string;
begin
  G := TableInterimaires[colheuresjour, lig];
  D := TableInterimaires[coltauxhoraire, lig];
  H := TableInterimaires[colheuressamedi, lig];
  i := TableInterimaires[colheuresnuitcoldimanche, lig];
  J := TableInterimaires[colpetitpagnier, lig];
  L := TableInterimaires[colgrandpagnier, lig];

  Somme := (G * D);
  Somme := Somme + (H * D * TauxHoraireSup);
  Somme := Somme + (i * D) * TauxHoraireSupInt;
  Somme := Somme + (J * CoutPetitPagnier);
  // Somme := Somme * TauxChargeBrut;
  Somme := Somme + (L * CoutGrandPagnier);

  // somme du pointage
  TableInterimaires[colsommepointage, lig] := Somme;
  temp := FormatCurr(FormatSortie(colsommepointage), Somme);
  FormBase.ListePointageINT.Cells[colsommepointage, lig] := temp;
end;

procedure PointageCalculEMP(lig: integer);
{ execute le calcul dans la grille de saisie pour une colonne donnee }
var
  G, D, H, i, J, L, Somme: currency;
  temp: string;
begin
  G := TableEmployes[colheuresjour, lig];
  D := TableEmployes[coltauxhoraire, lig];
  H := TableEmployes[colheuressamedi, lig];
  i := TableEmployes[colheuresnuitcoldimanche, lig];
  J := TableEmployes[colpetitpagnier, lig];
  L := TableEmployes[colgrandpagnier, lig];

  Somme := (G * D);
  Somme := Somme + (H * D * TauxHoraireSup);
  Somme := Somme + (i * D);
  Somme := Somme + (J * CoutPetitPagnier);
  Somme := Somme * TauxChargeBrut;
  Somme := Somme + (L * CoutGrandPagnier);

  // somme du pointage
  TableEmployes[colsommepointage, lig] := Somme;
  temp := FormatCurr(FormatSortie(colsommepointage), Somme);
  FormBase.ListePointageEMP.Cells[colsommepointage, lig] := temp;
end;

procedure PointageExecuterCalcul;
{ execute le calcul general de la grille de saisie et relance les calculs
  generaux de la fiche }
var
  nbl: integer;
  t: currency;
  i: integer;
begin
  t := 0;
  nbl := FormBase.ListePointageEMP.RowCount - 1;
  if nbl >= 0 then
  begin
    for i := 0 to (nbl - 1) do
      t := t + TableEmployes[colsommepointage, i];
  end;

  SommeEMP := t;

  t := 0;
  nbl := FormBase.ListePointageINT.RowCount - 1;
  if nbl >= 0 then
  begin
    for i := 0 to (nbl - 1) do
      t := t + TableInterimaires[colsommepointage, i];
  end;

  SommeINT := t;

  // lance le calcul generalde la fiche Hebdo;
  CalculGeneralHebdo;
end;

function CurrToInt(valeur: currency): integer;
begin
  result := StrToInt(CurrToStr(valeur));
end;

function IntToCurr(valeur: integer): currency;
begin
  result := StrToCurr(IntToStr(valeur));
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure PointageEnreg(numhebdo: integer);
{ enregistre dans la base de donnees le contenu de la grille de saisie }
var
  nbe: integer;
  nbi: integer;
  C: currency;
  L: integer;
  s: string;

  procedure ChercheEnreg(inter: boolean; ligne: integer);
  var
    C: currency;
    id: integer;
  begin
    if inter then
      C := TableInterimaires[colindex, ligne]
    else
      C := TableEmployes[colindex, ligne];

    id := CurrToInt(C);
    FormBase.TabFichPoint.editkey;
    FormBase.TabFichPoint.FieldByName('id_pointage').asinteger := id;
    FormBase.TabFichPoint.gotokey;
    FormBase.TabFichPoint.edit;
  end;

begin
  // supprimer les enregs precedents
  PointageSupprime(numhebdo);
  ModeLigneAppend := false;

  nbe := FormBase.ListePointageEMP.RowCount - 1;
  nbi := FormBase.ListePointageINT.RowCount - 1;
  if (nbe = 0) and (nbi = 0) then
    exit;

  // pas de mofifs en cours
  ResetModifsEtat;

  with FormBase.TabFichPoint do
  begin
    Open;

    if nbe <> 0 then
    begin
      // creer chaque ligne et la remplir
      for L := 0 to (nbe - 1) do
      begin
        if ModeLigneAppend = false then
          // creer nouveau enregistrement de ligne
          insert
        else
          // chercher enreg existant
          ChercheEnreg(false, L);

        s := FormBase.ListePointageEMP.Cells[colNom, L];
        FieldByName('emp_enr_nomemploye').AsString := s;

        C := TableEmployes[coltauxhoraire, L];
        FieldByName('emp_enr_tauxhoraire').Ascurrency := C;

        FieldByName('emp_enr_interimaire').AsBoolean := false;
        FieldByName('emp_enr_actuel').AsBoolean := true;
        FieldByName('emp_enr_petitpannier').Ascurrency := CoutPetitPagnier;
        FieldByName('emp_enr_grandpannier').Ascurrency := CoutGrandPagnier;
        FieldByName('emp_enr_tauxheuresup').Ascurrency := TauxHoraireSup;
        FieldByName('emp_enr_facteurcharge').Ascurrency := TauxChargeBrut;

        C := TableEmployes[colnombrejours, L];
        FieldByName('emp_nbjourtravail').Ascurrency := C;

        C := TableEmployes[colheuresroute, L];
        FieldByName('emp_heuresroute').Ascurrency := C;

        C := TableEmployes[colheuresjour, L];
        FieldByName('emp_heuresjour').Ascurrency := C;

        C := TableEmployes[colheuressamedi, L];
        FieldByName('emp_heuressamedi').Ascurrency := C;

        C := TableEmployes[colheuresnuitcoldimanche, L];
        FieldByName('emp_heuresnuitdimanche').Ascurrency := C;

        C := TableEmployes[colpetitpagnier, L];
        FieldByName('emp_petitdeplacement').Ascurrency := C;

        C := TableEmployes[colgrandpagnier, L];
        FieldByName('emp_granddeplacement').Ascurrency := C;

        C := TableEmployes[colsommepointage, L];
        FieldByName('emp_total').Ascurrency := C;

        FieldByName('id_hebdo').asinteger := numhebdo;

        // valider nouvelle ligne avec ces valeurs
        post;
      end;
    end;

    if nbi <> 0 then
    begin
      for L := 0 to (nbi - 1) do
      begin
        if ModeLigneAppend = false then
          // creer nouveau enregistrement de ligne
          insert
        else
          // modifier enreg existant
          ChercheEnreg(true, L);

        s := FormBase.ListePointageINT.Cells[colNom, L];
        FieldByName('emp_enr_nomemploye').AsString := s;

        C := TableInterimaires[coltauxhoraire, L];
        FieldByName('emp_enr_tauxhoraire').Ascurrency := C;

        FieldByName('emp_enr_interimaire').AsBoolean := true;
        FieldByName('emp_enr_actuel').AsBoolean := true;
        FieldByName('emp_enr_petitpannier').Ascurrency := CoutPetitPagnier;
        FieldByName('emp_enr_grandpannier').Ascurrency := CoutGrandPagnier;
        FieldByName('emp_enr_tauxheuresup').Ascurrency := TauxHoraireSup;
        FieldByName('emp_enr_facteurcharge').Ascurrency := TauxChargeBrut;

        C := TableInterimaires[colnombrejours, L];
        FieldByName('emp_nbjourtravail').Ascurrency := C;

        C := TableInterimaires[colheuresroute, L];
        FieldByName('emp_heuresroute').Ascurrency := C;

        C := TableInterimaires[colheuresjour, L];
        FieldByName('emp_heuresjour').Ascurrency := C;

        C := TableInterimaires[colheuressamedi, L];
        FieldByName('emp_heuressamedi').Ascurrency := C;

        C := TableInterimaires[colheuresnuitcoldimanche, L];
        FieldByName('emp_heuresnuitdimanche').Ascurrency := C;

        C := TableInterimaires[colpetitpagnier, L];
        FieldByName('emp_petitdeplacement').Ascurrency := C;

        C := TableInterimaires[colgrandpagnier, L];
        FieldByName('emp_granddeplacement').Ascurrency := C;

        C := TableInterimaires[colsommepointage, L];
        FieldByName('emp_total').Ascurrency := C;

        FieldByName('id_hebdo').asinteger := numhebdo;

        // valider nouvelle ligne avec ces valeurs
        post;
      end;
    end;

    // fermeture de la table apres ajouts
    close;
  end;

  // sortie du mode de modification
  ModeLigneAppend := false;
end;

procedure ChargeLigneExiste;
{ execute le chargement a partir de la BDD du contenu de la grille de saisie }
var
  nc: integer;
  nblig: integer;

  idemp: integer;
  inter: boolean;
  monaie: currency;
  valeur: currency;
  nomemp: string;

  procedure Ecrire(inter: boolean; Col, lig: integer; valeur: currency);
  var
    t: string;
  begin
    t := FormatCurr(FormatSortie(Col), valeur);
    if inter then
    begin
      TableInterimaires[Col, lig] := valeur;
      FormBase.ListePointageINT.Cells[Col, lig] := t;
    end
    else
    begin
      TableEmployes[Col, lig] := valeur;
      FormBase.ListePointageEMP.Cells[Col, lig] := t;
    end
  end;

begin
  with FormBase.TabFichPoint do
  begin
    // verifie la liste a mettre a jour
    inter := FieldByName('emp_enr_interimaire').AsBoolean;

    CoutPetitPagnier := FieldByName('emp_enr_petitpannier').Ascurrency;
    CoutGrandPagnier := FieldByName('emp_enr_grandpannier').Ascurrency;
    TauxHoraireSup := FieldByName('emp_enr_tauxheuresup').Ascurrency;
    TauxChargeBrut := FieldByName('emp_enr_facteurcharge').Ascurrency;

    // definition de la ligne de pointage
    if inter then
      nblig := FormBase.ListePointageINT.RowCount - 1
    else
      nblig := FormBase.ListePointageEMP.RowCount - 1;

    if inter then
    begin
      SetLength(TableInterimaires, 10, nblig + 1);

      idemp := FieldByName('id_pointage').asinteger;
      TableInterimaires[colindex, nblig] := IntToCurr(idemp);

      nomemp := FieldByName('emp_enr_nomemploye').AsString;
      FormBase.ListePointageINT.Cells[colNom, nblig] := nomemp;

      // debut chargement liste ecran
      LigneEnCours := true;

      monaie := FieldByName('emp_enr_tauxhoraire').Ascurrency;
      Ecrire(inter, coltauxhoraire, nblig, monaie);

      valeur := FieldByName('emp_nbjourtravail').Ascurrency;
      Ecrire(inter, colnombrejours, nblig, valeur);

      valeur := FieldByName('emp_heuresroute').Ascurrency;
      Ecrire(inter, colheuresroute, nblig, valeur);

      valeur := FieldByName('emp_heuresjour').Ascurrency;
      Ecrire(inter, colheuresjour, nblig, valeur);

      valeur := FieldByName('emp_heuressamedi').Ascurrency;
      Ecrire(inter, colheuressamedi, nblig, valeur);

      valeur := FieldByName('emp_heuresnuitdimanche').Ascurrency;
      Ecrire(inter, colheuresnuitcoldimanche, nblig, valeur);

      valeur := FieldByName('emp_petitdeplacement').Ascurrency;
      Ecrire(inter, colpetitpagnier, nblig, valeur);

      valeur := FieldByName('emp_granddeplacement').Ascurrency;
      Ecrire(inter, colgrandpagnier, nblig, valeur);

      // calculer la ligne
      PointageCalculINT(nblig);

      // fin chargement liste ecran
      LigneEnCours := false;

      // defintion de la prochaine ligne
      nc := FormBase.ListePointageINT.RowCount;
      inc(nc);
      FormBase.ListePointageINT.RowCount := nc;

      // selection de la 1ere colonne a editer
      FormBase.ListePointageINT.SetFocus;
      FormBase.ListePointageINT.Col := colnombrejours;
      FormBase.ListePointageINT.Row := nblig;
    end
    else
    begin
      SetLength(TableEmployes, 10, nblig + 1);

      idemp := FieldByName('id_pointage').asinteger;
      TableEmployes[colindex, nblig] := IntToCurr(idemp);

      nomemp := FieldByName('emp_enr_nomemploye').AsString;
      FormBase.ListePointageEMP.Cells[colNom, nblig] := nomemp;

      // debut chargement liste ecran
      LigneEnCours := true;

      monaie := FieldByName('emp_enr_tauxhoraire').Ascurrency;
      Ecrire(inter, coltauxhoraire, nblig, monaie);

      valeur := FieldByName('emp_nbjourtravail').Ascurrency;
      Ecrire(inter, colnombrejours, nblig, valeur);

      valeur := FieldByName('emp_heuresroute').Ascurrency;
      Ecrire(inter, colheuresroute, nblig, valeur);

      valeur := FieldByName('emp_heuresjour').Ascurrency;
      Ecrire(inter, colheuresjour, nblig, valeur);

      valeur := FieldByName('emp_heuressamedi').Ascurrency;
      Ecrire(inter, colheuressamedi, nblig, valeur);

      valeur := FieldByName('emp_heuresnuitdimanche').Ascurrency;
      Ecrire(inter, colheuresnuitcoldimanche, nblig, valeur);

      valeur := FieldByName('emp_petitdeplacement').Ascurrency;
      Ecrire(inter, colpetitpagnier, nblig, valeur);

      valeur := FieldByName('emp_granddeplacement').Ascurrency;
      Ecrire(inter, colgrandpagnier, nblig, valeur);

      // calculer la ligne
      PointageCalculEMP(nblig);

      // fin chargement liste ecran
      LigneEnCours := false;

      // defintion de la prochaine ligne
      nc := FormBase.ListePointageEMP.RowCount;
      inc(nc);
      FormBase.ListePointageEMP.RowCount := nc;

      // selection de la 1ere colonne a editer
      FormBase.ListePointageEMP.SetFocus;
      FormBase.ListePointageEMP.Col := colnombrejours;
      FormBase.ListePointageEMP.Row := nblig;
    end;
  end;
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure PointageCharge(index: integer);
{ charge a partir de la base de donnees le contenu de la grille de saisie }
var
  nbrec: integer;
  t: string;
  nblig: integer;
begin
  ChargeConfiguration;

  // pas de mofifs en cours
  ResetModifsEtat;

  // definition des lignes de pointage
  FormBase.ListePointageINT.RowCount := 1;
  FormBase.ListePointageEMP.RowCount := 1;

  with FormBase.TabFichPoint do
  begin
    // recherche des enreg contenant 'index'

    // preparer le filtre
    str(index, t);
    filter := 'id_hebdo = ' + t;
    filtered := true;

    Open;
    setkey;

    nbrec := recordcount;
    if nbrec <> 0 then
    begin
      // entree dans le mode de modification
      ModeLigneAppend := true;
      edit;

      for nblig := 0 to (nbrec - 1) do
      begin
        // se positionner sur 1er enreg ou sur suivant
        if nblig = 0 then
          first
        else
          next;

        // charger liste de pointage
        ChargeLigneExiste;
      end;
    end;

    // termine la lecture de la table
    close;
    filtered := false;

    // recalcul tout
    PointageExecuterCalcul;
  end;
end;

procedure GetEnregEmployeCourant(var i: integer; var a: boolean; var t: boolean;
  var m: currency; var n: string);
begin
  with FormBase.TableEmployes do
  begin
    edit;
    i := FieldValues['id_employe'];
    a := FieldByName('emp_actuel').AsBoolean;
    t := FieldByName('emp_interimaire').AsBoolean;
    m := FieldByName('emp_tauxhoraire').Ascurrency;
    n := FieldByName('emp_nomemploye').AsString;
    post;
  end;
end;

procedure TraitePointageChargeLigne;
{ gere l'insertion d'une ligne dans la grille de saisie }
var
  nc: integer;
  nblig: integer;

  idemp: integer;
  inter: boolean;
  monaie: currency;
  actuel: boolean;
  nomemp: string;

  function ChercheExiste(inter: boolean; nblig: integer;
    nomemp: string): boolean;
  var // idemp: integer
    i: integer;
    t: string;
  begin
    result := false;
    for i := 0 to nblig - 1 do
    begin
      if inter then
        // c := TableInterimaires[colindex, I]
        t := FormBase.ListePointageINT.Cells[colNom, i]
      else
        // c := TableEmployes[colindex, I];
        t := FormBase.ListePointageEMP.Cells[colNom, i];

      // if idemp = StrToInt(CurrToStr(c)) then result := true;
      if nomemp = trim(t) then
        result := true;
    end;
  end;

  procedure Ecrire(inter: boolean; Col, lig: integer; valeur: currency);
  var
    t: string;
  begin
    t := FormatCurr(FormatSortie(Col), valeur);
    if inter then
    begin
      TableInterimaires[Col, lig] := valeur;
      FormBase.ListePointageINT.Cells[Col, lig] := t;
    end
    else
    begin
      TableEmployes[Col, lig] := valeur;
      FormBase.ListePointageEMP.Cells[Col, lig] := t;
    end
  end;

begin
  // inclure un employ� dans la liste de pointage

  if FormBase.TableEmployes.recordcount = 0 then
    exit;

  // modifs en cours
  SetModifsEtat;

  // lire l'enreg correspondant de l'employe
  GetEnregEmployeCourant(idemp, actuel, inter, monaie, nomemp);

  ChargeConfiguration;

  // definition de la ligne de pointage
  if inter then
    nblig := FormBase.ListePointageINT.RowCount - 1
  else
    nblig := FormBase.ListePointageEMP.RowCount - 1;

  // verifier si nb maxi de lignes atteint
  { if inter then
    begin
    if nblig = nbligint then
    exit;
    end
    else
    begin
    if nblig = nbligemp then
    exit;
    end;
  }
  if nblig <> 0 then
  begin
    // verifier si le nom existe dans liste alors ne pas charger
    if ChercheExiste(inter, nblig, nomemp) then // idemp
      exit;
  end;

  if inter then
  begin
    SetLength(TableInterimaires, 10, nblig + 1);

    TableInterimaires[colindex, nblig] := IntToCurr(idemp);
    FormBase.ListePointageINT.Cells[colNom, nblig] := nomemp;

    // debut chargement liste ecran
    LigneEnCours := true;

    Ecrire(inter, coltauxhoraire, nblig, monaie);
    for nc := colnombrejours to colsommepointage do
      Ecrire(inter, nc, nblig, 0);

    // calculer la ligne
    PointageCalculINT(nblig);

    // fin chargement liste ecran
    LigneEnCours := false;

    // defintion de la prochaine ligne
    nc := FormBase.ListePointageINT.RowCount;
    inc(nc);
    FormBase.ListePointageINT.RowCount := nc;

    // selection de la 1ere colonne a editer
    FormBase.ListePointageINT.SetFocus;
    FormBase.ListePointageINT.Col := colnombrejours;
    FormBase.ListePointageINT.Row := nblig;
  end
  else
  begin
    SetLength(TableEmployes, 10, nblig + 1);

    TableEmployes[colindex, nblig] := IntToCurr(idemp);
    FormBase.ListePointageEMP.Cells[colNom, nblig] := nomemp;

    LigneEnCours := true; // debut chargement liste ecran

    Ecrire(inter, coltauxhoraire, nblig, monaie);
    for nc := colnombrejours to colsommepointage do
      Ecrire(inter, nc, nblig, 0);

    // calculer la ligne
    PointageCalculEMP(nblig);

    LigneEnCours := false; // fin chargement liste ecran

    // defintion de la prochaine ligne
    nc := FormBase.ListePointageEMP.RowCount;
    inc(nc);
    FormBase.ListePointageEMP.RowCount := nc;

    // selection de la 1ere colonne a editer
    FormBase.ListePointageEMP.SetFocus;
    FormBase.ListePointageEMP.Col := colnombrejours;
    FormBase.ListePointageEMP.Row := nblig;
  end;

  // recalcul tout
  PointageExecuterCalcul;
end;

function TraitePointageSelection(inter: boolean; ACol, ARow: integer): boolean;
{ gere la selection d'une cellule dans la grille de saisie }
var
  OK: boolean;
  i: integer;
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    result := false;
    exit;
  end;

  result := true;
  OK := true;

  // utile pour la suppression de la derniere ligne
  if Suppressionencours and (ACol = 10) then
    exit;

  if inter then
  begin
    // verifier si depassement nbr de lignes
    i := FormBase.ListePointageINT.RowCount - 1;
    if ARow >= i then
      OK := false;

    if (ACol > coltauxhoraire) and (ACol <= colgrandpagnier) and OK then
    begin
      // autoriser l'edition de la cellule
      with FormBase.ListePointageINT do
        options := options + [goEditing];

      RechargerCellule(inter, ACol, ARow);
    end
    else
    begin
      // interdire l'edition et la selection de la cellule
      with FormBase.ListePointageINT do
        options := options - [goEditing];
      result := false;
      exit;
    end;
  end
  else
  begin
    // verifier si depassement nbr de lignes
    i := FormBase.ListePointageEMP.RowCount - 1;
    if ARow >= i then
      OK := false;

    if (ACol > coltauxhoraire) and (ACol <= colgrandpagnier) and OK then
    begin
      // autoriser l'edition de la cellule
      with FormBase.ListePointageEMP do
        options := options + [goEditing];

      RechargerCellule(inter, ACol, ARow);
    end
    else
    begin
      // interdire l'edition et la selection de la cellule
      with FormBase.ListePointageEMP do
        options := options - [goEditing];
      result := false;
      exit;
    end;
  end;
end;

procedure TraitePointageValide(inter: boolean; var Key: Char);
{ gere les entrees au clavier et la validation d'une valeur dans la grille de
  saisie }
var
  ACol: integer;
  ARow: integer;
  temp: string;
  valeur: currency;
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    Key := chr(0);
    exit;
  end;

  if (Key = ' ') then
  begin
    // ne traite pas ces touches
    Key := chr(0);
    exit;
  end;

  if (Key = '.') then
  begin;
    // modifs en cours
    SetModifsEtat;

    // pour faciliter la saisie des nombres decimaux
    Key := ',';

    exit;
  end;

  // valide la saisie
  if Key = chr(13) then
  begin
    // modifs en cours
    SetModifsEtat;

    if inter then
    begin
      // Stocker la saisie entree au clavier
      ACol := FormBase.ListePointageINT.Col;
      ARow := FormBase.ListePointageINT.Row;
    end
    else
    begin
      // Stocker la saisie entree au clavier
      ACol := FormBase.ListePointageEMP.Col;
      ARow := FormBase.ListePointageEMP.Row;
    end;

    try
      if inter then
      begin
        // Stocker la saisie entree au clavier
        temp := FormBase.ListePointageINT.Cells[ACol, ARow];
        valeur := StrToCurr(trim(temp));

        // test maxi
        if VerifMaxiOK(ACol, valeur) then
        begin
          // valeur saisie ok
          TableInterimaires[ACol, ARow] := valeur;
          temp := FormatCurr(FormatSortie(ACol), valeur);
          FormBase.ListePointageINT.Cells[ACol, ARow] := temp;
        end
        else
          // valeur hors de la bone maximum donc recharge
          RechargerCellule(inter, ACol, ARow);

        // calculer la ligne
        PointageCalculINT(ARow);
      end
      else
      begin
        // Stocker la saisie entree au clavier
        temp := FormBase.ListePointageEMP.Cells[ACol, ARow];
        valeur := StrToCurr(trim(temp));

        // test maxi
        if VerifMaxiOK(ACol, valeur) then
        begin
          TableEmployes[ACol, ARow] := valeur;
          temp := FormatCurr(FormatSortie(ACol), valeur);
          FormBase.ListePointageEMP.Cells[ACol, ARow] := temp;
        end
        else
          // valeur hors de la bone maximum donc recharge
          RechargerCellule(inter, ACol, ARow);

        // calculer la ligne
        PointageCalculEMP(ARow);
      end;

      // recalcul tout
      PointageExecuterCalcul;

    except
      // si erreur de saisie dans la cellule
      on EConvertError do
        RechargerCellule(inter, ACol, ARow);
    end;
  end;

  // si touche numerique
  CelluleEnCours := false;
  EditionEnCours := false;

  if ((ord(Key) > 47) and (ord(Key) < 58)) or (Key = '-') or (Key = '+') then
  begin
    // modifs en cours
    SetModifsEtat;

    CelluleEnCours := true;
    EditionEnCours := true;
    CelluleEnCoursInter := inter;
    if inter then
    begin
      // Stocker la saisie entree au clavier
      CelluleEnCoursCol := FormBase.ListePointageINT.Col;
      CelluleEnCoursRow := FormBase.ListePointageINT.Row;
    end
    else
    begin
      // Stocker la saisie entree au clavier
      CelluleEnCoursCol := FormBase.ListePointageEMP.Col;
      CelluleEnCoursRow := FormBase.ListePointageEMP.Row;
    end;
  end;
end;

procedure traitePointageInitialiserProcEffaceLigne(inter: boolean;
  X, Y: integer);
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  if inter then
    with FormBase.ListePointageINT do
      MouseToCell(X, Y, positioncolonne, positionligne)
  else
    with FormBase.ListePointageEMP do
      MouseToCell(X, Y, positioncolonne, positionligne)
end;

procedure TraitePointageEffacerLigne(inter: boolean);
{ gere la suppression d'une ligne dans la grille de saisie }
var
  nbl: integer;
  i: integer;

  procedure transfert(inter: boolean; deb, fin: integer);
  var
    L: integer;
    C: integer;
    t: currency;
  begin
    if inter then
    begin
      for L := deb to fin - 2 do
      begin
        for C := colindex to colsommepointage do
        begin
          with FormBase.ListePointageINT do
            Cells[C, L] := Cells[C, L + 1];

          t := TableInterimaires[C, L + 1];
          TableInterimaires[C, L] := t;
        end;
      end;
    end
    else
    begin
      for L := deb to fin - 2 do
      begin
        for C := colindex to colsommepointage do
        begin
          with FormBase.ListePointageEMP do
            Cells[C, L] := Cells[C, L + 1];

          t := TableEmployes[C, L + 1];
          TableEmployes[C, L] := t;
        end;
      end;
    end;
  end;

  procedure viderligne(inter: boolean; fin: integer);
  var
    C: integer;
  begin
    if inter then
      for C := colindex to colsommepointage do
        FormBase.ListePointageINT.Cells[C, fin - 1] := ''
    else
      for C := colindex to colsommepointage do
        FormBase.ListePointageEMP.Cells[C, fin - 1] := '';
  end;

begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  // suppression ligne de pointage selectionn�e
  if (positioncolonne = colNom) then
  begin
    // modifs en cours
    SetModifsEtat;

    // diminuer d'une ligne
    if inter then
    begin
      // verifier si depassement nbr de lignes
      nbl := FormBase.ListePointageINT.RowCount - 1;

      if positionligne >= nbl then
        exit;

      // remonter d'une unite toutes les lignes
      if positionligne < nbl then
        transfert(inter, positionligne, nbl);

      if nbl = 1 then
        unselectINT
      else
      begin
        FormBase.ListePointageINT.Col := colautoselection;
        FormBase.ListePointageINT.Row := 0;
      end;

      // effacer derniere ligne
      viderligne(inter, nbl);

      i := FormBase.ListePointageINT.RowCount;
      dec(i);
      FormBase.ListePointageINT.RowCount := i;
    end
    else
    begin
      // verifier si depassement nbr de lignes
      nbl := FormBase.ListePointageEMP.RowCount - 1;

      if positionligne >= nbl then
        exit;

      // remonter d'une unite toutes les lignes
      if positionligne < nbl then
        transfert(inter, positionligne, nbl);

      if nbl = 1 then
        unselectEMP
      else
      begin
        FormBase.ListePointageEMP.Col := colautoselection;
        FormBase.ListePointageEMP.Row := 0;
      end;

      // effacer derniere ligne
      viderligne(inter, nbl);

      i := FormBase.ListePointageEMP.RowCount;
      dec(i);
      FormBase.ListePointageEMP.RowCount := i;
    end;

    // pour eviter des suppression intenpestives
    positioncolonne := 1;
  end;

  // recalcul tout
  PointageExecuterCalcul;
end;

procedure traitepointagetouchesFIN();
begin
  CelluleEnCours := false;
end;

procedure traitepointagetouches(inter: boolean; Key: word);
{ gere les interventions des touches du clavier dans la grille de saisie }
var
  ACol: integer;
  ARow: integer;

  // recupere position cellule en cours
  procedure Cellule(inter: boolean; var ACol: integer; var ARow: integer);
  begin
    if inter then
    begin
      ACol := FormBase.ListePointageINT.Col;
      ARow := FormBase.ListePointageINT.Row;
    end
    else
    begin
      ACol := FormBase.ListePointageEMP.Col;
      ARow := FormBase.ListePointageEMP.Row;
    end;
  end;

  function lignes(inter: boolean): boolean;
  begin
    result := false;
    if inter then
      if FormBase.ListePointageINT.RowCount > 2 then
        result := true
      else if FormBase.ListePointageEMP.RowCount > 2 then
        result := true;
  end;

begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  // sortie si 0 lignes
  if not lignes(inter) then
    exit;

  if CelluleEnCours = true then
    exit;

  if TestTouchesFleches(Key) or (VK_TAB = Key) then
  begin
    if LigneEnCours = false then
    begin
      // retour en arriere d'une cellule suivant touche
      Cellule(inter, ACol, ARow);
      case Key of
        VK_TAB:
          dec(ACol);
        VK_LEFT:
          inc(ACol);
        VK_RIGHT:
          dec(ACol);
        VK_DOWN:
          if lignes(inter) then
            dec(ARow);
        VK_UP:
          if lignes(inter) then
            inc(ARow);
      end;

      // reaffichage suite a entree non validee par CRLF
      if CelluleEnCours = false then
        if colsommepointage <> ACol then
        begin
          // modifs en cours
          SetModifsEtat;

          RechargerCellule(inter, ACol, ARow);
        end;
    end;
  end;
end;

procedure traitepointagesouris(inter: boolean);
{ gere les interventions de la souris dans la grille de saisie }
var
  ACol: integer;
  ARow: integer;
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  if EditionEnCours = false then
    exit;

  if inter then
  begin
    ACol := FormBase.ListePointageINT.Col;
    ARow := FormBase.ListePointageINT.Row;
  end
  else
  begin
    ACol := FormBase.ListePointageEMP.Col;
    ARow := FormBase.ListePointageEMP.Row;
  end;

  if not ForcerRestitution then
    if (CelluleEnCoursCol = ACol) and (CelluleEnCoursRow = ARow) then
      exit;

  EditionEnCours := false;

  // modifs en cours
  SetModifsEtat;

  RechargerCellule(CelluleEnCoursInter, CelluleEnCoursCol, CelluleEnCoursRow);
end;

procedure PointageSupprime(index: integer);
{ supprime des enregistrement donn�s dans la BDD }
var
  t: string;
begin
  with FormBase.TabFichPoint do
  begin
    // preparer le filtre recherche des enreg contenant 'index'
    str(index, t);
    filter := 'id_hebdo = ' + t;
    filtered := true;

    Open;
    setkey;

    // supprimer tous les enregs
    while recordcount <> 0 do
      delete;

    // termine la suppression dans la table
    close;
    filtered := false;
  end;
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure PointageChargeChaines(index: integer; var nbrec: integer);
{ charge directement a partir de la BDD dans des chaiines de caracteres }

  function LireChaine(param: string): string;
  begin
    result := FormBase.TabFichPoint.FieldByName(param).AsString;
  end;

  function LireValeur(param: string): currency;
  begin
    result := FormBase.TabFichPoint.FieldByName(param).Ascurrency;
  end;

  procedure Charge(nblig: integer);
  var
    i: boolean;
    temp: string;
    valeur: currency;
  begin
    // recuperer le type de ligne
    i := FormBase.TabFichPoint.FieldByName('emp_enr_interimaire').AsBoolean;
    if i then
      temp := 'I'
    else
      temp := '';
    Tableautemp[0, nblig] := temp;

    Tableautemp[1, nblig] := LireChaine('emp_enr_nomemploye');

    temp := FormatSortie(coltauxhoraire);
    valeur := LireValeur('emp_enr_tauxhoraire');
    temp := FormatCurr('0.00', valeur);
    Tableautemp[2, nblig] := temp;
    Tableauvaltemp[2, nblig] := valeur;

    temp := FormatSortie(colnombrejours);
    valeur := LireValeur('emp_nbjourtravail');
    temp := FormatCurr('0.00', valeur); // temp
    Tableautemp[3, nblig] := temp;
    Tableauvaltemp[3, nblig] := valeur;

    valeur := LireValeur('emp_heuresroute');
    temp := FormatSortie(colheuresroute);
    temp := FormatCurr('0.00', valeur);
    Tableautemp[4, nblig] := temp;
    Tableauvaltemp[4, nblig] := valeur;

    valeur := LireValeur('emp_heuresjour');
    temp := FormatSortie(colheuresjour);
    temp := FormatCurr('0.00', valeur);
    Tableautemp[5, nblig] := temp;
    Tableauvaltemp[5, nblig] := valeur;

    valeur := LireValeur('emp_heuressamedi');
    temp := FormatSortie(colheuressamedi);
    temp := FormatCurr('0.00', valeur);
    Tableautemp[6, nblig] := temp;
    Tableauvaltemp[6, nblig] := valeur;

    valeur := LireValeur('emp_heuresnuitdimanche');
    temp := FormatSortie(colheuresnuitcoldimanche);
    temp := FormatCurr('0.00', valeur);
    Tableautemp[7, nblig] := temp;
    Tableauvaltemp[7, nblig] := valeur;

    valeur := LireValeur('emp_petitdeplacement');
    temp := FormatSortie(colpetitpagnier);
    temp := FormatCurr('0', valeur);
    Tableautemp[8, nblig] := temp;
    Tableauvaltemp[8, nblig] := valeur;

    valeur := LireValeur('emp_granddeplacement');
    temp := FormatSortie(colgrandpagnier);
    temp := FormatCurr('0', valeur);
    Tableautemp[9, nblig] := temp;
    Tableauvaltemp[9, nblig] := valeur;

    valeur := LireValeur('emp_total');
    temp := FormatSortie(colsommepointage);
    temp := FormatCurr('0.00', valeur); // temp
    Tableautemp[10, nblig] := temp;
    Tableauvaltemp[10, nblig] := valeur;
  end;

var
  t: string;
  nblig: integer;

begin
  with FormBase.TabFichPoint do
  begin
    // preparer le filtre recherche des enreg contenant 'index'
    str(index, t);
    filter := 'id_hebdo = ' + t;
    filtered := true;

    Open;
    setkey;

    nbrec := recordcount;
    if nbrec <> 0 then
    begin
      // entree dans le mode de modification
      SetLength(Tableautemp, 11, nbrec + 1);
      SetLength(Tableauvaltemp, 11, nbrec + 1);
      edit;

      for nblig := 0 to (nbrec - 1) do
      begin
        // se positionner sur 1er enreg ou sur suivant
        if nblig = 0 then
          first
        else
          next;

        // charger liste de pointage
        Charge(nblig);
      end;
    end;

    // termine la lecture de la table
    close;
    filtered := false;
  end;
end;

procedure PointageCumulsReset;
{ remise a zero des cumuls }
begin
  nbPNT := 0;

  SetLength(TblNomPNT, 2, nbPNT);
  SetLength(TblValPNT, 9, nbPNT);
end;

procedure CumulsPointage(idh: integer);
{ execute le cumuls des valeur a partir des donnees de la BDD }

  function ChercheAjoute(nom: string; var ind: integer): boolean;
  var
    i: integer;
    t: string;
  begin
    result := true;

    if nbPNT = 0 then
      exit;
    for i := 0 to (nbPNT - 1) do
    begin
      t := TblNomPNT[1, i];
      if t = nom then
      begin
        ind := i;
        result := false;
        exit;
      end;
    end;
  end;

var
  ind: integer;
  nbrec: integer;
  i: integer;
  n: integer;
  C: currency;
begin
  // charger les pointages
  PointageChargeChaines(idh, nbrec);

  // si aucuns pointage sortir
  if nbrec = 0 then
    exit;

  // traiter tous les pointages
  for i := 0 to (nbrec - 1) do
  begin
    // verifier si le pointage existe deja
    if ChercheAjoute(Tableautemp[1, i], ind) then
    begin
      // ajoute une ligne au tableau
      inc(nbPNT);
      SetLength(TblNomPNT, 2, nbPNT);
      SetLength(TblValPNT, 9, nbPNT);

      // stocker nom et type ligne de pointage
      ind := nbPNT - 1;
      TblNomPNT[0, ind] := Tableautemp[0, i];
      TblNomPNT[1, ind] := Tableautemp[1, i];

      // raz valeurs nouvelle ligne
      for n := 1 to 8 do
        TblValPNT[n, ind] := 0;

      // stocker taux horaire
      TblValPNT[0, ind] := Tableauvaltemp[2, i];
    end;

    // cumuler a la ligne en cours la nouvelle ligne
    for n := 1 to 8 do
    begin
      C := Tableauvaltemp[n + 2, i];
      C := TblValPNT[n, ind] + C;
      TblValPNT[n, ind] := C;
    end;
  end;
end;

procedure PointageCumulsLire(var nbr: integer);
{ opere une lecture des cumuls effectues }
var
  i: integer;
  t: string;
begin
  nbr := nbPNT;
  if nbPNT = 0 then
    exit;

  // pour stocker tous les pointages
  SetLength(Tableautemp, 11, nbPNT);

  for i := 0 to (nbPNT - 1) do
  begin
    Tableautemp[0, i] := TblNomPNT[0, i];
    Tableautemp[1, i] := TblNomPNT[1, i];

    t := FormatSortie(coltauxhoraire);
    t := FormatCurr('0.00', TblValPNT[0, i]);
    Tableautemp[2, i] := t;

    t := FormatSortie(colnombrejours);
    t := FormatCurr('0.00', TblValPNT[1, i]);
    Tableautemp[3, i] := t;

    t := FormatSortie(colheuresroute);
    t := FormatCurr('0.00', TblValPNT[2, i]);
    Tableautemp[4, i] := t;

    t := FormatSortie(colheuresjour);
    t := FormatCurr('0.00', TblValPNT[3, i]);
    Tableautemp[5, i] := t;

    t := FormatSortie(colheuressamedi);
    t := FormatCurr('0.00', TblValPNT[4, i]);
    Tableautemp[6, i] := t;

    t := FormatSortie(colheuresnuitcoldimanche);
    t := FormatCurr('0.00', TblValPNT[5, i]);
    Tableautemp[7, i] := t;

    t := FormatSortie(colpetitpagnier);
    t := FormatCurr('0', TblValPNT[6, i]);
    Tableautemp[8, i] := t;

    t := FormatSortie(colgrandpagnier);
    t := FormatCurr('0', TblValPNT[7, i]);
    Tableautemp[9, i] := t;

    t := FormatSortie(colsommepointage);
    t := FormatCurr('0.00', TblValPNT[8, i]);
    Tableautemp[10, i] := t;
  end;
end;

/// ///////////////////////////////////////////////////////////////////////////
end.
