unit MasqueDiamant;

interface

procedure ResetCumulsDiamant;
procedure CumulsDiamant(idh: integer);
procedure CumulsDiamantLire(var nbr: integer);
procedure DiamantViderListe;
procedure DiamantInitMasque;
procedure DiamantExecuterCalcul;
function DiamantLireCalcul: currency;
procedure DiamantEnreg(numhebdo: integer);
procedure DiamantCharge(index: integer);
procedure DiamantChargeChaines(index: integer; var nbrec: integer);
procedure DiamantSupprime(index: integer);
procedure DiamantModifsNon;
procedure DiamantModifsOui;
function DiamantLireModifsEtat: boolean;

function traiteDiamantLireFormatCellules(ACol: integer): string;
procedure traiteDiamantsouris;
procedure traiteDiamantInitialiserProcEffaceLigne(X, Y: integer);
function TraiteDiamantSelection(ACol, ARow: integer): boolean;
procedure TraiteDiamantValide(var Key: Char);
procedure traiteDiamanttouches(Key: word);
procedure traiteDiamanttouchesFIN();
procedure TraiteDiamantChargeLigne;
procedure TraiteDiamantEffacerLigne;

implementation

uses

  SysUtils, grids, Dialogs, windows,
  winbase, accesBDD, Hebdo, impression;

const

  colindex = 0;
  colNom = 0;
  colcout = 1;
  colnombre = 2;
  colcoeff = 3;
  colsommeligne = 4;

  coldeselection = 10;
  colautoselection = colnombre;

type

  TlargeurLigne = array [colNom .. colsommeligne] of integer;

const

  Largeur: TlargeurLigne = (145, 40, 40, 40, 70);

var

  TblNomDIA: array of array of string;
  TblValDIA: array of array of currency;
  nbDIA: integer;

  positioncolonne: integer = 1;
  positionligne: integer;
  LigneEnCours: boolean = true;
  TableauDiamants: array of array of currency;
  CelluleEnCours: boolean;
  EditionEnCours: boolean;
  CelluleEnCoursCol: integer;
  CelluleEnCoursRow: integer;
  Suppressionencours: boolean;
  ModeLigneAppend: boolean;

  ModifsEtat: boolean;
  AutoriseModifs: boolean;

  SommeDIA: currency;

  /// ///////////////////////////////////////////////////////////////////////////
procedure unselectDIA;
begin
  Suppressionencours := true;
  FormBase.ListeUtilisationDIA.Col := coldeselection;
  FormBase.ListeUtilisationDIA.Row := 0;
  Suppressionencours := false;
end;

procedure DiamantInitMasque;
var
  i: integer;
begin
  for i := colindex to colsommeligne do
    FormBase.ListeUtilisationDIA.ColWidths[i] := Largeur[i];

  for i := colsommeligne + 1 to coldeselection do
    FormBase.ListeUtilisationDIA.ColWidths[i] := 0;

  unselectDIA;
end;

procedure DiamantModifsOui;
begin
  AutoriseModifs := true;
  FormBase.ListeDiamant.Enabled := true;
end;

procedure DiamantModifsNon;
begin
  unselectDIA;
  AutoriseModifs := false;
  FormBase.ListeDiamant.Enabled := false;
  FormBase.ListeDiamant.KeyValue := -1;
end;

function DiamantLireModifsEtat: boolean;
begin
  result := ModifsEtat;
end;

procedure SetModifsEtat;
begin
  ModifsEtat := true;

  // prevenir qu'il y a eu une modif d'un masque
  EvenementModifMasqueAnnexe;
end;

procedure ResetModifsEtat;
begin
  ModifsEtat := false;
end;

procedure DiamantViderListe;

  procedure viderligne(fin: integer);
  var
    C: integer;
  begin
    for C := colindex to colsommeligne do
      FormBase.ListeUtilisationDIA.Cells[C, fin - 1] := '';
  end;

begin
  // pour ne pas repasser par le traitement evenement masque
  LigneEnCours := true;

  while FormBase.ListeUtilisationDIA.RowCount > 1 do
  begin
    positioncolonne := 0;
    positionligne := FormBase.ListeUtilisationDIA.RowCount - 2;
    TraiteDiamantEffacerLigne;
  end;

  positioncolonne := 1;
  LigneEnCours := false;
end;

function VerifMaxiOK(ACol: integer; valeur: currency): boolean;
begin
  result := false;
  case ACol of
    colnombre, colcoeff:
      if (valeur < -999.9) or (valeur > 999.9) then
        exit;
  end;
  result := true;
end;

function traiteDiamantLireFormatCellules(ACol: integer): string;
begin
  case ACol of
    colNom:
      result := '';
    colcout:
      result := '999,99';
    colnombre, colcoeff:
      result := '';
    9:
      result := '#####,##';
  end;
end;

function FormatSortie(Col: integer): string;
begin
  case Col of
    colcout:
      result := '000.00';
    colnombre:
      result := '000.00';
    colcoeff:
      result := '000.00';
    colsommeligne:
      result := '00000.00';
  else
    result := '00.00';
  end;
end;

Procedure RechargerCellule(Col, lig: integer);
var
  valeur: currency;
  temp: string;
begin
  valeur := TableauDiamants[Col, lig];
  temp := FormatCurr(FormatSortie(Col), valeur);
  FormBase.ListeUtilisationDIA.Cells[Col, lig] := temp;
end;

function DiamantLireCalcul: currency;
begin
  result := SommeDIA
end;

procedure CalculDiamant(lig: integer);
var
  C, N, F, Somme: currency;
  temp: string;
begin

  C := TableauDiamants[colcout, lig];
  N := TableauDiamants[colnombre, lig];
  F := TableauDiamants[colcoeff, lig];

  Somme := (C * N * F);

  // somme du Diamant
  TableauDiamants[colsommeligne, lig] := Somme;
  temp := FormatCurr(FormatSortie(colsommeligne), Somme);
  FormBase.ListeUtilisationDIA.Cells[colsommeligne, lig] := temp;
end;

procedure DiamantExecuterCalcul;
var
  nbl: integer;
  t: currency;
  i: integer;
begin
  t := 0;
  nbl := FormBase.ListeUtilisationDIA.RowCount - 1;
  if nbl >= 0 then
  begin
    for i := 0 to (nbl - 1) do
      t := t + TableauDiamants[colsommeligne, i];
  end;

  SommeDIA := t;

  // lance le calcul generalde la fiche Hebdo;
  CalculGeneralHebdo;
end;

function CurrToInt(valeur: currency): integer;
begin
  result := StrToInt(CurrToStr(valeur));
end;

function IntToCurr(valeur: integer): currency;
begin
  result := StrToCurr(IntToStr(valeur));
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure DiamantEnreg(numhebdo: integer);
var
  nbe: integer;
  C: currency;
  l: integer;
  s: string;

  procedure ChercheEnreg(ligne: integer);
  var
    C: currency;
    id: integer;
  begin
    C := TableauDiamants[colindex, ligne];

    id := CurrToInt(C);
    FormBase.TabFichDiam.editkey;
    FormBase.TabFichDiam.FieldByName('id_Diamant').asinteger := id;
    FormBase.TabFichDiam.gotokey;
    FormBase.TabFichDiam.edit;
  end;

begin
  // supprimer les enregs precedents
  DiamantSupprime(numhebdo);
  ModeLigneAppend := false;

  nbe := FormBase.ListeUtilisationDIA.RowCount - 1;
  if (nbe = 0) then
    exit;

  // pas de mofifs en cours
  ResetModifsEtat;

  with FormBase.TabFichDiam do
  begin
    Open;

    if nbe <> 0 then
    begin
      // creer chaque ligne et la remplir
      for l := 0 to (nbe - 1) do
      begin
        if ModeLigneAppend = false then
          // creer nouveau enregistrement de ligne
          insert
        else
          // chercher enreg existant
          ChercheEnreg(l);

        s := FormBase.ListeUtilisationDIA.Cells[colNom, l];
        FieldByName('tra_enr_libellenom').AsString := s;

        C := TableauDiamants[colcout, l];
        FieldByName('tra_enr_cout').Ascurrency := C;

        C := TableauDiamants[colnombre, l];
        FieldByName('tra_nombre').Ascurrency := C;

        C := TableauDiamants[colcoeff, l];
        FieldByName('tra_coeffficient').Ascurrency := C;

        C := TableauDiamants[colsommeligne, l];
        FieldByName('tra_total').Ascurrency := C;

        FieldByName('id_hebdo').asinteger := numhebdo;

        // valider nouvelle ligne avec ces valeurs
        post;
      end;
    end;

    // fermeture de la table apres ajouts
    close;
  end;

  // sortie du mode de modification
  ModeLigneAppend := false;
end;

procedure ChargeLigneExiste;
var
  nc: integer;
  nblig: integer;

  idemp: integer;
  monaie: currency;
  valeur: currency;
  nomemp: string;

  procedure Ecrire(Col, lig: integer; valeur: currency);
  var
    t: string;
  begin
    t := FormatCurr(FormatSortie(Col), valeur);
    TableauDiamants[Col, lig] := valeur;
    FormBase.ListeUtilisationDIA.Cells[Col, lig] := t;
  end;

begin
  with FormBase.TabFichDiam do
  begin
    // definition de la ligne de Diamant
    nblig := FormBase.ListeUtilisationDIA.RowCount - 1;

    SetLength(TableauDiamants, 10, nblig + 1);

    idemp := FieldByName('id_Diamant').asinteger;
    TableauDiamants[colindex, nblig] := IntToCurr(idemp);

    nomemp := FieldByName('tra_enr_libellenom').AsString;
    FormBase.ListeUtilisationDIA.Cells[colNom, nblig] := nomemp;

    // debut chargement liste ecran
    LigneEnCours := true;

    monaie := FieldByName('tra_enr_cout').Ascurrency;
    Ecrire(colcout, nblig, monaie);

    valeur := FieldByName('tra_nombre').Ascurrency;
    Ecrire(colnombre, nblig, valeur);

    valeur := FieldByName('tra_coeffficient').Ascurrency;
    Ecrire(colcoeff, nblig, valeur);

    // calculer la ligne
    CalculDiamant(nblig);

    // fin chargement liste ecran
    LigneEnCours := false;

    // defintion de la prochaine ligne
    nc := FormBase.ListeUtilisationDIA.RowCount;
    inc(nc);
    FormBase.ListeUtilisationDIA.RowCount := nc;

    // selection de la 1ere colonne a editer
    FormBase.ListeUtilisationDIA.SetFocus;
    FormBase.ListeUtilisationDIA.Col := colnombre;
    FormBase.ListeUtilisationDIA.Row := nblig;
  end;
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure DiamantCharge(index: integer);
var
  nbrec: integer;
  t: string;
  nblig: integer;
begin
  ChargeConfiguration;

  // pas de mofifs en cours
  ResetModifsEtat;

  // definition des lignes de Diamant
  FormBase.ListeUtilisationDIA.RowCount := 1;

  with FormBase.TabFichDiam do
  begin
    // recherche des enreg contenant 'index'

    // preparer le filtre
    str(index, t);
    filter := 'id_hebdo = ' + t;
    filtered := true;

    Open;
    setkey;

    nbrec := recordcount;
    if nbrec <> 0 then
    begin
      // entree dans le mode de modification
      ModeLigneAppend := true;
      edit;

      for nblig := 0 to (nbrec - 1) do
      begin
        // se positionner sur 1er enreg ou sur suivant
        if nblig = 0 then
          first
        else
          next;

        // charger liste de Diamant
        ChargeLigneExiste;
      end;
    end;

    // termine la lecture de la table
    close;
    filtered := false;

    // recalcul tout
    DiamantExecuterCalcul;
  end;
end;

procedure GetEnregDiamantCourant(var i: integer; var N: string;
  var C: currency);
begin
  with FormBase.TableDiamant do
  begin
    edit;
    i := FieldValues['id_travail'];
    N := FieldByName('tra_libellenom').AsString;
    C := FieldByName('tra_cout').Ascurrency;
    post;
  end;
end;

procedure TraiteDiamantChargeLigne;
var
  nc: integer;
  nblig: integer;

  idemp: integer;
  monaie: currency;
  nomemp: string;

  function ChercheExiste(nblig: integer; nomemp: string): boolean;
  var
    i: integer;
    t: string;
  begin
    result := false;
    for i := 0 to nblig - 1 do
    begin
      t := FormBase.ListeUtilisationDIA.Cells[colNom, i];
      if nomemp = trim(t) then
        result := true;
    end;
  end;

  procedure Ecrire(Col, lig: integer; valeur: currency);
  var
    t: string;
  begin
    t := FormatCurr(FormatSortie(Col), valeur);
    TableauDiamants[Col, lig] := valeur;
    FormBase.ListeUtilisationDIA.Cells[Col, lig] := t;
  end;

begin
  // inclure un employ� dans la liste de Diamant

  if FormBase.TableDiamant.recordcount = 0 then
    exit;

  // modifs en cours
  SetModifsEtat;

  // lire l'enreg correspondant de l'employe modifier
  GetEnregDiamantCourant(idemp, nomemp, monaie);

  ChargeConfiguration;

  // definition de la ligne de Diamant
  nblig := FormBase.ListeUtilisationDIA.RowCount - 1;

  // verifier si nb maxi de lignes atteint
  if nblig = nbligdia then
    exit;

  if nblig <> 0 then
  begin
    // verifier si le nom existe dans liste alors ne pas charger
    if ChercheExiste(nblig, nomemp) then
      exit;
  end;

  SetLength(TableauDiamants, 10, nblig + 1);

  TableauDiamants[colindex, nblig] := IntToCurr(idemp);
  FormBase.ListeUtilisationDIA.Cells[colNom, nblig] := nomemp;

  LigneEnCours := true; // debut chargement liste ecran

  Ecrire(colcout, nblig, monaie);
  Ecrire(colnombre, nblig, 0);
  Ecrire(colcoeff, nblig, 0);

  // calculer la ligne
  CalculDiamant(nblig);

  LigneEnCours := false; // fin chargement liste ecran

  // defintion de la prochaine ligne
  nc := FormBase.ListeUtilisationDIA.RowCount;
  inc(nc);
  FormBase.ListeUtilisationDIA.RowCount := nc;

  // selection de la 1ere colonne a editer
  FormBase.ListeUtilisationDIA.SetFocus;
  FormBase.ListeUtilisationDIA.Col := colnombre;
  FormBase.ListeUtilisationDIA.Row := nblig;

  // recalcul tout
  DiamantExecuterCalcul;
end;

function TraiteDiamantSelection(ACol, ARow: integer): boolean;
var
  OK: boolean;
  i: integer;
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    result := false;
    exit;
  end;

  result := true;
  OK := true;

  // utile pour la suppression de la derniere ligne
  if Suppressionencours and (ACol = 10) then
    exit;

  // verifier si depassement nbr de lignes
  i := FormBase.ListeUtilisationDIA.RowCount - 1;
  if ARow >= i then
    OK := false;

  if (ACol > colcout) and (ACol <= colcoeff) and OK then
  begin
    // autoriser l'edition de la cellule
    with FormBase.ListeUtilisationDIA do
      options := options + [goEditing];

    RechargerCellule(ACol, ARow);
  end
  else
  begin
    // interdire l'edition et la selection de la cellule
    with FormBase.ListeUtilisationDIA do
      options := options - [goEditing];
    result := false;
    exit;
  end;
end;

procedure TraiteDiamantValide(var Key: Char);
var
  ACol: integer;
  ARow: integer;
  temp: string;
  valeur: currency;
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    Key := chr(0);
    exit;
  end;

  if (Key = ' ') then
  begin
    // ne traite pas ces touches
    Key := chr(0);
    exit;
  end;

  if (Key = '.') then
  begin;
    // modifs en cours
    SetModifsEtat;

    // pour faciliter la saisie des nombres decimaux
    Key := ',';

    exit;
  end;

  // valide la saisie
  if Key = chr(13) then
  begin
    // modifs en cours
    SetModifsEtat;

    // Stocker la saisie entree au clavier
    ACol := FormBase.ListeUtilisationDIA.Col;
    ARow := FormBase.ListeUtilisationDIA.Row;

    try
      begin
        // Stocker la saisie entree au clavier
        temp := FormBase.ListeUtilisationDIA.Cells[ACol, ARow];
        valeur := StrToCurr(trim(temp));

        // test maxi
        if VerifMaxiOK(ACol, valeur) then
        begin
          TableauDiamants[ACol, ARow] := valeur;
          temp := FormatCurr(FormatSortie(ACol), valeur);
          FormBase.ListeUtilisationDIA.Cells[ACol, ARow] := temp;
        end
        else
          // valeur hors de la bone maximum donc recharge
          RechargerCellule(ACol, ARow);

        // calculer la ligne
        CalculDiamant(ARow);
      end;

      // recalcul tout
      DiamantExecuterCalcul;

    except
      // si erreur de saisie dans la cellule
      on EConvertError do
        RechargerCellule(ACol, ARow);
    end;
  end;

  // si touche numerique
  CelluleEnCours := false;
  EditionEnCours := false;
  if ((ord(Key) > 47) and (ord(Key) < 58)) or (Key = '-') or (Key = '+') then
  begin
    // modifs en cours
    SetModifsEtat;

    CelluleEnCours := true;
    EditionEnCours := true;

    // Stocker la saisie entree au clavier
    CelluleEnCoursCol := FormBase.ListeUtilisationDIA.Col;
    CelluleEnCoursRow := FormBase.ListeUtilisationDIA.Row;
  end;
end;

procedure traiteDiamantInitialiserProcEffaceLigne(X, Y: integer);
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  with FormBase.ListeUtilisationDIA do
    MouseToCell(X, Y, positioncolonne, positionligne)
end;

procedure TraiteDiamantEffacerLigne;
var
  nbl: integer;
  i: integer;

  procedure transfert(deb, fin: integer);
  var
    l: integer;
    C: integer;
    t: currency;
  begin
    for l := deb to fin - 2 do
    begin
      for C := colindex to colsommeligne do
      begin
        with FormBase.ListeUtilisationDIA do
          Cells[C, l] := Cells[C, l + 1];

        t := TableauDiamants[C, l + 1];
        TableauDiamants[C, l] := t;
      end;
    end;
  end;

  procedure viderligne(fin: integer);
  var
    C: integer;
  begin
    for C := colindex to colsommeligne do
      FormBase.ListeUtilisationDIA.Cells[C, fin - 1] := '';
  end;

begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  // suppression ligne de Diamant selectionn�e
  if (positioncolonne = colNom) then
  begin
    // modifs en cours
    SetModifsEtat;

    // diminuer d'une ligne verifier si depassement nbr de lignes
    nbl := FormBase.ListeUtilisationDIA.RowCount - 1;
    if positionligne >= nbl then
      exit;

    // remonter d'une unite toutes les lignes
    if positionligne < nbl then
      transfert(positionligne, nbl);

    if nbl = 1 then
      unselectDIA
    else
    begin
      FormBase.ListeUtilisationDIA.Col := colautoselection;
      FormBase.ListeUtilisationDIA.Row := 0;
    end;

    // effacer derniere ligne
    viderligne(nbl);

    i := FormBase.ListeUtilisationDIA.RowCount;
    dec(i);
    FormBase.ListeUtilisationDIA.RowCount := i;

    // pour eviter des suppression intenpestives
    positioncolonne := 1;
  end;

  // recalcul tout
  DiamantExecuterCalcul;
end;

procedure traiteDiamanttouchesFIN();
begin
  CelluleEnCours := false;
end;

procedure traiteDiamanttouches(Key: word);
var
  ACol: integer;
  ARow: integer;

  // recupere position cellule en cours
  procedure Cellule(var ACol: integer; var ARow: integer);
  begin
    ACol := FormBase.ListeUtilisationDIA.Col;
    ARow := FormBase.ListeUtilisationDIA.Row;
  end;

  function lignes: boolean;
  begin
    result := false;
    if FormBase.ListeUtilisationDIA.RowCount > 2 then
      result := true;
  end;

begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  // sortie si 0 lignes
  if not lignes then
    exit;

  if CelluleEnCours = true then
    exit;

  if TestTouchesFleches(Key) or (VK_TAB = Key) then
  begin
    if LigneEnCours = false then
    begin
      // retour en arriere d'une cellule suivant touche
      Cellule(ACol, ARow);
      case Key of
        VK_TAB:
          dec(ACol);
        VK_LEFT:
          inc(ACol);
        VK_RIGHT:
          dec(ACol);
        VK_DOWN:
          if lignes then
            dec(ARow);
        VK_UP:
          if lignes then
            inc(ARow);
      end;

      // reaffichage suite a entree non validee par CRLF
      if CelluleEnCours = false then
        if colsommeligne <> ACol then
        begin
          // modifs en cours
          SetModifsEtat;

          RechargerCellule(ACol, ARow);
        end;
    end;
  end;
end;

procedure traiteDiamantsouris;
var
  ACol: integer;
  ARow: integer;
begin
  // verifier si modifs autorisees
  if AutoriseModifs = false then
  begin
    // modifs interdites
    exit;
  end;

  if EditionEnCours = false then
    exit;

  ACol := FormBase.ListeUtilisationDIA.Col;
  ARow := FormBase.ListeUtilisationDIA.Row;

  if not ForcerRestitution then
    if (CelluleEnCoursCol = ACol) and (CelluleEnCoursRow = ARow) then
      exit;

  EditionEnCours := false;

  // modifs en cours
  SetModifsEtat;

  RechargerCellule(CelluleEnCoursCol, CelluleEnCoursRow);
end;

/// ///////////////////////////////////////////////////////////////////////////
procedure DiamantChargeChaines(index: integer; var nbrec: integer);

  function LireChaine(param: string): string;
  begin
    result := FormBase.TabFichDiam.FieldByName(param).AsString;
  end;

  function LireValeur(param: string): currency;
  begin
    result := FormBase.TabFichDiam.FieldByName(param).Ascurrency;
  end;

  procedure Charge(nblig: integer);
  var
    temp: string;
    valeur: currency;
  begin
    TableauTemp[0, nblig] := '';

    TableauTemp[1, nblig] := LireChaine('tra_enr_libellenom');

    temp := FormatSortie(colcout);
    valeur := LireValeur('tra_enr_cout');
    temp := FormatCurr('0.00', valeur);
    TableauTemp[2, nblig] := temp;
    TableauvalTemp[2, nblig] := valeur;

    temp := FormatSortie(colnombre);
    valeur := LireValeur('tra_nombre');
    temp := FormatCurr('0.00', valeur);
    TableauTemp[3, nblig] := temp;
    TableauvalTemp[3, nblig] := valeur;

    temp := FormatSortie(colcoeff);
    valeur := LireValeur('tra_coeffficient');
    temp := FormatCurr('0.00', valeur);
    TableauTemp[4, nblig] := temp;
    TableauvalTemp[4, nblig] := valeur;

    temp := FormatSortie(colsommeligne);
    valeur := LireValeur('tra_total');
    temp := FormatCurr('0.00', valeur);
    TableauTemp[5, nblig] := temp;
    TableauvalTemp[5, nblig] := valeur;
  end;

var
  t: string;
  nblig: integer;

begin
  with FormBase.TabFichDiam do
  begin
    // preparer le filtre recherche des enreg contenant 'index'
    str(index, t);
    filter := 'id_hebdo = ' + t;
    filtered := true;

    Open;
    setkey;

    nbrec := recordcount;
    if nbrec <> 0 then
    begin
      // entree dans le mode de modification
      SetLength(TableauTemp, 11, nbrec + 1);
      SetLength(TableauvalTemp, 11, nbrec + 1);
      edit;

      for nblig := 0 to (nbrec - 1) do
      begin
        // se positionner sur 1er enreg ou sur suivant
        if nblig = 0 then
          first
        else
          next;

        // charger liste de Diamant
        Charge(nblig);
      end;
    end;

    // termine la lecture de la table
    close;
    filtered := false;
  end;
end;

procedure DiamantSupprime(index: integer);
var
  t: string;
begin
  with FormBase.TabFichDiam do
  begin
    // preparer le filtre recherche des enreg contenant 'index'
    str(index, t);
    filter := 'id_hebdo = ' + t;
    filtered := true;

    Open;
    setkey;

    // supprimer tous les enregs
    while recordcount <> 0 do
      delete;

    // termine la suppression dans la table
    close;
    filtered := false;
  end;
end;

procedure ResetCumulsDiamant;
begin
  nbDIA := 0;

  SetLength(TblNomDIA, 2, nbDIA);
  SetLength(TblValDIA, 4, nbDIA);
end;

procedure CumulsDiamant(idh: integer);

  function ChercheAjoute(nom: string; var ind: integer): boolean;
  var
    i: integer;
    t: string;
  begin
    result := true;

    if nbDIA = 0 then
      exit;
    for i := 0 to (nbDIA - 1) do
    begin
      t := TblNomDIA[1, i];
      if t = nom then
      begin
        ind := i;
        result := false;
        exit;
      end;
    end;
  end;

var
  ind: integer;
  nbrec: integer;
  i: integer;
  N: integer;
  C: currency;
begin
  // charger les pointages
  DiamantChargeChaines(idh, nbrec);

  // si aucuns pointage sortir
  if nbrec = 0 then
    exit;

  // traiter tous les pointages
  for i := 0 to (nbrec - 1) do
  begin
    // verifier si le pointage existe deja
    if ChercheAjoute(TableauTemp[1, i], ind) then
    begin
      // ajoute une ligne au tableau
      inc(nbDIA);
      SetLength(TblNomDIA, 2, nbDIA);
      SetLength(TblValDIA, 4, nbDIA);

      // stocker nom et type ligne de pointage
      ind := nbDIA - 1;
      TblNomDIA[0, ind] := '';
      TblNomDIA[1, ind] := TableauTemp[1, i];

      // raz valeurs nouvelle ligne
      for N := 1 to 3 do
        TblValDIA[N, ind] := 0;

      // stocker le cout
      TblValDIA[0, ind] := TableauvalTemp[2, i];
    end;

    // cumuler a la ligne en cours la nouvelle ligne
    for N := 1 to 3 do
    begin
      C := TableauvalTemp[N + 2, i];
      C := TblValDIA[N, ind] + C;
      TblValDIA[N, ind] := C;
    end;
  end;
end;

procedure CumulsDiamantLire(var nbr: integer);
var
  i: integer;
  t: string;
begin
  nbr := nbDIA;
  if nbDIA = 0 then
    exit;

  // pour stocker tous les pointages
  SetLength(TableauTemp, 11, nbDIA);

  for i := 0 to (nbDIA - 1) do
  begin
    TableauTemp[0, i] := TblNomDIA[0, i];
    TableauTemp[1, i] := TblNomDIA[1, i];

    t := FormatSortie(colcout);
    t := FormatCurr('0.00', TblValDIA[0, i]);
    TableauTemp[2, i] := t;

    t := FormatSortie(colnombre);
    t := FormatCurr('0.00', TblValDIA[1, i]);
    TableauTemp[3, i] := t;

    t := FormatSortie(colcoeff);
    t := FormatCurr('0.00', TblValDIA[2, i]);
    TableauTemp[4, i] := t;

    t := FormatSortie(colsommeligne);
    t := FormatCurr('0.00', TblValDIA[3, i]);
    TableauTemp[5, i] := t;
  end;
end;
/// ///////////////////////////////////////////////////////////////////////////

end.
