�
 TFICHESIMPLE 0  TPF0TFichesimpleFichesimpleLeftTop� BorderIcons BorderStylebsDialogClientHeight� ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPosition	poDefaultOnCreate
FormCreatePixelsPerInch`
TextHeight TLabelLabel1LeftTop@Width1Height	AlignmenttaCenterAutoSizeCaptionxxx  TLabelLabel2LeftHTop@WidthyHeight	AlignmenttaCenterAutoSizeCaptionxxx  TLabelLabel3Left� Top@WidthyHeight	AlignmenttaCenterAutoSizeCaptionxxx  TLabelLabel4Left� Top� Width HeightCaptionLabel4  TDBNavigatorDBNavigator1LeftTopWidth� Height
DataSourceDataSource1VisibleButtonsnbFirstnbPriornbNextnbLastnbInsertnbDelete Hints.StringsPremier enregistrement   Enregistrement précédentEnregistrement suivantDernier enregistrement   Insérer enregistrementSupprimer enregistrementModifier enregistrementEnregistrer modificationsAnnuler modifications   Rafraîchir données ParentShowHintShowHint	TabOrder BeforeActionDBNavigator1BeforeAction  TDBEditDBEdit1LeftTopXWidth1Height
DataSourceDataSource1EnabledTabOrderVisible  TDBEditDBEdit2LeftHTopXWidthyHeightHintLibelle enregistrement
DataSourceDataSource1ParentShowHintShowHint	TabOrderVisibleOnChangeDBEdit2Change  TDBEditDBEdit3Left� TopXWidthyHeightHintValeur enregistrement
DataSourceDataSource1ParentShowHintShowHint	TabOrderVisibleOnChangeDBEdit2Change  TButtonButton1LeftTopWidthJHeightHint9   Quitter outil de création des enregistrements des listesCaptionFermerParentShowHintShowHint	TabOrderOnClickButton1Click  TDBCheckBoxDBCheckBox1Left(TopxWidth� HeightHintStatus d'interimaire ou nonCaptionDBCheckBox1
DataSourceDataSource1ParentShowHintShowHint	TabOrderValueCheckedVraiValueUncheckedFauxVisibleOnClickDBCheckBox1Click  TDBCheckBoxDBCheckBox2Left(Top� Width� HeightHint'Presence ou non au sein de l'entrepriseCaptionDBCheckBox2
DataSourceDataSource1ParentShowHintShowHint	TabOrderValueCheckedVraiValueUncheckedFauxVisibleOnClickDBCheckBox2Click  TDBEditDBEdit4Left� Top� WidthAHeight
DataSourceDataSource1TabOrderVisibleOnChangeDBEdit4Change  
TColorGrid
ColorGrid1LeftTopxWidth0HeightHintJ   Couleur utilisée lors des editions des chantiers attribués au commercialGridOrderinggo16x1ForegroundIndex
BackgroundEnabledParentShowHintShowHint	TabOrderTabStop	OnClickColorGrid1Click  TTableTable1	AfterPostTable1AfterPostAfterScrollTable1AfterPostDatabaseName
BASEGCFORA	FieldDefsNameid_commercial
Attributes
faReadonly DataType	ftAutoInc Namecom_libellenomDataTypeftStringSize Namecom_couleurDataType	ftInteger  	IndexDefsNameTable1Index1  	StoreDefs		TableName
commercial	TableType	ttParadoxLeft(Top(  TDataSourceDataSource1DataSetTable1Left� Top(   