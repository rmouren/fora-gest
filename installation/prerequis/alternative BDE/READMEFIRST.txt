-------------------------------------------------------
	One BDE Installation to Rule Them ALL
-------------------------------------------------------

This folder contains all that is required to install the Borland Database Engine
including SQL Links.


The sequence of events is as follows:


1. The BDE Merge Module installs or Updates the BDE as usual.

2. After this process has been done a custom action is launched which uses
   the BDESUPP.INI file to instruct it to perform a number of optional tasks
   as follows:

	i)	Move just the Config File to an alternative location.


	ii)	Move the entire BDE to an alternative location 
		(including the cfg file if it is located in that folder).


	iii)	Install BDE Admin Help Files that can be opened on Windows Vista
		without the need for the "winhelp" subsystem to be present.


	iv)	Install the bdeadmin manifest file.


	v)	Install the 4GB Diskspace Patch for large hard drive.


	vi)	Add default Aliases at install time.


	vii)	Ensure that the Config File and not the registry is used to store
		all settings (required under Windows Vista).


	viii)	Set a Default Net Dir which can be relative to the newly relocated
		BDE folder.


	iX)	Update any of the System or other Native Driver Settings.



Important Notes:

1.  The Borland Database Engine is NOT free. To use and distribute this installation 
    routine you MUST comply with the licence restrictions as required by 
    Borland/Codegear/whoever they are called this week !!


2.  The WindowsVersion value only applies to the BDE and Config File relocation routines.
    This allows you to specify that the relocation of the BDE occurs only on Windows XP
    and newer for example. All other settings are updated regardless of the version 
    of Windows.


3.  The "Extras" folder contains the various files that will be applied at install time.
    If any updated versions of those files become available in the future then they can
    be safely replaced, but the filenames MUST remain the same.


4.  Due to bugs in Installshield Express 3.54 right through to Installshield Express 12,
    you cannot install this silently. You can however run it without requiring user
    input by using the command "setup /s /v/qr" quotes not neccessary.


5.  Setup requests Administrator Privileges when running under Windows Vista
    (and hopefully newer version of windows).


6.  The New Help Files and the 4GB Diskspace patches require the relevant permissions to
    store files in the Windows System Folder. A copy of bdeker32.dll is also placed in 
    the BDE folder but it is unlikely to be used at runtime.


7.  When adding the New Help Files, a control panel entry is created called
    "Borland Database Help". I have noticed that the first time this is done under 
    Windows XP (other Windows versions not yet tested) if you go into the Control Panel 
    without rebooting the machine, the Control Panel will crash.


8.  When an Alias is specified, if it already exists then it is deleted and then re-added
    with your settings. In other words you always get what you asked for.


9.  When running my application under a Windows Vista (Business) limited Account, I have
    noticed the first time it runs and the alias is created for the very first time, the
    BDE will sometimes freeze at that point. This is regardless of the setting telling the
    BDE to use the CFG file and not the registry. Rebooting Vista and running the
    application again resolves the problem.

    The better solution I have found is to add the Alias with a dummy path at BDE
    installation time. When my application runs, all is good and there is no freeze,
    Strange !


10. When moving the BDE to an alternative location, no attempt is made to remove the
    Borland Shared Folder and so it may remain on the target system as an empty folder.


11. This installation routine is currently in Beta Test phase and is still being tested
    under Windows Vista.





